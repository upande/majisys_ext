from datetime import datetime
from datetime import timedelta

def process(identifier, parameters, records):
    timeunit = parameters.get('time_unit')
    multiplier = parameters.get('multiplier')
    if not timeunit is None and not multiplier is None:
        try:
            timeunit = timeunit['value']
            multiplier = int(multiplier['value'])
        except ValueError:
            timeunit = 'D'
            multiplier = 1
    else:
        timeunit = 'D'
        multiplier = 1

    newrecords = []
    firstdate = None
    lastdate = None
    period = timedelta(days=1)
    if timeunit == 'H':
        period = timedelta(hours=multiplier)
    elif timeunit == 'D':
        period = timedelta(days=multiplier)
    elif timeunit == 'M':
        period = timedelta(days=multiplier*30)
    elif timeunit == 'Y':
        period = timedelta(days=multiplier*365)
    for record in records:
        if firstdate is None:
            firstdate = record['date']
            lastdate = firstdate
        elif record['date'] < lastdate + period:
            lastdate = record['date']
        else:
            newrecord = {}
            newrecord['date'] = firstdate
            newrecord['value'] = 0
            newrecords.append(newrecord)
            newrecord = {}
            newrecord['date'] = firstdate
            newrecord['value'] = 1
            newrecords.append(newrecord)
            newrecord = {}
            newrecord['date'] = lastdate
            newrecord['value'] = 1
            newrecords.append(newrecord)
            newrecord = {}
            newrecord['date'] = lastdate
            newrecord['value'] = 0
            newrecords.append(newrecord)
            firstdate = record['date']
            lastdate = firstdate
    if not lastdate is None: # still add the last data
            newrecord = {}
            newrecord['date'] = firstdate
            newrecord['value'] = 0
            newrecords.append(newrecord)
            newrecord = {}
            newrecord['date'] = firstdate
            newrecord['value'] = 1
            newrecords.append(newrecord)
            newrecord = {}
            newrecord['date'] = lastdate
            newrecord['value'] = 1
            newrecords.append(newrecord)
            newrecord = {}
            newrecord['date'] = lastdate
            newrecord['value'] = 0
            newrecords.append(newrecord)

    for record in newrecords: # change datetime.datetime to str for json.dumps()
       record['date'] = datetime.strftime(record['date'], "%Y-%m-%d %H:%M:%S")

    return newrecords
