#!/usr/bin/python3

import os
import cgi
import psycopg2
from datetime import datetime
from datetime import timedelta
import sys
import json
from psycopg2.extras import RealDictCursor
import xml.etree.ElementTree as xml
import urllib
from importlib.machinery import SourceFileLoader

def cgierror(message):
    print("Content-type: text/html")
    print()
    print(message)
    
def nonamespace(tag):
    i = tag.find('}')
    if i >= 0:
        tag = tag[i+1:]
    return tag

def printGetCapabilities():
    print("Content-type: text/xml")
    print()
    file = open("../../sources/GetCapabilities.xml")
    line = file.readline()
    while line:
        print (line)
        line = file.readline()
    file.close()
    sys.stdout.flush()

def printDescribeProcess(identifier):
    if identifier is None:
        cgierror("DescribeProcess: bad identifier: None")
    elif not os.path.exists("../../sources/DescribeProcess_" + identifier + ".xml"):
        cgierror("DescribeProcess: identifier does not exist: '" + identifier + "'")
    else:
        file = open("../../sources/DescribeProcess_" + identifier + ".xml")
        print("Content-type: text/xml")
        print()
        line = file.readline()
        while line:
            print (line)
            line = file.readline()
        file.close()
        sys.stdout.flush()

def printExecute(identifier, parameters):
    if identifier is None:
        cgierror("Execute: bad identifier: None")
    elif not os.path.exists("../../sources/DescribeProcess_" + identifier + ".xml"):
        cgierror("Execute: identifier does not exist: '" + identifier + "'")
    else:
        processor = SourceFileLoader(identifier, identifier + ".py").load_module()
        locations = parameters.get('locations')
        parameterid = parameters.get('parameterid')
        if locations is None or locations.get('value') is None:
            cgierror("Execute: bad locations: None")
        elif parameterid is None or parameterid.get('value') is None:
            cgierror("Execute: bad parameterid: None")
        else:
            locations = locations['value'].split(',')
            parameterid = parameterid['value']
            startdate = parameters.get('startdate')
            enddate = parameters.get('enddate')
            if (not startdate is None) and (not enddate is None):
                startdate = startdate['value']
                enddate = enddate['value']
                startdate = datetime.strptime(startdate, "%d-%m-%Y")
                enddate = datetime.strptime(enddate, "%d-%m-%Y")
                enddate += timedelta(days=1)
                startdate = datetime.strftime(startdate, "%Y-%m-%d %H:%M:%S")
                enddate = datetime.strftime(enddate, "%Y-%m-%d %H:%M:%S")
            
            print("Content-type: application/json")
            print()
            
            parametername = ""
            unit = ""

            file = open("../.credentials/.postgres")
            connection_string = file.readline()
            pg = psycopg2.connect(connection_string)
            query = pg.cursor(cursor_factory=RealDictCursor)
            
            for locationid in locations:
                sql = "SELECT timeserieskeys.serieskey AS serieskey, parameterstable.name AS parametername, parametergroups.displayunit AS unit \
                        FROM fews.timeserieskeys \
                        JOIN fews.locations ON timeserieskeys.locationkey = locations.locationkey \
                        JOIN fews.parameterstable ON timeserieskeys.parameterkey = parameterstable.parameterkey \
                        JOIN fews.parametergroups ON parameterstable.groupkey = parametergroups.groupkey \
                        WHERE locations.id = '" + locationid + "' and parameterstable.id = '" + parameterid + "'"
                query.execute(sql)
                result = query.fetchall()
                key = result[0]['serieskey']    
                if parametername == "" and unit == "":
                    parametername = result[0]['parametername']
                    unit = result[0]['unit']
                    print (parametername + "," + unit + ",[", end='')
                else:
                    print(",", end='')
                print("{\"timeseriesid\": " + str(key) + ",\"locationid\": \"" + locationid + "\"", end='')
                sql = "SELECT datetime AS date, scalarvalue AS value FROM fews.timeseriesvaluesandflags WHERE serieskey = " + str(key)
                if (not startdate is None) and (not enddate is None):
                    sql += " AND datetime >= '" + startdate + "' AND datetime < '" + enddate + "'";
                sql += " ORDER BY 1"
                query.execute(sql)
                records = query.fetchall()
                records = processor.process(identifier, parameters, records)
                result = json.dumps(records)
                print(", \"obs\":" + result.replace("'",'"') + "}", end='')
            print("]")

# main()

# GET/POST
querystring = os.environ.get('QUERY_STRING')
querystring = querystring.split('&')
query = {}
for qs in querystring:
    qs = qs.split('=')
    if len(qs) > 1:
        query[qs[0].lower()] = qs[1]
request = query.get('request')
identifier = ''
parameters = {}
if request is None: # POST
    contentlength = os.environ.get('CONTENT_LENGTH', 0)
    if contentlength > 0:
        root = sys.stdin.read(int(contentlength))
        root = xml.parse(postdata).getroot()
        request = nonamespace(root.tag)
        if request != 'GetCapabilities':
            identifier = root.find('{http://www.opengis.net/ows/1.1}Identifier').text
            if request == 'Execute':
                inputs = root.findall('{http://www.opengis.net/wps/1.0.0}DataInputs/{http://www.opengis.net/wps/1.0.0}Input')
                for input in inputs:
                    id = input.find('{http://www.opengis.net/ows/1.1}Identifier').text
                    parameter = {}
                    parameter['title'] = input.find('{http://www.opengis.net/ows/1.1}Title').text
                    parameter['value'] = input.find('{http://www.opengis.net/wps/1.0.0}Data/{http://www.opengis.net/wps/1.0.0}LiteralData').text
                    parameters[id] = parameter
else: # GET
    if request != 'GetCapabilities':
        identifier = query.get('identifier')
        if request == 'Execute':
            inputs = query.get('datainputs')
            if not inputs is None:
                inputs = urllib.parse.unquote(inputs)
                inputs = inputs.split('&')
                for input in inputs:
                    input = input.split('=')
                    parameter = {}
                    id = input[0]
                    parameter['value'] = input[1]
                    parameters[id] = parameter
            else:
                 cgierror("No inputs")

if not request is None:
    if request == 'GetCapabilities':
        printGetCapabilities()
    elif request == 'DescribeProcess':
        printDescribeProcess(identifier)
    elif request == 'Execute':
        printExecute(identifier, parameters)
    else:
        cgierror("Bad request: '" + request + "'")
else:
    printGetCapabilities()

