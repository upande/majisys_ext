from datetime import datetime
from datetime import timedelta
import numpy as np

def process(identifier, parameters, records):
    # TODO: To compute a correct series, this should fill gaps by an interpolation method. Then the mean must be computed over all values, and afterwards all values must be returned (including the interpolated dates).
    values = []
    for record in records:
        values.append(record['value'])
    mean = np.mean(values)
    for record in records:
        record['date'] = datetime.strftime(record['date'], "%Y-%m-%d %H:%M:%S") # change datetime.datetime to str for json.dumps()
        record['value'] = record['value'] - mean
        
    return records
