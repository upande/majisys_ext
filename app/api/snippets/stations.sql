SELECT DISTINCT s.id AS stationname 
FROM fews.locations AS s JOIN fews.timeserieskeys AS t ON t.locationkey = s.locationkey JOIN geography.wrua AS g ON
	(ST_Contains(g.geom, ST_setSRID(ST_MakePoint(s.x, s.y),s.srid)))
WHERE g.wrua_name = '%s'
ORDER BY stationname;
