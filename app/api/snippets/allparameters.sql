SELECT DISTINCT p.id AS parameterid, p.name AS parametername, p.featureclasskey, p.groupkey, g.displayunit
FROM fews.parameterstable AS p JOIN fews.parametergroups AS g ON p.groupkey = g.groupkey
--WHERE id IN ('H.obs','P.obs','E.obs') 
ORDER BY parametername;
