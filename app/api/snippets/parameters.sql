SELECT DISTINCT p.id AS parameterid, p.name AS parametername 
FROM fews.parameterstable AS p JOIN fews.timeserieskeys AS t ON t.parameterkey = p.parameterkey;
