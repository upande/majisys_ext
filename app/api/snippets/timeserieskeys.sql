WITH valuesdata AS (
  SELECT timeseriesvaluesandflags.serieskey,
    MIN(timeseriesvaluesandflags.datetime) AS startdate,
    MAX(timeseriesvaluesandflags.datetime) AS enddate, COUNT(*) AS nritems
  FROM fews.timeseriesvaluesandflags GROUP BY serieskey
)
SELECT timeserieskeys.serieskey, 
  locations.id AS locationid,
  parameterstable.id AS parameterid,
  parameterstable.name AS parametername, 
  valuesdata.startdate::VARCHAR, 
  valuesdata.enddate::VARCHAR,
  valuesdata.nritems, 
  timeserieskeys.modificationtime::VARCHAR AS lastmodified 
FROM fews.timeserieskeys JOIN valuesdata on timeserieskeys.serieskey = valuesdata.serieskey 
       JOIN fews.locations ON timeserieskeys.locationkey = locations.locationkey 
       JOIN fews.parameterstable ON timeserieskeys.parameterkey = parameterstable.parameterkey 
ORDER BY lastmodified DESC; 
