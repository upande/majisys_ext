#!/usr/bin/python3

import os
import psycopg2
import json
from psycopg2.extras import RealDictCursor

print ("Content-type: application/json")
print ()

path = os.environ['REQUEST_URI']

terms = path.split('/')

if (len(terms) > 2) and (terms[len(terms) - 1] != ''):
  sql_code = "" 
  filename = "snippets/%s.sql" % (terms[len(terms) - 1])

  if os.path.exists(filename) and (terms[len(terms) - 2] == 'naivasha'):
    file = open(filename)

    for line in file:
      sql_code += line
    #sql_code = 'SELECT shortname FROM majisys.locations LIMIT 5;'
    # sql_code = sql_code.replace('\n', ' ')
    sql_lower = sql_code.lower()
    # print(sql_code)

    if (sql_lower.find('create')>-1) or (sql_lower.find('insert')>-1) or (sql_lower.find('delete')>-1) or (sql_lower.find('drop')>-1):
      print ('{ "message" : "Could not execute, the query string contains non-allowed commands ,e.g., create, insert." }')
    else:
      file = open(".credentials/.postgres")
      connection_string = file.readline()
      pg = psycopg2.connect(connection_string)
      query = pg.cursor(cursor_factory=RealDictCursor)
      query.execute(sql_code)

      result = json.dumps(query.fetchall())
      print ('{"records":',result.replace("'",'"'),'}')

  else:
    print ('{ "message" : "There is no SQL file called --%s.sql-- for %s" }' % (terms[len(terms) - 1],terms[len(terms) - 2].capitalize()) )

else:
  print ('{ "message" : "The URL --http://majiserver%s-- does not exist" }' % (path))
    
