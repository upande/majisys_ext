SELECT DISTINCT f.featureclasskey, f.featureclassname, p.id AS parameterid, p.name AS parametername 
FROM fews.parameterstable AS p JOIN fews.timeserieskeys AS t ON t.parameterkey = p.parameterkey, 
     fews.featureclass AS f 
WHERE f.featureclasskey = p.featureclasskey 
ORDER BY 1,2,3