SELECT DISTINCT s.id AS stationname 
FROM fews.locations AS s JOIN fews.timeserieskeys AS t ON t.locationkey = s.locationkey 
ORDER BY stationname;
