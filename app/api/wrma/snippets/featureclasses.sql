WITH counts AS (
    SELECT featureclasskey, COUNT(*) AS nrparams 
    FROM fews.parameterstable GROUP BY featureclasskey 
)
SELECT f.featureclasskey, f.featureclassname, c.nrparams
FROM fews.featureclass AS f JOIN counts AS c ON f.featureclasskey = c.featureclasskey 
ORDER BY f.featureclassname;

