SELECT DISTINCT s.id AS stationname, s.description
FROM fews.locations AS s
ORDER BY stationname;
