#!/usr/bin/python3

import csv
import cgi
import json
import re
from datetime import datetime
import fewsdb

# time is a string in dateformat, it will be converted to postgres ISO time string (YYYY-MM-DD HH:MM:SS)
def dateconvertT(time, dateformat): # dateformat: dmy, ymd, mdy, ydm
    time = time.replace('/','-').strip() # normalize
    twodigityear = False
    if dateformat == 'dmy':
        try:
            time = datetime.strptime(time, "%d-%m-%Y %H:%M:%S")
        except ValueError:
            try:
                time = datetime.strptime(time, "%d-%m-%Y %H:%M")
            except ValueError:
                try:
                    time = datetime.strptime(time, "%d-%m-%Y")
                except ValueError:
                    twodigityear = True
                    try:
                        time = datetime.strptime(time, "%d-%m-%y %H:%M:%S")
                    except ValueError:
                        try:
                            time = datetime.strptime(time, "%d-%m-%y %H:%M")
                        except ValueError:
                            time = datetime.strptime(time, "%d-%m-%y")
    elif dateformat == 'ymd': # probably obsolete, but we have it here to supplement the seconds if they are missing
        try:
            time = datetime.strptime(time, "%Y-%m-%d %H:%M:%S")
        except ValueError:
            try:
                time = datetime.strptime(time, "%Y-%m-%d %H:%M")
            except ValueError:
                try:
                    time = datetime.strptime(time, "%Y-%m-%d")
                except ValueError:
                    twodigityear = True
                    try:
                        time = datetime.strptime(time, "%y-%m-%d %H:%M:%S")
                    except ValueError:
                        try:
                            time = datetime.strptime(time, "%y-%m-%d %H:%M")
                        except ValueError:
                            time = datetime.strptime(time, "%y-%m-%d")
    elif dateformat == 'mdy':
        try:
            time = datetime.strptime(time, "%m-%d-%Y %H:%M:%S")
        except ValueError:
            try:
                time = datetime.strptime(time, "%m-%d-%Y %H:%M")
            except ValueError:
                try:
                    time = datetime.strptime(time, "%m-%d-%Y")
                except ValueError:
                    twodigityear = True
                    try:
                        time = datetime.strptime(time, "%m-%d-%y %H:%M:%S")
                    except ValueError:
                        try:
                            time = datetime.strptime(time, "%m-%d-%y %H:%M")
                        except ValueError:
                            time = datetime.strptime(time, "%m-%d-%y")
    elif dateformat == 'ydm':
        try:
            time = datetime.strptime(time, "%Y-%d-%m %H:%M:%S")
        except ValueError:
            try:
                time = datetime.strptime(time, "%Y-%d-%m %H:%M")
            except ValueError:
                try:
                    time = datetime.strptime(time, "%Y-%d-%m")
                except ValueError:
                    twodigityear = True
                    try:
                        time = datetime.strptime(time, "%y-%d-%m %H:%M:%S")
                    except ValueError:
                        try:
                            time = datetime.strptime(time, "%y-%d-%m %H:%M")
                        except ValueError:
                            time = datetime.strptime(time, "%y-%d-%m")
    if twodigityear and (time.year > datetime.now().year):
        time = datetime(time.year - 100, time.month, time.day, time.hour, time.minute, time.second)
    time = time.strftime("%Y-%m-%d %H:%M:%S") # get postgres ISO string
    return time

# date is a string in dateformat, time is HH:MM or HH:MM:SS, date + time will be converted to postgres ISO time string (YYYY-MM-DD HH:MM:SS)
def dateconvertDT(date, time, dateformat): # dateformat: dmy, ymd, mdy, ydm
    return dateconvertT(date + " " + time, dateformat)

def parsecsvdv(linereader, dateformat):
    times = []
    values = []
    messages = []
    for row in linereader:
        if len(row) != 2:
            messages = ["CSV file has " + str(len(row)) + " columns; this doesn't match DateTime, Value."]
            continue
        time = row[0].strip()
        if time == '':
            messages = ["CSV file has line with empty datetime field."]
            continue
        value = row[1].strip()
        time = dateconvertT(time, dateformat)
        if value != '':
            try:
                float(value)
            except ValueError:
                messages = ["Encountered non-numeric value '" + value + "' in CSV file"]
                continue
        times.append(time)
        values.append(value)
    return times,values,messages
    
def parsecsvdtv(linereader, dateformat):
    times = []
    values = []
    messages = []
    for row in linereader:
        if len(row) != 3:
            messages = ["CSV file has " + str(len(row)) + " columns; this doesn't match Date, Time, Value."]
            continue
        date = row[0].strip()
        if date == '':
            messages = ["CSV file has line with empty date field."]
            continue
        time = row[1].strip()
        value = row[2].strip()
        time = dateconvertDT(date, time, dateformat)
        if value != '':
            try:
                float(value)
            except ValueError:
                messages = ["Encountered non-numeric value '" + value + "' in CSV file"]
                continue
        times.append(time)
        values.append(value)
    return times,values,messages

def parsecsvdvt(linereader, dateformat):
    times = []
    values = []
    messages = []
    for row in linereader:
        if len(row) != 3:
            messages = ["CSV file has " + str(len(row)) + " columns; this doesn't match Date, Value, Time."]
            continue
        date = row[0].strip()
        if date == '':
            messages = ["CSV file has line with empty date field."]
            continue
        value = row[1].strip()
        time = row[2].strip()
        time = dateconvertDT(date, time, dateformat)
        if value != '':
            try:
                float(value)
            except ValueError:
                messages = ["Encountered non-numeric value '" + value + "' in CSV file"]
                continue
        times.append(time)
        values.append(value)
    return times,values,messages
    
    
def parsecsvdvvtt(linereader, dateformat):
    times = []
    values = []
    messages = []
    for row in linereader:
        if len(row) != 5:
            messages = ["CSV file has " + str(len(row)) + " columns; this doesn't match Date, Value, Value, Time, Time."]
            continue
        date = row[0].strip()
        if date == '':
            messages = ["CSV file has line with empty date field."]
            continue
        value1 = row[1].strip()
        value2 = row[2].strip()
        time1 = row[3].strip()
        time2 = row[4].strip()
        time1 = dateconvertDT(date, time1, dateformat)
        time2 = dateconvertDT(date, time2, dateformat)
        if value1 != '':
            try:
                float(value1)
            except ValueError:
                messages = ["Encountered non-numeric value '" + value1 + "' in CSV file"]
                continue
        times.append(time1)
        values.append(value1)
        if value2 != '':
            try:
                float(value2)
            except ValueError:
                messages = ["Encountered non-numeric value '" + value2 + "' in CSV file"]
                continue
        times.append(time2)
        values.append(value2)
    return times,values,messages

def parsecsvdtvtv(linereader, dateformat):
    times = []
    values = []
    messages = []
    for row in linereader:
        if len(row) != 5:
            messages = ["CSV file has " + str(len(row)) + " columns; this doesn't match Date, Time, Value, Time, Value."]
            continue
        date = row[0].strip()
        if date == '':
            messages = ["CSV file has line with empty date field."]
            continue
        time1 = row[1].strip()
        value1 = row[2].strip()
        time2 = row[3].strip()
        value2 = row[4].strip()
        time1 = dateconvertDT(date, time1, dateformat)
        time2 = dateconvertDT(date, time2, dateformat)
        if value1 != '':
            try:
                float(value1)
            except ValueError:
                messages = ["Encountered non-numeric value '" + value1 + "' in CSV file"]
                continue
        times.append(time1)
        values.append(value1)
        if value2 != '':
            try:
                float(value2)
            except ValueError:
                messages = ["Encountered non-numeric value '" + value2 + "' in CSV file"]
                continue
        times.append(time2)
        values.append(value2)
    return times,values,messages
    
def parsecsvdvtvt(linereader, dateformat):
    times = []
    values = []
    messages = []
    for row in linereader:
        if len(row) != 5:
            messages = ["CSV file has " + str(len(row)) + " columns; this doesn't match Date, Time, Value, Time, Value."]
            continue
        date = row[0].strip()
        if date == '':
            messages = ["CSV file has line with empty date field."]
            continue
        value1 = row[1].strip()
        time1 = row[2].strip()
        value2 = row[3].strip()
        time2 = row[4].strip()
        time1 = dateconvertDT(date, time1, dateformat)
        time2 = dateconvertDT(date, time2, dateformat)
        if value1 != '':
            try:
                float(value1)
            except ValueError:
                messages = ["Encountered non-numeric value '" + value1 + "' in CSV file"]
                continue
        times.append(time1)
        values.append(value1)
        if value2 != '':
            try:
                float(value2)
            except ValueError:
                messages = ["Encountered non-numeric value '" + value2 + "' in CSV file"]
                continue
        times.append(time2)
        values.append(value2)
    return times,values,messages

def parsecsvdttvv(linereader, dateformat):
    times = []
    values = []
    messages = []
    for row in linereader:
        if len(row) != 5:
            messages = ["CSV file has " + str(len(row)) + " columns; this doesn't match Date, Time, Time, Value, Value."]
            continue
        date = row[0].strip()
        if date == '':
            messages = ["CSV file has line with empty date field."]
            continue
        time1 = row[1].strip()
        time2 = row[2].strip()
        value1 = row[3].strip()
        value2 = row[4].strip()
        time1 = dateconvertDT(date, time1, dateformat)
        time2 = dateconvertDT(date, time2, dateformat)
        if value1 != '':
            try:
                float(value1)
            except ValueError:
                messages = ["Encountered non-numeric value '" + value1 + "' in CSV file"]
                continue
        times.append(time1)
        values.append(value1)
        if value2 != '':
            try:
                float(value2)
            except ValueError:
                messages = ["Encountered non-numeric value '" + value2 + "' in CSV file"]
                continue
        times.append(time2)
        values.append(value2)
    return times,values,messages

def parsecsv(linereader, csvlayout, dateformat): # dtv dvt dtvtv dvtvt dttvv dvvtt dv
    if csvlayout == 'dv':
        return parsecsvdv(linereader, dateformat)
    elif csvlayout == 'dtv':
        return parsecsvdtv(linereader, dateformat)
    elif csvlayout == 'dvt':
        return parsecsvdvt(linereader, dateformat)
    elif csvlayout == 'dtvtv':
        return parsecsvdtvtv(linereader, dateformat)
    elif csvlayout == 'dvtvt':
        return parsecsvdvtvt(linereader, dateformat)
    elif csvlayout == 'dttvv':
        return parsecsvdttvv(linereader, dateformat)        
    elif csvlayout == 'dvvtt':
        return parsecsvdvvtt(linereader, dateformat)        
    else:
        return [],[],["Unimplemented csv layout."]

# main()

form = cgi.FieldStorage()
uploadedFile = form['filedata-button']
location = form.getvalue('locationid-inputEl')
parameter = form.getvalue('parameterid-inputEl')
csvlayout = form.getvalue('csvlayout') # dtv, dvvtt, dtvtv, dttvv
dateformat = form.getvalue('dateformat') # dmy, ymd, mdy, ydm
filename = uploadedFile.filename
comment = "Imported from \"" + filename + "\""
comment = comment[:64]
messages = []
try:
    csvfile = uploadedFile.file.read().decode('utf-8','ignore')
    sample = csvfile[0:1024]
    dialect = csv.Sniffer().sniff(sample)
    header = ''
    linereader = csv.reader(csvfile.splitlines(), dialect)
    if csv.Sniffer().has_header(sample):
        header = next(linereader)
    elif re.search('[a-zA-Z]', sample.splitlines()[0]):
        header = next(linereader)
    times,values,messages = parsecsv(linereader, csvlayout, dateformat)
    if len(times) > 0: # no issues with the parsing
        comments = [comment]
        flags = []
        messages = fewsdb.importdb(parameter, location, times, values, flags, comments)
    else:
        messages += ["Timeseries from CSV not inserted: problem parsing CSV file."]
except Exception as err:
    messages.append(str(err) + ".")
    messages.append("Timeseries from CSV not inserted: problem parsing CSV file.")

print("Content-type: application/json")
print()
print("{\"success\": true, \"messages\": " + json.dumps(messages) + "}")
