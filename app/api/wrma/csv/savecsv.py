#!/usr/bin/python3

import os
import cgi
import psycopg2
from datetime import datetime
from io import BytesIO, StringIO
import sys
from zipfile import ZipFile, ZIP_DEFLATED
from psycopg2.extras import RealDictCursor

def cgierror(message):
    print("Content-type: text/html")
    print()
    print(message)

def WriteCsv(times, values, locationid, parametername, ofile):
    ofile.write("\"Time\",\"" + locationid + " - " + parametername + "\"\n")
    for i in range(0, len(times)):
        ofile.write(times[i].strftime("%d/%m/%Y %H:%M:%S") + "," + str(values[i]) + "\n")
    startTime = times[0]
    endTime = times[len(times) - 1]
    starttime = startTime.strftime("%Y%m%d_%H%M%S")
    endtime = endTime.strftime("%Y%m%d_%H%M%S")
    name = locationid + " - " + parametername + " - From " + starttime + " To " + endtime
    return name

# main()

params = cgi.FieldStorage()
serieskeys = params.getvalue('serieskeys')
startdate = params.getvalue('startdate')
enddate = params.getvalue('enddate')
if not serieskeys is None:
    serieskeys = serieskeys.split(',')

if not serieskeys is None:
    if len(serieskeys) > 1:
        file = open("../.credentials/.postgres")
        connection_string = file.readline()
        pg = psycopg2.connect(connection_string)
        query = pg.cursor(cursor_factory=RealDictCursor)
        
        if (not startdate is None) and (not enddate is None) and (startdate != 'start'):
            startdate = datetime.strptime(startdate, "%d-%m-%Y")
            enddate = datetime.strptime(enddate, "%d-%m-%Y")
            startdate = datetime.strftime(startdate, "%Y-%m-%d %H:%M:%S")
            enddate = datetime.strftime(enddate, "%Y-%m-%d %H:%M:%S")
        name = ''
        # start writing zip file
        memoryZipFile = BytesIO()
        dfszip = ZipFile(memoryZipFile, 'w', ZIP_DEFLATED)
        for key in serieskeys:
            sql = "SELECT locations.id AS locationid, parameterstable.name AS parametername \
                    FROM fews.timeserieskeys \
                    JOIN fews.locations ON timeserieskeys.locationkey = locations.locationkey \
                    JOIN fews.parameterstable ON timeserieskeys.parameterkey = parameterstable.parameterkey \
                    WHERE timeserieskeys.serieskey = " + key
            query.execute(sql)
            result = query.fetchall()
            locationid = result[0]['locationid']
            parametername = result[0]['parametername']
            sql = "SELECT datetime::text, scalarvalue FROM fews.timeseriesvaluesandflags WHERE serieskey = " + key
            if (not startdate is None) and (not enddate is None) and (startdate != 'start'):
                sql += " AND datetime >= '" + startdate + "' AND datetime < '" + enddate + "'";
            query.execute(sql)
            result = query.fetchall()
            times = []
            values = []
            for res in result:
                time = res['datetime']
                value = res['scalarvalue']
                time = datetime.strptime(time, "%Y-%m-%d %H:%M:%S")
                value = float(value)
                times.append(time)
                values.append(value)
            
            if (len(times) > 0):
                memoryCsvFile = StringIO(newline='\r\n') # force ms-dos line endings
                name = WriteCsv(times, values, locationid, parametername, memoryCsvFile)
                # append to zip file
                filename = name + ".csv"
                dfszip.writestr(filename, memoryCsvFile.getvalue())
                # end appending to zip file
                memoryCsvFile.close()
            else:
                dfszip.writestr(locationid + " - " + parametername + " - no values found in requested period.txt", locationid + " - " + parametername + " - no values found in requested period.")

        dfszip.close() # close appends the ZIP EOCD record 
        # end writing zip file
        
        if name == '':
            name = 'empty'

        # start outputting cgi
        zipname = name + ".zip"
        print("Content-type: application/octet-stream")
        print("Content-Disposition: attachment; filename=\"" + zipname + "\"")
        print()
        sys.stdout.flush() # flush it here otherwise the sys.stdout.buffer.write will happen before the print
        sys.stdout.buffer.write(memoryZipFile.getvalue())
        sys.stdout.flush()
        # end outputting cgi
        memoryZipFile.close()
    elif len(serieskeys) == 1:
        file = open("../.credentials/.postgres")
        connection_string = file.readline()
        pg = psycopg2.connect(connection_string)
        query = pg.cursor(cursor_factory=RealDictCursor)
        
        key = serieskeys[0]
        sql = "SELECT locations.id AS locationid, parameterstable.name AS parametername \
                FROM fews.timeserieskeys \
                JOIN fews.locations ON timeserieskeys.locationkey = locations.locationkey \
                JOIN fews.parameterstable ON timeserieskeys.parameterkey = parameterstable.parameterkey \
                WHERE timeserieskeys.serieskey = " + key
        query.execute(sql)
        result = query.fetchall()
        locationid = result[0]['locationid']
        parametername = result[0]['parametername']
        sql = "SELECT datetime::text, scalarvalue FROM fews.timeseriesvaluesandflags WHERE serieskey = " + key
        if (not startdate is None) and (not enddate is None) and (startdate != 'start'):
            startdate = datetime.strptime(startdate, "%d-%m-%Y")
            enddate = datetime.strptime(enddate, "%d-%m-%Y")
            startdate = datetime.strftime(startdate, "%Y-%m-%d %H:%M:%S")
            enddate = datetime.strftime(enddate, "%Y-%m-%d %H:%M:%S")
            sql += " AND datetime >= '" + startdate + "' AND datetime < '" + enddate + "'";
        sqlfile = open("/tmp/sql.txt", "a")
        sqlfile.write(sql)
        sqlfile.close()
        query.execute(sql)
        result = query.fetchall()
        times = []
        values = []
        for res in result:
            time = res['datetime']
            value = res['scalarvalue']
            time = datetime.strptime(time, "%Y-%m-%d %H:%M:%S")
            value = float(value)
            times.append(time)
            values.append(value)
            
        # start writing single csv file
        memoryCsvFile = StringIO(newline='\r\n') # force ms-dos line endings
        if (len(times) > 0):
            filename = WriteCsv(times, values, locationid, parametername, memoryCsvFile)
            filename += ".csv"
        else:
            memoryCsvFile.write(locationid + " - " + parametername + " - no values found in requested period.")
            filename = locationid + " - " + parametername + " - no values found in requested period.txt"
        # end writing csv file

        # start outputting cgi
        print("Content-type: text/csv")
        print("Content-Disposition: attachment; filename=\"" + filename + "\"")
        print()
        sys.stdout.flush() # flush it here otherwise the sys.stdout.buffer.write will happen before the print
        sys.stdout.write(memoryCsvFile.getvalue())
        sys.stdout.flush()
        memoryCsvFile.close()
        # end outputting cgi
    else:
        cgierror("Please select at least one time series to download")
else:
    cgierror("Please select at least one time series to download")


