#!/usr/bin/python3

import json
import psycopg2
from psycopg2.extras import RealDictCursor

print ("Content-type: application/json")
print ()

file = open(".credentials/.postgres")
connection_string = file.readline()
pg = psycopg2.connect(connection_string)

query = pg.cursor()
boundsQuery = "SELECT ST_Extent(geom) AS bounds FROM fews.stations;"
query.execute(boundsQuery)

result = str(query.fetchall())
result = result.replace("[('BOX(",'')
result = result.replace(")',)]",'')
result = result.replace(' ',',')
result = result.split(',')
if result[0] == "-inf":
    result[0] = "-179"
if result[1] == "-inf":
    result[1] = "-89"
if result[2] == "inf":
    result[2] = "179"
if result[3] == "inf":
    result[3] = "89"
result = ','.join(result)
print ('['+result+']')

