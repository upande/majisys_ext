#!/usr/bin/python3

import json
import psycopg2
from psycopg2.extras import RealDictCursor

print ("Content-type: application/json")
print ()

file = open(".credentials/.postgres")
connection_string = file.readline()
pg = psycopg2.connect(connection_string)

query = pg.cursor(cursor_factory=RealDictCursor)

stationsQuery = "SELECT 'FeatureCollection' AS type, json_agg(row_to_json(items)) AS features FROM \
    (SELECT 'Feature' as type, l.locationkey as id, row_to_json(( \
        SELECT list \
        FROM (SELECT l.locationkey, l.id as stationid, l.name AS stationname, x, y, z, srid, false AS active) AS list )) as properties, \
        st_asgeojson(st_transform(st_setsrid(st_point(l.x,l.y),l.srid),4326)) as geometry \
    FROM fews.locations as l \
    ORDER BY l.id) AS items"

query.execute(stationsQuery)

result = json.dumps(query.fetchall())

result = result[1:]
result = result[:-1]
result = result.replace('\\','')
result = result.replace('"[','[')
result = result.replace(']"',']')
result = result.replace('"{','{')
result = result.replace('}"','}')
result = result.replace('null','""')

print ('['+result+']')