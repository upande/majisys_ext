#!/usr/bin/python3

import json
import os
import psycopg2
import sys
from psycopg2.extras import RealDictCursor

def deleteLocations(locationDeletes):
    success = True
    messages = []
    file = open(".credentials/.postgresadmin")
    connection_string = file.readline()
    pg = psycopg2.connect(connection_string)
    query = pg.cursor(cursor_factory=RealDictCursor)
    try:
        sql = "BEGIN;"
        query.execute(sql)
        locationids = ""
        for locationkey in locationDeletes:
            if locationids != '':
                locationids += ", "
            locationids += str(locationkey)
        sql = "DELETE FROM fews.locations WHERE locationkey IN (" + locationids + ");"
        query.execute(sql)
        sql = "COMMIT;"
        query.execute(sql)
        messages = ["Successfully deleted " + str(len(locationDeletes)) + " locations"]
        
    except Exception as err:
        success = False
        messages.append(str(err) + ".")
        messages.append("Failed to delete the majisys locations!")
    return success, messages

contentlength = os.environ.get('CONTENT_LENGTH', 0)
postdata = sys.stdin.read(int(contentlength))
postdata = json.loads(postdata)
locationDeletes = []
for record in postdata:
    if 'id' in record: # delete
        locationDeletes.append(record['id'])

if len(locationDeletes) > 0:
    success, messages = deleteLocations(locationDeletes)
else:
    success = false
    messages = ["Please select at least one location to delete."]

print ("Content-type: application/json")
print ()
if success:
    print("{\"success\": true, \"messages\": " + json.dumps(messages) + "}")
else:
    print("{\"success\": false, \"messages\": " + json.dumps(messages) + "}")

