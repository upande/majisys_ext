#!/usr/bin/python3

import os
import cgi
import psycopg2
import json
from psycopg2.extras import RealDictCursor

def add_where(parameterid, stationid):
    sql = ''
    if parameterid != 'all' or stationid != 'All Stations':
        sql += " WHERE "
        if parameterid != 'all':
            sql += "parameterstable.id='" + parameterid + "' "
        if parameterid != 'all' and stationid != 'All Stations':
            sql += " AND "
        if stationid != 'All Stations':
            sql += "locations.id='" + stationid + "' "
    return sql

def add_sort(sort):
    sql = ''
    if not sort is None:
        sort = json.loads(sort)[0]
        sql = " ORDER BY " + sort['property'] + " " + sort['direction']
    else:
        sql = " ORDER BY lastmodified DESC" # force a default ordering when no order was specified; most recent addition on-top
    return sql
    
# main()    
params = cgi.FieldStorage()
offset = params.getvalue('start')
limit = params.getvalue('limit')
parameterid = params.getvalue('parameterid', 'all')
stationid = params.getvalue('stationid', 'All Stations') # note that stationid == stationname, while parameterid != parametername
sort = params.getvalue('sort')

print ("Content-type: application/json")
print ()

file = open(".credentials/.postgres")
connection_string = file.readline()
pg = psycopg2.connect(connection_string)
query = pg.cursor(cursor_factory=RealDictCursor)

count_code = "SELECT COUNT(*) \
  FROM fews.timeserieskeys \
  JOIN fews.locations ON timeserieskeys.locationkey = locations.locationkey \
  JOIN fews.parameterstable ON timeserieskeys.parameterkey = parameterstable.parameterkey"
count_code += add_where(parameterid, stationid)
query.execute(count_code)
total = query.fetchall()

if offset is None:
  offset = 0

if limit is None:
  limit = total[0]['count']
  
sql_code = "\
  WITH valuesdata AS ( \
    SELECT timeseriesvaluesandflags.serieskey, \
      min(timeseriesvaluesandflags.datetime) as startdate, \
      max(timeseriesvaluesandflags.datetime) as enddate, COUNT(*) AS nritems \
    FROM fews.timeseriesvaluesandflags GROUP BY serieskey \
  ) \
  SELECT timeserieskeys.serieskey, \
    locations.id AS locationid, \
    parameterstable.id AS parameterid, \
    parameterstable.name AS parametername, \
    parameterstable.featureclasskey, \
    valuesdata.startdate::VARCHAR, \
    valuesdata.enddate::VARCHAR, \
    valuesdata.nritems, \
    timeserieskeys.modificationtime::VARCHAR AS lastmodified \
  FROM fews.timeserieskeys JOIN valuesdata on timeserieskeys.serieskey = valuesdata.serieskey \
         JOIN fews.locations ON timeserieskeys.locationkey = locations.locationkey \
         JOIN fews.parameterstable ON timeserieskeys.parameterkey = parameterstable.parameterkey"
sql_code += add_where(parameterid, stationid)
sql_code += add_sort(sort)
sql_code += " OFFSET %s LIMIT %s;" % (offset,limit)

query.execute(sql_code)

result = json.dumps(query.fetchall())
print ('{"total" :',total[0]['count'],',"records" :',result.replace("'",'"'),'}')
    
