from datetime import datetime
from datetime import timedelta
import numpy as np
from scipy.stats.kde import gaussian_kde

def process(identifier, parameters, records):
    values = []
    for record in records:
        values.append(record['value'])
    newrecords = []
    try:
        KDEpdf = gaussian_kde(values)
        x_vals = list(np.linspace(np.min(values), np.max(values), 1000))
        y_vals = KDEpdf(x_vals)
        for i in range(1000):
            newrecord = {}
            newrecord['date'] = x_vals[i]
            newrecord['value'] = y_vals[i]
            newrecords.append(newrecord)
        return newrecords
    except ValueError:
        for record in records:
            record['date'] = record['value']
            record['value'] = 1
        return records

