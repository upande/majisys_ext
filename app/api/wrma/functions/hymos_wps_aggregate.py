from datetime import datetime
from datetime import timedelta

def process(identifier, parameters, records):
    timestep = parameters.get('timestep')
    if not timestep is None:
        timestep = int(timestep['value'])
    else:
        timestep = 1

    newrecords = []
    date = datetime.now()
    value = 0
    nrs = 0
    period = timedelta(days=timestep)
    for record in records:
        if nrs == 0:
            record_date = record['date']
            date = datetime(record_date.year, record_date.month, record_date.day)
            value = record['value']
            nrs = 1
        elif record['date'] < date + period:
            value = value + record['value']
            nrs = nrs + 1
        else:
            newrecord = {}
            newrecord['date'] = date
            newrecord['value'] = value / nrs
            newrecords.append(newrecord)
            record_date = record['date']
            date = datetime(record_date.year, record_date.month, record_date.day)
            value = record['value']
            nrs = 1
    if nrs > 0: # still add the last data
        newrecord = {}
        newrecord['date'] = date
        newrecord['value'] = value / nrs
        newrecords.append(newrecord)
        
    for record in newrecords: # change datetime.datetime to str for json.dumps()
       record['date'] = datetime.strftime(record['date'], "%Y-%m-%d %H:%M:%S")

    return newrecords
