#!/bin/bash
#
MAPSERV="/usr/lib/cgi-bin/mapserv"
MAPFILE=$dir"$(readlink -f $(dirname "$0"))/layers.map"
if [ "${REQUEST_METHOD}" = "GET" ]; then
  QUERY_STRING="map=${MAPFILE}&${QUERY_STRING}"
  exec ${MAPSERV}
else
  echo "Sorry, I only understand GET requests."
fi
exit 1
# End of Script
