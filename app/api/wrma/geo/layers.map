MAP
    NAME "Majisys_OWS_Services"
    IMAGECOLOR 255 255 255
    TRANSPARENT ON
    SIZE 2000 2000
    
    # Background color for the map canvas -- change as desired
    IMAGECOLOR 255 255 255
    IMAGEQUALITY 95  
    
    # FONTSET "fonts/fonts.list"
	
    PROJECTION
        "init=epsg:4326"
    END

    SYMBOL
        NAME 'circle'
        TYPE ELLIPSE
        FILLED TRUE
        POINTS
            1 1
        END
    END 
    
    SYMBOL
        NAME 'triangle'
        TYPE VECTOR
        FILLED TRUE
        POINTS
            0 1
            .5 0
            1 1
            0 1
        END
    END 	
	
    #---
    EXTENT -10000000 -10000000 10000000 10000000
	
#----- Start of WEB DEFINITION ------
	WEB
		IMAGEPATH    '/home/majisys/webapps/ms_tmp/'
		IMAGEURL     '/majisys/ms_tmp/'
		#---
		METADATA
			"ows_schemas_location"          "http://schemas.opengeospatial.net"
			"ows_title"                     "Map Connector to PostGIS"
			"ows_abstract"                  "WMS examples with Mapserver for ITC exercises."
			"ows_onlineresource"            "http://127.0.0.1/majisys/app/api/geo/layers"
			"ows_fees"                      "None"
			"ows_accessconstraints"         "None"
			#---
			"wms_contactperson"             "Majisys Team"
			"wms_contactorganization"       "UT-ITC"
			"wms_addresstype"               "Postal"
			"wms_address"                   "Hengelosestraat 99"
			"wms_city"                      "Enschede"
			"wms_stateorprovince"           "Overijssel"
			"wms_postcode"                  "7514 AE"
			"wms_country"                   "The Netherlands"
			"wms_title"                     "Majisys - Geo-spatial Services"
			"wms_srs"                       "EPSG:4326 EPSG:3857"
			"wms_feature_info_mime_type"    "application/vnd.ogc.gml"
			"wms_feature_info_mime_type"    "text/plain"
			"wms_feature_info_mime_type"    "text/html"
			"wms_server_version"            "1.1.1 1.3.0"
			"wms_formatlist"                "image/png,image/gif,image/bmp,image/jpeg"
			"wms_format"                    "image/png"
			"wms_enable_request"		    "GetMap GetFeatureInfo GetCapabilities"
            #---
            "wfs_title"                     "Majisys - Geo-spatial Services"
            "wfs_onlineresource"            "http://127.0.0.1/majisys/app/api/geo/layers"
            "wfs_srs"                       "EPSG:4326 EPSG:3857"
            "wfs_server_version"            "1.0.0"
            "wfs_namespace_prefix"          "majisys"
			"wfs_enable_request"		    "GetFeature GetCapabilities"
            "wfs_getfeature_formatlist"     "geojson, csv, ogrgml"
            #--- 			
		END
        TEMPLATE 'OnlyForWMSGetFeatureInfo'
		
		#---
	END
#----- End of WEB DEFINITION ------


    OUTPUTFORMAT
        NAME "geojson"
        DRIVER "OGR/GEOJSON"
        MIMETYPE "application/json; subtype=geojson; charset=utf-8"
        FORMATOPTION "STORAGE=stream"
        FORMATOPTION "FORM=SIMPLE"
        FORMATOPTION "LCO:COORDINATE_PRECISION=5"
    END


#----- Start of LAYER DEFINITIONS ---------------------------------------------

    #----- Polygon Connector -----
    LAYER
        GROUP 'polygeom'
        NAME 'polygon_interior'
        TYPE POLYGON
        STATUS OFF
        DUMP TRUE
        
        CONNECTIONTYPE postgis
        CONNECTION "dbname='majisys' host=localhost port=5432 user='webapp' password='w3b4pp' sslmode=disable options='-c client_encoding=UTF8'"
        DATA  "ogc_geom FROM ( %SQLCODE% ) AS query USING UNIQUE ogc_id USING SRID=%SQLSRID%" 
        
        VALIDATION
          'SQLCODE' '^.+$'
          'SQLSRID' '^.+$'
        END        
        
        METADATA
            'wms_title' 'Polygon Connector'
            'wms_group_title' 'Polygon Connector'
            "wms_abstract" "Displays interior of polygon layers from the Majisys database"
            "wms_include_items" "all"
        END
    
        PROJECTION
            "init=epsg:4326"
        END
        
        CLASS
            NAME     "Interior Color"
            TEMPLATE "template.html"
            STYLE
                COLOR 55 255 55
            END
        END
        OPACITY 30
    END    
    #---
    LAYER
        GROUP 'polygeom'
        NAME 'polygon_boundary'
        TYPE LINE
        STATUS OFF
        DUMP TRUE
        
        CONNECTIONTYPE postgis
        CONNECTION "dbname='majisys' host=localhost port=5432 user='webapp' password='w3b4pp' sslmode=disable options='-c client_encoding=UTF8'"
        DATA  "ogc_geom FROM ( %SQLCODE% ) AS query USING UNIQUE ogc_id USING SRID=%SQLSRID%" 

        VALIDATION
          'SQLCODE' '^.+$'
          'SQLSRID' '^.+$'
        END
        
        METADATA
            'wms_title' 'Polygon Connector'
            'wms_group_title' 'Polygon Connector'
            "wms_abstract" "Displays boundaries of polygon layers from the Majisys database"
            "wms_include_items" "all"
        END
    
        PROJECTION
            "init=epsg:4326"
        END
        
        CLASS
            NAME     "Boundary Color"
            TEMPLATE "template.html"
            STYLE
                OUTLINECOLOR 55 255 55
                WIDTH        1.5
            END
        END
    END
    #----- Polygon Connector
    
    
    #----- Line Connector -----
    LAYER
        NAME 'linegeom'
        TYPE LINE
        STATUS OFF
        DUMP TRUE
        
        CONNECTIONTYPE postgis
        CONNECTION "dbname='majisys' host=localhost port=5432 user='webapp' password='w3b4pp' sslmode=disable options='-c client_encoding=UTF8'"
        DATA  "ogc_geom FROM ( %SQLCODE% ) AS query USING UNIQUE ogc_id USING SRID=%SQLSRID%" 

        VALIDATION
          'SQLCODE' '^.+$'
          'SQLSRID' '^.+$'
        END
        
        METADATA
            'wms_title' 'Line Connector'
            "wms_abstract" "Displays line layers from the Majisys database"
        END
    
        PROJECTION
            "init=epsg:4326"
        END
        
        CLASS
            NAME     "Line Color"
            TEMPLATE "template.html"
            STYLE
                OUTLINECOLOR 178 178 178
                WIDTH        1.5
            END
        END
    END
    #----- Line Connector
    
    
    
    #----- Point Connector -----
    LAYER
        NAME 'pointgeom'
        TYPE POINT
        STATUS OFF
        DUMP TRUE
        
        CONNECTIONTYPE postgis
        CONNECTION "dbname='majisys' host=localhost port=5432 user='webapp' password='w3b4pp' sslmode=disable options='-c client_encoding=UTF8'"
        DATA  "ogc_geom FROM ( %SQLCODE% ) AS query USING UNIQUE ogc_id USING SRID=%SQLSRID%" 

        VALIDATION
          'SQLCODE' '^.+$'
          'SQLSRID' '^.+$'
        END
        
        METADATA
            'wms_title' 'Point Connector'
            "wms_abstract" "Displays point layers from the Majisys database"
        END
    
        PROJECTION
            "init=epsg:4326"
        END
        
        CLASS
            NAME        "Point Symbol"
            TEMPLATE	"template.html"
            STYLE
                SYMBOL 			"circle"
                SIZE 			7
		        COLOR    		128 128 255
		        OUTLINECOLOR 	110 110 110
            END
        END
    END    
  #----- Point Connector
    
    
    
  #----- Lake -----
    LAYER
      NAME              "lake"
      TYPE              POLYGON
      STATUS            ON
      DUMP              TRUE
    #---
      CONNECTIONTYPE    postgis
      CONNECTION        "dbname='majisys' host=localhost port=5432 user='webapp' password='w3b4pp' sslmode=disable options='-c client_encoding=UTF8'"
      DATA              "ogc_geom FROM ( 
                          SELECT geom AS ogc_geom, gid AS ogc_id  FROM geography.lake 
                        ) 
                        AS query USING UNIQUE ogc_id USING SRID=21037"                                           
    #---
      PROJECTION
        "init=epsg:21037"   
      END
    #---
      METADATA
        "ows_title"     "Lake Naivasha"
        "wms_srs"       "EPSG:4326 EPSG:3857 EPSG:21037"
        "gml_featureid" "ogc_id"
        "wfs_extent"    "174950 9898040 244100 9984500"
        "wfs_srs"       "EPSG:4326 EPSG:3857 EPSG:21037"
      END
    #---
      CLASS
        TEMPLATE        "empty" 
        NAME            "Lake"
        STYLE
          COLOR         130 200 255 
          OUTLINECOLOR  0 120 200
          WIDTH         1.5
        END #style
      END
      OPACITY 70
    END
  #----- Lake    
  
  
  #----- WRUAs -----
    LAYER
      GROUP           "wrua"
      NAME            "wrua_interior"
      TYPE            POLYGON
      STATUS          ON
      DUMP            TRUE
    #---
      CONNECTIONTYPE  postgis
      CONNECTION      "dbname='majisys' host=localhost port=5432 user='webapp' password='w3b4pp' sslmode=disable options='-c client_encoding=UTF8'"
      DATA            "ogc_geom FROM ( 
                        SELECT geom AS ogc_geom, gid AS ogc_id, wrua_name, rgb FROM geography.wrua 
                      ) 
                      AS query USING UNIQUE ogc_id USING SRID=21037"                                           
    #---
      PROJECTION
        "init=epsg:21037"   
      END
    #---
      METADATA
        "ows_title"          "WRUAs"
        "wms_srs"            "EPSG:4326 EPSG:3857 EPSG:21037"
        "wms_group_title"    "WRUA Connector"
        "gml_featureid"      "ogc_id"
        "gml_include_items"  "all"				
        "wfs_extent"         "174950 9898040 244100 9984500"
        "wfs_srs"            "EPSG:4326 EPSG:3857 EPSG:21037"
      END
    #---
      CLASS
        TEMPLATE        "empty" 
        NAME            "WRUA's"
        STYLE
          COLOR         [rgb]  
        END #style
      END
      OPACITY 30
    END
    ####
    LAYER
      GROUP           "wrua"
      NAME            "wrua_boundary"
      TYPE            POLYGON
      STATUS          ON
      DUMP            TRUE
    #---
      CONNECTIONTYPE  postgis
      CONNECTION      "dbname='majisys' host=localhost port=5432 user='webapp' password='w3b4pp' sslmode=disable options='-c client_encoding=UTF8'"
      DATA            "ogc_geom FROM ( 
                        SELECT geom AS ogc_geom, gid AS ogc_id, wrua_name, rgb FROM geography.wrua 
                      ) 
                      AS query USING UNIQUE ogc_id USING SRID=21037"                                           
    #---
      PROJECTION
        "init=epsg:21037"   
      END
    #---
      METADATA
        "ows_title"          "WRUAs"
        "wms_srs"            "EPSG:4326 EPSG:3857 EPSG:21037"
        "wms_group_title"    "WRUA Connector"
        "gml_featureid"      "ogc_id"
        "gml_include_items"  "all"				
        "wfs_extent"         "174950 9898040 244100 9984500"
        "wfs_srs"            "EPSG:4326 EPSG:3857 EPSG:21037"
      END
    #---
      CLASS
        TEMPLATE        "empty" 
        NAME            "WRUA's"
        STYLE
          OUTLINECOLOR  119 119 119
          WIDTH         1
        END #style
      END
    END
  #----- WRUAs  
	
		
#----- End of LAYER DEFINITIONS ---------------------------------------------
		
END		
