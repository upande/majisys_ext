#!/usr/bin/python3

import json
import psycopg2
from psycopg2.extras import RealDictCursor

print ("Content-type: application/json")
print ()

file = open("../.credentials/.postgres")
connection_string = file.readline()
pg = psycopg2.connect(connection_string)

query = pg.cursor(cursor_factory=RealDictCursor)

tableQuery = "WITH table_list AS ( \
                SELECT f_table_schema, f_table_name, f_geometry_column, srid \
                FROM geometry_columns), \
            details_list AS ( \
                SELECT n.nspname, c.relname, c.relkind, a.attname \
                FROM pg_class c, \
                    pg_attribute a, \
                    pg_type t, \
                    pg_namespace n \
                WHERE t.typname = 'geometry' \
                AND a.attisdropped = false \
                AND a.atttypid = t.oid \
                AND a.attrelid = c.oid \
                AND c.relnamespace = n.oid) \
             SELECT tbl.f_table_schema AS schema_name, tbl.f_table_name AS table_name, \
                    tbl.f_geometry_column AS geometry_column, tbl.srid, dtl.relkind \
             FROM table_list AS tbl LEFT JOIN details_list AS dtl ON tbl.f_table_schema = dtl.nspname \
                  AND tbl.f_table_name = dtl.relname \
                  AND tbl.f_geometry_column = dtl.attname \
             ORDER by 1,2"



query.execute(tableQuery)

result = str(query.fetchall())
result = result.replace("'",'"')
# result = result.replace("[('BOX(",'')
# result = result.replace(")',)]",'')
# result = result.replace(' ',',')

print (result)

