#!/usr/bin/python3

import json
import os
import sys
from datetime import datetime
import fewsdb
import re

# time is a string in dateformat, it will be converted to postgres ISO time string (YYYY-MM-DD HH:MM:SS)
def dateconvertT(time, dateformat): # dateformat: dmy, ymd, mdy, ydm
    time = time.replace('/','-') # normalize
    if dateformat == 'dmy':
        try:
            time = datetime.strptime(time, "%d-%m-%Y %H:%M:%S")
        except ValueError:
            time = datetime.strptime(time, "%d-%m-%Y %H:%M")
    elif dateformat == 'ymd': # probably obsolete, but we have it here to supplement the seconds if they are missing
        try:
            time = datetime.strptime(time, "%Y-%m-%d %H:%M:%S")
        except ValueError:
            time = datetime.strptime(time, "%Y-%m-%d %H:%M")
    elif dateformat == 'mdy':
        try:
            time = datetime.strptime(time, "%m-%d-%Y %H:%M:%S")
        except ValueError:
            time = datetime.strptime(time, "%m-%d-%Y %H:%M")
    elif dateformat == 'ydm':
        try:
            time = datetime.strptime(time, "%Y-%d-%m %H:%M:%S")
        except ValueError:
            time = datetime.strptime(time, "%Y-%d-%m %H:%M")
    time = time.strftime("%Y-%m-%d %H:%M:%S") # get postgres ISO string
    return time

# date is a string in dateformat, time is HH:MM or HH:MM:SS, date + time will be converted to postgres ISO time string (YYYY-MM-DD HH:MM:SS)
def dateconvertDT(date, time, dateformat): # dateformat: dmy, ymd, mdy, ydm
    return dateconvertT(date + " " + time, dateformat)

# main()

messages = []
try:
    contentlength = os.environ.get('CONTENT_LENGTH', 0)
    postdata = sys.stdin.read(int(contentlength))
    querystring = os.environ.get('QUERY_STRING')
    querystring = querystring.split('&')
    query = {}
    for qs in querystring:
        qs = qs.split('=')
        query[qs[0]] = qs[1]
    location = query['locationid']
    parameter = query['paramaterid']
    dateformat = 'dmy' # dmy, ymd, mdy, ydm
    comment = "Submitted by Manual Entry in Form"
    comment = comment[:64]
    postdata = json.loads(postdata)
    times = []
    values = []
    flags = []
    fourdigits = re.compile("\d{4}") # 4 digits
    for row in postdata:
        date = row['obsdate']
        flag = row['quality']
        hours = []
        for key in row.keys():
            if fourdigits.match(key):
                if not row[key] is None:
                    hours.append((key, str(row[key])))
                else:
                    hours.append((key, "NULL"))
        for h in sorted(hours):
            time = h[0]
            value = h[1]
            time = time[:2] + ":" + time[2:]
            time = dateconvertDT(date, time, dateformat)
            times.append(time)
            values.append(value)

    comments = [comment]
    messages = fewsdb.importdb(parameter, location, times, values, flags, comments)
except Exception as err:
    messages.append(str(err) + ".")
    messages.append("Timeseries data not inserted: problem parsing the data entered.")

print("Content-type: application/json")
print()
print("{\"success\": true, \"messages\": " + json.dumps(messages) + "}")
