#!/usr/bin/python3

import os
import psycopg2
from datetime import datetime
import sys
import json
from psycopg2.extras import RealDictCursor

def initform():
    print("Content-type: application/json")
    print()

    file = open("../.credentials/.postgres")
    connection_string = file.readline()
    pg = psycopg2.connect(connection_string)
    query = pg.cursor(cursor_factory=RealDictCursor)

    sql = "SELECT DISTINCT id FROM fews.locations ORDER BY id;"
    query.execute(sql)
    result = json.dumps(query.fetchall())
    print("[{\"locations\":" + result.replace("'",'"') + ",", end='')

    sql = "SELECT featureclasskey, initcap(featureclassname) as featureclassname FROM fews.featureclass;"
    query.execute(sql)
    result = json.dumps(query.fetchall())
    print("\"featureclass\":" + result.replace("'",'"') + ",", end='')

    sql = "SELECT id, initcap(name) as name, featureclasskey FROM fews.parameterstable WHERE id NOT LIKE '%calc';"
    query.execute(sql)
    result = json.dumps(query.fetchall())
    print("\"parameters\":" + result.replace("'",'"') + "}]")


# main()

initform()


