#!/usr/bin/python3

import cgi
import psycopg2
import json
from psycopg2.extras import RealDictCursor


def getserieskey(parameterid, locationid):
  sqlcode = "SELECT timeserieskeys.serieskey \
             FROM fews.timeserieskeys \
             JOIN fews.locations ON timeserieskeys.locationkey = locations.locationkey \
             JOIN fews.parameterstable ON timeserieskeys.parameterkey = parameterstable.parameterkey \
             JOIN fews.parametergroups ON parameterstable.groupkey = parametergroups.groupkey \
             WHERE parameterstable.id ='%s' \
               AND locations.id = '%s';" % (parameterid, locationid)

  query.execute(sqlcode)
  result = query.fetchall()
  if len(result) > 0:
    serieskey = result[0]['serieskey']
  else:
    serieskey = None
  return serieskey


def updateview(serieskey):
  sqlcode = "CREATE OR REPLACE VIEW monitoring.seriestab AS \
             WITH params AS ( \
               SELECT \
                 '%s 00:00:00'::TIMESTAMP AS period_start, \
                 '%s 24:00:00'::TIMESTAMP AS period_end ) \
             SELECT to_char(datetime, 'DD-MM-YYYY') as obsdate, \
                    to_char(datetime, 'HH24MI') as period, \
                    scalarvalue AS value \
             FROM fews.timeseriesvaluesandflags, params as p \
             WHERE serieskey = %s \
               AND datetime <@ tsrange(p.period_start,p.period_end) \
             ORDER BY 1;" % (startdate, enddate, serieskey)
  query.execute(sqlcode)


def crosstab():
  sqlcode = "SELECT monitoring.pivotcode('monitoring.seriestab','obsdate','period','avg(value)','REAL');"
  query.execute(sqlcode)
  result = query.fetchall()
  seriestab = result[0]['pivotcode']
  seriestab = seriestab.replace('\r\n','')
  seriestab = seriestab.replace(',_',', "')
  seriestab = seriestab.replace(' REAL','" REAL')
  return seriestab

# main()

params = cgi.FieldStorage()
serieskey =  params.getvalue('serieskey')
parameterid =  params.getvalue('parameterid')
locationid =  params.getvalue('locationid')
startdate =  params.getvalue('startdate')
enddate =  params.getvalue('enddate')

# parameterid = 'H.obs'
# locationid = '2GB04'
# startdate = '2014-12-01'
# enddate = '2014-12-31'

file = open("../.credentials/.postgres")
connection_string = file.readline()
file.close()

pg = psycopg2.connect(connection_string)
query = pg.cursor(cursor_factory=RealDictCursor)


print ("Content-type: application/json")
print ()

if serieskey is None:
  if parameterid is None or locationid is None or startdate is None or enddate is None:
    print ('{"success":false, "message":"Missing one or more parameters (locationid, parameterid, startdate, enddate) or (serieskey, startdate, enddate) "}')
  else:
    serieskey = getserieskey(parameterid, locationid)
    if serieskey is None:
      print ('{"success":false, "message":"Series does not exist"}')
else:
  if startdate is None or enddate is None:
    print ('{"success":false, "message":"Missing one or more parameters (serieskey, startdate, enddate)"}')
    serieskey = None
  else:
    serieskey = int(serieskey)

if not serieskey is None:
  updateview(serieskey)
  sqlcode = 'SELECT COUNT(*) FROM monitoring.seriestab;'
  query.execute(sqlcode)
  result = (query.fetchall())
  total = result[0]['count']
  if total == 0:
    print ('{"success":false, "message":"No data found for the given parameter set"}')
  else:
    sqlcode = crosstab()
    query.execute(sqlcode)
    result = json.dumps(query.fetchall())
    print ('{"success":true, "obs":',result.replace("'",'"'),'}')

