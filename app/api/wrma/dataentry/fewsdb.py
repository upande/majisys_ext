#!/usr/bin/python3

import os
import psycopg2
import sys
from psycopg2.extras import RealDictCursor
import subprocess

#insert into postgres
#call fews import db
#cleanup monitoring.dump
#capture fews errors in log.txt
#report 

def importdb(parameterid, locationid, times, values, flags, comments):
    messages = []
    file = open("../.credentials/.postgresadmin")
    connection_string = file.readline()
    pg = psycopg2.connect(connection_string)
    query = pg.cursor(cursor_factory=RealDictCursor)
    sql = "BEGIN;"
    flagsmax = 0
    if not flags is None:
        flagsmax = len(flags)
    commentsmax = 0
    if not comments is None:
        commentsmax = len(comments)
    try:
        query.execute(sql)
        nr = 0
        for nr in range(0, len(times)):
            time = times[nr]
            value = values[nr]
            flag = ''
            comment = ''
            if nr < flagsmax:
                flag = str(flags[nr])
            if nr < commentsmax:
                comment = comments[nr]
            sql = "INSERT INTO monitoring.dump(datetime, scalarvalue, parameterid, locationid, flags, commenttext) VALUES ('"
            sql += time + "'," + value +",'" + parameterid + "','" + locationid + "',"
            if len(flag) > 0:
                sql += flag + ","
            else:
                sql += "NULL,"
            if len(comment) > 0:
                sql += "'" + comment + "'"
            else:
                sql += "NULL"
            sql += ");"
            query.execute(sql)
        sql = "COMMIT;"
        query.execute(sql)
        cwdorig = os.getcwd() # current folder
        os.chdir(os.path.join(os.path.abspath(sys.path[0]), "../../../../processing/fews/")) # change to location ot runimportpostgrestable.sh
        devnull = open(os.devnull, 'w')
        process = subprocess.Popen(["sh", "./runimportpostgrestable.sh"], shell=False, stdout=subprocess.PIPE, stderr=devnull)
        process.wait()
        logfile, err = process.communicate()
        logfile = logfile.decode('utf-8')
        logfile = logfile.splitlines()
        error = ''
        warning = ''
        count = ''
        for line in logfile:
            if "ERROR" in line:
                error = line[(8 + line.find("ERROR")):]
            elif "WARN" in line:
                warning = line[(7 + line.find("WARN")):]
            elif "rows imported from" in line:
                count = line[(30 + line.find("GeneralDatabaseParser.parse")):(line.find("rows imported from") - 1)]
        if count == '':
            count = '0'
        os.chdir(cwdorig) # restore to current folder
        nr = len(times)
        if int(count) == nr:
            messages.append("Inserted one timeseries with " + str(nr) + " records!")
        elif int(count) > 0 and int(count) < nr:
            messages.append("Error: timeseries inserted partially: " + count + " out of " + str(nr) + " records were inserted!")
            if len(error) > 0:
                messages.append("Additional info follows:")
                messages.append(error)
            elif len(warning) > 0:
                messages.append("Additional info follows:")
                messages.append(warning)
        elif int(count) == 0:
            messages.append("Error: timeseries not inserted!")
            if len(error) > 0:
                messages.append("Additional info follows:")
                messages.append(error)
            elif len(warning) > 0:
                messages.append("Additional info follows:")
                messages.append(warning)
    except Exception as err:
        messages.append(str(err) + ".")
        messages.append("Failed to insert the form data into majisys!")
    sql = "DELETE FROM monitoring.dump;"
    query.execute(sql)
    return messages



