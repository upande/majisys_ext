#!/usr/bin/python3

import json
import os
import psycopg2
import sys
from psycopg2.extras import RealDictCursor

def translateColToPg(column_name): # store/basedata/Stations.js to postgres
    if column_name == 'stationid':
        column_name = 'id'
    elif column_name == 'stationname':
        column_name = 'name'
    return column_name

def translateValToPg(column_name, value):
    if (not value is None) and (str(value).strip() != ''):
        if column_name == 'x' or column_name == 'y' or column_name == 'z' or column_name == 'srid':
            return str(value).strip()
        else:
            return "'" + str(value).strip() + "'"
    else:
        return "NULL"

def updateLocations(locationInserts, locationUpdates):
    success = True
    messages = []
    file = open(".credentials/.postgresadmin")
    connection_string = file.readline()
    pg = psycopg2.connect(connection_string)
    query = pg.cursor(cursor_factory=RealDictCursor)
    try:
        sql = "BEGIN;"
        query.execute(sql)
        locations = {}
        for locationguid in locationInserts.keys():
            sql = ""
            inserts = locationInserts[locationguid]
            for column_name in inserts.keys():
                if sql == "":
                    sql += "INSERT INTO fews.locations ("
                else:
                    sql += ", "
                sql += translateColToPg(column_name)
            sql += ") VALUES ("
            first = True
            for column_name in inserts.keys():
                if first:
                    first = False
                else:
                    sql += ", "
                sql += translateValToPg(column_name, inserts[column_name])
            sql += ") RETURNING locationkey;"
            query.execute(sql)
            locationid = query.fetchone()['locationkey']
            location = {}
            location['id'] = locationguid
            location['locationkey'] = locationid
            locations[locationid] = location
        for locationid in locationUpdates.keys():
            sql = ""
            updates = locationUpdates[locationid]
            for column_name in updates.keys():
                if sql == "":
                    sql += "UPDATE fews.locations SET "
                else:
                    sql += ", "
                sql += translateColToPg(column_name) + "=" + translateValToPg(column_name, updates[column_name])
            sql += " WHERE locationkey=" + str(locationid) + ";"
            query.execute(sql)
            location = {}
            location['id'] = locationid # optional
            location['locationkey'] = locationid
            locations[locationid] = location
        locationids = ""
        for locationid in locations.keys():
            if locationids == "":
                locationids = locationids + str(locationid)
            else:
                locationids = locationids + "," + str(locationid)
        sql = "SELECT locationkey, id, x, y FROM fews.wgslocations WHERE locationkey IN (" + locationids + ")";
        query.execute(sql)
        records = query.fetchall()
        for record in records:
            locationid = record['locationkey']
            locations[locationid]['x'] = record['x']
            locations[locationid]['y'] = record['y']
        messages = list(locations.values())
        sql = "COMMIT;"
        query.execute(sql)
        
    except Exception as err:
        success = False
        messages.append(str(err) + ".")
        messages.append("Failed to update the majisys locations!")
    return success, messages

contentlength = os.environ.get('CONTENT_LENGTH', 0)
postdata = sys.stdin.read(int(contentlength))
postdata = json.loads(postdata)
locationUpdates = {}
locationInserts = {}
for record in postdata:
    if 'id' in record: # update
        locationid = record['id']
        updates = {}
        for key in record.keys():
            if key != 'id':
                updates[key] = record[key]
        locationUpdates[locationid] = updates
    elif 'locationkey' in record: # insert
        locationguid = record['locationkey']
        inserts = {}
        for key in record.keys():
            if key != 'locationkey':
                inserts[key] = record[key]
        locationInserts[locationguid] = inserts

success, messages = updateLocations(locationInserts, locationUpdates)
print ("Content-type: application/json")
print ()
if success:
    print("{\"success\": true, \"messages\": " + json.dumps(messages) + "}")
else:
    print("{\"success\": false, \"messages\": " + json.dumps(messages) + "}")

