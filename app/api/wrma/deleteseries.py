#!/usr/bin/python3

import os
import cgi
import psycopg2
from datetime import datetime
from io import StringIO
import sys
import json
from psycopg2.extras import RealDictCursor

def cgimessage(message):
    print(message + "<br/>")

def delete(serieskeys, startdate, enddate):
    file = open(".credentials/.postgresadmin")
    connection_string = file.readline()
    try:
        pg = psycopg2.connect(connection_string)
        cursor = pg.cursor(cursor_factory=RealDictCursor)
        rowsbefore = cursor.rowcount
        sql = ''
        if (not startdate is None) and (not enddate is None) and (startdate != 'start'):
            startdate = datetime.strptime(startdate, "%d-%m-%Y")
            enddate = datetime.strptime(enddate, "%d-%m-%Y")
            startdate = datetime.strftime(startdate, "%Y-%m-%d %H:%M:%S")
            enddate = datetime.strftime(enddate, "%Y-%m-%d %H:%M:%S")
            sql = "BEGIN; DELETE FROM fews.timeseriescomments WHERE serieskey IN (" + serieskeys + ") AND datetime >= '" + startdate + "' AND datetime < '" + enddate + "'; DELETE FROM fews.timeseriesvaluesandflags WHERE serieskey IN (" + serieskeys + ") AND datetime >= '" + startdate + "' AND datetime < '" + enddate + "';" 
            for serieskey in serieskeys.split(','):
                sql = sql + " DELETE FROM fews.timeserieskeys WHERE serieskey = " + serieskey + " AND NOT EXISTS (SELECT * FROM fews.timeseriesvaluesandflags WHERE serieskey = " + serieskey + " LIMIT 1);"
            sql = sql + " COMMIT;"
        else:
            sql = "BEGIN; DELETE FROM fews.timeseriescomments WHERE serieskey IN (" + serieskeys + "); DELETE FROM fews.timeseriesvaluesandflags WHERE serieskey IN (" + serieskeys + "); DELETE FROM fews.timeserieskeys WHERE serieskey IN (" + serieskeys + "); COMMIT;"
        cursor.execute(sql)
        if (not startdate is None) and (not enddate is None) and (startdate != 'start'):
            cgimessage("Deleted the selected records from " + str(1 + serieskeys.count(',')) + " timeseries!")
        else:
            cgimessage("Deleted " + str(1 + serieskeys.count(',')) + " timeseries!")
    except Exception as err:
        cgimessage(str(err) + ".")
        cgimessage("Failed to delete data from the selected timeseries records!")

# main()

print("Content-type: text/html")
print()

params = cgi.FieldStorage()
serieskeys = params.getvalue('serieskeys')
startdate = params.getvalue('startdate')
enddate = params.getvalue('enddate')

if not serieskeys is None and len(serieskeys) > 0:
    delete(serieskeys, startdate, enddate)
else:
    cgimessage("Please select at least one time series to delete.")
