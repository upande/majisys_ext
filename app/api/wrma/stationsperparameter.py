#!/usr/bin/python3

import os
import cgi
import psycopg2
import json
from psycopg2.extras import RealDictCursor

params = cgi.FieldStorage()
parameterid = params.getvalue('parameterid')

print ("Content-type: application/json")
print ()

file = open(".credentials/.postgres")
connection_string = file.readline()
pg = psycopg2.connect(connection_string)
query = pg.cursor(cursor_factory=RealDictCursor)

sql_code = "\
    WITH aggregates AS ( \
        SELECT v.serieskey, min(v.datetime) AS startdate, \
               max(v.datetime) AS enddate \
        FROM fews.timeseriesvaluesandflags as v \
        GROUP BY 1 \
    ) \
    SELECT k.serieskey, p.id, l.stationkey, l.station_id AS id, l.station_name AS name, ST_X(l.geom) AS x, ST_Y(l.geom) AS y, \
           to_char(a.startdate, 'DD-MM-YYYY') AS startdate, to_char(a.enddate, 'DD-MM-YYYY') as enddate \
    FROM fews.timeserieskeys AS k JOIN fews.parameterstable AS p ON k.parameterkey = p.parameterkey \
         JOIN fews.stations AS l ON l.stationkey = k.locationkey \
         JOIN aggregates AS a ON k.serieskey = a.serieskey  \
    WHERE p.id = '%s';" % ( parameterid )
    
query.execute(sql_code)    
result = json.dumps(query.fetchall())

print (result)