#!/usr/bin/python3

import os
import cgi
import psycopg2
from datetime import datetime
import sys
import json
from psycopg2.extras import RealDictCursor

def cgierror(message):
    print("Content-type: text/html")
    print()
    print(message)

def getvalues(serieskeys):
    print("Content-type: application/json")
    print()

    parametername = ""
    unit = ""

    file = open("../.credentials/.postgres")
    connection_string = file.readline()
    pg = psycopg2.connect(connection_string)
    query = pg.cursor(cursor_factory=RealDictCursor)

    for key in serieskeys:
        if key == "":
            print("Empty key provided")
            return
        sql = "SELECT locations.id AS locationid, parameterstable.name AS parametername, parametergroups.displayunit AS unit \
                FROM fews.timeserieskeys \
                JOIN fews.locations ON timeserieskeys.locationkey = locations.locationkey \
                JOIN fews.parameterstable ON timeserieskeys.parameterkey = parameterstable.parameterkey \
                JOIN fews.parametergroups ON parameterstable.groupkey = parametergroups.groupkey \
                WHERE timeserieskeys.serieskey = " + key
        query.execute(sql)
        result = query.fetchall()
        locationid = result[0]['locationid']
        if parametername == "" and unit == "":
            parametername = result[0]['parametername']
            unit = result[0]['unit']
            print (parametername + "," + unit + ",[", end='')
        else:
            print(",", end='')
        print("{\"timeseriesid\": " + key + ",\"locationid\": \"" + locationid + "\"", end='')
        sql = "SELECT datetime::text AS date, scalarvalue AS value FROM fews.timeseriesvaluesandflags WHERE serieskey = " + key + " ORDER BY 1"
        query.execute(sql)
        result = json.dumps(query.fetchall())
        print(", \"series\":" + result.replace("'",'"') + "}", end='')
    print("]")


# main()

params = cgi.FieldStorage()
serieskeys = params.getvalue('serieskeys')

if not serieskeys is None:
    serieskeys = serieskeys.split(',')

if not serieskeys is None:
    if len(serieskeys) > 0:
        getvalues(serieskeys)
    else:
        cgierror("Please select at least one time series to download")
else:
    cgierror("Please select at least one time series to download")


