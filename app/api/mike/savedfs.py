#!/usr/bin/python3

import os
import cgi
import psycopg2
from datetime import datetime
from io import BytesIO
import sys
from zipfile import ZipFile, ZIP_DEFLATED
from psycopg2.extras import RealDictCursor
import dfs

def translate(parameterid):
    if parameterid == "H.obs":
        return "WaterLevel"
    elif parameterid == "P.obs":
        return "Rainfall"
    elif parameterid == "E.obs":
        return "Evaporation"
    else:
        return "WaterLevel" #error; add more mappings

# main()

params = cgi.FieldStorage()
serieskeys = params.getvalue('serieskeys')
startdate = params.getvalue('startdate')
enddate = params.getvalue('enddate')
if not serieskeys is None:
    serieskeys = serieskeys.split(',')

if not serieskeys is None and len(serieskeys) > 0:
    file = open("../.credentials/.postgres")
    connection_string = file.readline()
    pg = psycopg2.connect(connection_string)
    query = pg.cursor(cursor_factory=RealDictCursor)
    
    if (not startdate is None) and (not enddate is None) and (startdate != 'start'):
        startdate = datetime.strptime(startdate, "%d-%m-%Y")
        enddate = datetime.strptime(enddate, "%d-%m-%Y")
        startdate = datetime.strftime(startdate, "%Y-%m-%d %H:%M:%S")
        enddate = datetime.strftime(enddate, "%Y-%m-%d %H:%M:%S")
    zipname = ''
    # start writing zip file
    memoryZipFile = BytesIO()
    dfszip = ZipFile(memoryZipFile, 'w', ZIP_DEFLATED)
    for key in serieskeys:
        sql = "SELECT locations.id AS locationid, parameterstable.id AS parameterid \
                FROM fews.timeserieskeys \
                JOIN fews.locations ON timeserieskeys.locationkey = locations.locationkey \
                JOIN fews.parameterstable ON timeserieskeys.parameterkey = parameterstable.parameterkey \
                WHERE timeserieskeys.serieskey = " + key
        query.execute(sql)
        result = query.fetchall()
        locationid = result[0]['locationid']
        parameterid = result[0]['parameterid']
        parameterid = translate(parameterid)
        sql = "SELECT datetime::text, scalarvalue FROM fews.timeseriesvaluesandflags WHERE serieskey = " + key
        if (not startdate is None) and (not enddate is None) and (startdate != 'start'):
            sql += " AND datetime >= '" + startdate + "' AND datetime < '" + enddate + "'";
        query.execute(sql)
        result = query.fetchall()
        times = []
        values = []
        for res in result:
            time = res['datetime']
            value = res['scalarvalue']
            time = datetime.strptime(time, "%Y-%m-%d %H:%M:%S")
            value = float(value)
            times.append(time)
            values.append(value)

        if (len(times) > 0):
            zipname = dfs.WriteDfsZero(times, values, locationid, parameterid, dfszip)
        else:
            dfszip.writestr(locationid + " - " + parameterid + " - no values found in requested period.txt", locationid + " - " + parameterid + " - no values found in requested period")

    dfszip.close() # close appends the ZIP EOCD record 
    # end writing zip file

    if zipname == '':
        zipname = 'empty'

    # start outputting cgi
    zipname += ".zip"
    print("Content-type: application/octet-stream")
    print("Content-Disposition: attachment; filename=\"" + zipname + "\"")
    print()
    sys.stdout.flush() # flush it here otherwise the sys.stdout.buffer.write will happen before the print
    sys.stdout.buffer.write(memoryZipFile.getvalue())
    sys.stdout.flush()
    # end outputting cgi
    memoryZipFile.close()
else:
    print("Content-type: text/html")
    print("Content-Disposition: attachment; filename=\"errormessage.txt\"")
    print()
    sys.stdout.flush()

