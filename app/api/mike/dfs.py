#!/usr/bin/python3

import struct
from datetime import datetime
from io import BytesIO
import sys
from zipfile import ZipFile, ZIP_DEFLATED
from operator import itemgetter
import eumconstants
import stationids
import tsids

def WriteDouble(ofile, rVal):
    w = ofile.write(struct.pack("<d", rVal))

def WriteFloat(ofile, rVal):
    w = ofile.write(struct.pack("<f", rVal))

def WriteInt(ofile, iVal):
    w = ofile.write(struct.pack("<i", iVal))

def WriteUnsignedInt(ofile, iVal):
    w = ofile.write(struct.pack("<I", iVal))

def WriteShort(ofile, iVal):
    w = ofile.write(struct.pack("<h", iVal))

def WriteUnsignedShort(ofile, iVal):
    w = ofile.write(struct.pack("<H", iVal))

def WriteByte(ofile, b):
    w = ofile.write(struct.pack("B", b))

def WriteDoubleArray(ofile, values):
    WriteByte(ofile, 2) # type double array
    WriteInt(ofile, len(values))
    for value in values:
        WriteDouble(ofile, value)

def WriteFloatArray(ofile, values):
    WriteByte(ofile, 1) # type float array
    WriteInt(ofile, len(values))
    for value in values:
        WriteFloat(ofile, value)

def WriteIntArray(ofile, values):
    WriteByte(ofile, 4) # type int array
    WriteInt(ofile, len(values))
    for value in values:
        WriteInt(ofile, value)

def WriteUnsignedIntArray(ofile, values):
    WriteByte(ofile, 5) # type unsigned int array
    WriteInt(ofile, len(values))
    for value in values:
        WriteUnsignedInt(ofile, value)

def WriteShortArray(ofile, values):
    WriteByte(ofile, 6) # type short array
    WriteInt(ofile, len(values))
    for value in values:
        WriteShort(ofile, value)

def WriteUnsignedShortArray(ofile, values):
    WriteByte(ofile, 7) # type unsigned short array
    WriteUnsignedShort(ofile, len(values))
    for value in values:
        WriteUnsignedShort(ofile, value)

def WriteCharArray(ofile, string):
    WriteByte(ofile, 3) # type char array
    iSize = len(string) + 1 # add one to the size for the null-terminating character
    WriteInt(ofile, iSize)
    w = ofile.write(struct.pack(str(iSize) + "s", bytes(string, 'utf-8'))) # writing one character more than the length of the string to force adding a null-terminating character in the file

def WriteString(ofile, string):
    iSize = len(string) # as-is, without null-terminating character
    w = ofile.write(struct.pack(str(iSize) + "s", bytes(string, 'utf-8')))

def WriteTag(ofile, tag):
    WriteByte(ofile, 254)
    WriteUnsignedShort(ofile, tag)
    WriteByte(ofile, 255)

def WriteDoubleArrayWithTag(ofile, tag, values):
    WriteByte(ofile, 254)
    WriteUnsignedShort(ofile, tag)
    WriteDoubleArray(ofile, values)
    WriteByte(ofile, 255)

def WriteFloatArrayWithTag(ofile, tag, values):
    WriteByte(ofile, 254)
    WriteUnsignedShort(ofile, tag)
    WriteFloatArray(ofile, values)
    WriteByte(ofile, 255)

def WriteIntArrayWithTag(ofile, tag, intArray):
    WriteByte(ofile, 254)
    WriteUnsignedShort(ofile, tag)
    WriteIntArray(ofile, intArray)
    WriteByte(ofile, 255)

def WriteUnsignedIntArrayWithTag(ofile, tag, values):
    WriteByte(ofile, 254)
    WriteUnsignedShort(ofile, tag)
    WriteUnsignedIntArray(ofile, values)
    WriteByte(ofile, 255)

def WriteShortArrayWithTag(ofile, tag, shortArray):
    WriteByte(ofile, 254)
    WriteUnsignedShort(ofile, tag)
    WriteShortArray(ofile, shortArray)
    WriteByte(ofile, 255)

def WriteUnsignedShortArrayWithTag(ofile, tag, values):
    WriteByte(ofile, 254)
    WriteUnsignedShort(ofile, tag)
    WriteUnsignedShortArray(ofile, values)
    WriteByte(ofile, 255)

def WriteCharArrayWithTag(ofile, tag, string):
    WriteByte(ofile, 254)
    WriteUnsignedShort(ofile, tag)
    WriteCharArray(ofile, string)
    WriteByte(ofile, 255)

def countRuns(values, nodata):
    count = 0
    inside = False
    for value in values:
        if value == nodata:
            inside = False
        else:
            if not inside:
                inside = True
                count = count + 1
    return count

def sumsq(values):
    return sum(x*x for x in values)
    
def cgierror(message):
    print("Content-type: text/html")
    print()
    print(message)

def WriteDfsZero(times, values, stationid, parameterid, dfszip):
    item = tsids.getItems(stationid, parameterid) # this returns a dictionary
    if len(item) < 1:
        cgierror("Error: station '" + stationid + "' with parameterid '" + parameterid + "' not registered in file tsids.py; please add it")
        return
    elif len(item) > 1:
        # print ("Warning: station '" + stationid + "' with parameterid '" + parameterid + "' is not unique in file tsids.py; using random entry") # not in cgi-bin
        item = sorted(item.items(), key=itemgetter(1)) # sort by value (the entire tsid tuple), in practice this sorts by tsid, smallest first
        item = item[0] # replace item with the first item in the sorted list
    else:
        item = next(iter(item.items())) # this replaces item with the first (and in this case the only) element of the dictionary
    tsid_name = item[0]
    tsid = item[1].DHI_ID
    eumtype = item[1].EUMType
    eumunit = item[1].EUMUnit
    itemvaluetype = item[1].ValueType
    
    floatnodatavalue = -1e-030
    statsvalues = [x for x in values if x != floatnodatavalue]
    rMin = min(statsvalues)
    rMax = max(statsvalues)
    rSum = sum(statsvalues)
    startTime = times[0]
    endTime = times[len(times) - 1]
    nowTime = datetime.now()
    starttime = startTime.strftime("%Y%m%d_%H%M%S")
    endtime = endTime.strftime("%Y%m%d_%H%M%S")
    name = "TSID " + str(tsid) + " From " + starttime + " To " + endtime
    filename = name + ".dfs0"

    # start writing dfs0 file
    ofile = BytesIO()

    # magic string
    WriteString(ofile, "DHI_DFS_ MIKE Zero - this file contains binary data, do not edit  FOpenFileCreate")

    # header
    WriteByte(ofile, 26)
    WriteShort(ofile, nowTime.year)
    WriteShort(ofile, nowTime.month)
    WriteShort(ofile, nowTime.day)
    WriteShort(ofile, nowTime.hour)
    WriteShort(ofile, nowTime.minute)
    WriteShort(ofile, nowTime.second)
    WriteInt(ofile, 104) # tag
    WriteInt(ofile, 206)
    WriteShort(ofile, nowTime.year)
    WriteShort(ofile, nowTime.month)
    WriteShort(ofile, nowTime.day)
    WriteShort(ofile, nowTime.hour)
    WriteShort(ofile, nowTime.minute)
    WriteShort(ofile, nowTime.second)
    WriteInt(ofile, 104) #tag
    WriteInt(ofile, 206)
    WriteInt(ofile, 0)
    WriteInt(ofile, 0)

    WriteTag(ofile, 10000) # 10000
    WriteShortArrayWithTag(ofile, 10004, [4]) # 10004
    WriteCharArrayWithTag(ofile, 10001, "") # 10001 : description
    WriteCharArrayWithTag(ofile, 10002, "dfs Timeseries Bridge") # 10002 : producing software; should eventually be MAJISYS
    WriteIntArrayWithTag(ofile, 10003, [100]) # 10003
    WriteIntArrayWithTag(ofile, 10012, [0]) # 10012
    WriteIntArrayWithTag(ofile, 10013, [0]) # 10013
    WriteIntArrayWithTag(ofile, 10005, [2]) # 10005
    WriteFloatArrayWithTag(ofile, 10006, [floatnodatavalue]) # 10006, float nodata value
    WriteDoubleArrayWithTag(ofile, 10007, [floatnodatavalue]) # 10007, double nodata value
    WriteCharArrayWithTag(ofile, 10008, "") # 10008, string nodata value
    WriteIntArrayWithTag(ofile, 10009, [2147483647]) # 10009, integer nodata value
    WriteUnsignedIntArrayWithTag(ofile, 10010, [2147483647]) # 10010, unsigned integer nodata value
    WriteTag(ofile, 20000) # 20000

    # 20054, time unit
    WriteByte(ofile, 254)
    WriteUnsignedShort(ofile, 20054)
    WriteCharArray(ofile, startTime.strftime("%Y-%m-%d")) # "2012-12-01";
    WriteCharArray(ofile, startTime.strftime("%H:%M:%S")) # "08:15:00";
    WriteIntArray(ofile, [1400]) # time step unit; this is 1400 for seconds, 1401 for minutes and 1402 for hours
    WriteDoubleArray(ofile, [0, (endTime - startTime).days * 86400 + (endTime - startTime).seconds]) # tstart: always 0, tstep: total nr. of seconds (time of last sample minus time of first sample). Minutes or hours also supported, this depends on the "time step unit" above.
    WriteIntArray(ofile, [len(values), 0]) # nr items (size of sampleslist), unknown 0
    WriteByte(ofile, 255)

    WriteTag(ofile, 30000) # 30000
    WriteIntArrayWithTag(ofile, 30001, [1]) #30001

    # 30005, details
    WriteByte(ofile, 254)
    WriteUnsignedShort(ofile, 30005)
    WriteIntArray(ofile, [eumtype]) # EUMType; EUM = Engineering Unit Management
    WriteCharArray(ofile, tsid_name) # "2FC11 LITTLE SHURU - WaterLevel"; description of the data
    WriteIntArray(ofile, [eumunit]) # EUMUnit
    WriteIntArray(ofile, [2]) # DataType; 1: FloatArray, 2: DoubleArray, 3: CharArray, 4: IntArray, 5: UnsignedIntArray, 6: ShortArray, 7: UnsignedShortArray
    WriteDoubleArray(ofile, [rMax, rMin, len(values)-len(statsvalues)]) # max, min, nr. of nodata values
    WriteFloatArray(ofile, [0, 0, 0]) # data1: unknown what this is
    WriteFloatArray(ofile, [0, 0, 0]) # data2: unknown what this is
    WriteByte(ofile, 255)

    WriteIntArrayWithTag(ofile, 30006, [itemvaluetype]) # ItemValueType: 0 == Instantaneous, 1 == Accumulated, 2 == Step_Accumulated, 3 == Mean_Step_Accumulated, 4 == Reverse_Mean_Step_Accumulated
    WriteIntArrayWithTag(ofile, 31051, [0])
    runs = countRuns(values, floatnodatavalue)
    WriteDoubleArrayWithTag(ofile, 30003, [rMax, rMin, rSum, sumsq(statsvalues), 0, values[len(values)-1], len(statsvalues), len(statsvalues) - runs, len(values)-len(statsvalues)]) # 30003, statistics; [max, min, sum, sumsq, ?, lastval, nrvals_noundef, nrvals_noundef - runs, nr_undefs]

    # 50000, start of the data
    WriteTag(ofile, 50000)
    for i in range(0, len(values)):
        WriteDoubleArrayWithTag(ofile, 50001, [(times[i] - startTime).days * 86400 + (times[i] - startTime).seconds]) # 50001, time record
        WriteDoubleArray(ofile, [values[i]])

    # end writing dfs0 file

    # start writing xml file
    filenamexml = name + ".xml"
    xfile = BytesIO()
    WriteByte(xfile, 239) # Byte-order mark: UTF-8 (see https://en.wikipedia.org/wiki/Byte_order_mark)
    WriteByte(xfile, 187) #
    WriteByte(xfile, 191) #
    WriteString(xfile, "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><!--This file shall always follow the dfs0 file with time series data to be loaded to the database--><root><TimeSeries ID=\"")
    WriteString(xfile, str(tsid))
    WriteString(xfile, "\" EumType=\"")
    WriteString(xfile, eumconstants.eumTypes[eumtype])
    WriteString(xfile, "\" EumUnit=\"")
    WriteString(xfile, eumconstants.eumUnits[eumunit])
    WriteString(xfile, "\" ValueType=\"")
    WriteString(xfile, eumconstants.itemValueType[itemvaluetype])
    WriteString(xfile, "\" /><Feature FeatureClass=\"")
    WriteString(xfile, stationids.stationFeatureClass(stationid))
    WriteString(xfile, "\" ID=\"")
    WriteString(xfile, str(stationids.stationFeatureClassId(stationid)))
    WriteString(xfile, "\" /></root>")
    # end writing xml file

    # append to zip file
    dfszip.writestr(filename, ofile.getvalue())
    dfszip.writestr(filenamexml, xfile.getvalue())
    # end appending to zip file

    ofile.close()
    xfile.close()
    
    return name # caller incorporates this name to construct the zipfilename

#f = open("/home/majisys/webapps/TSID 200000414 From 20130101_080000 To 20130430_150000_3.csv", "r")
#times = []
#values = []
#f.readline()
#for line in f.readlines():
#    time,value = line.strip().split(',')
#    time = datetime.strptime(time, "%d/%m/%Y %H:%M:%S")
#    times.append(time)
#    value = float(value)
#    values.append(value)
#WriteDfsZero(times, values, "2FC06", "WATER LEVEL - Rating curves") # times = datetime without timezone; values = plain floating point values; stationid = from majisys; measurement = from majisys, translated to mike
#f.close()


