Ext.define('majisys_dds.store.main.WRMANavigationTree', {
    extend: 'Ext.data.TreeStore',

    storeId: 'WRMANavigationTree',

    root: {
        expanded: true,
        children: [{
            text: 'HOME',
            title: 'HOME',
            iconCls: 'x-fa fa-home',
            classColor: 'blue',
            route: 'home',
            viewType: 'homehome',
            rendered: false,
            leaf: true
        }, {
            text: 'BASE DATA',
            title: 'DEFINITION OF TIME SERIES COMPONENTS',
            iconCls: 'x-fa fa-database',
            classColor: 'blue',
            route: 'basedata',
            viewType: 'wrmabasedatadashboard',
            rendered: false,
            leaf: true
        }, {
            text: 'DATA ENTRY',
            title: 'MANUAL DATA ENTRY',
            iconCls: 'x-fa fa-edit',
            classColor: 'blue',
            route: 'dataentry',
            viewType: 'wrmadataentry',
            rendered: false,
            leaf: true
        }, {
            text: 'TIME SERIES',
            title: 'TIME SERIES MANAGEMENT',
            iconCls: 'x-fa fa-line-chart',
            classColor: 'blue',
            route: 'timeseries',
            viewType: 'wrmatimeserieskeys',
            rendered: false,
            leaf: true
        }, {
            text: 'VISUALIZATION',
            title: 'VISUALIZATION',
            iconCls: 'x-fa fa-desktop',
            classColor: 'plain',
            route: null,
            viewType: 'panel',
            extended: false,
            children: [{
                text: 'ANALYSIS',
                title: 'FUNCTIONAL ANALYSIS OF TIME SERIES',
                iconCls: 'x-fa fa-sliders',
                classColor: 'blue',
                route: 'analysis',
                viewType: 'wrmavisualizationanalysis', 
                rendered: false,
                leaf: true
            },{
                text: 'CROSSTABS',
                title: 'AGGRGATED TIME SERIES DISPLAY',
                iconCls: 'x-fa fa-table',
                classColor: 'blue',
                route: 'crosstabs',
                viewType: 'wrmavisualizationcrosstabs',
                rendered: false,
                leaf: true
            }]
        }, {
            text: 'HELP',
            title: 'HELP',
            iconCls: 'x-fa fa-question',
            classColor: 'plain',
            route: 'help',
            viewType: 'panel',
            rendered: false,
            leaf: true
        }, {
            text: 'ABOUT',
            title: 'ABOUT',
            iconCls: 'x-fa fa-info',
            classColor: 'plain',
            rowCls: 'nav-tree-badge nav-tree-badge-hot',
            route: 'about',
            viewType: 'panel',
            rendered: false,
            leaf: true
        }]
    }
});
