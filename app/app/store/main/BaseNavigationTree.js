Ext.define('majisys_dds.store.main.BaseNavigationTree', {
  extend: 'Ext.data.TreeStore',

  storeId: 'BaseNavigationTree',

  fields: [{
    name: 'text'
  }],

  root: {
    expanded: true,
    children: [{
      text: 'HOME',
      //title: 'HOME',
      title: '&nbsp;&nbsp;Public View - AGGRGATED TIME SERIES DISPLAY&nbsp;&nbsp;',
      iconCls: 'x-fa fa-home',
      rowCls: 'nav-tree-badge nav-tree-badge-new',
      route: 'home',
      viewType: 'crosstabspublic',
      //viewType: 'panel',
      rendered: false,
      leaf: true
    }, {
      text: 'ABOUT',
      title: 'ABOUT',
      iconCls: 'x-fa fa-info',
      rowCls: 'nav-tree-badge nav-tree-badge-hot',
      route: 'about',
      viewType: 'panel',
      rendered: false,
      leaf: true
    }]
  }
});
