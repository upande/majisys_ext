Ext.define('majisys_dds.store.main.WRUANavigationTree', {
    extend: 'Ext.data.TreeStore',

    storeId: 'WRUANavigationTree',

    root: {
        expanded: true,
        children: [{
            text: 'HOME',
            title: 'HOME',
            iconCls: 'x-fa fa-home',
            classColor: 'blue',
            route: 'home',
            viewType: 'wruahome',
            rendered: false,
            leaf: true
        },{
            text: 'TIME SERIES',
            title: 'TIME SERIES MANAGEMENT',
            iconCls: 'x-fa fa-line-chart',
            classColor: 'blue',
            route: 'timeseries',
            viewType: 'timeserieskeys',
            rendered: false,
            leaf: true
        },{
            text: 'VISUALIZATION',
            title: 'VISUALIZATION',
            iconCls: 'x-fa fa-desktop',
            classColor: 'plain',
            route: null,
            viewType: 'panel',
            extended: false,
            children: [{
                text: 'CROSSTABS',
                title: '&nbsp;&nbsp;AGGRGATED TIME SERIES DISPLAY&nbsp;&nbsp;',
                iconCls: 'x-fa fa-table',
                classColor: 'blue',
                route: 'crosstabs',
                viewType: 'crosstabswrua',
                rendered: false,
                leaf: true
            }]
        }, {
            text: 'HELP',
            title: 'HELP',
            iconCls: 'x-fa fa-question',
            classColor: 'plain',
            route: 'help',
            viewType: 'panel',
            rendered: false,
            leaf: true
        }, {
            text: 'ABOUT',
            title: 'ABOUT',
            iconCls: 'x-fa fa-info',
            classColor: 'plain',
            rowCls: 'nav-tree-badge nav-tree-badge-hot',
            route: 'about',
            viewType: 'panel',
            rendered: false,
            leaf: true
        }]
    }
});
