Ext.define('majisys_dds.store.wrma.basedata.Stations', {
    extend: 'Ext.data.Store',
   
    fields: [
        {name: 'locationkey', type: 'integer'},
        {name: 'stationid', type: 'string'},
        {name: 'stationname', type: 'string'},
        {name: 'x', type: 'float', allowNull: true},
        {name: 'y', type: 'float', allowNull: true},
        {name: 'z', type: 'float', allowNull: true},
        {name: 'srid', type: 'integer'}
    ],
   
    proxy: {
        type: 'memory'
    },
    
    /* sorters: {
        property: 'stationid',
        direction: 'ASC'    
    }, */
    
    alias: 'store.wrmastationseditor',
    storeId: 'wrma_stations_editor'
}); 
