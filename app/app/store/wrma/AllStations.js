Ext.define('majisys_dds.store.wrma.AllStations', {
    extend: 'Ext.data.Store',

    alias: 'store.wrmaallstations',
    storeId: 'wrma_all_stations',

    fields: [
        'locationid',
        'stationname'
    ],

    proxy: {
      type:             'rest',
      noCache:          false,
      limitParam:       '',
      startParam:       '',
      pageParam:        '',
      filterParam:      '',
      url:              'api/wrma/custom/naivasha/allstations',
      reader: {
        type:           'json',
        rootProperty:   'records' 
      }
    },
    
    autoLoad: true
});