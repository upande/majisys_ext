Ext.define('majisys_dds.store.wrma.ParametersTree', {
    extend: 'Ext.data.Store',

    alias: 'store.wrmaparameterstree',
    storeId: 'wrma_parameters_tree',   
    
    fields:[
        {name: 'featureclasskey', type: 'integer'},
        {name: 'featureclassname', type: 'string'},
        {name: 'parametername', type: 'string'},
        {name: 'parameterid', type: 'string'}
    ],
    
    groupField: 'featureclassname',

    proxy: {
        type:        'rest',
        noCache:     false,
        limitParam:  '',
        startParam:  '',
        pageParam:   '',
        filterParam: '',
        url:         'api/wrma/custom/naivasha/parameterstree',
        reader: {
            type:         'json',
            rootProperty: 'records' 
        }      
    },
    
    
    autoLoad: true
});