Ext.define('majisys_dds.store.wrma.SeriesKeys', {
    extend: 'Ext.data.Store',
    
    model: 'majisys_dds.model.wrma.SeriesKey',

    alias: 'store.wrmaserieskeys',
    storeId: 'wrma_series_keys',

    pageSize: 22,
    remoteSort : true,

    proxy: {
        type: 'ajax',
        noCache: false,
        pageParam: '',
        url: 'api/wrma/serieskeys.py',
        reader: {
            type: 'json',
            rootProperty: 'records',
            totalProperty: 'total'
        }
    },
    
    autoLoad: true
});
