Ext.define('majisys_dds.store.wrma.AllParameters', {
    extend: 'Ext.data.Store',

    alias: 'store.wrmaallparameters',
    storeId: 'wrma_all_parameters',

    fields: [
        'parameterid',
        'parametername',
        'displayunit',
        'featureclasskey',
        'groupkey'
    ],

    proxy: {
      type:             'rest',
      noCache:          false,
      limitParam:       '',
      startParam:       '',
      pageParam:        '',
      filterParam:      '',
      url:              'api/wrma/custom/naivasha/allparameters',
      reader: {
        type:           'json',
        rootProperty:   'records' 
      }
    },
    
    autoLoad: true
});