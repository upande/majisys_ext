Ext.define('majisys_dds.store.wrma.FeatureClasses', {
    extend: 'Ext.data.Store',

    alias: 'store.wrmafeatureclasses',
    storeId: 'wrma_feature_classes',
    
    model: 'majisys_dds.model.wrma.FeatureClass',

    proxy: {
      type:             'rest',
      noCache:          false,
      limitParam:       '',
      startParam:       '',
      pageParam:        '',
      filterParam:      '',
      url:              'api/wrma/custom/naivasha/featureclasses',
      reader: {
        type:           'json',
        rootProperty:   'records' 
      }
    },
    
    autoLoad: true
});