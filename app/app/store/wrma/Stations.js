Ext.define('majisys_dds.store.wrma.Stations', {
    extend: 'Ext.data.Store',

    alias: 'store.wrmastations',
    storeId: 'wrma_stations',
    
    requires :[
        'Ext.data.proxy.Rest'
    ],

    fields: [
        'locationid',
        'stationname'
    ],

    proxy: {
      type:             'rest',
      noCache:          false,
      limitParam:       '',
      startParam:       '',
      pageParam:        '',
      filterParam:      '',
      url:              'api/wrma/custom/naivasha/stations',
      reader: {
        type:           'json',
        rootProperty:   'records' 
      }
    },
    
    autoLoad: true,
    listeners:{
      load: function(){   
        this.add({
            'locationid':   'all',
            'stationname':  'All Stations'
        });
      }
    }
});