Ext.define('majisys_dds.store.wrma.Parameters', {
    extend: 'Ext.data.Store',

    alias: 'store.wrmaparameters',
    storeId: 'wrma_parameters',

    fields: [
        'parameterid',
        'parametername'
    ],

    proxy: {
      type:             'rest',
      noCache:          false,
      limitParam:       '',
      startParam:       '',
      pageParam:        '',
      filterParam:      '',
      url:              'api/wrma/custom/naivasha/parameters',
      reader: {
        type:           'json',
        rootProperty:   'records' 
      }
    },
    
    autoLoad: true,
    listeners:{
      load: function(){   
        this.add({
            'parameterid':      'all',
            'parametername':    'All Parameters'
        });
      }
    }
});