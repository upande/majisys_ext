Ext.define('majisys_dds.store.wrma.Functions', {
    extend: 'Ext.data.Store',
    
    requires: [
        'Ext.data.reader.Xml'
    ],    

    alias: 'store.wrmafunctions',
    storeId: 'wrmafunctions',
    
    fields: [
        {name: 'title', mapping: 'ows|Title'},
        {name: 'abstract', mapping: 'ows|Abstract'},
        {name: 'identifier', mapping: 'ows|Identifier'},
        {name: 'implemented', mapping: 'ows|Identifier@implemented', type: 'boolean', defaultValue: false},
        {name: 'fieldsloaded', type: 'boolean', defaultValue: false},
        {name: 'fields', defaultValue: []}
    ],

    proxy: {
      type: 'ajax',
      url: 'api/functions/wps.py',
      reader: {
        type: 'xml',
        record: 'wps|Process',
        idProperty: 'ows|Identifier' 
      }
    },
    
    autoLoad: true
});