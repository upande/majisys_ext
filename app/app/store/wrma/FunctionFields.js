Ext.define('majisys_dds.store.wrma.FunctionFields', {
    extend: 'Ext.data.Store',
    
    requires: [
        'Ext.data.reader.Xml'
    ],    

    alias: 'store.wrmafunctionfields',
    storeId: 'wrma_function_fields',
    
    fields: [
        {name: 'name', mapping: 'ows|Identifier'},
        {name: 'tip', mapping: 'ows|Title'},
        {name: 'type', mapping: 'LiteralData>ows|DataType'}
    ],

    proxy: {
      type: 'ajax',
      url: '',
      reader: {
        type: 'xml',
        record: 'Input',
        idProperty: 'ows|Identifier' 
      }
    },
    
    autoLoad: false
});