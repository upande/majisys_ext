Ext.define('majisys_dds.store.public.Parameters', {
    extend: 'Ext.data.Store',

    alias: 'store.publicparameters',
    autoLoad: true,
    proxy: {
        type: 'ajax',
        //url: '../../../app/sources/crosstabs_public/app/scripts/parameters.py',
        url: '../app/app/scripts/public/parameters.py',
        reader: {
            type: 'json',
            rootProperty: 'params'
        }
    },

    fields: [
        {name: 'parameter_name'},
        {name: 'parameter_id'}
    ]
});
