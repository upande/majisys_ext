Ext.define('majisys_dds.store.public.Observations', {
    extend: 'Ext.data.Store',

    alias: 'store.observations',
    //autoLoad: true,
    proxy: {
        type: 'ajax',
        //url: '../../../app/sources/crosstabs_public/app/scripts/multi_watertab.py',
        url: '../app/app/scripts/public/multi_watertab.py',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    },

    listeners: {
        metachange: function(store, meta){
           	/* Generates grid using new data */
            ctPublicController.generateGrid(store, meta.columns);

            //waterApp.monthCol = meta.columns

            ctPublicController.lookupReference('chartPanel').enable();
            d3.select("#svgChart").remove();

            /* Generates data for line chart */
            var data = store.data
            var columns = meta.columns;
            var linechart_data = ctPublicController.generateSeries(data, columns);

            if( linechart_data != null ){
                ctPublicController.drawChart( linechart_data );
            }
        }   
    }
});
