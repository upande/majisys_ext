/* Stations store */
Ext.define('majisys_dds.store.public.Stations', {
	extend: 'Ext.data.Store',
	
	alias: 'store.publicstations',
	model: 'majisys_dds.model.StationModel',
	//autoLoad: true,

	proxy: {
		type: 'ajax',
        //url: '../../../app/sources/crosstabs_public/app/scripts/stations.py',
		url: '../app/app/scripts/public/stations.py',
		reader: {
			keepRawData: true,
			type: 'json',
			rootProperty: 'features',
			successProperty: 'type'
		}
	}
});