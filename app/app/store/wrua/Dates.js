Ext.define('majisys_dds.store.wrua.Dates', {
    extend: 'Ext.data.Store',

    alias: 'store.wrua_dates',
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: '../app/app/scripts/wrua/dates.py',
        reader: {
            type: 'json',
            rootProperty: 'datetimefield'
        }
    },

    fields: [
        { name: 'date_id' },
        { name: 'date_name' }
    ]
});
