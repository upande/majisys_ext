Ext.define('majisys_dds.store.wrua.timeseries.AllParameters', {
    extend: 'Ext.data.Store',

    alias: 'store.wrua_allparameters_store',
    storeId: 'all_parameters',

    fields: [
        'parameterid',
        'parametername',
        'displayunit',
        'featureclasskey',
        'groupkey'
    ],

    proxy: {
      type:             'rest',
      noCache:          false,
      limitParam:       '',
      startParam:       '',
      pageParam:        '',
      filterParam:      '',
      url:              'api/wrma/custom/naivasha/allparameters',
      reader: {
        type:           'json',
        rootProperty:   'records' 
      }
    },
    
    autoLoad: true
});