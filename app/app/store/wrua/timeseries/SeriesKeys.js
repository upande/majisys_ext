Ext.define('majisys_dds.store.wrua.timeseries.SeriesKeys', {
    extend: 'Ext.data.Store',
    
    model: 'majisys_dds.model.SeriesKey',

    alias: 'store.wrua_serieskeys_store',
    storeId: 'series_keys',

    pageSize: 25,
    remoteSort : true,

    proxy: {
        type: 'ajax',
        noCache: false,
        pageParam: '',
        url: 'api/serieskeys.py',
        reader: {
            type: 'json',
            rootProperty: 'records',
            totalProperty: 'total'
        }
    },
    autoLoad: false
});
