Ext.define('majisys_dds.store.wrua.timeseries.Parameters', {
    extend: 'Ext.data.Store',

    alias: 'store.wrua_parameters_store',
    storeId: 'parameters',

    fields: [
        'parameterid',
        'parametername'
    ],

    proxy: {
      type:             'rest',
      noCache:          false,
      limitParam:       '',
      startParam:       '',
      pageParam:        '',
      filterParam:      '',
      url:              'api/custom/naivasha/parameters',
      reader: {
        type:           'json',
        rootProperty:   'records' 
      }
    },
    
    //autoLoad: true,
    listeners:{
      load: function(){   
        this.add({
            'parameterid':      'all',
            'parametername':    'All Parameters'
        });
      }
    }
});