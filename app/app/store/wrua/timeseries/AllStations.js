Ext.define('majisys_dds.store.wrua.timeseries.AllStations', {
    extend: 'Ext.data.Store',

    alias: 'store.wrua_allstations_store',
    storeId: 'all_stations',

    fields: [
        'locationid',
        'stationname'
    ],

    proxy: {
      type:             'rest',
      noCache:          false,
      limitParam:       '',
      startParam:       '',
      pageParam:        '',
      filterParam:      '',
      url:              'app/api/custom/naivasha/allstations',
      reader: {
        type:           'json',
        rootProperty:   'records' 
      }
    },
    
    autoLoad: true
});