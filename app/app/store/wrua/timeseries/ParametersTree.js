Ext.define('majisys_dds.store.wrua.timeseries.ParametersTree', {
    extend: 'Ext.data.Store',

    alias: 'store.wrua_parameterstree_store',
    storeId: 'parameters_tree',   
    
    fields:[
        {name: 'featureclasskey', type: 'integer'},
        {name: 'featureclassname', type: 'string'},
        {name: 'parametername', type: 'string'},
        {name: 'parameterid', type: 'string'}
    ],
    
    groupField: 'featureclassname',

    proxy: {
        type:        'rest',
        noCache:     false,
        limitParam:  '',
        startParam:  '',
        pageParam:   '',
        filterParam: '',
        url:         'api/custom/naivasha/parameterstree',
        reader: {
            type:         'json',
            rootProperty: 'records' 
        }      
    },
    
    
    autoLoad: true
});