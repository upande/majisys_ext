Ext.define('majisys_dds.store.wrua.timeseries.Stations', {
    extend: 'Ext.data.Store',

    alias: 'store.wrua_stations_store',
    storeId: 'stations',
    
    requires :[
        'Ext.data.proxy.Rest'
    ],

    fields: [
        'locationid',
        'stationname'
    ],

    proxy: {
      type:             'rest',
      noCache:          false,
      limitParam:       '',
      startParam:       '',
      pageParam:        '',
      filterParam:      '',
      url:              'api/custom/naivasha/stations',
      reader: {
        type:           'json',
        rootProperty:   'records' 
      }
    },
    
    //autoLoad: true,
    listeners:{
      load: function(){   
        this.add({
            'locationid':   'all',
            'stationname':  'All Stations'
        });
      }
    }
});