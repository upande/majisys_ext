Ext.define('majisys_dds.store.wrua.Observations', {
	extend: 'Ext.data.Store',
	alias: 'store.wrua_observations',
	proxy: {
		type: 'ajax',
		url: '../app/app/scripts/wrua/multi_watertab.py',
		reader: {
			type: 'json',
			rootProperty: 'data'
		}
	},

	listeners: {
		metachange: function(store, meta){
			/* Generates grid using new data */
            ctWruaController.generateGrid(store, meta.columns);

            ctWruaController.monthCol = meta.columns;
            Ext.getCmp('chartPanel').enable();
            d3.select("#svgChart").remove();

            /* Generates data for line chart */
            var data = store.data
            
            var columns = meta.columns;
            var linechart_data = ctWruaController.generateSeries(data, columns);
            if( linechart_data != null ){
                ctWruaController.drawChart( linechart_data );
            }
		},

		load: function(store, records, success, operation){
			if( records.length > 0 ){
				if( records[0].data['error'] !== undefined ){
					alert( "There was an error loading Observations data" );
				}
			}
		}
	}
});