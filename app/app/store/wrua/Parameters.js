Ext.define('majisys_dds.store.wrua.Parameters', {
    extend: 'Ext.data.Store',

    alias: 'store.wrua_parameters',
    //autoLoad: true,
    proxy: {
        type: 'ajax',
        url: '../app/app/scripts/wrua/parameters.py',
        reader: {
            type: 'json',
            rootProperty: 'params'
        }
    },

    fields: [
        { name: 'parameter_name' },
        { name: 'parameter_id' }
    ],

    listeners: {
        load: function( store, records, success, operation ){
            if( records.length > 0 ){
                if( records[0].data['error'] !== undefined ){
                    alert( "There was an error loading Parameters data" );
                }
            }
        }
    }
});
