/* Stations store */
Ext.define('majisys_dds.store.wrua.Stations', {
	extend: 'Ext.data.Store',
	
	alias: 'store.wrua_stations',
	model: 'majisys_dds.model.StationModel',
	
	proxy: {
		type: 'ajax',
        url: '../app/app/scripts/wrua/stations.py',
		reader: {
			keepRawData: true,
			type: 'json',
			rootProperty: 'features',
			successProperty: 'type'
		}
	},

	listeners: {
        load: function( store, records, success, operation ){
        	if( records.length > 0 ){
				if( records[0].data['error'] !== undefined ){
					alert( "There was an error loading Stations data" );
				}
			}
        }
    }
});