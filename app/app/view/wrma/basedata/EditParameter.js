Ext.define('majisys_dds.view.wrma.basedata.EditParameter', {
    extend: 'Ext.window.Window',
    alias: 'widget.wrmabasedataeditparameter',  
    
    autoShow:   true,
    constrain:  true,
    resizable:  false,
    
    id: 'bd_edit_parameter_window',
    parentContainer: 'basedata_cnt',
    
    title: 'Parameter Details',

    header: {
        cls: 'blue-header'
    },    
    layout: 'fit',
    width: 450,
    height: 320,    
    bodyPadding: 10,
    
    closeAction: 'destroy',
    
    items: [{
        xtype: 'form',
        padding: '0 5',        
        defaults: {
            anchor: '100%',
            allowBlank: false,
            labelWidth: 110,
            labelAlign: 'right',
            msgTarget: 'side',
            labelStyle: 'font-weight:500;'
        },
        
        items: [{
            name:       'parametername',
            id:         'bdpf-parameter_name',
            xtype:      'textfield',
            fieldLabel: 'Name:',
            emptyText:  'Descriptive name',
            allowBlank: true
        },{
            name:       'parameterid',
            id:         'bdpf-parameter_id',
            xtype:      'textfield',
            fieldLabel: 'ID:',
            emptyText:  'Parameter Identifier'
        },{
            name:       'featureclasskey',
            id:         'bdpf-featureclass',
            xtype:      'textfield',
            fieldLabel: 'Feature Class:',
            emptyText:  'Select the appropriate feature class'
        },{
            name:       'groupkey',
            id:         'bdpf-group',
            xtype:      'textfield',
            fieldLabel: 'Parameter Group:',
            emptyText:  'Select the measurement units'
        },{
            name:       'displayunit',
            id:         'bdpf-units',
            xtype:      'textfield',
            fieldLabel: 'Units:'
        }]
    }],
    
    dockedItems : [{
        xtype:        'toolbar',
        dock:         'bottom',
        style: {
            background: '#e0ebf3'
        },        
        layout: {
            pack: 'center'
        },
        items: [{
            xtype:      'button',
            id:         'bdpf-save_parameter',
            tooltip:    'Save',
            iconCls:    '',
            text:       'Save',
            width:      80
        },' ',{
            xtype:      'button',
            id:         'bdpf-close_button',
            tooltip:    'Close this window',
            text:       'Close',
            width:      80,
            handler:    function(){
                this.up('wrmabasedataeditparameter').close();
            }
        }]
    }],

    listeners: {
        'show': function(){
            Ext.ComponentQuery.query('#basedata_cnt')[0].mask();
        },
        'hide': function(){
            Ext.ComponentQuery.query('#basedata_cnt')[0].unmask();
        }
    }    
});