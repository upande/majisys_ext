Ext.define('majisys_dds.view.wrma.basedata.DashboardController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.wrma-basedata-dashboard',

    requires: [
        'GeoExt.component.Map',
        'GeoExt.data.store.Features',

        'majisys_dds.view.wrma.basedata.EditStations',
        'majisys_dds.view.wrma.basedata.EditParameter'
    ],

    navMap: null,
    stationsLayer: null,
    selectedStations: new ol.Collection(),

    defaultStyle: new ol.style.Style({
        image: new ol.style.Circle({
            radius: 6,
            fill: new ol.style.Fill({
                color: 'rgba(0, 179, 60, 0.2)'
            }),
            stroke: new ol.style.Stroke({
                color: '#00b33c',
                width: 2
            })
        })
    }),

    hiddenStyle: new ol.style.Style({
        image: new ol.style.Circle({
            radius: 6,
            fill: new ol.style.Fill({
                color: 'rgba(0, 0, 0, 0)'
            }),
            stroke: new ol.style.Stroke({
                color: 'rgba(0, 0, 0, 0)',
                width: 2
            })
        })
    }),

    /* --- */
    selectedStyle: function(feature, resolution){
        return new ol.style.Style({
            image: new ol.style.Circle({
                radius: 6,
                fill: new ol.style.Fill({
                    color: 'rgba(255, 102, 0, 0.2)'
                }),
                stroke: new ol.style.Stroke({
                    color: '#ff6600',
                    width: 2
                })
            }),
            text: new ol.style.Text({
                textAlign: 'center',
                textBaseline: 'middle',
                font: 'Normal 16px Arial',
                text: feature.get('stationid'),
                fill: new ol.style.Fill({color: '#ff6600'}),
                stroke: new ol.style.Stroke({color: '#fff', width: 3}),
                offsetX: 0,
                offsetY: -16,
                rotation: 0
            })
        });
    },

    /* --- */
    hoverStyle: function() {
        return function(feature, resolution){
            return new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 6,
                    fill: new ol.style.Fill({
                        color: 'rgba(0, 0, 204, 0.2)'
                    }),
                    stroke: new ol.style.Stroke({
                        color: '#0000cc',
                        width: 2
                    })
                }),
                text: new ol.style.Text({
                    textAlign: 'center',
                    textBaseline: 'middle',
                    font: 'Normal 16px Arial',
                    text: feature.get('stationid'),
                    fill: new ol.style.Fill({color: '#0000cc'}),
                    stroke: new ol.style.Stroke({color: '#fff', width: 3}),
                    offsetX: 0,
                    offsetY: -16,
                    rotation: 0
                })
            });
        };
    },

    /* --- */
    zoomToAllStations: function(){
        this.navMap.getView().fit(this.stationsLayer.getSource().getExtent(), this.navMap.getSize());
    },

    /* --- */
    zoomToSelectedStations: function(){
        var features = this.selectedStations;
        if (features.getLength() > 0){
            var bbox = [null,null,null,null];
            features.forEach(function(item){
                featExtent = item.getGeometry().getExtent();
                bbox[0] = (bbox[0]) ? Math.min(bbox[0],featExtent[0]) : featExtent[0];
                bbox[1] = (bbox[1]) ? Math.min(bbox[1],featExtent[1]) : featExtent[1];
                bbox[2] = (bbox[2]) ? Math.max(bbox[2],featExtent[2]) : featExtent[2];
                bbox[3] = (bbox[3]) ? Math.max(bbox[3],featExtent[3]) : featExtent[3];
            });
            this.navMap.getView().fit(bbox, this.navMap.getSize());
        }
    },

    /* --- */
    clearStationSelection: function(){
        var gridEl = this.getReferences().StationsGrid;
        gridEl.getSelectionModel().deselectAll();
    },

    /* --- */
    filterStations: function(filterValue, oldValue){
        var stStore = Ext.getStore('stations_geojson');
        var me = this;
        if (filterValue.length < oldValue.length && filterValue != '')
            stStore.clearFilter();
        if (!filterValue){
            stStore.clearFilter();
            stStore.each(function(record){
                record.getFeature().setStyle(me.defaultStyle);
            });

        } else {
            var visible;
            Ext.getStore('stations_geojson').filterBy(function(record){
                visible = record.data.stationid.toLowerCase().startsWith(filterValue.toLowerCase());
                if (visible){
                    if (record.data.active)
                        record.getFeature().setStyle(me.selectedStyle(record.getFeature()));
                    else
                        record.getFeature().setStyle(me.defaultStyle);
                } else {
                    if (record.data.active)
                        record.set('active',false);
                    record.getFeature().setStyle(me.hiddenStyle);
                }
                return visible;
            });
        }
    },

    /* --- */
    getSatations: function(){
        me = this;
        Ext.Ajax.request({
            url: 'api/wrma/stationsgeojson.py',
            disableCaching: false,
            success: function(response){
                var data = response.responseText;
                data = data.replace(/\[inf,/g,'[null,');
                data = data.replace(/,inf\]/g,',null]');
                me.prepareStationsPanel(eval(data)[0]);
            },
            failure: function(response){
                console.log('error: '+response.status);
            }
        });
    },

    /* --- */
    prepareStationsPanel: function(stationsGeoJson){
        var me = this,
            stGrid = this.getReferences().StationsGrid,
            mapPanel = this.getReferences().NavMapPanel,
            mapBbox = majisys_dds.app.getApplication().bounds;

        this.stationsLayer = new ol.layer.Vector({
            source: new ol.source.Vector({
                features: (new ol.format.GeoJSON()).readFeatures(stationsGeoJson, {
                    dataProjection: 'EPSG:4326',
                    featureProjection: 'EPSG:3857'
                })
            }),
            style: this.defaultStyle
        });

        this.navMap = new ol.Map({
            interactions : ol.interaction.defaults({
                doubleClickZoom: true,
                select: false
            }),
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM(),
                    name: 'OSM'
                }),
                this.stationsLayer
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([0,0]),
                zoom: 10,
                minZoom: 2,
                maxZoom: 18
            }),
            controls: ol.control.defaults().extend([
                new ol.control.ScaleLine()
            ])         
        });

        this.navMap.addControl(new ol.control.MousePosition({
            projection: 'EPSG:4326',
            coordinateFormat: ol.coordinate.createStringXY(6),
            target: (Ext.ComponentQuery.query('#bd-coordinates')[0]).getEl().dom
        }));

        mapPanel.add({
            xtype: 'gx_map',
            map: this.navMap
        });
        this.navMap.getView().fit(ol.proj.transformExtent(mapBbox, 'EPSG:4326', 'EPSG:3857'), this.navMap.getSize());

        var stationsGeoJsonStore = Ext.create('GeoExt.data.store.Features', {
            id: 'stations_geojson',
            layer: this.stationsLayer,
            map: this.navMap,
            sorters: [{
                property: 'stationid',
                direction: 'ASC'
            }],
            proxy: {
                type: 'ajax',
                api: {
                    create: 'api/wrma/updatestations.py',
                    update: 'api/wrma/updatestations.py',
                    destroy: 'api/wrma/deletestations.py'
                },
                writer: {
                    type: 'json',
                    allowSingle: false,
                    clientIdProperty: 'locationkey'
                },
                reader: {
                    keepRawData : true
                }
            }
        });

        stationsGeoJsonStore.getModel().addFields({
            name: 'active',
            type: 'boolean',
            defaultValue: false,
            persist: false
        });
        stGrid.setStore(stationsGeoJsonStore);
        stationsGeoJsonStore.each(function(record){
            record.set('id',record.data.locationkey);
        });
        stationsGeoJsonStore.commitChanges();

        this.navMap.on('singleclick',
            function(evt){
                me.navMap.forEachFeatureAtPixel(evt.pixel, function(feature) {
                    var record,
                        selLen = me.selectedStations.getLength();
                    me.selectedStations.remove(feature);
                    record = stGrid.getStore().findRecord('stationid',feature.getProperties().stationid);
                    if (selLen == me.selectedStations.getLength()) {
                        feature.setStyle(me.selectedStyle(feature));
                        me.selectedStations.push(feature);
                        stGrid.getSelectionModel().select(record,true,true);
                        feature.set('active',true);
                    } else {
                        feature.setStyle(null);
                        stGrid.getSelectionModel().deselect(record,true);
                        feature.set('active',false);
                    }
                },
                null,
                function(selectionLayer){
                    return selectionLayer == me.stationsLayer;
                });
            }
        );

        hoverInteraction = new ol.interaction.Select({
            layers: [this.stationsLayer],
            condition: ol.events.condition.pointerMove,
            style: this.hoverStyle()
        });
        this.navMap.addInteraction(hoverInteraction);

        stGrid.on({
            'select': function(grid,record){
                var feature = record.getFeature();
                feature.setStyle(me.selectedStyle(feature));
                me.selectedStations.push(feature);
                feature.set('active',true);
            },
            'deselect': function(grid,record){
                var feature = record.getFeature();
                feature.setStyle(null);
                me.selectedStations.remove(feature);
                feature.set('active',false);
            }
        });
    },

    /* --- */
    onFeatureClassSelect: function(fcChooser){
        var gridEl = this.getReferences().ParametersGrid;
        if (fcChooser.getValue() == 9999) {
            gridEl.getStore().clearFilter();
        } else {
            gridEl.getStore().filter('featureclasskey',fcChooser.getValue());
        }
    },

    /* --- */
    onParameterRowEnter: function(view, record){
        var refs = this.getReferences(),
            panelEl = refs.BasedataFeatureClasses,
            gridEl = panelEl.down('gridpanel');

        var fcRecord = gridEl.getView().getRow(gridEl.getStore().getById(record.data.featureclasskey));
        fcRecord.style["background-color"] = '#e6eaee';
    },

    /* --- */
    onParameterRowLeave: function(view, record){
        var refs = this.getReferences(),
            panelEl = refs.BasedataFeatureClasses,
            gridEl = panelEl.down('gridpanel');

        var fcRecord = gridEl.getView().getRow(gridEl.getStore().getById(record.data.featureclasskey));
        fcRecord.style["background-color"] = '';
    },

    /* --- */
    stationEditor: function(btn){
        Ext.create({
            xtype: 'wrmabasedataeditstations',
            constrainTo: Ext.getCmp('content_panel').getEl()
        });

        var refs = this.getReferences(),
            stGrid = refs.StationsGrid,
            stationsEdStore = Ext.getStore('wrma_stations_editor');

        Ext.getCmp('bd-stattions_filter').setValue();
        if (btn.action == 'edit'){
            stGrid.getSelection().forEach(function(item){
                stationsEdStore.add(item.copy());
            });
            stationsEdStore.sort('stationid','ASC');
            stationsEdStore.sorters.clear();
            Ext.getCmp('bdse-add_station').setDisabled(true);
        } else {
            this.addStation();
        }
    },

    /* --- */
    updateStations: function(){
        var me = this,
            refs = this.getReferences(),
            stGridStore = refs.StationsGrid.getStore(),
            records = Ext.getStore('wrma_stations_editor').getModifiedRecords();

        stGridStore.rejectChanges();
        if (records.length > 0){
            var station, recNum = -1;
            records.forEach(function(record){
                if (record.data.locationkey == -1){
                    recNum++;
                    feature = new ol.Feature();
                    stGridStore.getLayer().getSource().addFeature(feature);
                    station = stGridStore.getNewRecords()[recNum];
                    station.set({
                        'locationkey': -1
                    });
                } else {
                    station = stGridStore.findRecord('locationkey',record.data.locationkey);
                }
                station.set({
                    'stationid': record.data.stationid,
                    'stationname': record.data.stationname,
                    'x': record.data.x,
                    'y': record.data.y,
                    'z': record.data.z,
                    'srid': record.data.srid
                });
            });
            stGridStore.sync({
                success: function(batch, syncObject) {
                    me.onStationsSyncSuccess(syncObject, this.getReader().rawData);
                },
                failure: function(){
                    var response = this.getReader().rawData;
                    stGridStore.rejectChanges();
                    Ext.MessageBox.show({
                        title: 'Server Response',
                        header: { cls: 'blue-header' },
                        msg: response.messages[0],
                        icon: Ext.MessageBox.ERROR,
                        width: 400,
                        buttons: Ext.MessageBox.OK
                    })
                }

            });
        }
    },

    /* --- */
    addStation: function(){
        var stationsEdStore = Ext.getStore('wrma_stations_editor'),
            edGrid = Ext.ComponentQuery.query('#bd-edit_station_grid')[0];

        stationsEdStore.insert(0,{
            locationkey: -1,
            stationid: null,
            stationname: null,
            srid: null,
            x:    null,
            y:    null,
            z: null
        });
        edGrid.getPlugin('station_editor_plugin').startEditByPosition({
            row: 0,
            column: 0
        });
    },

    /* --- */
    deleteStations: function(){
        var me = this,
            refs = this.getReferences(),
            stGrid = refs.StationsGrid,
            stGridStore = stGrid.getStore(),
            layerSource = stGridStore.getLayer().getSource(),
            message, first, list, conn;

        first = true;
        list = '';
        stGrid.getSelection().forEach(function(record){
            conn = (first) ? '' : ', ';
            list += conn + record.data.stationid;
            first = false;
        });
        message = 'Are you sure, you want to delete selected stations ' + list + '?';
        Ext.Msg.confirm('Delete Stations', message, function(btn){
            if (btn == 'yes'){
                Ext.getCmp('bd-stattions_filter').setValue();
                stGridStore.remove(stGrid.getSelection());
                stGridStore.sync({
                    success: function(batch, syncObject){
                        list = '';
                        first = true;
                        syncObject.operations.destroy.forEach(function(record){
                            conn = (first) ? '' : ', ';
                            list += conn + record.data.stationid;
                            first = false;
                            me.selectedStations.remove(record.getFeature());
                            layerSource.removeFeature(record.getFeature());
                        });
                        var response = this.getReader().rawData;
                        Ext.MessageBox.show({
                            title: 'Server Response',
                            header: { cls: 'blue-header' },
                            msg: response.messages[0].replace('.',' ') + '(' + list + ').',
                            icon: Ext.MessageBox.OK,
                            width: 400,
                            buttons: Ext.MessageBox.OK
                        });
                        stGridStore.commitChanges();
                    },
                    failure: function(){
                        var response = this.getReader().rawData;
                        stGridStore.rejectChanges();
                        Ext.MessageBox.show({
                            title: 'Server Response',
                            header: { cls: 'blue-header' },
                            msg: response.messages[0],
                            icon: Ext.MessageBox.ERROR,
                            width: 400,
                            buttons: Ext.MessageBox.OK
                        })
                    }
                });

            }
        });
    },

    /* --- */
    onStationsSyncSuccess: function(syncObject, response){
        var me = this,
            refs = this.getReferences(),
            stGrid = refs.StationsGrid,
            stGridStore = stGrid.getStore(),
            keys, create, feature,
            responseRecords = response.messages;

        keys = Object.keys(syncObject.operations);
        create = (keys.indexOf('create') != -1) ? true : false;

        responseRecords.forEach(function(item){
            record = stGridStore.findRecord('id',item.id);
            feature = record.getFeature();
            feature.setGeometry(
                new ol.geom.Point([item.x, item.y]).transform('EPSG:4326','EPSG:3857')
            );
            feature.setStyle(me.selectedStyle(feature));
            if (create){
                feature.set('id',item.locationkey);
                feature.set('locationkey',item.locationkey);
                feature.set('active',true);
                stGrid.getSelectionModel().select(record,true,true);
                me.selectedStations.push(feature);
            }
        });
        stGridStore.commitChanges();
        Ext.getCmp('bd-edit_station_window').close();
    },

    /* --- */
    featureclassEditor: function(){
        /* console.log('edit FC'); */
    },

    /* --- */
    parameterEditor: function(btn){
        var refs = this.getReferences(),
            paramGrid = refs.ParametersGrid,
            paramForm, record;

        if (btn.action == 'edit'){

            record = paramGrid.getSelection()[0],

            /* console.log(record); */
            paramForm = Ext.create({
                xtype: 'wrmabasedataeditparameter',
                constrainTo: Ext.getCmp('content_panel').getEl()
            }).down('form');
            paramForm.loadRecord(record);
        }
    }
});