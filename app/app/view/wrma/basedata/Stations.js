Ext.define('majisys_dds.view.wrma.basedata.Stations',{
    extend: 'Ext.panel.Panel',
    xtype: 'wrmabasedatastations',
    
    requires: [
        'Ext.layout.container.Border'
    ],

    title: 'Stations',
    header: {
        cls: 'blue-header',
        items: [{
            itemId: 'bd-coordinates',
            xtype: 'label',
            width: 300,
            text: ''
        }]
    },

    height: 600,
    layout: 'border',
    
    items: [{
        xtype: 'gridpanel',
        reference: 'StationsGrid',
        id: 'bd-stations_grid',
        region: 'west',
        width: 350,
        layout: 'fit',
        header: false,
        columnLines: true,
        enableColumnHide: false,
        selModel: {
            selType: 'checkboxmodel',
            headerText: '&#10004;',
            showHeaderCheckbox: false
        },
        scroll: 'y',
        columns: [{
            xtype: 'gridcolumn',
            dataIndex: 'stationid',
            text: 'ID',
            align: 'left',
            width: 100
        },{
            xtype: 'gridcolumn',
            dataIndex: 'stationname',
            text: 'Name',
            align: 'left',
            flex: 1
        }],
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            cls: 'toolbar-style',
            items: [{
                xtype: 'textfield',
                id: 'bd-stattions_filter',
                flex: 1,
                fieldLabel: 'Filter',
                anchor: '100%',
                hideLabel: true,
                emptyText: 'Filter text...',
                listeners: {
                    'change': function(fieldEl, newValue, oldValue){
                        fieldEl.up('wrmabasedatadashboard').getController().filterStations(newValue, oldValue);
                    }
                }
            }]
        },{
            xtype: 'toolbar',
            dock: 'bottom',
            cls: 'toolbar-style',
            padding: '8 5 5 5',
            items:[{
                xtype: 'button',
                width: 40,
                border: false,
                tooltip: 'Unselect all stations',
                cls: 'blue-button',
                iconCls: 'x-fa fa-square-o blue-icon',
                margin:'0 0 0 10',
                handler: 'clearStationSelection'
            },{
                xtype: 'button',
                width: 40,
                border: false,
                cls: 'blue-button',
                tooltip: 'Sort on selected',
                iconCls: 'x-fa fa-sort blue-icon',
                margin: '0 0 0 10',
                enableToggle: true,
                toggleHandler: function(btn,toggled){
                    if (toggled)
                        Ext.getStore('stations_geojson').sort('active','DESC');
                    else
                        Ext.getStore('stations_geojson').sort('stationname','ASC');
                }
            },'->',{
                xtype: 'button',
                id: 'bd-add_station',
                action: 'add',
                width: 40,
                border: false,
                tooltip: 'Add new stations',
                cls: 'blue-button',
                iconCls: 'x-fa fa-plus blue-icon',
                margin:'0 10 0 0',
                handler: 'stationEditor'                
            },{
                xtype: 'button',
                id: 'bd-edit_station',
                action: 'edit',
                width: 40,
                border: false,
                tooltip: 'Edit selected station(s) or add new stations',
                cls: 'blue-button',
                iconCls: 'x-fa fa-pencil blue-icon',
                margin:'0 10 0 0',
                handler: 'stationEditor'
            },{
                xtype: 'button',
                width: 40,
                border: false,
                tooltip: 'Remove selected stations from the database',
                cls: 'blue-button',
                iconCls: 'x-fa fa-trash blue-icon',
                margin:'0 10 0 0',
                handler: 'deleteStations'
            }]
        }]
    },{
        region: 'center',
        xtype: 'panel',
        layout: 'fit',
        height: '100%',
        reference: 'NavMapPanel',
        padding: '2 0 2 2',
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'right',
            cls: 'toolbar-style',
            padding: '6 6 6 6',
            margin: '0 0 0 2',
            items: [{
                xtype: 'button',
                width: 40,
                border: false,
                cls: 'blue-button',
                tooltip: 'Unselect all stations',
                iconCls: 'x-fa fa-square-o blue-icon',
                margin: '0 0 10 0',
                handler: 'clearStationSelection'
            },'->',{
                xtype: 'button',
                width: 40,
                border: false,
                cls: 'blue-button',
                tooltip: 'Zoom to all stations',
                iconCls: 'x-fa fa-arrows-alt blue-icon',
                margin: '10 0 0 0',
                handler: 'zoomToAllStations'
            },{
                xtype: 'button',
                width: 40,
                border: false,
                cls: 'blue-button',
                tooltip: 'Zoom to selected stations',
                iconCls: 'x-fa fa-search blue-icon',
                margin: '10 0 0 0',
                handler: 'zoomToSelectedStations'

            }]
        }]
    }],

    listeners: {
        'afterrender': 'getSatations'
    }
});