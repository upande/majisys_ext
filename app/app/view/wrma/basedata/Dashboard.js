Ext.define('majisys_dds.view.wrma.basedata.Dashboard',{
    extend: 'Ext.panel.Panel',
    xtype: 'wrmabasedatadashboard',

    requires: [
        'Ext.ux.layout.ResponsiveColumn',
        
        'majisys_dds.view.wrma.basedata.DashboardController',
        'majisys_dds.view.wrma.basedata.Stations',
        'majisys_dds.view.wrma.basedata.Parameters',
        'majisys_dds.view.wrma.basedata.FeatureClasses'
    ],
    controller: 'wrma-basedata-dashboard',
    
    id: 'basedata_dashboard',

    layout: 'responsivecolumn',
    
    scrollable: true,
    
    items: [{
        xtype: 'wrmabasedatastations',
        userCls: 'box-100'
    },{
        xtype: 'wrmabasedatafeatureclasses',
        userCls: 'box-40'
    },{
        xtype: 'wrmabasedataparameters',
        userCls: 'box-60'
    }]
});