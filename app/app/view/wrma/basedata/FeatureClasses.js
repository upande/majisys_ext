Ext.define('majisys_dds.view.wrma.basedata.FeatureClasses',{
    extend: 'Ext.panel.Panel',
    xtype: 'wrmabasedatafeatureclasses',
    
    requires: [
        'majisys_dds.store.wrma.FeatureClasses'
    ],    
    
    reference: 'BasedataFeatureClasses',
    
    title: 'Feature Classes',
    header: {
        cls: 'blue-header'
    },
 
    height: 500,
    bodyPadding: 10,

    items: [{
        xtype: 'gridpanel',
        header: false,
        flex: 1,
        reference: 'FeatureClassGrid',
        id: 'bd-featureclass_grid',
        scroll: 'y',
        store: {
            type: 'wrmafeatureclasses'
        },
        columns: [{
            xtype: 'gridcolumn',
            dataIndex: 'featureclassname',
            text: 'Name',
            align: 'left',
            flex: 2
        },{
            xtype: 'gridcolumn',
            dataIndex: 'nrparams',
            text: 'No. Params',
            align: 'left',
            width: 100          
        }],
        listeners: {
            'rowdblclick': 'featureclassEditor'
        }        
    }],
      
    dockedItems: [{
        xtype: 'container',
        layout: 'hbox',
        dock: 'bottom',
        cls: 'toolbar-style',     
        padding: '10 10 10 10',
        items: [{
            xtype: 'textfield',
            flex: 1,
            fieldLabel: 'Add New Parameter',
            hideLabel: true,
            emptyText: 'Add a new Feature Class',
            validator: function (val) {
                var errMsg = 'field can not be empty';
                return (val != null) ? true : false;
            }            
        },{
            xtype: 'button',
            width: 40,
            border: false,
            tooltip: 'Add a feature class',
            cls: 'blue-button',
            iconCls: 'x-fa fa-plus blue-icon',
            margin:'0 0 0 10'
        },{
            xtype: 'button',
            width: 40,
            border: false,
            tooltip: 'Edut Selected',
            cls: 'blue-button',
            iconCls: 'x-fa fa-pencil blue-icon',
            margin:'0 0 0 10'
        }]
    }]    
});