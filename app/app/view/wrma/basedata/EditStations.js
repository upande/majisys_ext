Ext.define('majisys_dds.view.wrma.basedata.EditStations', {
    extend: 'Ext.window.Window',
    alias: 'widget.wrmabasedataeditstations', 
    
    requires: [
        'majisys_dds.store.wrma.basedata.Stations'
    ],
    
    autoShow:   true,
    constrain:  true,
    resizable:  true,
    
    id: 'bd-edit_station_window',
    parentContainer: 'basedata_cnt',
    
    title: 'Station\'s Editor',

    header: {
        cls: 'blue-header'
    },    
    layout: 'fit',
    width: 900,
    height: 360,    
    
    closeAction: 'destroy',
     
    items: [{
        xtype: 'grid',
        id : 'bd-edit_station_grid',
        padding: '0 5',   
        columnLines: true,   
    
        store: {
            type:'wrmastationseditor'
        },
    
        plugins: {
            id: 'station_editor_plugin',
            ptype: 'cellediting',
            completeOnEnter: true,
            clicksToEdit: 1
        },
        
        enableColumnHide: false,
        columns: [{
            text: 'ID',
            dataIndex: 'stationid',
            align: 'left',
            width: 140,
            editor: {
                allowBlank: false
            }           
        },{
            text: 'Name',
            dataIndex: 'stationname',
            align: 'left',
            flex: 1,
            editor: {
                allowBlank: true
            }
        },{
            text: 'X (<i>Longitude</i>)',
            dataIndex: 'x',
            align: 'left',
            width: 120,
            editor: {
                allowBlank: false
            }
        },{
            text: 'Y (<i>Latitude</i>)',
            dataIndex: 'y',
            align: 'left',
            width: 120,
            editor: {
                allowBlank: false
            }
        },{
            text: 'Z',
            dataIndex: 'z',
            /* format:"0.00", */
            align: 'left',
            width: 80,
            editor: {
                allowBlank: true
            },
            renderer: function(value){
                if (value == null)
                    return null;
                else
                    return Ext.util.Format.number(value,'0.00');
            }
        },{
            text: 'SRID',
            dataIndex: 'srid',
            align: 'left',
            width: 70,
            editor: {
                allowBlank: false
            }
        }]
    }],
    
    dockedItems : [{
        xtype:        'toolbar',
        dock:         'bottom',
        style: {
            background: '#e0ebf3'
        },        
        layout: {
            pack: 'center'
        },
        items: [{
            xtype:      'button',
            id:         'bdse-add_station',
            tooltip:    'Add a new station',
            cls:        'blue-button',
            iconCls:    'x-fa fa-plus blue-icon',
            width:      40,
            handler:    function(){
                Ext.ComponentQuery.query('wrmabasedatadashboard')[0].getController().addStation();
            }
        },' ',{
            xtype:      'button',
            tooltip:    'Save changes',
            cls:        'blue-button',
            iconCls:    'x-fa fa-floppy-o blue-icon',
            width:      40,
            handler:    function(){
                Ext.ComponentQuery.query('wrmabasedatadashboard')[0].getController().updateStations();
            }
        },' ',{
            xtype:      'button',
            tooltip:    'Ignore changes and close',
            cls:        'blue-button',
            iconCls:    'x-fa fa-times blue-icon',
            width:      40,
            handler:    function(){
                this.up('wrmabasedataeditstations').close();
            }
        }]
    }],

    listeners: {
        'show': function(){
            Ext.ComponentQuery.query('#basedata_cnt')[0].mask();
        },
        'hide': function(){
            Ext.ComponentQuery.query('#basedata_cnt')[0].unmask();
        }
    }
});