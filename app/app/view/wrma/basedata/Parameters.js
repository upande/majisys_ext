Ext.define('majisys_dds.view.wrma.basedata.Parameters',{
    extend: 'Ext.panel.Panel',
    xtype: 'wrmabasedataparameters',
    
    requires: [
        'majisys_dds.store.wrma.AllParameters',
        'majisys_dds.store.wrma.FeatureClasses'
    ],
    
    reference: 'BasedataParameters',
    
    title: 'Observable Parameters',
    header: {
        cls: 'blue-header'
    },
    
    height: 500,
    bodyPadding: 10,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    
    items: [{
        xtype: 'gridpanel',
        reference: 'ParametersGrid',
        id: 'bd-parameters_grid',
        header: false,
        columnLines: true,
        flex: 1,
        scroll: 'y',
        store: {
            type: 'allparameters'
        },
        columns: [{
            xtype: 'gridcolumn',
            dataIndex: 'parameterid',
            text: 'Id',
            align: 'left',
            width: 110            
        },{
            xtype: 'gridcolumn',
            dataIndex: 'parametername',
            text: 'Name',
            align: 'left',
            flex: 2
        },{
            xtype: 'gridcolumn',
            dataIndex: 'displayunit',
            text: 'Units',
            align: 'left',
            resizable: false,
            width: 120 
        }],

        listeners: {
            'itemmouseenter': 'onParameterRowEnter',
            'itemmouseleave': 'onParameterRowLeave',
            'select': function(){
                Ext.getCmp('bd-delete_parameter_btn').enable();
                Ext.getCmp('bd-edit_parameter_btn').enable();
            }
        }
    }],

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'bottom',
        cls: 'toolbar-style',
        padding: '10 10 10 10',
        items: [{
            xtype:      'tbtext',
            text:       'Filter by Feature Class:'
        },{            
            xtype:      'combobox',
            id:         'bd-featureclass_chooser',
            queryMode:  'local',  
            emptyText:  'Select a feature class',
            width:      200,
                
            store: {
                type: 'wrmafeatureclasses',
                listeners: {
                    'load': function(){
                        this.add({
                            'featureclasskey':     9999,
                            'featureclassname':    'All Feature Calsses',
                            'nrparams' :           'null'
                        });
                    }
                }
            },
            forceSelection: true,
            editable: true,
            
            displayField: 'featureclassname',
            valueField:   'featureclasskey',     
            
            listeners: {
                'select': 'onFeatureClassSelect',
                'afterrender': function() {
                    this.setValue(9999);
                }                
            }          
         },'->',{
            xtype: 'button',
            width: 40,
            border: false,
            tooltip: 'Add a new parameter',
            cls: 'blue-button',
            iconCls: 'x-fa fa-plus blue-icon',
            margin:'0 0 0 10',
            action: 'add',
            handler: 'parameterEditor'
        },{            
            xtype: 'button',
            id: 'bd-edit_parameter_btn',
            width: 40,
            border: false,
            disabled: true,
            tooltip: 'Edit selected parameter',
            cls: 'blue-button',
            iconCls: 'x-fa fa-pencil blue-icon',
            margin:'0 0 0 10',
            action: 'edit',
            handler: 'parameterEditor'
        },{            
            xtype: 'button',
            id: 'bd-delete_parameter_btn',
            width: 40,
            border: false,
            disabled: true,
            tooltip: 'Delete selected parameter',
            cls: 'blue-button',
            iconCls: 'x-fa fa-trash blue-icon',
            margin:'0 0 0 10'             
        }]
    }]    
});