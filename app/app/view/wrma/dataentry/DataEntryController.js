Ext.define('majisys_dds.view.wrma.dataentry.DataEntryController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.wrma-dataentry-dataentry',

    Requires: [
        'majisys_dds.util.Chart'
    ],

    graph: null,



    /* --- */
    onViewShow: function(){
        var viewMessage = majisys_dds.app.getApplication().viewMessage;
        if (viewMessage.value == 'timeseries|dataentry'){
            this.refreshView();
            var refs = this.getReferences(),
                formEl = refs.ParametersForm,
                params = viewMessage.params;
            formEl.down('[name=def-locationid]').setValue(params.locationid);
            formEl.down('[name=def-featureclass]').setValue(params.featureclasskey);
            formEl.down('[name=def-parameter]').setValue(params.parameterid);
            formEl.down('[name=def-startdate]').setValue(Ext.Date.parse(params.startdate,'d-m-Y'));
            formEl.down('[name=def-enddate]').setValue(Ext.Date.parse(params.enddate,'d-m-Y'));
            viewMessage.value = null;
            viewMessage.params = null;
            this.checkExistingRecords(false);
        };
    },




    /* --- */
    refreshView: function(){
        var refs = this.getReferences(),
            formEl = refs.ParametersForm,
            gridEl = refs.DataEntryGrid,
            gridCols = new Array,
            obsStoreFields = new Array;

        formEl.query('field').forEach(function(fld){
            fld.reset();
            fld.setReadOnly(false);
        });
        formEl.query('button').forEach(function(btn){
            btn.setDisabled(false);
            btn.toggle(false)
        });
        formEl.down('[name=def-observer]').setReadOnly(true);

        var observationsStore = Ext.create('Ext.data.Store', {
            id: 'new_observations',
            fields: obsStoreFields,
            autolad: false
        });

        gridEl.getPlugin('data_entry_plugin').cancelEdit();
        this.graph.reset();
        gridEl.reconfigure(observationsStore,gridCols);
    },




    /* --- */
    timeValidation: function(v0,v1,v2,v3){
        var t0 = Ext.Date.format(Ext.getCmp('def-obstime'+v0).value,'H:i');
        if (t0 == ''){
            return true;
        } else {
            var t1 = Ext.Date.format(Ext.getCmp('def-obstime'+v1).value,'H:i'),
                t2 = Ext.Date.format(Ext.getCmp('def-obstime'+v2).value,'H:i'),
                t3 = Ext.Date.format(Ext.getCmp('def-obstime'+v3).value,'H:i');
            return (t0 != t1 && t0 != t2 && t0 != t3) ? true : 'Value is duplicated';
        }
    },




    /* --- */
    initialiseForm: function(){
        var me = this,
            refs = me.getReferences(),
            formEl = refs.ParametersForm;

        Ext.Ajax.request({
            url: 'api/wrma/dataentry/storesparams.py',
            disableCaching: false,

            success: function(response){
                var storesData = eval(response.responseText);
                me.createStores(storesData, formEl);
            },
            failure: function(response){
                console.log('error: '+response.status);
            }
        });

        this.graph = Ext.create('majisys_dds.util.Chart',{
            panelId: 'def-chart_panel',
            withSlider: false
        });
        this.graph.clsPrefix = 'de';
        this.graph.nullValue = -0.0000000001;
    },




    /* --- */
    createStores: function(storesData){
        var refs = this.getReferences(),
            formEl = refs.ParametersForm;

        var locationsStore = Ext.create('Ext.data.Store', {
                fields: [{name:'id',type:'string'}],
                data: storesData[0],
                proxy: {
                type: 'memory',
                    reader: {
                        type: 'json',
                        rootProperty: 'locations'
                    }
                }
            });
        formEl.down('[name=def-locationid]').bindStore(locationsStore);

        var featureclassStore = Ext.create('Ext.data.Store', {
            fields: [
                {name:'featureclasskey',type:'integer'},
                {name:'featureclassname',type:'string'}
            ],
            data:storesData[0],
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    rootProperty: 'featureclass'
                }
            },
            sorters: [{
            property: 'featureclassname',
            direction: 'ASC'
            }]
        });
        formEl.down('[name=def-featureclass]').bindStore(featureclassStore);

        var parametersStore = Ext.create('Ext.data.Store', {
            fields: [
                {name:'featureclasskey',type:'integer'},
                {name:'id',type:'string'},
                {name:'name',type:'string'}
            ],
            data:storesData[0],
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    rootProperty: 'parameters'
                }
            },
            sorters: [{
                property: 'name',
                direction: 'ASC'
            }]
        });
        formEl.down('[name=def-parameter]').bindStore(parametersStore);

/* 
        formEl.down('[name=def-locationid]').setValue('14022401');
        formEl.down('[name=def-locationid]').setValue('2FA08');
        formEl.down('[name=def-featureclass]').setValue(3);
        formEl.down('[name=def-parameter]').setValue('E.obs');
        formEl.down('[name=def-startdate]').setValue(Ext.Date.parse('01-Aug-2016','d-M-Y'));
        formEl.down('[name=def-enddate]').setValue(Ext.Date.parse('15-Aug-2016','d-M-Y'));
*/
    },




    /* --- */
    checkExistingRecords: function(fromForm){
        var me = this,
            refs = me.getReferences(),
            formEl = refs.ParametersForm;;

        Ext.Ajax.request({
            url: 'api/wrma/dataentry/seriestab.py',
            disableCaching: false,
            method: 'GET',
            params: {
                locationid:  formEl.down('[name=def-locationid]').value,
                startdate:   Ext.Date.format(formEl.down('[name=def-startdate]').value,'Y-m-d'),
                enddate:     Ext.Date.format(formEl.down('[name=def-enddate]').value,'Y-m-d'),
                parameterid: formEl.down('[name=def-parameter]').value
            },
            success: function(response){
                var checkData = JSON.parse(response.responseText);
                if (checkData.success){
                    var infomsg = 'The existing data for this time period has been loaded into the form.<br />' +
                                  'The time columns were adjusted to the previously entered data.';
                    Ext.MessageBox.show({
                        title:'Loaded existing data!',
                        header: { cls: 'blue-header' },
                        msg: infomsg,
                        buttons: Ext.MessageBox.OK,
                        animateTarget: 'generategridbutton',
                        icon: Ext.MessageBox.INFO
                    });
                    var currObs = checkData.obs,
                        keys = (Object.keys(currObs[0])).filter(function(k){ return k != 'obsdate'; })
                        timeValues = keys.map(function(k){ return k.substr(0,2)+':'+k.substr(2,2); });
                    me.generateGrid(timeValues, currObs);
                    me.renderChart(currObs);
                } else {
                    if (fromForm){
                        me.generateGrid();
                        me.renderChart();
                    } else {
                        formEl.down('[name=def-featureclass]').enable();
                        formEl.down('[name=def-parameter]').enable();
                    }
                }
            },
            failure: function(response){
                console.log('error: '+response.status);
                 /* button.toggle(false,true); */
            }
        });
    },




    /* --- */
    generateGrid: function(timeValues, currObs){

        var refs = this.getReferences(),
            formEl = refs.ParametersForm,
            gridEl = refs.DataEntryGrid,
            gridCols = new Array,
            obsStoreFields = new Array;

        gridCols.push({
            text: 'No.',
            width: 50,
            sortable : false,
            resizable : false,
            dataIndex: 'recordid'
        },{
            text: 'Date',
            width: 150,
            dataIndex: 'obsdate',
            resizable : false,
            format: 'd-m-Y'
        });

        obsStoreFields.push(
            { name: 'recordid', type: 'integer' },
            { name: 'obsdate', type: 'string' }
        );

        if (!timeValues){
            var colHeader = null;
            var timeValues = new Array;
            for (i = 1; i < 5; ++i){
                colHeader = Ext.getCmp('def-obstime'+i).value
                if (colHeader) {
                    timeValues.push(colHeader.toTimeString().substring(0,5))
                }
            };
        };

        timeValues.sort()
        for (i = 0; i <timeValues.length; ++i){
            gridCols.push({
                text: timeValues[i],
                flex: 1,
                xtype: 'numbercolumn',
                format:'00.00',
                sortable: false,
                style: 'text-align:left',
                align: 'right',
                resizable : false,
                dataIndex: timeValues[i].replace(':',''),
                editor: {
                    allowBlank: true
                }
            });
            obsStoreFields.push({ name: timeValues[i].replace(':',''), type: 'float', allowNull: true });
        };

        gridCols.push({
            text: 'Quality',
            dataIndex: 'quality',
            sortable: false,
            resizable : false,
            flex: 2,

            editor: new Ext.form.field.ComboBox({
                triggerAction: 'all',
                queryMode: 'local',
                editable: false,
                store: {
                    fields : ['id', 'fullName'],
                    data : [
                    {id:'&nbsp;', fullName:' '},
                    {id:'Unknown', fullName:'Unknown'},
                    {id:'Good', fullName:'Good'},
                    {id:'Poor', fullName:'Poor'}
                    ]
                },
                displayField: "fullName",
                valueField: "id",
                tpl : '<tpl for="."><div class="x-boundlist-item">{fullName}&nbsp;</div></tpl>'
            })
        });
        obsStoreFields.push({ name: 'quality', type: 'string' });

        var observationsStore = Ext.create('Ext.data.Store', {
            id: 'new_observations',
            fields: obsStoreFields,
            autolad: false,

            proxy: {
                noCache: false,
                type: 'ajax',
                api: {
                    create: 'api/wrma/dataentry/submitobservations.py',
                    update: 'api/wrma/dataentry/submitobservations.py'
                },
                writer: {
                    type: 'json',
                    allowSingle: false
                },
                reader: {
                    keepRawData : true
                }

            },
            listeners: {
                beforesync: function(){
                    observationsStore.getProxy().setExtraParam('locationid',formEl.down('[name=def-locationid]').value);
                    observationsStore.getProxy().setExtraParam('paramaterid',formEl.down('[name=def-parameter]').value);
                    Ext.MessageBox.show({
                        msg: 'Sending data to the server,<br />please wait...',
                        progressText: 'Saving...',
                        width:300,
                        wait:true,
                        waitConfig: {interval:200},
                        icon:'ts-download', /* custom class in index.html */
                        animateTarget: 'submitButton'
                    });
                }
            }

        });

        formEl.query('field').forEach(function(fld){ fld.setReadOnly(true); });
        formEl.query('button').forEach(function(btn){ btn.setDisabled(true); });

        gridEl.reconfigure(observationsStore,gridCols);

        var i = 1,
            record,
            curRec,
            checkDate,
            ts = formEl.down('[name=def-startdate]').value,
            te = formEl.down('[name=def-enddate]').value;

        while(ts <= te) {
            record = new Object;
            record['recordid'] = i;
            record['obsdate'] = Ext.Date.format(ts, 'd-m-Y');
            checkDate = Ext.Date.format(ts, 'd-m-Y');

            for (j = 2; j<obsStoreFields.length-1; ++j){
                currRec = null;
                if (currObs) {
                    currRec = currObs.find(function(r){ return r.obsdate == checkDate; });
                }
                if (currRec)
                    record[obsStoreFields[j].name] = currRec[obsStoreFields[j].name];
                else
                    record[obsStoreFields[j].name] = null;
            }
            record['quality'] = null;
            observationsStore.add(record);

            i+=1;
            ts = new Date(ts.setDate(
                ts.getDate() + 1
            ));
        }
/* 
        dataEntryGrid.setTitle(
            '<div style="text-align:center;">Data Entry - ' +
            dataDefinitionForm.getForm().findField('parameter').getDisplayValue() +
            ' at Station: ' +
            dataDefinitionForm.getForm().findField('locationid').getDisplayValue() +
            '</div>'
        );
*/
    },




    /* --- */
    renderChart: function(currObs){
        var refs = this.getReferences(),
            formEl = refs.ParametersForm,
            gridEl = refs.DataEntryGrid;

        this.graph.createMainChart();

        var ts = new Array,
            series = new Array;

        var d, obj, obsValue, currRec, checkDate;

        for ( i = 0; i < gridEl.getStore().getCount(); i++ ){
            d = gridEl.getStore().data.items[i].data.obsdate;
            checkDate = d;
            /* checkDate = Ext.Date.format(Ext.Date.parse(d, 'd-m-Y'),'d-m-Y'); */
            for ( j = 2; j < gridEl.getStore().getModel().getFields().length - 2; j++ ){
                obj = gridEl.getStore().getModel().getFields()[j].name;
                currRec = null;
                obsValue = null;
                if (currObs){ currRec = currObs.find(function(r){ return r.obsdate == checkDate; }); }
                if (currRec)
                    if (currRec[obj] != null)
                        obsValue = currRec[obj];
                    else
                        obsValue = this.graph.nullValue;
                else
                    obsValue = this.graph.nullValue;

                ts.push({
                    date: Ext.Date.parse(d + ' ' + obj.substr(0,2)+':'+obj.substr(2,2)+':00','d-m-Y H:i:s'),
                    value: obsValue
                })
            }
        };

        series.push({
            timeseriesid: 1,
            locationid : formEl.down('[name=def-locationid]').value,
            obs : ts,
            active: true,
            color : this.graph.seriesColor[3]
        });
        this.graph.addSeries(series);
        this.graph.deployed = true;
    },




    /* --- */
    onGridCellEdit: function(editor, item){
        if (item.field != 'quality'){
            var d = Ext.Date.parse(item.record.getData().obsdate + ' ' + item.field.substr(0,2) +':'+ item.field.substr(2,2) +':00','d-m-Y H:i:s');
            var el = this.graph.series[0].obs.filter(function ( ds ) {
                return Ext.Date.isEqual(ds.date,d);
            })[0];
            (item.value == "") ? el.value = this.graph.nullValue : el.value = Number(item.value);
            this.graph.refresh(false);
        }
    },




    /* -- */
    onChartPanelResize: function(){
        if (this.graph.deployed)
            this.graph.resize();
    },



    /* --- */
    submitRecords: function (){
        var me = this,
            refs = this.getReferences(),
            gridEl = refs.DataEntryGrid,
            observationsStore = gridEl.getStore();

        observationsStore.sync({
            callback: function(){
                var response = this.getReader().rawData; /* 'this' Defaults to the store's proxy */
                Ext.MessageBox.hide();
                Ext.MessageBox.show({
                    title: 'Server Response',
                    header: { cls: 'blue-header' },
                    msg: response.messages[0],
                    icon: Ext.MessageBox.INFO,
                    width: 400,
                    buttons: Ext.MessageBox.OK,
                    animateTarget: 'submitButton',
                    fn: function(){
                        if (response.success) {
                            var viewMessage = majisys_dds.app.getApplication().viewMessage;
                            viewMessage.value = 'dataentry|timeseries';
                            me.refreshView();
                            me.refreshAppStores();
                            me.redirectTo('timeseries')
                        }
                    }
                });
            }
        });
    },




    /* --- */
    refreshAppStores: function(){
        /* refresh the parameters store of the analysis page*/
        if (Ext.getStore('wrma_parameters_tree'))
            Ext.getStore('wrma_parameters_tree').reload();

        /* refresh the parameters store of the timeseries page*/
        if (Ext.getStore('wrma_parameters'))
            Ext.getStore('wrma_parameters').reload();
    }
});
