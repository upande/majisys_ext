Ext.define('majisys_dds.view.wrma.dataentry.ParametersForm', {
    extend: 'Ext.form.Panel',
    xtype: 'wrmaparametersform',
    
    requires: [
        'widget.timefield'
    ],
    
    reference: 'ParametersForm',

    width:       450,
    minWidth:    450,
    maxWidth:    450,
    bodyPadding: '0 5 0 5',
    style: {
        borderLeft: 'solid 1px #cccccc',
        borderRight: 'solid 1px #cccccc',
        borderBottom: 'solid 1px #cccccc'
    },
    
    fieldDefaults: {
        msgTarget:  'qtip',
        labelAlign: 'right',
        labelWidth: 65,
        margins:    '0 0 3 0'
    },
     
    items: [{
        xtype: 'fieldset',
        scrollable: {
            y: 'auto',
            x: false
        },
        title: '&nbsp;Observation Type:&nbsp;',
        padding: '0 5',        
        defaults: {
            anchor: '100%',
            allowBlank: false,
            msgTarget: 'side'
        },
        items: [{
            name:       'def-observer',
            xtype:      'textfield',
            fieldLabel: 'Observer:',
            editable:   false,
            value:      'WRMA Typist'
        },{
            name:           'def-locationid',
            xtype:          'combobox',
            fieldLabel:     'Station:',
            emptyText:      'Select the location for this timeseries',
            allowBlank:     false,
            forceSelection: true,
            queryMode:      'local',
            selectOnFocus:  true,
            triggerAction:  'all',
            valueField:     'id',
            displayField:   'id',
            listeners: {
                select: function(locationChooser){
                    var featureclassChooser = locationChooser.nextSibling();
                    featureclassChooser.reset();        
                    featureclassChooser.enable();
                    var parametersChooser = featureclassChooser.nextSibling();
                    parametersChooser.reset();
                    parametersChooser.disable();
                    parametersChooser.store.clearFilter();
                }
            }
        },{
            name:           'def-featureclass',
            xtype:          'combobox',
            fieldLabel:     'Feature:',
            emptyText:      'Feature Class',
            disabled:       true,
            allowBlank:     false,
            forceSelection: true,
            queryMode:      'local',
            selectOnFocus:  true,
            triggerAction:  'all',
            valueField:     'featureclasskey',
            displayField:   'featureclassname',
            listeners: {
                select: function(featureclassChooser){
                    var parametersChooser = featureclassChooser.nextSibling();
                    parametersChooser.store.clearFilter();
                    parametersChooser.store.filter(
                        'featureclasskey',
                        featureclassChooser.getValue()
                    );
                    parametersChooser.enable();
                }
            }            
        },{
            name: 'def-parameter',
            xtype: 'combobox',
            fieldLabel: 'Parameter:',
            emptyText: 'Parameter observed',
            disabled: true,
            allowBlank: false,
            forceSelection: true,
            queryMode: 'local',
            selectOnFocus: true,
            triggerAction: 'all',
            valueField: 'id',
            displayField: 'name'
        },{
            xtype: 'textarea',
            fieldLabel: 'Comment:',
            emptyText: 'Time series description (64 chars)',
            name: 'def-message',
            maxLength: 64,
            allowBlank: true,
            enforceMaxLength: true
        },{
             xtype: 'menuseparator'
        },{
            xtype: 'fieldcontainer',
                padding: '10 0 0 0',
            layout: 'hbox',
            items: [{
                id: 'def-startdate',
                name: 'def-startdate',
                xtype: 'datefield',
                fieldLabel: 'Start:',
                labelWidth: 35,
                flex: 1,
                value: Ext.Date.format(new Date(),'d-M-Y'),
                format: 'd-M-Y',
                emptyText: 'Select start date',
                allowBlank: false
            },{
                id: 'def-enddate',
                name: 'def-enddate',
                xtype: 'datefield',
                fieldLabel: 'End:',
                labelWidth: 35,
                flex: 1,
                format: 'd-M-Y',
                emptyText: 'Select end date',
                allowBlank: false,
                validator: function(){
                    var errMsg = 'The end date should be later than or equal to the start date';
                    return this.getValue() >= this.previousSibling().getValue() ? true : errMsg;
                }   
            }]
        },{
            xtype: 'fieldcontainer',
            fieldLabel: 'Times',
            labelWidth: 35,
            layout: 'hbox',
            margins: '0',
            defaultType: 'timefield',
            defaults: {
                hideLabel: true,
                increment: 30,
                format: 'H:i',
                minValue: '06:00',
                maxValue: '20:00'
            },
            items: [{
                id: 'def-obstime1',
                name: 'def-obstime1',
                flex: 1,
                value: '09:00',
                emptyText: '00:00',
                allowBlank: false,
                validator: function(){
                    return this.up('wrmadataentry').getController().timeValidation(1,2,3,4);
                }
            }, {
                id: 'def-obstime2',
                name: 'def-obstime2',
                flex: 1,
                padding: '0 0 0 5',
                value: '16:00',
                emptyText: '00:00',
                validator: function(){
                    return this.up('wrmadataentry').getController().timeValidation(2,1,3,4)
                }
            }, {
                id: 'def-obstime3',
                name: 'def-obstime3',
                flex: 1,
                padding: '0 0 0 5',
                emptyText: '00:00',
                validator: function(){
                    return this.up('wrmadataentry').getController().timeValidation(3,1,2,4);
                }
            }, {
                id: 'def-obstime4',
                name: 'def-obstime4',
                flex: 1,
                padding: '0 0 0 5',
                emptyText: '00:00',
                validator: function(val){
                    return this.up('wrmadataentry').getController().timeValidation(4,1,2,3);
                }
            }]            
        }]
    }],
    
    dockedItems: [{
        xtype:        'toolbar',
        dock:         'bottom',
        style: {
            background: '#e0ebf3'
        },
        layout: {
            pack: 'center'
        },
        items: [{
            xtype:      'button',
            id:         'def-grid_button',
            tooltip:    '',
            iconCls:    'x-fa fa-table blue-icon-text',
            text:       '&nbsp;&nbsp;&nbsp;Generate Data Entry Grid&nbsp;&nbsp;&nbsp;',
            enableToggle: true,
            pressed: false, 
            toggleHandler: function(){ 
                if (this.up('wrmaparametersform').getForm().isValid()){
                    this.up('wrmadataentry').getController().checkExistingRecords(true);
                } else {
                    this.toggle(false,true);
                }
            }
        }]
    }],
    
    listeners: {
        'afterrender': 'initialiseForm'
    }    
});