Ext.define('majisys_dds.view.wrma.dataentry.DataEntry',{
    extend: 'Ext.panel.Panel',
    xtype: 'wrmadataentry',

    requires: [
        'majisys_dds.view.wrma.dataentry.DataEntryController',
        'majisys_dds.view.wrma.dataentry.ParametersForm',
        'majisys_dds.view.wrma.dataentry.DataEntryGrid'
    ],

    controller: 'wrma-dataentry-dataentry',
    id: 'dataentry_dataentry',

    tools: [{
        type: 'refresh',
        tooltip: 'Reset the form - All data in the form will be lost)',
        callback: 'refreshView'
    }],
    
    layout: 'border',
    
    items:[{
        region: 'center',
        xtype: 'wrmadataentrygrid',
        title: ''
    },{
        collapsible: true,
        split: true,
        collapseMode: 'mini',
        hideCollapseTool: true,
        region: 'north',
        header: false,
        height:    460,
        minHeight: 300,
        maxHeight: 460,
        layout: 'border', 
        items:[{
            region: 'center',
            id: 'def-chart_panel',
            layout: 'fit',
            style: {
                borderLeft: 'solid 1px #cccccc',
                borderRight: 'solid 1px #cccccc',
                borderBottom: 'solid 1px #cccccc'
            },
            listeners: {
                resize: 'onChartPanelResize'
            }
        },{
            region: 'west',
            xtype: 'wrmaparametersform',
            layout: 'fit',
            collapsible: true,  
            collapseDirection: 'left',
            split: true,
            title: '456',
            collapseMode: 'mini',
            header: false
        }]
    }],
    

    listeners: {
        'afterlayout': 'onViewShow'
    }    
});
