Ext.define('majisys_dds.view.wrma.dataentry.DataEntryGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'wrmadataentrygrid',
    
    reference: 'DataEntryGrid',

    columnLines: true,
    enableColumnHide: false,
    plugins: {
        id: 'data_entry_plugin',
        ptype: 'cellediting',
        completeOnEnter: true,
        clicksToEdit: 1
    },
    viewConfig: {
        markDirty : false,
        enableTextSelection: true
    },

    columns: [],
    style: {
        borderLeft: 'solid 1px #cccccc',
        borderRight: 'solid 1px #cccccc',
        borderBottom: 'solid 1px #cccccc'
    },           
    
    buttonAlign: 'center',
    
    dockedItems: [{
        xtype:        'toolbar',
        dock:         'bottom',
        style: {
            background: '#e0ebf3'
        },
        layout: {
            pack: 'center'
        },
        items: [{
            xtype: "button",
            id: 'def-submit_button',
            tooltip: 'Send values to the database',
            text: "Submit values to the database",
            iconCls: 'fa fa-share-square-o',
            handler: 'submitRecords'
        }]
    }],
    
    listeners: {
        edit: 'onGridCellEdit'
    }    
  
});