Ext.define('majisys_dds.view.wrma.visualization.AnalysisController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.wrma-visualization-analysis',

    requires: [
        'majisys_dds.util.Chart',
        'majisys_dds.util.PointChart'
    ],

    analysisMap: null,
    stationsLayer: null,
    selectedStations: new ol.Collection(),
    locations: null,
    startdates: null,
    enddates: null,

    defaultStyle: new ol.style.Style({
        image: new ol.style.Circle({
            radius: 6,
            fill: new ol.style.Fill({
                color: 'rgba(255, 0, 102, 0.2)'
            }),
            stroke: new ol.style.Stroke({
                color: '#ff0066',
                width: 2
            })
        })
    }),

    graph: null,

    /* --- */
    selectedStyle: function(feature){
        return new ol.style.Style({
            image: new ol.style.Circle({
                radius: 6,
                fill: new ol.style.Fill({
                    color: 'rgba(0, 153, 204, 0.2)'
                }),
                stroke: new ol.style.Stroke({
                    color: '#0099cc',
                    width: 2
                })
            }),
            text: new ol.style.Text({
                textAlign: 'center',
                textBaseline: 'middle',
                font: 'Normal 16px Arial',
                text: feature.get('id'),
                fill: new ol.style.Fill({color: '#0099cc'}),
                stroke: new ol.style.Stroke({color: '#fff', width: 3}),
                offsetX: 0,
                offsetY: -16,
                rotation: 0
            })
        });
    },

    /* --- */
    labeledStyle: function(feature){
        return new ol.style.Style({
            image: new ol.style.Circle({
                radius: 6,
                fill: new ol.style.Fill({
                    color: 'rgba(255, 0, 102, 0.2)'
                }),
                stroke: new ol.style.Stroke({
                    color: '#ff0066',
                    width: 2
                })
            }),
            text: new ol.style.Text({
                textAlign: 'center',
                textBaseline: 'middle',
                font: 'Normal 16px Arial',
                text: feature.get('id'),
                fill: new ol.style.Fill({color: '#ff0066'}),
                stroke: new ol.style.Stroke({color: '#fff', width: 3}),
                offsetX: 0,
                offsetY: -16,
                rotation: 0
            })
        });
    },

    /* --- */
    hoverStyle: function() {
        return function(feature){
            return new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 6,
                    fill: new ol.style.Fill({
                        color: 'rgba(0, 0, 204, 0.2)'
                    }),
                    stroke: new ol.style.Stroke({
                        color: '#0000cc',
                        width: 2
                    })
                }),
                text: new ol.style.Text({
                    textAlign: 'center',
                    textBaseline: 'middle',
                    font: 'Normal 16px Arial',
                    text: feature.get('id'),
                    fill: new ol.style.Fill({color: '#0000cc'}),
                    stroke: new ol.style.Stroke({color: '#fff', width: 3}),
                    offsetX: 0,
                    offsetY: -16,
                    rotation: 0
                })
            });
        };
    },

    /* --- */
    configureView: function(){
        this.getView().getHeader().add([{
            xtype: 'button',
            id: 'anl-label_toggle_btn',
            text: 'Show Labels',
            cls: 'blue-button',
            border: false,
            disabled: true,
            iconCls: 'x-fa fa-tags',
            enableToggle: true,
            toggleHandler: 'onLabelToogle'
        },{
            xtype: 'tbspacer', width: 15
        },{
            xtype: 'button',
            text: 'Layer Switcher',
            cls: 'blue-button',
            border: false,
            iconCls: 'x-fa fa-map-o',
            menu: {
                items: [{
                    text: 'Base Map',
                    index: '0',
                    layername: 'OSM',
                    checked: true,
                    checkHandler: 'layerSwitcher'
                },{
                    text: 'WRUAs',
                    index: '1',
                    layername: 'WRUAs',
                    checked: true,
                    checkHandler: 'layerSwitcher'
                },{
                    text: 'Lake Naivasha',
                    index: '1',
                    layername: 'Lake Naivasha',
                    checked: true,
                    checkHandler: 'layerSwitcher'
                }]
            }
        }]);

        this.graph = Ext.create('majisys_dds.util.Chart',{
            panelId: this.getReferences().AnalysisChartPanel.id,
            withSlider: true,
            yStretch: true
        });
        this.graph.clsPrefix = 'wp';
        /* this.graph.nullValue = -0.0000000001; */
    },

    /* --- */
    onLabelToogle: function(btn, toggled){
        var me = this,
            pos,
            features = this.stationsLayer.getSource().getFeatures();

        features.forEach(function(feature){
            pos = me.locations.indexOf(feature.getProperties().id);
            if (pos == -1) {
                if (toggled){
                    feature.setStyle(me.labeledStyle(feature));
                } else {
                    feature.setStyle(null);
                }
            }
        });
    },

    /* --- */
    layerSwitcher: function(btn, checked){
        var layerList = this.analysisMap.getLayers().getArray();
        layerList.forEach(function(item){
            if (item.get('name') == btn.layername)
                item.setVisible(checked);
        });
    },

    /* --- */
    renderLayers: function(layerList){
        var me = this,
            layerSource, dataLayer,
            layerCode, wmsLayer,
            mapPanel = this.getReferences().AnalysisMapPanel,
            mapBbox = majisys_dds.app.getApplication().bounds;

        this.analysisMap = new ol.Map({
            interactions : ol.interaction.defaults({
                doubleClickZoom: true,
                select: false
            }),
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM(),
                    name: 'OSM'
                })
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([0,0]),
                zoom: 11,
                minZoom: 2,
                maxZoom: 18
            })
        });

        layerSource = new ol.source.ImageWMS({
            url: 'api/wrma/geo/layers?',
            params: {
                'LAYERS': 'wrua',
                'TILED': true
            }
        });
        dataLayer = new ol.layer.Image({
            source: layerSource,
            name: 'WRUAs',
            visible: true
        });
        this.analysisMap.addLayer(dataLayer);

        layerSource = new ol.source.ImageWMS({
            url: 'api/wrma/geo/layers?',
            params: {
                'LAYERS': 'lake',
                'TILED': true
            }
        });
        dataLayer = new ol.layer.Image({
            source: layerSource,
            name: 'Lake Naivasha',
            visible: true
        });
        this.analysisMap.addLayer(dataLayer);

        this.stationsLayer = new ol.layer.Vector({
            source: new ol.source.Vector(),
            style: this.defaultStyle
        });
        this.analysisMap.addLayer(this.stationsLayer);

        mapPanel.add({
            xtype: 'gx_map',
            map: this.analysisMap
        });
        this.analysisMap.getView().fit(ol.proj.transformExtent(mapBbox, 'EPSG:4326', 'EPSG:3857'), this.analysisMap.getSize());

        this.analysisMap.on('singleclick',
            function(evt){
                var record,
                    ds, de, dd,
                    pos;

                me.analysisMap.forEachFeatureAtPixel(evt.pixel, function(feature) {
                    separator = (me.selectedStations.getLength() == 0)? '' : ', '
                    pos = me.locations.indexOf(feature.getProperties().id);
                    if (pos == -1) {
                        feature.setStyle(me.selectedStyle(feature));
                        me.selectedStations.push(feature);
                        me.locations.push(feature.getProperties().id);
                        me.startdates.push(feature.getProperties().startdate);
                        me.enddates.push(feature.getProperties().enddate);
                    } else {
                        me.selectedStations.remove(feature);
                        if (Ext.getCmp('anl-label_toggle_btn').pressed)
                            feature.setStyle(me.labeledStyle(feature));
                        else
                            feature.setStyle(null);
                        me.locations.splice(pos,1);
                        pos = me.startdates.indexOf(feature.getProperties().startdate);
                        me.startdates.splice(pos,1);
                        pos = me.enddates.indexOf(feature.getProperties().enddate);
                        me.enddates.splice(pos,1);
                    }
                    ds = '';
                    de = '';
                    if (me.startdates.length > 0){
                        ds = Ext.Date.parse(me.startdates[0],'d-m-Y');
                        me.startdates.forEach(function(d){
                            ds = (ds < Ext.Date.parse(d,'d-m-Y')) ? ds : Ext.Date.parse(d,'d-m-Y');
                        });
                        de = Ext.Date.parse(me.enddates[0],'d-m-Y');
                        me.enddates.forEach(function(d){
                            de = (de > Ext.Date.parse(d,'d-m-Y')) ? de : Ext.Date.parse(d,'d-m-Y');
                        });
                    }
                    Ext.getCmp('wps-startdate').setValue(ds);
                    Ext.getCmp('wps-enddate').setValue(de);
                    Ext.getCmp('wps-locations_list').setValue(me.locations.sort().toString().replace(/,/g,', '));
                },
                null,
                function(selectionLayer){
                    return selectionLayer == me.stationsLayer;
                });
            }
        );

        hoverInteraction = new ol.interaction.Select({
            layers: [this.stationsLayer],
            condition: ol.events.condition.pointerMove,
            style: this.hoverStyle()
        });
        this.analysisMap.addInteraction(hoverInteraction);
    },

    /* --- */
    populateStations: function(grid,record){
        if (Ext.getCmp('anl-label_toggle_btn').isDisabled())
            Ext.getCmp('anl-label_toggle_btn').enable();
        Ext.getCmp('anl-label_toggle_btn').toggle(false,true);

        if (Ext.getCmp('wps-functions_panel').isDisabled())
            Ext.getCmp('wps-functions_panel').enable();

        var me = this;
        var stationsStore = Ext.create('Ext.data.Store', {
            id: 'analysis_stations_store',
            proxy: {
                type: 'ajax',
                url: 'api/wrma/stationsperparameter.py',
                reader: {
                    type: 'json',
                    rootProperty: 'records'
                }
            }
        });

        this.stationsLayer.getSource().clear();
        stationsStore.getProxy().extraParams = {
            parameterid: record.data.parameterid
        };
        stationsStore.load(function(records){
            var inf = null;
            records.forEach(function(record){
                var locationFeature = new ol.Feature({
                  geometry: new ol.geom.Point(
                    [record.data.x, record.data.y]
                  ).transform('EPSG:4326','EPSG:3857')
                });
                locationFeature.setProperties({
                    id: record.data.id,
                    serieskey: record.data.serieskey,
                    startdate: record.data.startdate,
                    enddate: record.data.enddate
                });
                me.stationsLayer.getSource().addFeature(locationFeature);
            });
            Ext.getCmp('wps-locations_list').setValue('');
            Ext.getCmp('wps-startdate').setValue('');
            Ext.getCmp('wps-enddate').setValue('');
            me.selectedStations.clear();
            me.locations = new Array();
            me.startdates = new Array();
            me.enddates = new Array();
        });
    },

    /* ---- */
    checkFunctionFields: function(node, record, el, functionForm){
        if (!record.data.fieldsloaded){
            var fieldsStore = Ext.getStore('function_fields');
            fieldsStore.getModel().getProxy().setConfig({
                url: 'app/sources/DescribeProcess_' + record.data.identifier + '.xml'
            });
            fieldsStore.load(function(){
                var checklist = ['startdate','enddate','parameterid','locations'];
                this.each(function(field){
                    record.data.fields.push([field.data]);
                    if (checklist.indexOf(field.data.name) == -1){
                        functionForm.add({
                            xtype: 'textfield',
                            id: 'wps-' + field.data.name,
                            labelCls: 'capitalize',
                            fieldLabel: '<b>' + field.data.name.replace('_','&nbsp;') + '</b>',
                            disabled: !record.data.implemented,
                            allowBlank: false,
                            listeners: {
                                'afterrender': function(){
                                    new Ext.tip.ToolTip({
                                        target: this.id,
                                        html: field.data.tip
                                    });
                                }
                            }
                        });
                    }
                });
                functionForm.down('button').functionName = record.data.title;
                functionForm.down('button').functionIdentifier = record.data.identifier;
                if (!record.data.implemented)
                    functionForm.down('button').disable();
            });
            record.data.fieldsloaded = true;
        }
    },

    /* --- */
    executeFunction: function(btn){
        var me = this,
            wpsForm = btn.up('form');
        if (wpsForm.isValid()){
            var locationsEl = Ext.getCmp('wps-locations_list');
            if (locationsEl.isValid()){
                var startdateEl = Ext.getCmp('wps-startdate');
                if (startdateEl.isValid()){
                    var enddateEl = Ext.getCmp('wps-enddate');
                    if (enddateEl.isValid()){
                        var params = '',
                            sep = '&';
                        var enddateEl = Ext.getCmp('wps-enddate');
                        params += 'parameterid=' + Ext.getCmp('wps-parameters_grid').getSelection()[0].data.parameterid;
                        params += sep + 'locations=' + locationsEl.getValue().replace(/ /g,'');
                        params += sep + 'startdate=' + Ext.Date.format(startdateEl.getValue(),'d-m-Y');
                        params += sep + 'enddate=' + Ext.Date.format(enddateEl.getValue(),'d-m-Y');
                        var formValues = wpsForm.getValues(),
                            formKeys = Object.keys(formValues);
                        for (i=0; i<formKeys.length; i++){
                            params += sep + formKeys[i].replace('wps-','').replace('-inputEl','') + '=';
                            params += formValues[formKeys[i]];
                        }
                        params = '?Request=Execute&Service=WPS&Version=1.0.0&Identifier=' +
                            btn.functionIdentifier + '&DataInputs=' + encodeURIComponent(params);

                        this.getReferences().AnalysisChartPanel.expand();
                        this.graph.reset();
                        this.graph.withPoints = true;
                        this.graph.temporalScale = true;
                        this.graph.deployed = false;

                        var panelTitle = btn.functionName + ' &mdash; ' + locationsEl.getValue() + ' () &mdash; ' +
                            Ext.Date.format(startdateEl.getValue(),'d-M-Y') + ' to ' +
                            Ext.Date.format(enddateEl.getValue(),'d-M-Y');

                        Ext.Ajax.request({
                            url: 'api/wrma/functions/wps.py' + params,
                            disableCaching: false,
                            success: function(response){
                               me.renderAnalysisChart(response.responseText, panelTitle, btn.functionIdentifier);
                            },
                            failure: function(response){
                                console.log('error: '+response.status);
                            }
                        });
                    }
                }
            }
        }
    },

    /* --- */
    renderAnalysisChart: function(response, panelTitle, functionId){
        var pos = response.indexOf('['),
            parameterData = response.substr(0,pos-1).split(','),
            series = eval(response.replace(response.slice(0,pos),'')),
            colorIndex, legendMarker,
            responseGraph;

        if (functionId == 'hymos_wps_probability_density'){
            this.graph.withPoints = false;
            this.graph.temporalScale = false;
        }

        for (i=0; i<series.length; i++){
            colorIndex = Math.floor(Math.random()*63);
            if (functionId != 'hymos_wps_probability_density')
                series[i].obs.forEach(function(d) { d.date = Ext.Date.parse(d.date,'Y-m-d H:i:s'); });
            series[i].color = this.graph.seriesColor[colorIndex];
            series[i].active = true;
            legendMarker = '<span style="color:' + this.graph.seriesColor[colorIndex] + '; font-weight: 700;">&marker;</span>';
            panelTitle = panelTitle.replace(series[i].locationid,legendMarker+series[i].locationid);
        }

        var chartPanel = this.getReferences().AnalysisChartPanel;
        chartPanel.getHeader().setTitle(panelTitle.replace('()', '( '+parameterData[0]+' )'));

        this.graph.createMainChart();
        this.graph.addSeries(series);
        this.graph.deployed = true;
    },

    /* --- */
    resizeAnalysisChart: function(){
        this.graph.resize();
    },

    /* --- */
    onMapPanelResize: function(){
        this.analysisMap.updateSize();
    }
});
