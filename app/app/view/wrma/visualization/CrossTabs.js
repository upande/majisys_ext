Ext.define('majisys_dds.view.wrma.visualization.CrossTabs',{
    extend: 'Ext.panel.Panel',
    xtype: 'wrmavisualizationcrosstabs',

    requires: [
        'majisys_dds.view.wrma.visualization.CrossTabsController'
    ],

    controller: 'visualization-crosstabs',

    html: '<iframe style="width:100%; height:100%; border:none;" src="sources/crosstabs" scrolling="no"></iframe>',
    
    listeners: {
        'afterrender': function(){
            this.getHeader().hide();
        }
    }
});
