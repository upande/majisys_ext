Ext.define('majisys_dds.view.wrma.visualization.Analysis',{
    extend: 'Ext.panel.Panel',
    xtype: 'wrmavisualizationanalysis',

    requires: [
        'Ext.app.ViewModel',
        
        'majisys_dds.view.wrma.visualization.AnalysisController',
        'majisys_dds.store.wrma.ParametersTree',
        'majisys_dds.store.wrma.Functions',
        'majisys_dds.store.wrma.FunctionFields'
    ],

    controller: 'wrma-visualization-analysis',

    id: 'wrma_visualization_analysis',

    layout: 'border',

    items: [{
        region: 'east',
        width: 400,
        minWidth: 400,
        maxWidth: 400,
        split: true,
        layout: 'border',
        collapsible: true,
        hideCollapseTool: true,
        collapseMode: 'mini',
        header: false,
        items: [{
            xtype: 'grid',
            id: 'wps-parameters_grid',
            region: 'center',
            minHeight: 150,
            style: {
                borderLeft: 'solid 1px #cccccc',
                borderRight: 'solid 1px #cccccc',
                borderBottom: 'solid 1px #cccccc'
            },
            store: {
                type: 'wrmaparameterstree'
            },
            features: [{
                id: 'wps-fc_group',
                ftype: 'grouping',
                groupHeaderTpl: '{name}',
                hideGroupedHeader: true,
                enableGroupingMenu: false
            }],
            columns:[{
                text: 'Parameters List',
                dataIndex: 'parametername',
                align: 'left',
                flex: 1,
                renderer: function(value){
                    return '&emsp;&emsp;<i class="fa fa-caret-right" aria-hidden="true"></i>&ensp;' + value;
                }
            }],
            listeners: {
                'select': 'populateStations'
            }
        },{
            region: 'south',
            id: 'wps-functions_panel',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            split: true,
            disabled: true,
            height: 400,
            minHeight: 350,
            bodyPadding: 10,
            style: {
                border: 'solid 1px #cccccc'
            },
            items: [{
                xtype: 'textfield',
                id: 'wps-locations_list',
                fieldLabel: '<strong>Locations</strong>',
                labelWidth: 70,
                emptyText: 'Click on one or more locations on the map',
                editable: false,
                width: '100%',
                allowBlank: false
            },{
                xtype: 'fieldcontainer',
                fieldLabel: '<strong>Analysis Period</strong>',
                labelAlign: 'top',
                height: 90,
                layout: 'hbox',
                items: [{
                    xtype: 'datefield',
                    id: 'wps-startdate',
                    emptyText: 'Start Date',
                    format: 'd-M-Y',
                    margin: '0 5 0 0',
                    width: 184,
                    allowBlank: false
                },{
                    xtype: 'datefield',
                    id: 'wps-enddate',
                    emptyText: 'End Date',
                    format: 'd-M-Y',
                    margin: '0 0 0 5',
                    width: 184,
                    allowBlank: false
                }]
            },{
                xtype: 'grid',
                id: 'wps-functions_grid',
                flex: 1,
                margin: '-20 0 0 0',
                style: {
                    border: 'solid 1px #cccccc'
                },
                store: {
                    type: 'wrmafunctions'
                },
                scrollable: true,
                columns: [{
                    text: 'Functions List',
                    dataIndex: 'title',
                    align: 'left',
                    renderer: function(functionName, el, record){
                        if (record.data.implemented)
                            return functionName;
                        else
                            return '<span class="disabled-function">' + functionName + '</span>';
                    },
                    flex: 1
                }],
                plugins: [{
                    ptype: 'rowwidget',
                    widget: {
                        xtype: 'form',
                        height: 'auto',

                        fieldDefaults: {
                            msgTarget:  'side',
                            labelAlign: 'right',
                            labelWidth: 80
                        },
                        bodyStyle:{
                            'background-color': 'rgba(255, 255, 255, 0.01)'
                        },
                        bind: {
                            /* title: '' */
                        },
                        items: [{
                            xtype: 'displayfield',
                            bind: {
                                value: '<p><b>Summary:</b> {record.abstract}'
                            }
                        }],
                        dockedItems: [{
                            xtype: 'toolbar',
                            dock: 'bottom',
                            style: {
                                background: 'rgba(255, 255, 255, 0.01)'
                            },
                            layout: {
                                pack: 'center'
                            },
                            items: [{
                                xtype:      'button',
                                tooltip:    '',
                                iconCls:    'x-fa fa-play-circle blue-icon-text',
                                text: 'Execute',
                                functionIdentifier: '',
                                functionName: '',
                                handler: 'executeFunction'
                            }]
                        }]
                    }
                }],
                viewConfig: {
                    listeners: {
                        'expandbody': 'checkFunctionFields'
                    }
                }
            }]
        }]
    },{
        xtype: 'panel',
        region: 'center',
        layout: 'border',
        id: 'anl-print_panel',
        bodyStyle:{
            'background-color': 'rgba(255, 255, 255, 0.01)'
        },
        items: [{
            reference: 'AnalysisMapPanel',
            region: 'center',
            layout: 'fit',
            /* minHeight: 200, */
            listeners: {
                'boxready': 'renderLayers',
                'resize': 'onMapPanelResize'
            }
        },{
            reference: 'AnalysisChartPanel',
            region: 'south',
            layout: 'fit',
            split: true,
            collapsible: true,
            collapsed: true,
            hideCollapseTool: true,
            collapseMode: 'mini',
            header: {
                cls: 'blue-header'
            },
            height: 400,
            style: {
                border: 'solid 1px #cccccc'
            },
            bodyStyle:{
                'background-color': 'rgba(255, 255, 255, 0.01)'
            },
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'left',
                cls: 'toolbar-style',
                padding: '8 5 5 5',
                items:[{
                    xtype: 'button',
                    width: 40,
                    border: false,
                    tooltip: 'Export results as CSV',
                    cls: 'blue-button',
                    iconCls: 'x-fa fa-download blue-icon',
                    margin:'0 0 10 0'
                    /* ,handler: '' */
                },{
                    xtype: 'button',
                    width: 40,
                    border: false,
                    tooltip: 'Print',
                    cls: 'blue-button',
                    iconCls: 'x-fa fa-print blue-icon',
                    margin:'0 0 10 0'
                    /* ,handler: '' */
                }]
            }],
            listeners: {
                'resize': 'resizeAnalysisChart'
            }
        }]
    }],

    listeners: {
        'afterrender': 'configureView'
    }

});