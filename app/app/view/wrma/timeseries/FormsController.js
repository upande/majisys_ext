Ext.define('majisys_dds.view.wrma.timeseries.FormsController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.wrma-timeseries-formscontroller',

    /* --- */
    initialiseImportForm: function(formEl){
        if (formEl.params.locationid)
            formEl.down('[id=locationid]').setValue(formEl.params.locationid);
        if (formEl.params.parameterid)
            formEl.down('[id=parameterid]').setValue(formEl.params.parameterid);
        var buttonEl = formEl.down('[id=if-action_button]');
        buttonEl.setText(formEl.params.actionButton);
        buttonEl.setTooltip(formEl.params.tooltip);
        buttonEl.setIconCls(formEl.params.cls);
    },

    /* --- */
    initialisePeriodForm: function(formEl){
        formEl.d1 = new Date();
        formEl.d2 = new Date();
        formEl.d3 = new Date();
        formEl.d0 = new Date();

        formEl.d1.setDate(1);
        formEl.d2.setDate(1);
        formEl.d2.setMonth(formEl.d2.getMonth()-1);
        formEl.d3.setDate(1);
        formEl.d3.setMonth(formEl.d3.getMonth()-2);
        formEl.d0.setDate(0);

        formEl.down('[boxLabel=rb_1]').setBoxLabel(Ext.Date.format(formEl.d1,'M Y'));
        formEl.down('[boxLabel=rb_2]').setBoxLabel(Ext.Date.format(formEl.d2,'M Y'));
        formEl.down('[boxLabel=rb_3]').setBoxLabel(Ext.Date.format(formEl.d3,'M Y'));

        formEl.down('[name=startdate]').setValue(formEl.d2);
        formEl.down('[name=enddate]').setValue(formEl.d0);

        var buttonEl = formEl.down('[id=pf-action_button]');
        buttonEl.setText(formEl.params.actionButton);
        buttonEl.setTooltip(formEl.params.tooltip);
        buttonEl.setIconCls(formEl.params.cls);
        if (formEl.params.action == 'edt'){
            formEl.down('[inputValue=all]').setDisabled(true);
        }
    },

    /* --- */
    executeSpecialAction: function(buttonEl){
        var me = this,
            formEl = buttonEl.up('wrmaperiodform');

        if (formEl.isValid()){
            var startdate,
                enddate,
                sourceUrl,
                radioVal = formEl.down('radiogroup').lastValue.periodspec,
                action = formEl.params.action,
                keys = formEl.keys.ids,
                nr = formEl.keys.nr;

            switch (radioVal){
                case 'd1':
                    startdate = formEl.d1;
                    enddate = new Date();
                    if (action != 'edt')
                      enddate.setDate(enddate.getDate() + 1);
                    break;
                case 'd2':
                    startdate = formEl.d2;
                    enddate = formEl.d1;
                    if (action == 'edt')
                        enddate.setDate(enddate.getDate() - 1);
                    break;
                case 'd3':
                    startdate = formEl.d3;
                    enddate = formEl.d2;
                    if (action == 'edt')
                        enddate.setDate(enddate.getDate() - 1);
                    break;
                case 'custom':
                    startdate = formEl.startdate;
                    enddate = formEl.enddate;
                    if (action != 'edt')
                        enddate.setDate(enddate.getDate() + 1);
                    break;
                case 'all':
                    startdate = 'start';
                    enddate = 'end';
                    break;
            };

            switch (action){
                case 'csv':
                case 'dfs':
                    sourceUrl = formEl.params.url + '?serieskeys=' + keys;
                    if (radioVal == 'all')
                        sourceUrl += "&startdate=" + startdate + "&enddate=" + enddate;
                    else
                        sourceUrl += "&startdate=" + Ext.Date.format(startdate,'d-m-Y') + "&enddate=" + Ext.Date.format(enddate,'d-m-Y');
                    window.location.href = sourceUrl;
                    break;

                case 'edt':
                    var store = Ext.getStore('wrma_series_keys'),
                        locationid = store.getById(keys).data.locationid,
                        featureclasskey = store.getById(keys).data.featureclasskey,
                        parameterid = store.getById(keys).data.parameterid,
                        viewMessage = majisys_dds.app.getApplication().viewMessage;

                    viewMessage.params = {
                        'locationid': locationid,
                        'startdate': Ext.Date.format(startdate,'d-m-Y'),
                        'enddate': Ext.Date.format(enddate,'d-m-Y'),
                        'featureclasskey': featureclasskey,
                        'parameterid': parameterid
                    };
                    viewMessage.value = 'timeseries|dataentry';

                    this.getView().up('wrmatimeseriesselectionwindow').close();
                    this.redirectTo('wrmadataentry');
                    break;

                case 'del':
                    var  title = '',
                         message = '',
                         start = '',
                         end = '';
                    sourceUrl = formEl.params.url;

                    if (radioVal == 'all') {
                        start = startdate;
                        end = enddate;
                        title = 'Delete ' + nr + ' timeseries?';
                        message = 'Your are about to delete the series ' + keys + '. Are you sure?';
                    } else {
                        start = Ext.Date.format(startdate,'d-m-Y');
                        end = Ext.Date.format(enddate,'d-m-Y');
                        title = 'Delete data from ' + nr + ' timeseries?';
                        message = 'Your are about to delete all data between ' + start +
                                  ' (midnight) and ' + end + ' (midnight) from the series ' + keys + '. Are you sure?';
                    }

                    Ext.Msg.confirm(title, message, function(btn){
                        if (btn == 'yes'){
                            Ext.Ajax.request({
                                url: sourceUrl,
                                disableCaching: false,
                                params: {
                                    serieskeys: keys,
                                    startdate: start,
                                    enddate: end
                                },
                                success: function(response){
                                    var response = response.responseText;
                                    Ext.Msg.alert('Message', response);
                                    Ext.getStore('wrma_series_keys').load();
                                    me.refreshAppStores();
                                },
                                failure: function(response){
                                    Ext.Msg.alert('Error', response);
                                }
                            });
                        }
                    });
                    break;
            };
        }
    },

    /* --- */
    onImportCsv: function(buttonEl){
        var me = this,
            formEl = buttonEl.up('wrmaimportform');

        if (formEl.isValid()){
            formEl.submit({
                url: formEl.params.url,
                waitMsg: 'Uploading CSV file...',
                success: function(form,callbackAction) {
                    var messages = Ext.JSON.decode(callbackAction.response.responseText)["messages"];
                    var message = ''
                    for (i = 0; i < messages.length; ++i)
                        message += messages[i].replace(/ /g,"&nbsp;") + "<br/>";
                    Ext.Msg.alert('Message', message);
                    Ext.getStore('wrma_series_keys').loadPage(1);
                    me.refreshAppStores();
                }
            });
        }
    },

    /* --- */
    refreshAppStores: function(){
        /* refresh the parameters store of the analysis page*/
        if (Ext.getStore('wrma_parameters_tree'))
            Ext.getStore('wrma_parameters_tree').reload();

        /* refresh the parameters store of the timeseries page*/
        if (Ext.getStore('wrma_parameters'))
            Ext.getStore('wrma_parameters').reload();
    }
});
