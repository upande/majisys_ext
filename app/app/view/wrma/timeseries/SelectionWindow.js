Ext.define('majisys_dds.view.wrma.timeseries.SelectionWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.wrmatimeseriesselectionwindow',  
    
    parentContainer: 'timeseries_cnt',
    
    autoShow:   true,
    constrain:  true,
    resizable:  false,

    header: {
        cls: 'blue-header'
    },    
    layout:     'fit',
    
    closeAction: 'destroy',
    
    listeners: {
        'show': function(){
            Ext.ComponentQuery.query('#timeseries_cnt')[0].mask();
        },
        'hide': function(){
            Ext.ComponentQuery.query('#timeseries_cnt')[0].unmask();
        }
    }

});