Ext.define('majisys_dds.view.wrma.timeseries.SeriesGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'wrmaseriesgrid',

    requires: [
        'majisys_dds.store.wrma.SeriesKeys',
        'majisys_dds.store.wrma.Parameters',
        'majisys_dds.store.wrma.Stations'
    ],

    store: {
        type:'wrmaserieskeys'
    },

    loadMask: true,
    selModel: {
        pruneRemoved: false
    },

    disableSelection: true,
    enableColumnHide: false,
    viewConfig: {
        markDirty : false
    },

    columns: [{
        header   : '',
        width    : 30,
        menuDisabled: true,
        dataIndex: 'seriescolor',
        renderer : function(value){
            if (value == '') {
                return '';
            } else {
                return '<span style="color:' + value + '; font-weight: 700;">&marker;</span>';
            }
        }
    },{
        xtype     : 'checkcolumn',
        id        : 'tsk-selection_column',
        text      : '&#10004;',
        dataIndex : 'selected',
        width     : 30,
        sortable  : false,
        resizable : false,
        menuDisabled: true,
        listeners: {
            'checkchange': 'onCheckChange'
        }
    },{
        text      : 'Series ID',
        dataIndex : 'serieskey',
        align     : 'center',
        width     : 80
    },{
        text      : 'Station ID',
        dataIndex : 'locationid',
        align     : 'left',
        flex: 1
    },{
        text      : 'Parameter',
        dataIndex : 'parametername',
        align     : 'left',
        width     : 180
    },{
        text      : 'From',
        dataIndex : 'startdate',
        xtype     : 'datecolumn',
        format    : 'd-M-Y H:i',
        align     : 'center',
        width     : 140
    },{
        text      : 'To',
        dataIndex : 'enddate',
        xtype     : 'datecolumn',
        format    : 'd-M-Y H:i',
        align     : 'center',
        width     : 140
    },{
        text      : 'No. Entries',
        dataIndex : 'nritems',
        align     : 'right',
        width     : 100,
        renderer  : Ext.util.Format.numberRenderer('0,000,000 ')
    },{
        text      : 'Updated On',
        dataIndex : 'lastmodified',
        xtype     : 'datecolumn',
        format    : 'd-M-Y H:i',
        align     : 'center',
        width     : 140
    }],
    
    listeners: {
        'rowdblclick': 'onRowDblClick'
    },
    
    dockedItems : [{
        xtype:        'pagingtoolbar',
        dock:         'top',
        displayInfo:  true,
        displayMsg:   'Displaying Series {0} - {1} of {2}',
        emptyMsg:     'No series to display'
    },{
        xtype:        'toolbar',
        dock:         'bottom',
        items: [{
            xtype:      'tbtext',
            text:       'Filter by Station:'
        },{
            xtype:      'combo',
            id:         'tsk-station_chooser',
            queryMode:  'local',
            emptyText:  'All Stations',
            width:      120,
            
            store: {    
                type:   'wrmastations'
            },
            forceSelection: true,
            
            displayField: 'stationname',
            valueField:   'stationname',
            
            listeners: {
                'select': 'onStationSelect',
                specialkey: function(f,e) {
                    if (e.getKey() == e.ENTER) {
                        var value = this.getRawValue();
                        if (value != null && value != '') {
                            var store = this.store;
                            var valueField = this.valueField;
                            var displayField = this.displayField;
                            var recordNumber = store.find(valueField, value, 0);
                            if (recordNumber == -1)
                                return;
                            var displayValue = store.getAt(recordNumber).data[displayField];
                            this.setValue(displayValue);
                            this.setRawValue(displayValue);
                            this.selectedIndex = recordNumber;
                        }
                        Ext.getCmp('wrma_timeseries_keys').getController().onStationSelect(this);
                    }
                }
            }
        },{          
            xtype:      'tbtext',
            text:       'Filter by Parameter:'
        },{            
            xtype:      'combo',
            dock:       'top',
            id:         'tsk-parameter_chooser',
            queryMode:  'local',  
            emptyText:  'All Parameters',
            width:      210,
            
            store: {    
                type:   'wrmaparameters'
            },
            forceSelection: true,
            
            displayField: 'parametername',
            valueField:   'parameterid',     
            
            listeners: {
                'select': 'onParameterSelect',
                specialkey: function(f,e) {
                    if (e.getKey() == e.ENTER) {
                        var value = this.getRawValue();
                        if (value != null && value != '') {
                            var store = this.store;
                            var valueField = this.valueField;
                            var displayField = this.displayField;
                            var recordNumber = store.find(valueField, value, 0);
                            if (recordNumber == -1)
                                return;
                            var displayValue = store.getAt(recordNumber).data[displayField];
                            this.setValue(displayValue);
                            this.setRawValue(displayValue);
                            this.selectedIndex = recordNumber;
                        }
                        Ext.getCmp('wrma_timeseries_keys').getController().onParameterSelect(this);
                    }
                }
            }
        },'->',{          
            xtype:      'button',
            id:         'tsk-graph_button',
            tooltip:    'Generate a graph of the selected series',
            enableToggle: true,
            pressed:    false,
            cls: 'blue-button',
            iconCls: 'x-fa fa-line-chart blue-icon',
            width:      40,
            toggleHandler: 'onGraphButtonToggle'
        },' ',{
            xtype:      'button',
            id:         'tsk-edit_button',
            tooltip:    'Edit the selected series',
            cls: 'blue-button',
            iconCls: 'x-fa fa-pencil blue-icon',
            width:      40,
            params: { 
                windowTitle:        'Edit Selected Time Series',
                actionButton:    	'Edit',
                cls:    	        'x-fa fa-edit',
                action:    	        'edt',
                url:                '',
                tooltip:            'Modify the values for the selected period'
            },            
            handler:    'onSpecialButtonClick'
        },' ',{
            xtype:      'button',
            id:         'tsk-delete_button',
            tooltip:    'Delete the selected series',
            cls: 'blue-button',
            iconCls: 'x-fa fa-trash blue-icon',
            width:      40,
            params: { 
                windowTitle:        'Delete Elements From Time Series',
                actionButton:    	'Delete',
                cls:    	        'x-fa fa-trash',
                action:    	        'del',
                url:                'api/wrma/deleteseries.py',
                tooltip:            'Delete the values for the selected period'
            },
            handler:    'onSpecialButtonClick'
        },' ',{
            xtype:      'button',
            id:         'tsk-csv_button',
            iconCls:    'x-fa fa-download blue-icon-text',
            text:       'Export CSV',
            tooltip:    'Export data as CSV file(s) for the selected series',
            params: { 
                windowTitle:        'Export as CSV',
                actionButton:    	'Download',
                cls:    	        'x-fa fa-download',
                action:    	        'csv',
                url:                'api/wrma/csv/savecsv.py',
                tooltip:            'Download the CSV file(s) for the selected period'
            },            
            handler:    'onSpecialButtonClick'
        },' ',{
            xtype:      'button',
            id:         'tsk-dfs_Button',
            iconCls:    'x-fa fa-download blue-icon-text',
            text:       'Export dfs0',
            tooltip:    'Export data as dfs0 file(s) for the selected series',
            params: { 
                windowTitle:        'Export as dfs0',
                actionButton:    	'Download',
                cls:    	        'x-fa fa-download',
                action:    	        'dfs',
                url:                'api/wrma/mike/savedfs.py',
                tooltip:            'Download the dfs0 file(s) for the selected period'
            },             
            handler:    'onSpecialButtonClick'
        },' ',{
            xtype:      'button',
            id:         'tsk-import_button',
            iconCls:    'x-fa fa-upload blue-icon-text',
            text:       'Import CSV',
            tooltip:    'Import a new time series from a CSV file',
            params: { 
                windowTitle:        'Import Time Series From CSV',
                actionButton:    	'Import',
                cls:    	        'x-fa fa-upload',
                action:    	        'imp',
                url:                'api/wrma/csv/uploadcsv.py',
                tooltip:            'Import the data according to th chosen values in this form.'
            },             
            handler:    'onImportButtonClick'
        }]
    }]    
});
