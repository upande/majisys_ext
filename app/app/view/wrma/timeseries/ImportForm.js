Ext.define('majisys_dds.view.wrma.timeseries.FileFieldAttributes', {
    override: 'Ext.form.field.File',
    onRender: function() {
        var me = this,
            attr = me.fileInputAttributes,
            fileInputEl, name;
        me.callParent();
        fileInputEl = me.getTrigger('filebutton').component.fileInputEl.dom;
        for (name in attr) {
            fileInputEl.setAttribute(name, attr[name]);
        }
    }
});

Ext.define('majisys_dds.view.wrma.timeseries.ImportForm', {
    extend: 'Ext.form.Panel',
    xtype: 'wrmaimportform',
    
    controller: 'wrma-timeseries-formscontroller',  
    
    requires: [
        'majisys_dds.view.wrma.timeseries.FormsController',
        'majisys_dds.store.wrma.AllStations',
        'majisys_dds.store.wrma.AllParameters'
    ],
    
    bodyPadding: 15,
    
    items: [{
        xtype: 'fieldset',
        title: '&nbsp;Time Series:&nbsp;',
        padding: '0 10',
        defaults: {
            anchor: '100%',
            allowBlank: false,
            msgTarget: 'side',
            labelWidth: 70
        },
        items: [{        
            xtype: 'fileuploadfield',
            id: 'filedata',
            emptyText: 'Select a CSV file to upload.',
            fieldLabel: '<strong>CSV File</strong>',
            buttonText: 'Browse...',
            fileInputAttributes: {
                accept: '.csv,text/csv,text/plain'
                /* comma-separated list of mime-types or .extensions, e.g. 'image/gif,.xls'. */
                /* test any change here also on windows: some mimetypes like text/csv aren't */
                /* supported on windows, so .csv must be added. for more info, google 'html  */
                /* file input accept attribute'.                                             */
            }
        },{
            xtype: 'combo',
            id: 'locationid',
            store: {
                type: 'wrmaallstations'
            },
            forceSelection: true,
            fieldLabel: '<strong>Station</strong>',
            displayField: "stationname",
            valueField: "stationname",
            queryMode: 'local',
            emptyText: 'Select the location for this CSV file'
        },{
            xtype: 'combo',
            id: 'parameterid',
            store: {
                type: 'allparameters'
            },            
            forceSelection: true,
            fieldLabel: '<strong>Parameter</strong>',
            displayField: "parametername",
            valueField: "parameterid",     
            queryMode: 'local',  
            emptyText: 'Select the parameter for this CSV file'          
        }]                
    },{        
        xtype: 'fieldset',
        title: '&nbsp;CSV Format:&nbsp;',
        padding: '0 10',

        items: [{
            xtype:   'radiogroup',
            columns: 2,         
            items: [{
                xtype:      'radiofield',
                boxLabel:   '<strong style="color:#a00;">Date</strong>, <strong style="color:#00c;">&nbsp;Time</strong>, <strong style="color:#0a0;">&nbsp;Value</strong>',
                inputValue: 'dtv',
                name:       'csvlayout',
                checked:    true
            },{
                xtype:      'radiofield',
                boxLabel:   '<strong style="color:#a00;">Date</strong>, <strong style="color:#0a0;">&nbsp;Value</strong>, <strong style="color:#00c;">&nbsp;Time</strong>',
                inputValue: 'dvt',
                name:       'csvlayout'
            },{
                xtype:      'radiofield',
                boxLabel:   '<strong style="color:#a00;">Date</strong>, <strong style="color:#00c;">&nbsp;Time</strong>, <strong style="color:#0a0;">&nbsp;Value</strong>' +
                            ', <strong style="color:#00c;">&nbsp;Time</strong>, <strong style="color:#0a0;">&nbsp;Value</strong>',
                inputValue: 'dtvtv',
                name:       'csvlayout'
            },{
                xtype:      'radiofield',
                boxLabel:   '<strong style="color:#a00;">Date</strong>, <strong style="color:#0a0;">&nbsp;Value</strong>, <strong style="color:#00c;">&nbsp;Time</strong>' +
                            ', <strong style="color:#0a0;">&nbsp;Value</strong>, <strong style="color:#00c;">&nbsp;Time</strong>',
                inputValue: 'dvtvt',
                name:       'csvlayout'
            },{
                xtype:      'radiofield',
                boxLabel:   '<strong style="color:#a00;">Date</strong>, <strong style="color:#00c;">&nbsp;Time</strong>, <strong style="color:#00c;">&nbsp;Time</strong>' +
                            ', <strong style="color:#0a0;">&nbsp;Value</strong>, <strong style="color:#0a0;">&nbsp;Value</strong>',
                inputValue: 'dttvv',
                name:       'csvlayout'
            },{
                xtype:      'radiofield',
                boxLabel:   '<strong style="color:#a00;">Date</strong>, <strong style="color:#0a0;">&nbsp;Value</strong>, <strong style="color:#0a0;">&nbsp;Value</strong>' +
                            ', <strong style="color:#00c;">&nbsp;Time</strong>, <strong style="color:#00c;">&nbsp;Time</strong>',
                inputValue: 'dvvtt',
                name:       'csvlayout'
            },{
                xtype:      'radiofield',
                boxLabel:   '<strong style="color:#a00;">DateTime</strong>, <strong style="color:#0a0;">&nbsp;Value</strong>',
                inputValue: 'dv',
                name:       'csvlayout'                
            },{
                xtype:      'radiofield',
                boxLabel:   '<strong style="color:#000;">Other structure</strong>',
                disabled:   true,
                inputValue: '',
                name:       'csvlayout'                
            }]   
        }]
    },{
        xtype: 'fieldset',
        title: '&nbsp;Date Format:&nbsp;',
        padding: '0 10',
        items: [{
            xtype:   'radiogroup',
            columns: 2,          
            items:[{
                    xtype:      'radiofield',
                    boxLabel:   '<strong>Day - Month - Year</strong>',
                    inputValue: 'dmy',
                    name:       'dateformat',
                    checked:    true
                },{
                    xtype:      'radiofield',
                    boxLabel:   '<strong>Year - Month - Day</strong>',
                    inputValue: 'ymd',
                    name:       'dateformat'
                },{
                    xtype:      'radiofield',
                    boxLabel:   '<strong>Month - Day - Year</strong>',
                    inputValue: 'mdy',
                    name:       'dateformat'
                },{
                    xtype:      'radiofield',
                    boxLabel:   '<strong>Year - Day - Month</strong>',
                    inputValue: 'ydm',
                    name:       'dateformat'
            }]
        }]
    }],
   
    dockedItems : [{
        xtype:        'toolbar',
        dock:         'bottom',
        style: {
            background: '#e0ebf3'
        },
        layout: {
            pack: 'center'
        },
        items: [{
            xtype:      'button',
            id:         'if-action_button',
            tooltip:    '',
            iconCls:    '',
            text:       'action',
            width:      100,
            handler:    'onImportCsv'
        },' ',{
            xtype:      'button',
            id:         'if-close_button',
            tooltip:    'Close this window',
            text:       'Close',
            width:      100,
            handler:    function(){
                this.up('wrmatimeseriesselectionwindow').close();
            }
        }]
    }],   
    
    listeners: {
        'beforerender': 'initialiseImportForm'
    }    
});
