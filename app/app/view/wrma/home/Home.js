Ext.define('majisys_dds.view.wrma.home.Home',{
    extend: 'Ext.panel.Panel',
    xtype: 'homehome',

    requires: [
        'majisys_dds.view.wrma.home.HomeController'
    ],

    id: 'wrma_home_home',
        
    controller: 'wrma-home-home',

    html: '<iframe style="width:100%; height:100%; border:none;" src="app/view/wrma/home/home_html/home_view.html" scrolling="no"></iframe>',
    
    listeners: {
        'afterrender': function(){
            this.getHeader().hide();
        }
    }
});