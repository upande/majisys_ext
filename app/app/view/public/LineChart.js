Ext.define('majisys_dds.view.public.LineChart', {
	extend: 'Ext.panel.Panel',
	//xtype: 'linechart',
	alias: 'widget.linechart',

	requires: [
        'majisys_dds.store.public.Stations'
    ],

	store:{
		type: 'stations'
	},

	//Format for aggregate summary except count(sum)
    aggFormat: '00.000',

    // Formats value to include '--' when null is encountered 
    formatValues: function(obsValue){
        if(obsValue == null){
            return '--';
        } else {
            return Ext.util.Format.number(obsValue, this.aggFormat);
        }
    },
	
	initComponent: function(){
		this.items = [{
			xtype: 'panel', // Panel that hosts the line chart
			id: 'chartPanel',
			reference: 'chartPanel',
			layout: 'fit',
			padding: 3,
			html: ' <button id="origBtn" class="float-l" type="button" onclick="ctPublicController.reDrawChart(false)" disabled="diabled">Original</button> <button id="smoothBtn" class="float-r" type="button" onclick="ctPublicController.reDrawChart(true)">Smooth</button> '
		}];

		this.dockedItems = [{
			dock: "bottom",
			xtype: "toolbar",

			items: [
				{
					text: '&boxbox;',
					tooltip: 'Layer Switcher',
					menu: {
						items: [
							{
								text: 'Base Map',
								id: 'mapquest_osm_tiles',
								reference: 'mapquest_osm_tiles',
								index: '0',
								checked: true
							},

							{
								text: 'Wruas',
								id: 'wruas',
								reference: 'wruas',
								index: '1',
								checked: true
							},

							{
								text: 'Lake Naivasha',
								id: 'lakenaivasha',
								reference: 'lakenaivasha',
								index: '2',
								checked: true
							}
						]
					}
				},
				{
					xtype: 'tbfill'
				},
				{
					xtype: 'button',
					id: 'csvButton',
					reference: 'csvButton',
					text: 'Generate CSV',
					handler: function(){
						ctPublicController.csvWindow.show();
						Ext.getElementById('csv-div').innerHTML = '';
				        Ext.getElementById('csv-div').innerHTML = ctPublicController.generateCSVData('html');
					}
				}
			]
		}];

		this.callParent(arguments);
	}
});