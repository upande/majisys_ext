Ext.define('majisys_dds.view.public.CrossTabsPublic',{
    extend: 'Ext.tab.Panel',
    xtype: 'crosstabspublic',

    requires: [
        // CrossTabsPublic ViewController
        'majisys_dds.view.public.CrossTabsPublicController',

        // CrossTabs View
        'majisys_dds.view.public.MapDisplay',
        'majisys_dds.view.public.YearlyGrid',
        'majisys_dds.view.public.LineChart'
    ],

    stores: [
        'majisys_dds.store.public.Observations',
        'majisys_dds.store.public.Parameters',
        'majisys_dds.store.public.Stations'
    ],

    controller: 'crosstabspublic',

    listeners : {
        scope       : 'controller',
        afterrender : 'onAfterRender'
    },

    tabPosition: 'bottom',
    bodyBorder: true,
    items: [{
        name: 'main-panel',
        title: '&nbsp;&nbsp;Aggregated Values&nbsp;&nbsp;',
        layout: 'border',
        padding: 2,
        items: [{
            /* Navigation View */
            xtype: 'mapdisplay',
            id: 'map-display-panel',
            region: 'center',
            margins: '2 -2 2 2',
            collapseDirection: 'left',
            minWidth: 200,
            layout: 'fit',
            border: 'true'
        },{
            /* CrossTabs View */
            xtype: 'panel',
            region: 'east',
            split: true,
            id: 'print-panel',
            margin: '2 2 2 0',
            width: 850,
            minWidth: 750,
            border: false,
            layout: 'border',
            items: [{
                /* Graph View */
                xtype: 'linechart',
                layout: 'fit',
                region: 'north',
                split: true,
                height: 300,
                padding: 0
            },{
                /* Grid View */
                xtype: 'public_yearlygrid',
                region: 'center',
                split: true,
                margin: '2 0 0 0'
            }]
        }]
    }]
});
