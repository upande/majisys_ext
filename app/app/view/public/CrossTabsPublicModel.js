/**
 * This class is the view model for the CrossTabsPublic view of the application.
 */
Ext.define('majisys_dds.view.main.CrossTabsPublicModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.crossTabsPublic',

    stores: {
        observations: {
            model: 'Observations',
            autoLoad: true/*,
            sorters: [{
                property: 'firstName',
                direction:'DESC'
            }]*/
        },
        stations: {
        	model: 'Stations',
        	autoLoad: true
        }
    }

    /*data: {
        name: 'majisys_dds',

        loremIpsum: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    }*/

    //TODO - add data, formulas and/or methods to support your view
});
