Ext.define('majisys_dds.view.public.YearlyGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'public_yearlygrid',
    reference: 'public_yearlygrid',

    requires: [
        'majisys_dds.store.public.Observations',
        'majisys_dds.store.public.Parameters'
    ],
    
    store: {
        type: 'observations'
    },

    viewConfig: {
        stripeRows:false,
        markDirty: false
    },

    columnLines: true, // Adds line to show column
    enableColumnHide: false, //  Disables column hiding within the grid

    // Display aggregation summary below column headers
    features: [{
        ftype: 'summary',
        dock: 'top', 
        id: 'f-summary'
    }],

    //Format for aggregate summary except count(sum)
    aggFormat: '00.000',

    // Formats value to include '--' when null is encountered 
    formatValues: function(obsValue){
        if(obsValue == ctPublicController.nullValue){
            return '--';
        } else {
            return Ext.util.Format.number(obsValue, this.aggFormat);
        }
    },

    initComponent: function(){
        this.columns = []; // Sets column as empty array
        // Task bar items
        this.dockedItems = [{
            dock: 'bottom',
            xtype: 'toolbar',

            items: [
                {
                    xtype: 'button',
                    id: 'selectionButton',
                    reference: 'selectionButton',
                    tooltip: 'Press to select or unselect all records',
                    text: 'Unselect All',
                    pressed: true,
                    enableToggle: true,
                    toggleHandler: function(button, pressed){ // Handles the toggling between Select/Unselect
                        var observationstore = Ext.getStore('majisys_dds.store.public.Observations');

                        // Suspends events in Observations store
                        observationstore.suspendEvents();

                        // Iterates over data in Observations store
                        observationstore.each(function(item){
                            item.set('enable', pressed);
                            if(pressed){
                                d3.selectAll("#st_"+item.get('serieskey')).attr('class','stline');
                                d3.selectAll("#feat_"+item.get('station')).attr('class','stpoint');
                                d3.selectAll("#rp_"+item.get('serieskey')).attr('class','rfpoint');
                            }else{
                                d3.selectAll("#st_"+item.get('serieskey')).attr('class','hidden')
                                d3.selectAll("#feat_"+item.get('station')).attr('class','hidden');
                                d3.selectAll("#rp_"+item.get('serieskey')).attr('class','hidden');
                            }
                        });

                        if (pressed){
                            ctPublicController.visibleRecords = ctPublicController.chartRecords.filter(function(el){
                                return el.record_id != null;
                            });
                            ctPublicController.hiddenRecords = new Array;
                            ctPublicController.reScaleChart();
                        } else {
                            ctPublicController.hiddenRecords = ctPublicController.chartRecords.filter(function(el){
                                return el.record_id != null;
                            });
                            ctPublicController.visibleRecords = new Array;
                        };

                        // Resume events in Observations store
                        observationstore.resumeEvents();

                        // Refreshes grid view. Sets the sort state and focus
                        // to the previously focused row
                        var yearGrid = ctPublicController.lookupReference('public_yearlygrid');
                        yearGrid.getView().refresh();
                        
                        // Toggles between text
                        pressed ? button.setText('Unselect All') : button.setText('Select All');
                    }
                },
                
                {
                    xtype: 'button',
                    text: 'Filter Selected',
                    tooltip: 'Press to remove unselected records from the view',
                    enableToggle: true,
                    toggleHandler: function(button, pressed){
                        var observationstore = Ext.getStore('majisys_dds.store.public.Observations');

                        // Toggles between text
                        pressed ? button.setText('Remove Filter') : button.setText('Filter Selected');

                        // Disables/Enables componenents depending on toggle
                        if( pressed ){
                            // Disables components
                            Ext.getCmp('selectionButton').disable();
                            Ext.getCmp('aggchooser').disable();
                            Ext.getCmp('stationtypechooser').disable();

                            // Filters Observation store data based on enable 
                            observationstore.filter('enable',true);
                        }
                        else {
                            // Enables components
                            Ext.getCmp('selectionButton').enable();
                            Ext.getCmp('aggchooser').enable();
                            Ext.getCmp('stationtypechooser').enable();

                            // Removes filter on Observations store
                            observationstore.clearFilter();
                        }
                    }
                },'-',
                
                {
                    xtype: 'tbtext',
                    text: '&nbsp;Aggregate Function:&nbsp;'
                },
                
                {
                    xtype: 'combo',
                    store: [["average","Average"],["sum","Total"],["min","Minimum"],["max","Maximum"],["count","Count"]],
                    id: 'aggchooser',
                    reference: 'aggchooser',
                    editable: false,
                    queryMode: 'local',
                    triggerAction: 'all',
                    listeners: {
                        afterrender: function(chooser){
                            chooser.setValue( chooser.getStore().getAt(0).get("field2") );
                        },

                        select: function(chooser){
                            ctPublicController.selectionFunction = chooser.getValue();
                            ctPublicController.refreshGrid();
                        }
                    }
                },'-',

                {
                    xtype: 'tbtext',
                    text: '&nbsp;Parameter:&nbsp;'
                },

                {
                    xtype: 'combo',
                    width: 200,
                    store: {
                        type: 'publicparameters'
                    },
                    value:'',
                    editable: false,
                    displayField: 'parameter_name',
                    valueField: 'parameter_id',
                    id: 'stationtypechooser',
                    reference: 'stationtypechooser',
                    queryMode: 'remote',
                    triggerAction: 'all',
                    listeners: {
                        /*afterrender: function(chooser){
                            //console.log( chooser.getStore().getAt(0) );
                           //chooser.setValue(chooser.getStore().getAt(0).get("val"));
                        },*/

                        select: function(chooser){
                            ctPublicController.selectionStationType = chooser.getValue();
                            ctPublicController.switchStationType = true;
                            ctPublicController.drawLayers( Ext.getCmp('stationtypechooser').getValue() )
                            //console.log(Ext.getCmp('stationtypechooser').getValue());
                            /*if (ctPublicController.selectionStationType == 1){
                                ctPublicController.stationCss = 'stpoint_gaug';
                            } else {
                                ctPublicController.stationCss = 'stpoint_rain';
                            };*/
                            ctPublicController.refreshGrid();
                        }/*,
                        beforeselect: function(chooser,record,index) { // temporarily disable (WRL)
                            if(index == 2) {
                                return false;
                            }
                        }*/
                    }
                }
            ]
        }]
        this.callParent(arguments);
    }
});