Ext.define('majisys_dds.view.public.CrossTabsPublicController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.crosstabspublic',

    station_dict: {},

    // Grid variables
	aggFormat: '00.000',

    // Map variables
	navMap: null,
	mapBounds : null,
	sm: new OpenLayers.Projection("EPSG:3857"),
	wgs: new OpenLayers.Projection("EPSG:4326"),
	station_data: null,

	// LineChart variables
	svgChart: null,
	seriesLine: null,
	seriesLineInt: null,
	recordPaths: null,
	recordPoints: null,
	chartRecords: null,
	visibleRecords: null,
	hiddenRecords: null,
	x: null,
	y: null,
	xAxis: null,
	yAxis: null,
	// Total of 170 colours to be used for graphs
	seriesColor: [ '#CA226B', '#B1FB17', '#8EEBEC', '#00FFFF', '#728FCE', '#FFF380', '#3BB9FF', '#F88158', '#488AC7', '#6D7B8D',
	 				'#7E354D', '#B87333', '#342D7E', '#806517', '#87AFC7', '#79BAEC', '#954535', '#FFF8DC', '#C48189', '#151B54',
	 				'#CC6600', '#89C35C', '#7F38EC', '#F3E5AB', '#8D38C9', '#FF2400', '#4B0082', '#FF00FF', '#2B65EC', '#FDD7E4',
	 				'#6F4E37', '#AFDCEC', '#FAEBD7', '#437C17', '#15317E', '#348017', '#A74AC7', '#306754', '#B041FF', '#FF8040',
	 				'#FFDFDD', '#9172EC', '#FFFF00', '#7F462C', '#F62217', '#5E7D7E', '#254117', '#F62817', '#3EA055', '#835C3B',
	 				'#357EC7', '#B7CEEC', '#000080', '#ADA96E', '#848b79', '#614051', '#347C2C', '#2C3539', '#E77471', '#8C001A',
	 				'#6698FF', '#6495ED', '#E38AAE', '#F87217', '#000000', '#FC6C85', '#FAAFBE', '#C47451', '#493D26', '#5CB3FF',
	 				'#ECC5C0', '#6C2DC7', '#7FFFD4', '#E2A76F', '#7BCCB5', '#7F4E52', '#EDE275', '#FF7F50', '#98FF98', '#571B7E',
	 				'#C2DFFF', '#FAAFBA', '#ECE5B6', '#F5F5DC', '#7DFDFE', '#77BFC7', '#F9B7FF', '#C12869', '#306EFF', '#25383C',
	 				'#E6A9EC', '#38ACEC', '#D4A017', '#A1C935', '#C58917', '#C8B560', '#3090C7', '#1589FF', '#FBF6D9', '#C34A2C',
	 				'#728C00', '#F7E7CE', '#4E8975', '#786D5F', '#C8A2C8', '#157DEC', '#E4287C', '#AF9B60', '#FF0000', '#2B1B17',
	 				'#EDDA74', '#659EC7', '#C3FDB8', '#B5A642', '#B38481', '#34282C', '#E3E4FA', '#C85A17', '#483C32', '#FFEBCD',
	 				'#E8ADAA', '#E3319D', '#C68E17', '#C19A6B', '#56A5EC', '#87F717', '#CFECEC', '#7A5DC7', '#348781', '#E55451',
	 				'#93FFE8', '#966F33', '#5EFB6E', '#E56E94', '#43C6DB', '#5E5A80', '#C04000', '#347235', '#438D80', '#EAC117',
	 				'#E7A1B0', '#81D8D0', '#F52887', '#8467D7', '#54C571', '#BDEDFF', '#151B8D', '#4AA02C', '#E56717', '#368BC1',
	 				'#95B9C7', '#D462FF', '#4E9258', '#461B7E', '#008080', '#E42217', '#7E3517', '#B2C248', '#6CC417', '#C25283',
	 				'#59E817', '#46C7C7', '#DC381F', '#C11B17', '#C9BE62', '#6A287E', '#F9966B', '#52D017', '#0C090A', '#E0FFFF'],


	// Global variables
    stationCss: 'stpoint_gaug',
    switchStationType: false,
    selectionStationType: 1, // Parameter id 
    selectionFunction: 'average', // Aggregation function to be used 
    nullValue: null, // Null value used to show no record in data
    appCenter: null, // center for the map
    map_layers: [],

    // Creates CSV window
    csvWindow: Ext.create('Ext.window.Window',{
        title: 'CSV Data Preview',
        closeAction: 'close',
        height: 400,
        width: 800,
        modal: true,
        layout: 'fit',
        items: [{
            xtype: 'panel',
            id: 'csv-panel',
            padding: 5,
            overflowY: 'auto',
            items: {
                xtype: 'button',
                id: 'csv-Button',
                tooltip: 'Download CSV file',
                text: 'Download CSV',
                handler: function(){ 
                    var data = ctPublicController.generateCSVData('csv');
                    var text =  data[0];
                    var file_name = data[1];
                    
                    var csvString= encodeURIComponent(text);
                    
                    var a = document.createElement('a');
                    a.href = 'data:attachment/csv,' + csvString;
                    a.target = '_blank';

                    a.download = file_name;

                    document.body.appendChild(a);
                    a.click();
                }
            },
            html: '<div id="csv-div"></div>',
            border: false
        }]
    }),

    /*-- Generates CSV to be displayed and saved as csv --*/
    generateCSVData: function(type){
        type = typeof type !== 'undefined' ? type : 'html';
        var text = "";
        var separator = ',';
        var includeHidden = false;
        var file_name = '';

        // Columns
        var grid = ctPublicController.lookupReference('public_yearlygrid');
        //var grid = ctPublicController.getYearGrid();
        var columns = grid.columnManager.columns;
        var columnsCount = columns.length;

        for (var i = 1; i < columnsCount; i++) {
            if (includeHidden || !columns[i].hidden) {
                text += columns[i].text + separator;
            }
        }
        text = text.substring(0, text.length-1);
        if(type=='csv'){
            text += "\r";
        }else if(type=='html'){
            text += '<br />';
        }
        
        if(columnsCount > 0){
            var file_name = columns[3].dataIndex + '-' + columns[columnsCount-1].dataIndex + '.csv';
        }
        
        // Row
        var observationstore = Ext.getStore('majisys_dds.store.public.Observations');
        //Ext.getStore('Observations').each(function( item ){
        observationstore.each(function( item ){
            var row = item.data;
            for (var index = 0; index<columnsCount; index++) {
                if (includeHidden || !columns[index].hidden) {
                    var value = row[columns[index].dataIndex];
                    if(index > 2){
                        text += "\"" + Ext.util.Format.number(value,'00.000') + "\"" + separator;
                    }
                    else if(index >= 1){
                        text += "\"" + value + "\"" + separator;
                    }
                }
            }
            text = text.substring(0, text.length-1);
            //text += "\r";
            if(type=='csv'){
                text += "\r";
            }else if(type=='html'){
                text += '<br />';
            }
        });
        if(type=='csv'){
            return [text, file_name];   
        }else{
            return text
        } 
    },

    /*-- Keeps control of the synchronization between GUI components --*/
	triggerSelected: function(item, checked){
		// Unhides checked lines on the linechart
		if (checked){
			d3.selectAll("#st_"+item.get('serieskey')).attr('class','stline');
			d3.selectAll("#hg_"+item.get('serieskey')).attr('class','hgline');
			d3.selectAll("#feat_"+item.get('station')).attr('class',ctPublicController.stationCss);
			d3.selectAll("#id_"+item.get('station')).attr('class','idpoint');
			d3.selectAll("#rp_"+item.get('serieskey')).attr('class','rfpoint');
			var addedRecord = ctPublicController.hiddenRecords.filter(function (el) { return el.record_id == item.data.serieskey; });
			ctPublicController.hiddenRecords = ctPublicController.hiddenRecords.filter(function (el) { return el.record_id != item.data.serieskey; });
			ctPublicController.visibleRecords.push(addedRecord[0]);
			ctPublicController.reScaleChart();
		}
		
		// Hides lines on the linechart
		else {
			if( item != null ){
				d3.selectAll("#lb_"+item.get('serieskey')).attr('class','hidden');
				d3.selectAll("#st_"+item.get('serieskey')).attr('class','hidden');
				d3.selectAll("#hg_"+item.get('serieskey')).attr('class','hidden');
				d3.selectAll("#feat_"+item.get('station')).attr('class','hidden');
				d3.selectAll("#id_"+item.get('station')).attr('class','hidden');
				d3.selectAll("#rp_"+item.get('serieskey')).attr('class','hidden');
				var removedRecord = ctPublicController.visibleRecords.filter(function (el) { return el.record_id == item.data.serieskey; });
				ctPublicController.hiddenRecords.push(removedRecord[0]);
				ctPublicController.visibleRecords = ctPublicController.visibleRecords.filter(function (el) { return el.record_id != item.data.serieskey; });
				ctPublicController.reScaleChart();
			}		
		};
	},

	/*-- Called when the view initializes. --*/
	init: function(){
		ctPublicController = this;
		//ctPublicController.loadElements();
		this.control({
   			'public_yearlygrid': {
   				itemmouseenter: function(grid, item){
					if (item.get('enable')) {
						ctPublicController.highlightSvg(item.get('serieskey'));
					};
				},
				itemmouseleave: function(grid, item){
					if (item.get('enable')) {
						ctPublicController.maskSvg(item.get('serieskey'));
					};
				}
   			}
   		})
	},

	onAfterRender: function() {
        ctPublicController.loadElements();
    },

	/*-- Load elements --*/
	loadElements: function(){
		// Refreshes Grid
		this.refreshGrid();

		// Loads map details and generates map
		Ext.Ajax.request({
			//url: '../../../app/sources/crosstabs_public/app/scripts/bounds.py',
			url: '../app/app/scripts/public/bounds.py',
			disableCaching: false,
			method: 'GET',
			success: function(response){
				ctPublicController.mapBounds = response.responseText; 
				ctPublicController.generateMap();
				ctPublicController.drawLayers();
			},
			failure: function(response){
				console.log('error: '+response.status)
			}
		})
	},

	/*
	** Map 
	*/
	/*-- Generates map --*/
	generateMap: function(){
		var mapExtent = new OpenLayers.Bounds(this.mapBounds).transform(this.wgs, this.sm);
		
		var mapOptions = {
			controls:[],
			projection: this.sm,
			displayProjection: this.wgs,
			units: "m",
			maxExtent: mapExtent
	    };

	    this.navMap = new OpenLayers.Map("mapPanel-body", mapOptions);
	   
	    this.navMap.addControl(new OpenLayers.Control.MousePosition({numDigits:4}));
	    this.navMap.addControl(new OpenLayers.Control.Navigation());
	    this.navMap.addControl(new OpenLayers.Control.Zoom());
	    this.navMap.allOverlays = true;

	    arrayOSM = [
			"http://a.tile.openstreetmap.org/${z}/${x}/${y}.png",
			"http://b.tile.openstreetmap.org/${z}/${x}/${y}.png",
			"http://c.tile.openstreetmap.org/${z}/${x}/${y}.png"
	    ];

	    var baseOSM = new OpenLayers.Layer.OSM("MapQuest-OSM Tiles", arrayOSM);
	    this.navMap.addLayer(baseOSM);

	    var appCenter = new OpenLayers.LonLat(36.422425, -0.578773).transform(this.wgs, this.sm);
	    this.navMap.setCenter(appCenter, 10);
	},

	/*--  --*/
	panToStation: function(lonlat){
		this.navMap.panTo( lonlat );
	},

	/*-- Generation of vector layers for the various stations --*/
  	drawLayers: function(parameters){
  		parameters = typeof parameters !== 'undefined' ? parameters : '';
  		var stationStore = Ext.getStore('majisys_dds.store.public.Stations');
  		if(stationStore!=null||stationStore!=undefined){
  			stationStore.getProxy().extraParams = {
				parameters: parameters
			};

			stationStore.load();
			
			stationStore.on('load', function(){
				length = stationStore.data.length;
				if(length == 0){
					// No data
					if(ctPublicController.map_layers.length>0){
						for (var i = ctPublicController.map_layers.length - 1; i >= 0; i--) {
							ctPublicController.navMap.removeLayer( ctPublicController.map_layers[i] );
						}
					}
					ctPublicController.map_layers = [];
				}
				else{
					// There's data
					if(ctPublicController.map_layers.length>0){
						for (var i = ctPublicController.map_layers.length - 1; i >= 0; i--) {
							ctPublicController.navMap.removeLayer( ctPublicController.map_layers[i] );
						}
					}

					ctPublicController.map_layers = [];

					ctPublicController.station_data = stationStore.proxy.reader.rawData;

					function projectPoint(x) {
						var point = ctPublicController.navMap.getViewPortPxFromLonLat(new OpenLayers.LonLat(x[0], x[1])
					        .transform(ctPublicController.wgs, ctPublicController.sm)); 
				        return [point.x, point.y];
				    };

				    var bounds = d3.geo.bounds(ctPublicController.station_data);
				    bounds[0][0] = bounds[0][0]-0.1;
				    bounds[0][1] = bounds[0][1]-0.1;
				    bounds[1][0] = bounds[1][0]+0.1;
				    bounds[1][1] = bounds[1][1]+0.1;
				    
				    //-- Generates and updates features
				    function renderFeatures() {
				        var bottomLeft = projectPoint(bounds[0]),
					        topRight = projectPoint(bounds[1]);

				        svg1.attr("width", topRight[0] - bottomLeft[0])
							.attr("height", bottomLeft[1] - topRight[1])
							.style("margin-left", bottomLeft[0] + "px")
							.style("margin-top", topRight[1] + "px");

				        svg3.attr("width", topRight[0] - bottomLeft[0])
							.attr("height", bottomLeft[1] - topRight[1])
							.style("margin-left", bottomLeft[0] + "px")
							.style("margin-top", topRight[1] + "px");

						g1.attr("transform", "translate(" + -bottomLeft[0] + "," + -topRight[1] + ")");
						g3.attr("transform", "translate(" + -bottomLeft[0] + "," + -topRight[1] + ")");
						feature1.attr("transform", function(d) { return "translate(" + projectPoint(d.geometry.coordinates) + ")"; });
						feature3.attr("d", path);
				    };

				    var path = d3.geo.path().projection(projectPoint);

				    var st_overlay = new OpenLayers.Layer.Vector("Gauging Stations");
				    var st_label = new OpenLayers.Layer.Vector("Gauging Stations Labels");
				    ctPublicController.navMap.events.register("moveend", ctPublicController.navMap, renderFeatures);

				    var svg1, svg3;
				    var feature1, feature3;

				    st_label.afterAdd = function(){
						var div = d3.select(Ext.getDom(st_label.div.id));
						div.selectAll("svg").remove();
						svg1 = div.append("svg");
						g1 = svg1.append("g");

						feature1 = g1.selectAll("path")
							.data(ctPublicController.station_data.features)
							.enter().append("text")
							.attr("class", "hidden")
							.attr("id", function(st) { return "id_" + st.properties.station_id; })
							.attr("transform", function(d) { return "translate(" + projectPoint(d.geometry.coordinates) + ")"; })
							.attr("dy", -12)
							.attr("x", -20)
							.text(function(d) { return d.properties.station_id; });
					};

					ctPublicController.navMap.addLayer(st_label);

					st_overlay.afterAdd = function () {
						var div = d3.select(Ext.getDom(st_overlay.div.id));
						div.selectAll("svg").remove();
						svg3 = div.append("svg");
						g3 = svg3.append("g");

						feature3 = g3.selectAll("path")
							.data(ctPublicController.station_data.features)
							.enter().append("path")
							.attr("class","hidden")
							.attr("class","stpoint")
							.attr("id", function(st) { return "feat_" + st.properties.station_id; })
							.attr("d",path.pointRadius(6))
							.on("mouseover", function (d) {
								var feat_id = d3.select(this)[0][0].id;
								var station_name = feat_id.replace("feat_","");

								for (var key in ctPublicController.station_dict) {
								  if (ctPublicController.station_dict.hasOwnProperty(key)) {
								  	if(ctPublicController.station_dict[key] == station_name){
								  		ctPublicController.highlightSvg(key, true);
								  		break;
								  	}
								  }
								}
								// var asdf = ctPublicController.station_dict.findIndex(station_name);
								// console.log(asdf);
								//ctPublicController.highlightSvg(feat_id.replace("feat_",""));
							})
							.on("mouseout", function (d) {
								var feat_id = d3.select(this)[0][0].id;
								var station_name = feat_id.replace("feat_","");
								//ctPublicController.maskSvg(feat_id.replace("feat_",""));

								for (var key in ctPublicController.station_dict) {
								  if (ctPublicController.station_dict.hasOwnProperty(key)) {
								  	if(ctPublicController.station_dict[key] == station_name){
								  		ctPublicController.maskSvg(key);
								  		break;
								  	}
								  }
								}
							});
						renderFeatures();
					};
					ctPublicController.navMap.addLayer(st_overlay);

					ctPublicController.map_layers.push(st_label);
					ctPublicController.map_layers.push(st_overlay);
				}
			});
			stationStore.resumeEvents();
  		}
  	},

  	/*
	** Grid
	*/
	/*-- Creates a set of columns when the filter parameters change --*/
	generateGrid: function(store, columns){
		var summaryFunction, aggFunction = ctPublicController.selectionFunction;
		yearGrid = this.lookupReference('public_yearlygrid');
		
		// Aggregation Label
		var aggLabel = Ext.getCmp('aggchooser').getDisplayValue();

		if (aggFunction == 'count'){
			summaryFunction = 'sum';
		}
		else {
			summaryFunction = aggFunction;
		};

		checkbox_column = {}
		checkbox_column['xtype'] = 'checkcolumn';
		checkbox_column['text'] = '&#10004';
		checkbox_column['summaryRenderer'] = function(){};
		checkbox_column['dataIndex'] = 'enable';
		checkbox_column['listeners'] = {
			checkchange: function(column, recordIndex, checked){
				var observationstore = Ext.getStore('majisys_dds.store.public.Observations');
				var item = observationstore.getAt(recordIndex);
				//var item = Ext.getStore('Observations').getAt(recordIndex);
				ctPublicController.triggerSelected(item, checked);
			}
		}

		columns.unshift( checkbox_column );
		
		for(var index=2;index<columns.length;index++){
			if(index==2){
				columns[index]['summaryRenderer'] = function(){return Ext.String.format('<b>'+aggLabel+'</b>')}
			}
			else{
				columns[index]['renderer'] = yearGrid.formatValues
				if (summaryFunction=='min'){
					columns[index]['summaryType'] = function(v, record, colName, data, rowIdx){
						var min_val = record.filter(function (x) { return x !== null; })
						    .reduce(function (a, b) { return Math.min(a, b); }, Infinity);
						
						if(min_val == Infinity){
							return ''
						}else{
							return Ext.util.Format.number(min_val, ctPublicController.aggFormat)
						}
					}
				}else{
					columns[index]['summaryType'] = summaryFunction
					columns[index]['summaryRenderer'] = function(value, summaryData, dataIndex) {
		               	return Ext.util.Format.number(value, ctPublicController.aggFormat)
		            }
				}
				
			}
		}
		yearGrid.reconfigure( store, columns );
	},

	/*-- Recreates the structure of the grid, and reloads the Observations store --*/
	refreshGrid: function(){
		var observationstore = Ext.getStore('majisys_dds.store.public.Observations');
		var stationStore = Ext.getStore('majisys_dds.store.public.Stations');
		var selectButton = this.lookupReference('selectionButton');
		selectButton.toggle(true);
		var filterParam;
		if( ctPublicController.switchStationType ){
			stationStore.clearFilter();
			stationStore.filter('station_type',ctPublicController.selectionStationType);
			ctPublicController.swithchStationType = false;
			observationstore.each(function(item){
				d3.selectAll("#feat_"+item.get('record_id')).attr('class','hidden');      
			});

			observationstore.getProxy().extraParams = {
				aggregate_type: ctPublicController.selectionFunction,
				parameters: Ext.getCmp('stationtypechooser').value 
			};
			
		}else{
			observationstore.getProxy().extraParams = {
				aggregate_type: ctPublicController.selectionFunction
			};
		}
		observationstore.load();
	},

	/*
	** LineChart
	*/
	/*-- Generates the time series as required for the chart */
	generateSeries: function(dataObservations, columns){
		var data = dataObservations.items;
		if(dataObservations.length == 0){
			alert('The chosen combination of parameters did not produced any result.');
			return null;
		}
		else {
			var dataSeries = '[';
			var stComma = false;
			for(index=0; index<dataObservations.length; index++){
				stComma ? dataSeries += ',' : stComma = true;
				// console.log( data[index].data.serieskey )
				ctPublicController.station_dict[data[index].data.serieskey] = data[index].data.station
				var series = '{ "record_id" : "'+data[index].data.serieskey/*station*/+'", "values" : [';
				var itemComma = false;
				
				for (var i=3; i < columns.length; i++){
					itemComma ? series += ',' : itemComma = true;
					
					var fieldName = columns[i].header;
					var fieldName2 = columns[i].dataIndex;
					var fieldValue = data[index].data[fieldName2];
					
					var date = fieldName.split(" ")
					var dateString = date[0]+"01"+"20"+date[1]
					series += '{ "date" : parseDate("' + dateString + '"), "level" : ' + fieldValue + ' }'
				}

				//If data loaded is less than colours avalailable sequentially use colours
				if(dataObservations.length <= this.seriesColor.length){
					series += '], "color" : "'+ this.seriesColor[index] +'" }';
				}
				// If data loaded is more than colours available randomly use colours and reuse some of them
				else{
					var sColor = Math.floor(Math.random() * 40); // use 'color++' for sequential coloring
					series += '], "color" : "'+ this.seriesColor[sColor] +'" }';	
				}
				
				dataSeries += series;
			}
			dataSeries += ']';
			return dataSeries;
		}
	},

	/*--  Generates the line graph */
	drawChart: function(dataSeries){
		var parseDate = d3.time.format("%b%d%Y").parse; // function required in the 'eval(dataSeries)' to parse the dates
		this.chartRecords = eval(dataSeries); // Converts parseDate functions to proper dates
		this.visibleRecords = eval(dataSeries); // Sets all records as visible records
		this.hiddenRecords = new Array; // Array object for storing hidden records

		var margin = {top: 20, right: 65, bottom: 70, left: 50};
		
		var chartPanel = ctPublicController.lookupReference('chartPanel');
		width = chartPanel.getWidth() - margin.left - margin.right,
		height = chartPanel.getHeight() - margin.top - margin.bottom;
		
		this.x = d3.time.scale().range([0, width]); // Creates the x-axis scale using the width of the chartPanel
		this.y = d3.scale.linear().range([height, 0]); // Creates the y-axis scale using the height of the chartPanel

		// Creates x-Axis using scale (this.x) and orients to the bottom
		this.xAxis = d3.svg.axis()
			.scale(this.x)
			.tickSize(-height)
			.tickPadding(10)
			.tickFormat(d3.time.format("%b %y"))
			.orient("bottom");

		// Creates y-Axis using scale (this.y) and orients to the left
		this.yAxis = d3.svg.axis()
			.scale(this.y)
			.tickSize(-width)
			.tickPadding(5)
			.orient("left");

		// Selects chartPanel-body element in html and adds attributes and values
		this.svgChart = d3.select("#chartPanel-body")
			.append("svg")
			.attr("id", "svgChart")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		var tip = d3.tip()
			.attr('class', 'd3-tip')
			.offset([-5, 0])
			.html(function(d) {
				return ctPublicController.tipEl + ": <span style='color:SteelBlue;'>" + d.level + "</span>";
			})

		this.svgChart.call(tip);

		// d3.extent gets array containing minimum and maximum value
		// domain defines maximum and minimum value to be plotted in the availlable space
		this.x.domain( d3.extent(this.chartRecords[0].values, function(obs) { return obs.date; }) );// Sets the domain for x-axis scale 

		var minVal = d3.min(this.chartRecords, function(s) { return d3.min(s.values, function(v) { return v.level; }); });
		var maxVal = d3.max(this.chartRecords, function(s) { return d3.max(s.values, function(v) { return v.level; }); });
		maxVal = maxVal * 1.05;
		this.y.domain([ minVal, maxVal ]);// Sets the domain for y-axis scale

		// Adds x-axis and chart title to the svg container
		this.svgChart.append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(0," + height + ")")
			.call(this.xAxis)
			.append("text")
			.attr("x", width/2)
			.attr("y", 40)
			.style("text-anchor", "middle")
			.text("Yearly aggregated values");

		// Adds y-axis and chart title to the svg container
		this.svgChart.append("g")
			.attr("class", "y axis")
			.call(this.yAxis);

		/*-- Creates the line --*/
		// d3.svg.line() is used to draw a line
		this.seriesLine = d3.svg.line()
			.defined(function(obs) { return obs.level != ctPublicController.nullValue; })
			.x(function(obs) { return ctPublicController.x(obs.date); })
			.y(function(obs) { return ctPublicController.y(obs.level); });

		this.seriesLineInt = d3.svg.line()
			.defined(function(obs) { return obs.level != ctPublicController.nullValue })
			.interpolate("monotone")
			.x(function(obs) { return ctPublicController.x(obs.date); })
			.y(function(obs) { return ctPublicController.y(obs.level); });

		this.recordPaths = this.svgChart.selectAll(".record")
			.data(this.chartRecords)
			.enter().append("g")
			.attr("class", "record");

		// Handles hiding of lines - I THINK
		this.recordPaths.append("path")
			.attr("class", "hidden")
			.attr("id", function(s) {
				return "hg_"+s.record_id; 
			})
			.attr("d", function(s) { return ctPublicController.seriesLine(s.values); })
			.style("stroke", function(s) { return s.color; })
			.style("stroke-opacity", ".2");

		// Draws line covering points from the data
		this.recordPaths.append("path")
			.attr("class", "stline")
			.attr("id", function(s) { return "st_"+s.record_id; })
			.attr("d", function(s){ return ctPublicController.seriesLine(s.values); })
			.style("stroke", function(s){ return s.color; })
			.on('mouseover', function(e){
				var stId = d3.select(this)[0][0].id;
				ctPublicController.highlightSvg(stId.replace("st_",""));
			})
			.on('mouseout', function(e){
				var stId = d3.select(this)[0][0].id;
				ctPublicController.maskSvg(stId.replace("st_",""));
			});

		this.recordPoints = this.recordPaths.append('g')
			.attr("class","rf-points");

		// Draws circles on points
		this.recordPoints.selectAll("circle")
			.data(function(s){ return s.values; })
			.enter().append('circle')
			.attr("class","rfpoint")
			.attr("id", function(p){ return "rp_"+this.parentNode.__data__.record_id; })
			.attr("r", 2.5)
			.style("display", function(d){ return d.level==null ?"none": null})
			.attr("month",function(p,idx) { return idx; }) 
			.attr("cx", function(p){ return ctPublicController.x(p.date); })
			.attr("cy", function(p){ return ctPublicController.y(p.level); })
			.style("opacity", function(d){ return d.level == 0 ? 0 : 1 })
			.style("fill", function(p) { return this.parentNode.__data__.color; })
			.on('mouseover', function(p){
				if (p.level != ctPublicController.nullValue){
					//ctPublicController.tipEl = ctPublicController.months[this.getAttribute('month')];
					//tip.show(p);
					ctPublicController.highlightSvg(this.parentNode.__data__.record_id);
				};
			})
			.on('mouseout', function(p){
				if (p.level != ctPublicController.nullValue){          
					//tip.hide(p);
					ctPublicController.maskSvg(this.parentNode.__data__.record_id);
				};
			});

		this.recordPaths.append("text")
			.datum(function(s) { return {record_id: s.record_id, value: s.values[s.values.length - 1]}; })
			.attr("transform", function(s) {
				return "translate(" + ctPublicController.x(s.value.date) + "," + ctPublicController.y(s.value.level) + ")";
			})
			.attr("x", 3)
			.attr("dy", ".35em")
			.attr("class", "hidden")
			.attr("id", function(s) { return "lb_"+s.record_id; })
			.text(function(s) { return ctPublicController.station_dict[ s.record_id ]; });
	},

	/*-- Connects the hovering over the multiple interface components */
	highlightSvg: function(id, isMap){
		isMap = typeof isMap !== 'undefined' ? isMap : false;
		d3.selectAll("#st_"+id).attr('class','stline-focus');
		d3.selectAll("#hg_"+id).attr('class','hgline');
		d3.selectAll("#lb_"+id).attr('class','stid');
		d3.selectAll("#feat_"+ctPublicController.station_dict[id]).attr('class','stpoint-focus');
		d3.selectAll("#id_" + ctPublicController.station_dict[id]).attr('class','idpoint');
		
		if(isMap == false){
			var stationStore = Ext.getStore('majisys_dds.store.public.Stations');
			var station_data = stationStore.findRecord('station_id', ctPublicController.station_dict[id]);
			if(station_data != null){
				var coord = station_data.data.geometry.coordinates;
				var lonlat = new OpenLayers.LonLat( coord[0], coord[1] ).transform(ctPublicController.wgs, ctPublicController.sm);
				ctPublicController.panToStation( lonlat );
			}	
		}
		
		var yearGrid = this.lookupReference('public_yearlygrid');
		// Selects and scroll to row with data
		var row_index = yearGrid.getStore().find('serieskey', id);
		if ( row_index >= 0){
			//  view.bufferedRenderer.scrollTo(index);
			yearGrid.getView().bufferedRenderer.scrollTo( row_index );
			yearGrid.getSelectionModel().select(row_index);
		}
	},
	
	maskSvg: function(id){
		d3.selectAll("#st_"+id).attr('class','stline');
		d3.selectAll("#hg_"+id).attr('class','hidden');
		d3.selectAll("#lb_"+id).attr('class','hidden');
		d3.selectAll("#feat_"+ctPublicController.station_dict[id]).attr('class',ctPublicController.stationCss);
		d3.selectAll("#id_" + ctPublicController.station_dict[id]).attr('class','hidden');

		var yearGrid = this.lookupReference('public_yearlygrid');
		yearGrid.getSelectionModel().deselectAll();
	},

	/*-- Refreshes the scale of the graph when the y domain changes --*/
	reScaleChart: function(){
		if (this.visibleRecords.length > 0){
			var minVal = d3.min(this.visibleRecords, function(s) { return d3.min(s.values, function(v) { return v.level == null ? 0 : v.level; }); });
			var maxVal = d3.max(this.visibleRecords, function(s) { return d3.max(s.values, function(v) { return v.level == null ? 0 : v.level; }); });

			maxVal < 0.1 ? maxVal = 0.1 : maxVal = maxVal*1.02;
			minVal = minVal*.9;
			ctPublicController.y.domain([ minVal, maxVal ]);
		};

		this.svgChart.selectAll(".y.axis")
			.transition()
			.call(this.yAxis);

		this.reDrawChart(Ext.get('smoothBtn').dom.disabled);
	},

	/*-- Refrreshes the scale of the graph when the y domain changes --*/
	reSizeChart: function(){
		var margin = {right: 65, left: 50},
		width = Ext.get('chartPanel-body').getWidth() - margin.left - margin.right;

		ctPublicController.x.range([0,width]);
		d3.selectAll('#svgChart')
			.attr("width", Ext.get('chartPanel-body').getWidth());

		ctPublicController.svgChart.selectAll(".x.axis")
			.transition()
			.call(ctPublicController.xAxis);

		ctPublicController.yAxis
			.tickSize(-width);

		ctPublicController.svgChart.selectAll(".y.axis")
			.transition()
			.call(ctPublicController.yAxis);

		ctPublicController.reDrawChart(Ext.get('smoothBtn').dom.disabled);
		ctPublicController.navMap.updateSize();
	},

	/*-- Redraws the chart with a different interpolation method --*/
	reDrawChart: function(smooth){
		if (smooth){
			Ext.get('smoothBtn').dom.disabled = true;
			Ext.get('origBtn').dom.disabled = false;
			this.recordPaths.selectAll('path')
				.transition().ease('sin-in-out')
				.attr("d", function(s) {
					return ctPublicController.seriesLineInt(s.values);
				});
		} else {
			Ext.get('origBtn').dom.disabled = true;
			Ext.get('smoothBtn').dom.disabled = false;
			this.recordPaths.selectAll('path')
				.transition().ease('sin-in-out')
				.attr("d", function(s) {
					return ctPublicController.seriesLine(s.values);
				});
		};

		this.recordPaths.selectAll("text")
			.transition()
			.attr("transform", function(s) {
				return "translate(" + ctPublicController.x(s.value.date) + "," + ctPublicController.y(s.value.level) + ")";
			});

		this.recordPoints.selectAll("circle")
			.data(function(s){ return s.values; })
			.transition()
			.attr("cx", function(p) { return ctPublicController.x(p.date); })
			.attr("cy", function(p) { return ctPublicController.y(p.level); })
	}
});

