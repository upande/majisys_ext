//Ext.Loader.setPath( "Majisys", "../../../app/sources/webapps/app" );

Ext.define('majisys_dds.view.main.Main', {
    extend: 'Ext.container.Viewport',

    requires: [
        'majisys_dds.view.main.MainController',
        'majisys_dds.view.public.CrossTabsPublic',
        'majisys_dds.view.wrua.CrossTabsWRUA',
        'majisys_dds.view.wrua.home.Home',

        'majisys_dds.view.wrua.timeseries.TimeSeriesKeys',
        
        // WRMA
        'majisys_dds.view.wrma.home.Home',
        'majisys_dds.view.wrma.basedata.Dashboard',
        'majisys_dds.view.wrma.timeseries.TimeSeriesKeys',
        'majisys_dds.view.wrma.visualization.Analysis',
        'majisys_dds.view.wrma.visualization.CrossTabs'
    ],

    controller: 'main',

    itemId: 'mainview',
    id: 'mainview',

    listeners: {
        afterrender: 'onViewRender'
    },
    
    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    padding: 2,

    items: [{
        reference: 'NavigationPanel',
        id: 'navigation_panel',
        xtype: 'panel',
        margin: '1 1 1 1',
        border: false,
        bodyStyle: {
            background: '#32404e'
        },

        glyph: 'xf0c9@FontAwesome',
        title: '<strong>&nbsp;&nbspMAJISYS<strong>',

        header: {
            id: 'mainheader',
            height: 80,
            listeners: {
                click: 'onToggleNavigationSize'
            }
        },

        layout: {
            type: 'hbox',
            align: 'stretchmax',
            animate: true,
            animatePolicy: {
                x: true,
                width: true
            }
        },

        scrollable: 'y',

        border: false,

        dockedItems: [{
            xtype: 'toolbar',
            dock: 'bottom',
            style: {
                background: '#32404e'
            },
            layout: {
                pack: 'center'
            },

            items: [{
                id: 'login_btn',
                xtype: 'button',
                text: 'Login',
                width: 100,
                enableToggle: true,
                toggleHandler: 'onLoginToggle'
            }
            ]
        }]
    }, {
        reference: 'ContentPanel',
        id: 'content_panel',
        flex: 1,
        padding: '2',
        layout: 'card'
    }]
});
