Ext.define('majisys_dds.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',
    btnToggle: true, // Identifies Login button toggle state

    navWidth: 240,

    loggedstate: false, // Is user logged in or not

    listen : {
        controller : {
            '#' : {
                unmatchedroute : 'onRouteChange'
            }
        }
    },

    routes: {
        ':node': 'onRouteChange'
    },

    // Login dialog window
    loginWindow: Ext.create('Ext.window.Window',{
        title: 'Login',
        itemId: 'login',
        height: 180,
        width: 360,
        layout: {
            type: 'fit'
        },
        closeAction: 'hide',
        closable: true,
        draggable: true,
        resizable: false,

        listeners:{
            hide: function(win, eOpts){
                var form = Ext.ComponentQuery.query('form#formId')[0];
                form.reset();
            }
        },

        items: [{
            xtype: 'form',
            id: 'formId',
            itemId: 'formId',
            reference: 'form',
            bodyPadding: 20,
            defaults: {
                xtype: 'textfield',
                anchor: '100%',
                labelWidth: 60
            },
            items: [
                {
                    name: 'user',
                    fieldLabel: 'User',
                    maxLength: 25,
                    msgTarget: 'under',
                    listeners: {
                        afterrender: function(field) {
                            field.focus();
                        }
                    }  
                },
                {
                    inputType: 'password',
                    name: 'password',
                    fieldLabel: 'Password',
                    allowBlank: false,
                    vtype: 'alphanum',
                    minLength: 4,
                    msgTarget: 'under',
                    listeners: {
                        specialkey: function(field, e, options) {
                            if (e.getKey() == e.ENTER){
                                var form = Ext.ComponentQuery.query('form#formId')[0];
                                if( form.isValid() ){
                                    var submitBtn = Ext.ComponentQuery.query('button#submitBtn')[0];
                                    submitBtn.fireEvent('click', submitBtn, e, options);
                                }
                            }
                        }
                    }
                }
            ]
        }],

        dockedItems: [
            {
                xtype: 'toolbar',
                dock: 'bottom',
                items: [
                    {
                        xtype: 'tbfill'
                    },
                    {
                        xtype: 'button',
                        iconCls: 'fa fa-times fa-lg',
                        text: 'Cancel',
                        listeners: {
                            click: function(){
                                var form = Ext.ComponentQuery.query('form#formId')[0];
                                form.reset();
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        formBind: true,
                        iconCls: 'fa fa-sign-in fa-lg',
                        id: 'submitBtn',
                        itemId: 'submitBtn',
                        text: 'Submit',
                        listeners: {
                            click: function(button, e, options){
                                var form = Ext.ComponentQuery.query('form#formId')[0];
                                var login = Ext.ComponentQuery.query('#login')[0];
                                

                                if(form.isValid()){
                                    Ext.get( login.getEl()).mask("Authenticating...Please wait...",'loading' );
                                    mainControl.doLogin();
                                    login.close();
                                    form.reset();
                                    Ext.get(login.getEl()).unmask()
                                }
                                else{
                                    console.log("Invalid form");
                                }
                            }
                        }
                    }
                ]
            }
        ]
    }),

    /*-- Called when application boots --*/
    init: function(){
        mainControl = this;
    },

    /* Called when afterrender signal is sent in view */
    onViewRender: function(){
        if( localStorage.getItem('LoggedIn')==null ){
            mainControl.loggedstate = false;
            mainControl.loadNavigation(init=true, treeStore="BaseNavigationTree");
        }else{
            loginBtn = Ext.getCmp('login_btn');
            mainControl.loggedstate = true;
            loginBtn.setText('Logout');
            var view = localStorage.getItem('View');
            mainControl.loadNavigation(init=true, treeStore=view);
        }
    },

    /* Creates Menu tree using tree store */
    createMenuTree: function(treeStore){
        var me = this,
        store = Ext.getStore(treeStore);
        store.each(function(item){
            if (item.data.route){
                if (item.data.parentId == "root"){
                    me.insertMenuPanel(item.data.route);
                    item.data.rendered = false;
                }
            } else {
                item.collapse();
                item.eachChild(function(childItem){
                    me.insertMenuPanel(childItem.data.route);
                    childItem.data.rendered = false;
                });
            }
        });
        
        var menuTree = Ext.create('Ext.list.Tree', {
            width: this.navWidth,
            ui: 'navigation',
            reference: 'NavigationTreeList',
            id: 'navigation_tree_list',
            store: treeStore,
            expanderOnly: false,
            listeners: {
                'selectionchange': 'onNavigationTreeSelectionChange'
            }
        });
        return menuTree;
    },

    // Changes navigation panel based if user has logged in
    loadNavigation: function(init, treeStore){
        init = typeof init !== 'undefined' ? init : false;
        if(init){
            // Fix for when localstorage has inaccurate data
            if(treeStore!=null && treeStore!='NavigationTree'){
                var navList = this.createMenuTree(treeStore);
            }else{
                var navList = this.createMenuTree('BaseNavigationTree')
                localStorage.clear();
            }
        
            var refs = this.getReferences(),
                navPanel = refs.NavigationPanel,
                store = navList.getStore();

            navPanel.add(navList);
            this.redirectTo(store.getAt(0).data.route);
        }else{
            var treeStore,
                refs = mainControl.getReferences(),
                navList = refs.NavigationTreeList,
                navPanel = refs.NavigationPanel,
                loginBtn = Ext.getCmp('login_btn');

            navList.getStore().each(function(item){
                if (item.data.route){
                    if (item.data.parentId == "root") 
                        Ext.getCmp(item.data.route + '_cnt').destroy();
                } else {
                    item.eachChild(function(childItem){
                        Ext.getCmp(childItem.data.route + '_cnt').destroy();
                    });
                }
            });

            navList.destroy();

            if( !mainControl.loggedstate ){
                loginBtn.setText('Logout');
                //treeStore = 'NavigationTree';
                mainControl.loggedstate = true;
            }else{
                loginBtn.setText('Login');
                treeStore = 'BaseNavigationTree';
                mainControl.loggedstate = false;
            }
            navList = mainControl.createMenuTree(treeStore);
            navPanel.add(navList);
            mainControl.redirectTo('home',true);
        }   
    },


    /* Redirect to login page */
    onLoginToggle: function(btn, btnPressed){
        if (!mainControl.loggedstate){
            mainControl.btnToggle = btnPressed;
            this.loginWindow.show();
        }else{
            this.doLogout();
        }
        
    },


    /* Changes view based on tab selected */
    onRouteChange:function(id){
        this.setActiveView(id);
    },

    /* Sets Active View based on selected tab */
    setActiveView: function(hash){
        var hashTag = !hash ? 'home' : hash;
        var refs = this.getReferences(),
        cntPanel = refs.ContentPanel,
        navList= refs.NavigationTreeList,
        store = navList.getStore(),
        node = store.findNode('route', hashTag),
        activePanel = Ext.getCmp(node.data.route+'_cnt');

        cntPanel.getLayout().setActiveItem(activePanel);
        navList.setSelection(node);
        
        if ( !node.data.rendered ){
            activePanel.add({
                xtype : node.data.viewType,
                title: node.data.title,
                header: {
                    cls: node.data.classColor+'-header-main'
                }
            });
            node.data.rendered = true;
        }
    },

    /*  */
    onNavigationTreeSelectionChange: function(tree, node){
        var refs = this.getReferences(),
        navList = refs.NavigationTreeList,
        store = navList.getStore();

        if (node.get('route')){
            this.redirectTo(node.get('route'));
        }
    },

    /* Inserts Menu Panel */
    insertMenuPanel: function(routeId){
        var cntPanel = this.getReferences().ContentPanel;
        cntPanel.add({
            id: routeId + '_cnt',
            layout: 'fit',    
            listeners: {
                'activate': function(){
                    var cntWindows = Ext.ComponentQuery.query('window[parentContainer=' + this.id + ']');
                    cntWindows.forEach(function(item){
                        item.show();
                    })
                },
                'deactivate': function(){
                    var cntWindows = Ext.ComponentQuery.query('window[parentContainer=' + this.id + ']');
                    cntWindows.forEach(function(item){
                        item.hide();
                    })
                }
            }
        });
    },

    /* Resizes Navigation panel */
    onToggleNavigationSize: function(){
        var refs = this.getReferences(),
        navList = refs.NavigationTreeList,
        navPanel = refs.NavigationPanel,
        collapsing = !navList.getMicro(),
        newWidth = collapsing ? 58 : this.navWidth;

        collapsing ? Ext.getCmp('login_btn').hide() : Ext.getCmp('login_btn').show();
        navList.setMicro(collapsing);
        navPanel.animate({
            to: {
                width: newWidth
            }
        });
    },    

    /*onLogout: function(button, e, options){
        var me = this;
        service =  encodeURIComponent(window.location.href)
        url = "http://localhost/api/logout/?service=" + service
        Ext.Ajax.request({
            url: url,
            scope: me,
            success: 'onLogoutSuccess',
            failure: 'onLogoutFailure'
        });
    },*/

    doLogin: function() {
        var me = this,
        form = me.lookupReference('form');

        service =  encodeURIComponent(window.location.href)
        //url = "http://41.212.6.75:8073/api/login/?service=" + service
        //url = "http://192.168.3.24:8002/api/login/?service=" + service
        url = "http://41.57.108.208:8073//api/login/?service=" + service
        username = form.getValues().user
        password = form.getValues().password


        Ext.Ajax.request({
            method: "POST",
            params: {username:username, password:password},
            dataType: "json",
            cors: true,
            useDefaultXhrHeader : false,
            withCredentials: true,
            url: url,
            scope: me,
            timeout: 60000,
            async: false,
            success: 'onLoginSuccess',
            failure: 'onLoginFailure'
        });
     }, 

    doLogout: function() {
        var me = this,
            service =  encodeURIComponent(window.location.href),
            ticket = localStorage.getItem('Ticket');
            //url = "http://41.212.6.75:8073/api/logout/?service=" + service;
        if(ticket!=null){
            //url = "http://41.212.6.75:8073/api/logout/?service=" + service + "&ticket=" + ticket 
            url = "http://41.57.108.208:8073//api/login/?service=" + service + "&ticket=" + ticket
        }else{
            //url = "http://41.212.6.75:8073/api/logout/?service=" + service
            url = "http://41.57.108.208:8073//api/login/?service=" + service 
        }

        Ext.Ajax.request({
            method: "GET",
            dataType: "json",
            cors: true,
            useDefaultXhrHeader : false,
            withCredentials: true,
            url: url,
            timeout: 60000,
            async: false,
            scope: me,
            success: 'onLogoutSuccess',
            failure: 'onLogoutFailure'
        });
    }, 

    onLoginSuccess: function(form, action, login) {
        var ticket = JSON.parse(form.responseText)['ticket'];
        var user = JSON.parse(form.responseText)['user'];

        var view;

        if (user == 'wrua'){
            var metadata = JSON.parse( form.responseText )['metadata'];
            var wrua_name = JSON.parse( metadata.replace( /'/g, '"' ) )['wrua_area'];
            view = 'WRUANavigationTree';
        }
        else if(user == 'wrma'){
            view = 'WRMANavigationTree';
        }

        localStorage.setItem('LoggedIn', true);
        localStorage.setItem('View', view);
        localStorage.setItem('Ticket', ticket);
        
        if(user == 'wrua'){
            localStorage.setItem('Wrua_name', wrua_name);
        }
        mainControl.loadNavigation(false, view); 
    }, 
    
    onLoginFailure: function(form, action) { 
        result = {};
        result.success = false;
        result.msg = "";

        Ext.Msg.show({
            title:'Error!',
                msg: 'Authentication failed. Please recheck credentials',
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });

    },

    onLogoutSuccess: function(form, action, login) {
        var loginBtn = Ext.getCmp('login_btn');
        loginBtn.setText('Login');
        mainControl.loadNavigation();
        localStorage.clear();
    }, 
    
    onLogoutFailure: function(form, action) { 
        result = {};
        result.success = false;
        result.msg = "";

        Ext.Msg.show({
            title:'Error!',
            msg: 'Logout Failed! Contact admin',
            icon: Ext.Msg.ERROR,
            buttons: Ext.Msg.OK
        });
    }
});

