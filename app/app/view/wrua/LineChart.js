Ext.define('majisys_dds.view.wrua.LineChart', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.wrua_linechart',

	requires: [
        'majisys_dds.store.wrua.Stations',
        'majisys_dds.store.wrua.Dates'
    ],

	//Format for aggregate summary except count(sum)
    aggFormat: '00.000',

    // Formats value to include '--' when null is encountered 
    formatValues: function(obsValue){
        if(obsValue == null){
            return '--';
        } else {
            return Ext.util.Format.number(obsValue, this.aggFormat);
        }
    },
	
	initComponent: function(){
		this.items = [{
			xtype: 'panel', // Panel that hosts the line chart
			id: 'chartPanel',
			layout: 'fit',
			padding: 3,
			html: ' <button id="origBtn" class="float-l" type="button" onclick="ctWruaController.reDrawChart(false)" disabled="diabled">Original</button> <button id="smoothBtn" class="float-r" type="button" onclick="ctWruaController.reDrawChart(true)">Smooth</button> '
		}];

		this.dockedItems = [{
			dock: "bottom",
			xtype: "toolbar",

			items: [
				{
					text: '&boxbox;',
					tooltip: 'Layer Switcher',
					menu: {
						items: [
							{
								text: 'Base Map',
								id: 'mapquest_osm_tiles',
								reference: 'mapquest_osm_tiles',
								index: '0',
								checked: true
							},

							{
								text: 'Wruas',
								id: 'wruas',
								reference: 'wruas',
								index: '1',
								checked: true
							},

							{
								text: 'Lake Naivasha',
								id: 'lakenaivasha',
								reference: 'lakenaivasha',
								index: '2',
								checked: true
							}
						]
					}
				},'-',

				{
					xtype: 'tbtext',
			        text: '&nbsp;Display Mode:'
				},
				{
					xtype: 'combo',
			        id: 'filterchooser',
			        reference: 'filterchooser',
			        width: 100,
			        store: [["year","Year-Based"],["station","Station-Based"]],
			        editable: false,
			        value: 'year',
			        queryMode: 'local',
			        triggerAction: 'all',
			        listeners: {
			        	afterrender: function(chooser){
							Ext.getCmp('stationchooser').disable();
							Ext.getCmp('filterchooser').disable();
                        },

                        select: function(chooser){
                        	ctWruaController.filterBy = chooser.getValue();
                        	if (ctWruaController.filterBy == 'year'){
								Ext.getCmp('yearchooser').enable();
								Ext.getCmp('stationchooser').disable();
								Ext.getCmp('timefilter').setValue('monthly');
								ctWruaController.yearFilter = Ext.getCmp('yearchooser').getValue();
								ctWruaController.drawLayers(ctWruaController.yearFilter, "", "");
							} else{

								Ext.getCmp('yearchooser').disable();
								Ext.getCmp('stationchooser').enable();
								Ext.getCmp('timefilter').setValue('yearly');
								ctWruaController.stationFilter = Ext.getCmp('stationchooser').getDisplayValue();
								ctWruaController.drawLayers("", "", Ext.getCmp('stationchooser').getValue() );
							}
							ctWruaController.refreshGrid();
                        }
			        }
				},

				{
					xtype: 'tbtext',
			        text: '&nbsp;Select Station:'
				},
				{
					xtype: 'combo',
                    width: 130,
                    allowBlank:false,
                    store: {
                        type: 'wrua_stations'
                    },
                    displayField: 'station_id',
                    valueField: 'id',
                    id: 'stationchooser',
                    reference: 'stationchooser',
                    queryMode: 'local',
                    editable: false,
                    //selectOnFocus: true,
                    triggerAction: 'all',
                    listeners:{
                    	afterrender: function(){
                    		// Station selection Combo box
							var cmb_station = Ext.getCmp("stationchooser");
							var stationStore = Ext.getStore('wrua_stations');
							var service_name = window.location.href;
							cmb_station.getStore().load({
								params:{
									wrua: localStorage.getItem('Wrua_name'),
									ticket: localStorage.getItem('Ticket'),
									service: service_name.slice(0, service_name.indexOf("#"))
								}
							});

							cmb_station.getStore().on(
							    "load",function() {
							    	length = cmb_station.getStore().data.length
							    	if(length > 0){
							    		var data = cmb_station.getStore().getAt(0).get('id');
								    	cmb_station.setValue( data );
							    	}

							    	Ext.getCmp('filterchooser').enable();
							    }
							);
                    	},

                    	select: function(chooser){
                    		ctWruaController.stationFilter = Ext.getCmp('stationchooser').getDisplayValue();
                    		ctWruaController.refreshGrid();
                    		ctWruaController.drawLayers("", "", Ext.getCmp('stationchooser').getValue());
                    	}
                    }
				},

				{
					xtype: 'tbtext',
			        text: '&nbsp;Select Year'
				},
				{
					xtype: 'combo',
			        id: 'yearchooser',
			        reference: 'yearchooser',
			        width: 100,
			        forceselection: false,
			        store: {
			        	type: 'wrua_dates'
			        },
			        displayField: 'date_id',
                    valueField: 'date_name',
			        queryMode: 'local',
			        editable: false,
			        triggerAction: 'all',
			        listeners: {
			        	afterrender: function(){
			        		// Year selection Combo box
							var cmb_year = Ext.getCmp("yearchooser");
							cmb_year.getStore().on(
							    "load",function() {
							    	length = cmb_year.getStore().data.length
							    	if(length > 0){
							    		var data = cmb_year.getStore().getAt(0).get('date_id')
							    		cmb_year.setValue( data );
							    		ctWruaController.drawLayers(data, "", "");
							    	}
							    }
							);
			        	},

			        	select: function(){
			        		if(ctWruaController.combofix){
			        			ctWruaController.yearFilter = Ext.getCmp('yearchooser').getValue();
				        		ctWruaController.refreshGrid();
				        		ctWruaController.combofix = false;
				        		ctWruaController.drawLayers(ctWruaController.yearFilter, "", "");	
			        		}
			        		
			        	},

			        	change: function (field, newValue, oldValue) {
				            ctWruaController.combofix = true;
				        }
			        }
				},

				{
					xtype: 'tbfill'
				},
				{
					xtype: 'button',
					id: 'csvButton',
					reference: 'csvButton',
					text: 'Generate CSV',
					handler: function(){
						ctWruaController.csvWindow.show();
						Ext.getElementById('csv-div').innerHTML = '';
				        Ext.getElementById('csv-div').innerHTML = ctWruaController.generateCSVData('html');
					}
				}
			]
		}];

		this.callParent(arguments);
	}
});