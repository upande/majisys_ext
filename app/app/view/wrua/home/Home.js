Ext.define('majisys_dds.view.wrua.home.Home',{
    extend: 'Ext.panel.Panel',
    xtype: 'wruahome',

    requires: [
        'majisys_dds.view.wrua.home.HomeController'
    ],

    id: 'wrua_home_home',
        
    controller: 'wrua-home-home',

    html: '<iframe style="width:100%; height:100%; border:none;" src="app/view/wrua/home/home_html/home_view.html" scrolling="no"></iframe>',
    
    listeners: {
        'afterrender': function(){
            this.getHeader().hide();
        }
    }
});