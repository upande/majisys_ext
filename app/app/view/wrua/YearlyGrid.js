Ext.define('majisys_dds.view.wrua.YearlyGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'wrua_yearlygrid',
    reference: 'wrua_yearlygrid',

    requires: [
        'majisys_dds.store.wrua.Observations',
        'majisys_dds.store.wrua.Parameters'
    ],

    stateful: false,

    store: {
        type: 'wrua_observations'
    },

    viewConfig: {
        stripeRows:false,
        markDirty: false
    },

    columnLines: true, // Adds line to show column
    enableColumnHide: false, //  Disables column hiding within the grid

    // Display aggregation summary below column headers
    features: [{
        ftype: 'summary',
        dock: 'top', 
        id: 'f-summary'
    }],

    //Format for aggregate summary except count(sum)
    aggFormat: '00.000',

    // Formats value to include '--' when null is encountered 
    formatValues: function(obsValue){
        if(obsValue == null){
            return '--';
        } else {
            return Ext.util.Format.number(obsValue, this.aggFormat);
        }
    },

    initComponent: function(){
        // Task bar items
        this.dockedItems = [{
            dock: 'bottom',
            xtype: 'toolbar',

            items: [
                {
                    xtype: 'button',
                    id: 'selectionButton',
                    reference: 'selectionButton',
                    tooltip: 'Press to select or unselect all records',
                    text: 'Unselect All',
                    pressed: true,
                    enableToggle: true,
                    toggleHandler: function(button, pressed){ // Handles the toggling between Select/Unselect
                        var observationstore = Ext.getStore('majisys_dds.store.wrua.Observations');

                        // Suspends events in Observations store
                        observationstore.suspendEvents();
                        
                        // Iterates over data in Observations store
                        observationstore.each(function(item){
                            item.set('enable', pressed);

                            if(pressed){
                                d3.selectAll("#st_"+item.get('station')).attr('class','stline');
                                d3.selectAll("#feat_"+item.get('station')).attr('class','stpoint');
                                d3.selectAll("#rp_"+item.get('station')).attr('class','rfpoint');
                            }else{
                                d3.selectAll("#st_"+item.get('station')).attr('class','hidden')
                                d3.selectAll("#feat_"+item.get('station')).attr('class','hidden');
                                d3.selectAll("#rp_"+item.get('station')).attr('class','hidden');
                            }
                        });

                        if (pressed){
                            ctWruaController.visibleRecords = ctWruaController.chartRecords.filter(function(el){
                                return el.record_id != null;
                            });
                            ctWruaController.hiddenRecords = new Array;
                            ctWruaController.reScaleChart();
                        } else {
                            ctWruaController.hiddenRecords = ctWruaController.chartRecords.filter(function(el){
                                return el.record_id != null;
                            });
                            ctWruaController.visibleRecords = new Array;
                        };

                        // Resume events in Observations store
                        observationstore.resumeEvents();

                        // Refreshes grid view. Sets the sort state and focus
                        // to the previously focused row
                        var yearGrid = ctWruaController.lookupReference('wrua_yearlygrid');
                        yearGrid.getView().refresh();

                        // Toggles between text
                        pressed ? button.setText('Unselect All') : button.setText('Select All');
                        
                    }
                },
                
                {
                    xtype: 'button',
                    text: 'Filter Selected',
                    tooltip: 'Press to remove unselected records from the view',
                    enableToggle: true,
                    toggleHandler: function(button, pressed){
                        var observationstore = Ext.getStore('majisys_dds.store.wrua.Observations');
                        // Toggles between text
                        pressed ? button.setText('Remove Filter') : button.setText('Filter Selected');
                        if (pressed){
                            Ext.getCmp('selectionButton').disable();
                            Ext.getCmp('timefilter').disable();
                            Ext.getCmp('yearchooser').disable();
                            Ext.getCmp('stationchooser').disable();
                            Ext.getCmp('aggchooser').disable();
                            Ext.getCmp('stationtypechooser').disable();
                            Ext.getCmp('filterchooser').disable();

                            //ctWruaController.getObservationsStore().filter('enable',true);
                            observationstore.filter('enable',true);
                        }else{
                            Ext.getCmp('selectionButton').enable();
                            Ext.getCmp('timefilter').enable();
                            Ext.getCmp('aggchooser').enable();
                            Ext.getCmp('stationtypechooser').enable();
                            Ext.getCmp('filterchooser').enable();
                            if (Ext.getCmp('filterchooser').getValue() == 'year'){
                              Ext.getCmp('yearchooser').enable();
                            } else {
                              Ext.getCmp('stationchooser').enable();
                            };
                            //ctWruaController.getObservationsStore().clearFilter();
                            observationstore.clearFilter();
                        }
                    }
                },'-',

                {
                    xtype: 'tbtext',
                    text: '&nbsp;Filter By:&nbsp;'
                },
                {
                    xtype: 'combo',
                    store: [["daily","Day"],["monthly","Month"],["yearly","Year"]],
                    id: 'timefilter',
                    reference: 'timefilter',
                    value: 'monthly',
                    editable: false,
                    width: 80,
                    queryMode: 'local',
                    //selectOnFocus: true,
                    triggerAction: 'all',
                    listeners:{
                        beforeselect: function(chooser, record, index) {
                            if(Ext.getCmp('filterchooser').getValue() == 'year'){
                                if(index == 2) {
                                    return false;
                                }
                            }
                            else if(Ext.getCmp('filterchooser').getValue() == 'station'){
                                if(index < 2){
                                    return false;
                                }
                            }
                        },

                        select: function(chooser){
                            ctWruaController.refreshGrid();
                        }
                    }
                },
                
                {
                    xtype: 'tbtext',
                    text: '&nbsp;Aggregate:&nbsp;'
                },
                
                {
                    xtype: 'combo',
                    store: [["average","Average"],["sum","Total"],["min","Minimum"],["max","Maximum"],["count","Count"]],
                    id: 'aggchooser',
                    reference: 'aggchooser',
                    value:'average',
                    editable: false,
                    queryMode: 'local',
                    triggerAction: 'all',
                    width: 100,
                    listeners: {
                        select: function(chooser){
                            ctWruaController.selectionFunction = chooser.getValue();
                            ctWruaController.refreshGrid();
                        }
                    }
                },


                {
                    xtype: 'tbtext',
                    text: '&nbsp;Parameter:&nbsp;'
                },

                {
                    xtype: 'combo',
                    width: 170,
                    store: {
                        type: 'wrua_parameters'
                    },
                    value:'',
                    editable: false,
                    displayField: 'parameter_name',
                    valueField: 'parameter_id',
                    id: 'parametertypechooser',
                    reference: 'parametertypechooser',
                    queryMode: 'local',
                    triggerAction: 'all',
                    listeners: {
                        afterrender: function(){
                            // Parameters selection Combo box
                            var cmb_parameters = Ext.getCmp("parametertypechooser");
                            var parameterStore = Ext.getStore('wrua_parameters');
                            var service_name = window.location.href;
                            
                            cmb_parameters.getStore().load({
                                params:{
                                    ticket: localStorage.getItem('Ticket'),
                                    service: service_name.slice(0, service_name.indexOf("#"))
                                }
                            });

                            cmb_parameters.getStore().on(
                                "load",function() {
                                    length = cmb_parameters.getStore().data.length

                                    if(length > 0){
                                        var data = cmb_parameters.getStore().getAt(0).get('parameter_id');
                                        cmb_parameters.setValue( data );
                                    }
                                }
                            );
                        },
                        select: function(chooser){
                            ctWruaController.refreshGrid();
                            if(Ext.getCmp('stationtypechooser').getValue() != ''){
                                ctWruaController.drawLayers( "", Ext.getCmp('stationtypechooser').getValue(), "" )
                            }else{
                                if(Ext.getCmp('filterchooser').getValue() == 'year'){
                                    ctWruaController.drawLayers( Ext.getCmp('yearchooser').getValue(), "", "" )
                                }else{
                                    ctWruaController.drawLayers( "", "", Ext.getCmp('stationchooser').getValue() )
                                }
                            }
                            
                        }
                    }
                }
            ]
        }]
        this.callParent(arguments);
    }
});