Ext.define('majisys_dds.view.wrua.timeseries.PeriodForm', {
    extend: 'Ext.form.Panel',
    xtype: 'periodform',

    requires: [
        'majisys_dds.view.wrua.timeseries.FormsController'
    ],

    controller: 'timeseries-formscontroller',  

    startdate:  null,
    enddate:    null,
    d1:         null,
    d2:         null,
    d3:         null,
    d0:         null,

    bodyPadding: 15,

    items: [{
        xtype: 'fieldset',
        title: '&nbsp;Period:&nbsp;',
        defaults: {
            anchor: '100%',
            allowBlank: false,
            msgTarget: 'side'
        },
        items:[{
            xtype:      'radiogroup',
            columns:    1,
            itemId:     'period',

            items: [{
                xtype:      'radiofield',
                boxLabel:   'rb_1',
                inputValue: 'd1',
                name:       'periodspec'
            },{
                xtype:      'radiofield',
                boxLabel:   'rb_2',
                inputValue: 'd2',
                name:       'periodspec',
                checked:    true
            },{
                xtype:      'radiofield',
                boxLabel:   'rb_3',
                inputValue: 'd3',
                name:       'periodspec'
            },{
                xtype:      'radiofield',
                boxLabel:   'Specific Range',
                inputValue: 'custom',
                name:       'periodspec',
                listeners: {
                    'change': function(field){
                        this.nextSibling().setDisabled(!this.getValue());
                    }
                }
            },{
                xtype:      'fieldset',
                layout:     'vbox',
                margin:     '5 0 5 22',
                padding:    '8 0 0 5',
                disabled:   true,
                style: {
                    border: '1px solid lightgrey'
                },
                fieldDefaults: {
                },
                items:[{
                    name:       'startdate',
                    xtype:      'datefield',
                    labelWidth: 70,
                    format:     'd-M-Y',
                    fieldLabel: 'Start Date:',
                    emptyText:  'Select the start date',
                    validator: function(){
                        this.up('periodform').startdate = this.getValue();
                        this.nextSibling().validate() /* find the enddate-datefield and trigger its validate() */
                        return true;
                    }
                },{
                    name:       'enddate',
                    xtype:      'datefield',
                    labelWidth: 70,
                    format:     'd-M-Y',
                    fieldLabel: 'End Date:',
                    emptyText:  'Also the end date',
                    validator: function(){
                        this.up('periodform').enddate = this.getValue();
                        errMsg = "Should be later than or equal to " + Ext.Date.format(this.up('periodform').startdate,'d-M-Y');
                        return (this.up('periodform').startdate <= this.up('periodform').enddate) ? true : errMsg;
                    }
                }]
            },{
                xtype:      'radiofield',
                boxLabel:   'From the begining of time',
                inputValue: 'all',
                name:       'periodspec',
                padding:    '0 0 -15 0'
            }]
        }]
    }],

    dockedItems : [{
        xtype:        'toolbar',
        dock:         'bottom',
        style: {
            background: '#e0ebf3'
        },
        layout: {
            pack: 'center'
        },
        items: [{
            xtype:      'button',
            id:         'pf-action_button',
            tooltip:    'action',
            iconCls:    '',
            text:       'action',
            width:      100,
            handler:    'executeSpecialAction'
        },' ',{
            xtype:      'button',
            id:         'pf-close_button',
            tooltip:    'Close this window',
            text:       'Close',
            width:      100,
            handler:    function(){
                this.up('timeseriesselectionwindow').close();
            }
        }]
    }],

    listeners: {
        'beforerender': 'initialisePeriodForm'
    }

});