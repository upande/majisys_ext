Ext.define('majisys_dds.view.wrua.timeseries.TimeSeriesKeys', {
    extend: 'Ext.panel.Panel',
    xtype: 'timeserieskeys',

    requires: [
        'majisys_dds.view.wrua.timeseries.TimeSeriesKeysController',
        'majisys_dds.view.wrua.timeseries.SeriesGrid'
    ],

    controller: 'timeseries-timeserieskeys',
    
    id: 'timeseries_keys',
    layout: 'border',
    bodyPadding: 10,
    
    items: [{
        id: 'tsk-chart_panel',
        title: 'Chart',
        header: {
            cls: 'blue-header'
        },
        collapsedCls: 'blue-header',

        region: 'south',
        layout: 'fit',
        height: 300,
        minHeight: 200,

        split: true,
        collapsible: true,
        collapsed: true,
        floatable: false,
        titleCollapse: true,

        listeners: {
            'collapse': 'onChartpanelCollapse',
            'expand': 'onChartpanelExpand',
            'resize': 'onChartPanelResize'
        }
    },{
        id: 'tsk-grid_panel',
        minHeight: 250,
        region: 'center',
        xtype: 'seriesgrid'
    }],

    listeners: {
        'afterrender': 'onViewRendered',
        'afterlayout': 'onViewShow'
    }
});