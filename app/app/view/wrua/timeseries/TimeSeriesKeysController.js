Ext.define('majisys_dds.view.wrua.timeseries.TimeSeriesKeysController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.timeseries-timeserieskeys',

    requires: [
        'majisys_dds.util.Chart',
        'majisys_dds.view.wrua.timeseries.SelectionWindow',
        'majisys_dds.view.wrua.timeseries.PeriodForm',
        'majisys_dds.view.wrua.timeseries.ImportForm'
    ],

    graph: null,
    key:   null,

    /* --- */
    onViewShow: function(){
        var viewMessage = majisys_dds.app.getApplication().viewMessage;
        if (viewMessage.value == 'dataentry|timeseries'){
            Ext.getStore('series_keys').loadPage(1);
            viewMessage.value = null;
            viewMessage.params = null;
        }
    },

    /* --- */
    onViewRendered: function(){
        this.graph = Ext.create('majisys_dds.util.Chart',{
            panelId: 'tsk-chart_panel'
        });
        this.graph.clsPrefix = 'tsk';
    },

    /* --- */
    onCheckChange: function(checkbox, rowIndex, checked, eOpts){
        var graphExpanded = !this.graph.panelCmp.collapsed;

        if (graphExpanded){
            var store = Ext.getStore('series_keys');
            if (checked){
                this.key = store.data.items[rowIndex].data.serieskey;
                this.graph.addSeries(this.key,rowIndex.toString(),this.gridLegend);
            } else {
                this.key = store.data.items[rowIndex].data.serieskey;
                store.getAt(rowIndex).set('seriescolor','');
                this.graph.removeSeries(this.key);
            }
        }
    },

    /* --- */
    onParameterSelect: function(chooser){
        var store = Ext.getStore('series_keys');
        store.getProxy().extraParams = {
            stationid: Ext.getCmp('tsk-station_chooser').getValue(),
            parameterid: chooser.getValue(),
            location: localStorage.getItem('Wrua_name')
        };
        store.loadPage(1);
        var graphExpanded = !this.graph.panelCmp.collapsed;
        if (graphExpanded){
            this.onChartpanelCollapse();
        }
        if (chooser.getValue() == 'all')
            chooser.setValue('');
    },

    /* --- */
    onStationSelect: function(chooser){
        var store = Ext.getStore('series_keys');
        store.getProxy().extraParams = {
            stationid: chooser.getValue(),
            parameterid: Ext.getCmp('tsk-parameter_chooser').getValue(),
            location: localStorage.getItem('Wrua_name')
        };
        store.loadPage(1);
        var graphExpanded = !this.graph.panelCmp.collapsed;
        if (graphExpanded){
            this.onChartpanelCollapse();
        }
        if (chooser.getValue() == 'All Stations')
            chooser.setValue('');
    },

    /* --- */
    onRowDblClick: function(grid, record, el, rowIndex, eOpts){
        record.set('selected',!record.data.selected);
        Ext.getCmp('tsk-selection_column').fireEvent('checkchange');
    },

    /* --- */
    onChartpanelCollapse: function(){
        Ext.getCmp('tsk-graph_button').toggle(false);
        this.plotGraph(false);
    },

    /* --- */
    onChartpanelExpand: function(){
        Ext.getCmp('tsk-graph_button').toggle(true);
        this.plotGraph(true);
    },

    /* --- */
    onGraphButtonToggle: function(button, pressed){
        if(pressed)
            Ext.getCmp('tsk-chart_panel').expand();
        else
            Ext.getCmp('tsk-chart_panel').collapse();
    },

    /* --- */
    onChartPanelResize: function(panel){
        if (!panel.collapsed){
            this.graph.resize();
        }
    },

    /* --- */
    onSpecialButtonClick: function(btn){
        var keys = this.getKeys(),
            message = '';

        if (keys.nr == 0){
            message = 'Select at least one time series to work with.';
        } else if (keys.nr > 1 && btn.params.action == 'edt'){
            message = 'You can only edit one time series at a time';
        }

        if (message == ''){
            Ext.create({
                xtype: 'timeseriesselectionwindow',
                title: btn.params.windowTitle,
                items:[{
                    xtype:  'periodform',
                    width:  350,
                    height: 410,
                    params: btn.params,
                    keys:   keys
                }],

                constrainTo: Ext.getCmp('content_panel').getEl()
            });
        } else {
            Ext.Msg.alert('Notification', '<br />&nbsp;&nbsp;&nbsp;' + message + '&nbsp;&nbsp;&nbsp;<br />&nbsp;');
        }
    },

    /* --- */
    onImportButtonClick: function(btn){
        var keys = this.getKeys();

        if (keys.nr == 1) {
            var store = Ext.getStore('series_keys');
            btn.params.locationid = store.getById(keys.ids).data.locationid;
            btn.params.parameterid = store.getById(keys.ids).data.parameterid;
            Ext.create({
                xtype: 'timeseriesselectionwindow',
                title: btn.params.windowTitle,
                items:[{
                    xtype:  'importform',
                    width:  560,
                    height: 570,
                    params: btn.params,
                    keys:   keys
                }],

                constrainTo: Ext.getCmp('content_panel').getEl()
            })
        } else {
            Ext.create({
                xtype: 'timeseriesselectionwindow',
                title: btn.params.windowTitle,
                items:[{
                    xtype:  'importform',
                    width:  560,
                    height: 570,
                    params: btn.params,
                    keys:   keys
                }],

                constrainTo: Ext.getCmp('content_panel').getEl()
            })
        }
    },

    /* --- */
    getKeys: function(){
        var keys = '',
            nr = 0,
            store = Ext.getStore('series_keys');
        for (i = 0; i < store.data.items.length; ++i)
            if (store.data.items[i].data.selected){
                if (nr > 0)
                    keys += ',';
                keys += store.data.items[i].data.serieskey;
                ++nr;
            }
        return {'ids':keys ,'nr':nr}
    },

    /* --- */
    gridLegend: function(seriesList){
        var record;
        for (i=0; i<seriesList.length; i++){
            record = Ext.getCmp('tsk-grid_panel').getStore().getAt(Number(seriesList[i].index));
            record.set('seriescolor',seriesList[i].color);
        }
    },

    /* --- */
    plotGraph: function(show){
        var store = Ext.getStore('series_keys');
        if (show){ /* graph is appearing */
            var series = '',
                indexes = '';
            for (i = 0; i < store.data.items.length; ++i)
                if (store.data.items[i].data.selected) {
                    if (series != '')
                        series += ',';
                    series += store.data.items[i].data.serieskey;
                    if (indexes != '')
                        indexes += ',';
                    indexes += i.toString();
                }
            if (series != '')
                this.graph.addSeries(series,indexes,this.gridLegend);
        } else { /* graph is hiding */
            for (i = 0; i < store.data.items.length; ++i)
                if (store.data.items[i].data.selected)
                    store.data.items[i].set('seriescolor','');;
            this.graph.reset();
        }
    }
});