Ext.define('majisys_dds.view.wrua.CrossTabsWRUA',{
    extend: 'Ext.tab.Panel',
    xtype: 'crosstabswrua',

    requires: [
	    // CrossTabsWRUA ViewController
	    'majisys_dds.view.wrua.CrossTabsWRUAController',
	    
	    // CrossTabsWRUA CrossTab
	    'majisys_dds.view.wrua.YearlyGrid',
	    'majisys_dds.view.wrua.LineChart',
	    'majisys_dds.view.wrua.MapDisplay'
    ],

    controller: 'crosstabswrua',

    listeners : {
        scope       : 'controller',
        afterrender : 'onAfterRender'
    },

    //title: '&nbsp;&nbsp;' + localStorage.getItem('Wrua_name') + '- AGGRGATED TIME SERIES DISPLAY&nbsp;&nbsp;',
    tabPosition: 'bottom',
    bodyBorder: true,

    items: [{
    	name: 'main-panel',
        title: '&nbsp;&nbsp;Aggregated Values&nbsp;&nbsp;',
        layout: 'border',
        padding: 2,
        items: [{
            /* Navigation View */
            xtype: 'mapdisplay',
            id: 'map-display-panel',
            reference: 'map-display-panel',
            region: 'center',
            margins: '2 -2 2 2',
            minWidth: 200,
            layout: 'fit',
            border: true
        },{
            /* CrossTabs View */
            xtype: 'panel',
            region: 'east',
            split: true,
            id: 'print-panel',
            reference: 'print',
            margin: '2 2 2 0',
            width: 850,
            minWidth: 750,
            border: false,
            layout: 'border',
            items: [{
                /* Graph View */
                xtype: 'wrua_linechart',
                layout: 'fit',
                region: 'north',
                split: true,
                height: 300,
                padding: 0
            },{
                /* Grid View */
                xtype: 'wrua_yearlygrid',
                region: 'center',
                split: true,
                margin: '2 0 0 0'
            }]
        }]
    }]
});