Ext.define('majisys_dds.view.wrua.MapDisplay', {
	extend: 'Ext.panel.Panel',
	alias : 'widget.wrua_mapdisplay',

    initComponent: function() {
        this.items = [{
            xtype: 'panel', /* Panel that hosts the map */
            id: 'mapPanel',
            reference: 'mapPanel',
            layout: 'fit',
            padding: 3
        }];

        this.callParent(arguments);
    }
});