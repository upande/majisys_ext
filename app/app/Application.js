/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('majisys_dds.Application', {
    extend: 'Ext.app.Application',
    
    name: 'majisys_dds',

    stores: [
        'main.BaseNavigationTree',
        'main.WRUANavigationTree',
        'main.WRMANavigationTree',

        // Public Stores
        'majisys_dds.store.public.Observations',
        'majisys_dds.store.public.Parameters',
        'majisys_dds.store.public.Stations',

        // WRUA Stores
        'majisys_dds.store.wrua.Dates',
        'majisys_dds.store.wrua.Observations',
        'majisys_dds.store.wrua.Parameters',
        'majisys_dds.store.wrua.Stations'
    ],

    bounds: null,

    viewMessage: {
        value: null,
        params: null
    },
    
    launch: function (){
        
        this.checkIfLoggedIn();
        mainApp = this;

        Ext.Ajax.request({
            url: 'api/wrma/bounds.py',
            disableCaching: false,

            success: function(response){
                mainApp.bounds = (eval(response.responseText));
            },
            failure: function(response){
                console.log('error: '+response.status)
            }
        });
        Ext.getDom('loadgif').remove();
    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    },

    getCookie: function(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return null;
    },

    checkIfLoggedIn: function(){
        var ticket = this.getCookie('ticket');
        var category = this.getCookie('category')
        current_domain = window.location.origin

        if (ticket !== null){
             if (category == "WRUA") {
                //Fetch wrua view from server
             }
             else if (category == "WRMA"){
                //Fetch wrma view from server
             }
            treeStore = 'NavigationTree';

        }else{
            console.log("No ticket found");
            treeStore = 'BaseNavigationTree';

        }
    }
});