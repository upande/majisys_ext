#!/usr/bin/python3

import json
import psycopg2
from psycopg2.extras import RealDictCursor
import datetime

import cgi

from dateutil.relativedelta import relativedelta
import datetime
import urllib.request

import datetime

#-- Query Parameters
params = cgi.FieldStorage()

def multi_watertab_scripts():
    aggregate = params.getvalue('aggregate', 'average') # average, total, minimum, maximum, count
    #formattype = params.getvalue('format') # json, jsonp
    req_year = params.getvalue('year', datetime.datetime.now().year)
    datatype = params.getvalue('data', 'yearly') # monthly, daily, yearly
    
    wrua_name = params.getvalue('wrua', 'Wanjohi')
    wrua_name = wrua_name.upper()
    
    station_id = params.getvalue('station', '2GB04')
    dispmode = params.getvalue('dispmode', 'station') # year, station
    parameters = params.getvalue('parameters')

    if dispmode == 'year':
        startdate = params.getvalue( 'startdate', req_year +"-01-01 00:00:00+00:00" )
        enddate = params.getvalue('enddate', req_year +"-12-31 23:59:59.999999+00:00" )


    file = open("../../../api/.credentials/.postgres")
    connection_string = file.readline()
    pg = psycopg2.connect(connection_string)
    query = pg.cursor(cursor_factory=RealDictCursor)

    aggregate_function = ""

    if aggregate == 'min':
        aggregate_function = "Min"
    elif aggregate == 'max':
        aggregate_function = "Max"
    elif aggregate == 'sum':
        aggregate_function = "Sum"
    elif aggregate == 'average':
        aggregate_function = "Avg"
    elif aggregate == 'count':
        aggregate_function = "Count"

    if dispmode == 'year':
        sqldate_range = "'"+startdate+"' AND '"+enddate+"'"#
        date_range = []
        start = datetime.date(int(req_year), 1, 1)
        end = datetime.date(int(req_year), 12, 31)
        
        if datatype == 'monthly':
            while start <= end:
                date_range.append(start)
                start += relativedelta(months=1)
            if parameters is None or parameters is '':
                sqlQuery = "SELECT fews.locations.id, fews.parameterstable.name, \
                        DATE_TRUNC('month', fews.timeseriesvaluesandflags.datetime AT TIME ZONE 'UTC') AS \"date\", \
                        " + aggregate_function + "(fews.timeseriesvaluesandflags.scalarvalue) AS \"aggregate\" \
                    FROM fews.timeseriesvaluesandflags INNER JOIN fews.timeserieskeys ON \
                        (fews.timeseriesvaluesandflags.serieskey = fews.timeserieskeys.serieskey) INNER JOIN fews.locations ON \
                        (fews.timeserieskeys.locationkey = fews.locations.locationkey) RIGHT JOIN geography.wrua AS g ON\
                        (ST_Contains(g.geom, ST_setSRID(ST_MakePoint(fews.locations.x, fews.locations.y),fews.locations.srid))) INNER JOIN fews.parameterstable ON \
                        (fews.timeserieskeys.parameterkey = fews.parameterstable.parameterkey)\
                    WHERE fews.timeseriesvaluesandflags.datetime BETWEEN " + sqldate_range + " AND\
                        g.wrua_name = '"+wrua_name+"'\
                    GROUP BY fews.timeseriesvaluesandflags.serieskey, \
                        fews.parameterstable.name,\
                        DATE_TRUNC('month', fews.timeseriesvaluesandflags.datetime AT TIME ZONE 'UTC'), \
                        fews.locations.id ORDER BY \"date\" ASC"
            else:
                sqlQuery = "SELECT fews.locations.id, fews.parameterstable.name,\
                        DATE_TRUNC('month', fews.timeseriesvaluesandflags.datetime AT TIME ZONE 'UTC') AS \"date\", \
                        " + aggregate_function + "(fews.timeseriesvaluesandflags.scalarvalue) AS \"aggregate\" \
                    FROM fews.timeseriesvaluesandflags INNER JOIN fews.timeserieskeys ON \
                        (fews.timeseriesvaluesandflags.serieskey = fews.timeserieskeys.serieskey) INNER JOIN fews.locations ON \
                        (fews.timeserieskeys.locationkey = fews.locations.locationkey) RIGHT JOIN geography.wrua AS g ON\
                        (ST_Contains(g.geom, ST_setSRID(ST_MakePoint(fews.locations.x, fews.locations.y),fews.locations.srid))) INNER JOIN fews.parameterstable ON \
                        (fews.timeserieskeys.parameterkey = fews.parameterstable.parameterkey)\
                    WHERE fews.timeseriesvaluesandflags.datetime BETWEEN " + sqldate_range + " AND fews.timeserieskeys.parameterkey = " + str(parameters) + " AND\
                        g.wrua_name = '"+wrua_name+"'\
                    GROUP BY fews.timeseriesvaluesandflags.serieskey, \
                        fews.parameterstable.name,\
                        DATE_TRUNC('month', fews.timeseriesvaluesandflags.datetime AT TIME ZONE 'UTC'), \
                        fews.locations.id ORDER BY \"date\" ASC"

            #print(sqlQuery)
            query.execute(sqlQuery)
            result = query.fetchall()
            #print(result)
            result_data = {}
            result_json = {"data": []}
            for temp in result:
                if temp['id'] in result_data:
                    result_data[temp['id']].append({ 'date':temp['date'], 'aggregate':temp['aggregate'], 'parameter': temp['name'] })
                else:
                    result_data[temp['id']] = [{ 'date':temp['date'], 'aggregate':temp['aggregate'], 'parameter': temp['name'] }]

            for key, value in result_data.items():
                temp = {'station': key, 'enable': True}
                
                for x in value:
                    temp['parameter'] = x['parameter']
                    temp[x['date'].strftime("%b_%y").lower()] = x['aggregate']

                result_json["data"].append( temp )
            
            for index, d in enumerate( result_json['data'] ):
                for date in date_range:
                    str_date = date.strftime("%b_%y").lower()
                    if d.get( str_date ) is None:
                        result_json['data'][index][str_date] = None

            result_json["metaData"] = {}
            result_json["metaData"]["successProperty"] = "success"
            result_json["metaData"]["root"] = "data"
            result_json["metaData"]["idProperty"] = "id"
            result_json["metaData"]["fields"] = []

            result_json['metaData']['columns'] = [{'header':'Station','dataIndex':'station'}]#, 'stateId':'station'}]
            result_json['metaData']['columns'].append( {'header':'Parameters','dataIndex':'parameter'} )

            for date in date_range:
                date = date.strftime('%b %y')
                result_json["metaData"]["columns"].append( {'header':date,'dataIndex':date.lower().replace(' ','_')})#,'stateId':date.lower().replace(" ", "_")} )

            for index,fields in enumerate(result_json['metaData']['columns']):
                if index <= 1:
                    temp_data = {'name':fields['dataIndex'],'type':'string'}
                else:
                    temp_data = {'name':fields['dataIndex'],'type':'auto'}
                result_json['metaData']['fields'].append(temp_data)
            print(json.dumps(result_json))
            

        elif datatype == 'daily':
            while start <= end:
                date_range.append(start)
                start += relativedelta(days=1)

            if parameters is None or parameters is '':
                sqlQuery = "SELECT fews.locations.id, fews.parameterstable.name, \
                        DATE_TRUNC('day', fews.timeseriesvaluesandflags.datetime AT TIME ZONE 'UTC') AS \"date\", \
                        " + aggregate_function + "(fews.timeseriesvaluesandflags.scalarvalue) AS \"aggregate\" \
                    FROM fews.timeseriesvaluesandflags INNER JOIN fews.timeserieskeys ON \
                        (fews.timeseriesvaluesandflags.serieskey = fews.timeserieskeys.serieskey) INNER JOIN fews.locations ON \
                        (fews.timeserieskeys.locationkey = fews.locations.locationkey) RIGHT JOIN geography.wrua AS g ON\
                        (ST_Contains(g.geom, ST_setSRID(ST_MakePoint(fews.locations.x, fews.locations.y),fews.locations.srid))) INNER JOIN fews.parameterstable ON \
                        (fews.timeserieskeys.parameterkey = fews.parameterstable.parameterkey)\
                    WHERE fews.timeseriesvaluesandflags.datetime BETWEEN " + sqldate_range + " AND\
                        g.wrua_name = '"+wrua_name+"'\
                    GROUP BY fews.timeseriesvaluesandflags.serieskey, \
                        fews.parameterstable.name,\
                        DATE_TRUNC('day', fews.timeseriesvaluesandflags.datetime AT TIME ZONE 'UTC'), \
                        fews.locations.id ORDER BY \"date\" ASC"
            else:
                sqlQuery = "SELECT fews.locations.id, fews.parameterstable.name, \
                        DATE_TRUNC('day', fews.timeseriesvaluesandflags.datetime AT TIME ZONE 'UTC') AS \"date\", \
                        " + aggregate_function + "(fews.timeseriesvaluesandflags.scalarvalue) AS \"aggregate\" \
                    FROM fews.timeseriesvaluesandflags INNER JOIN fews.timeserieskeys ON \
                        (fews.timeseriesvaluesandflags.serieskey = fews.timeserieskeys.serieskey) INNER JOIN fews.locations ON \
                        (fews.timeserieskeys.locationkey = fews.locations.locationkey) RIGHT JOIN geography.wrua AS g ON\
                        (ST_Contains(g.geom, ST_setSRID(ST_MakePoint(fews.locations.x, fews.locations.y),fews.locations.srid))) INNER JOIN fews.parameterstable ON \
                        (fews.timeserieskeys.parameterkey = fews.parameterstable.parameterkey)\
                    WHERE fews.timeseriesvaluesandflags.datetime BETWEEN " + sqldate_range + " AND fews.timeserieskeys.parameterkey = " + str(parameters) + " AND\
                        g.wrua_name = '"+wrua_name+"'\
                    GROUP BY fews.timeseriesvaluesandflags.serieskey, \
                        fews.parameterstable.name,\
                        DATE_TRUNC('day', fews.timeseriesvaluesandflags.datetime AT TIME ZONE 'UTC'), \
                        fews.locations.id ORDER BY \"date\" ASC"

            query.execute(sqlQuery)
            result = query.fetchall()
            result_data = {}
            result_json = {"data": []}

            for temp in result:
                if temp['id'] in result_data:
                    result_data[temp['id']].append({ 'date':temp['date'], 'aggregate':temp['aggregate'], 'parameter': temp['name'] })
                else:
                    result_data[temp['id']] = [{ 'date':temp['date'], 'aggregate':temp['aggregate'], 'parameter': temp['name'] }]
            #print result_data['2FC19']

            for key, value in result_data.items():
                temp = {'station': key, 'enable': True}
                for x in value:
                    temp['parameter'] = x['parameter']
                    temp[x['date'].strftime("%-d_%b_%y").lower()] = x['aggregate']

                result_json["data"].append(temp)

            for index, d in enumerate( result_json['data'] ):
                for date in date_range:
                    str_date = date.strftime("%-d_%b_%y").lower()
                    if d.get( str_date ) is None:
                        result_json['data'][index][str_date] = None

            result_json["metaData"] = {}
            result_json["metaData"]["successProperty"] = "success"
            result_json["metaData"]["root"] = "data"
            result_json["metaData"]["idProperty"] = "id"
            result_json["metaData"]["fields"] = []

            result_json['metaData']['columns'] = [{'header':'Station','dataIndex':'station'}]#,'stateId':'station'}]
            result_json['metaData']['columns'].append( {'header':'Parameters','dataIndex':'parameter'} )

            for date in date_range:
                date = date.strftime('%-d %b %y')
                result_json["metaData"]["columns"].append( {'header':date,'dataIndex':date.lower().replace(' ','_')} )#, 'stateId':date.lower().replace(' ','_')} )

            for index,fields in enumerate(result_json['metaData']['columns']):
                if index <= 1:
                    temp_data = {'name':fields['dataIndex'],'type':'string'}
                else:
                    temp_data = {'name':fields['dataIndex'],'type':'auto'}
                result_json['metaData']['fields'].append(temp_data)
            print(json.dumps(result_json))

    elif dispmode == 'station':
        date_range = []
        current_year = datetime.datetime.now().year

        start = datetime.date(1931, 1, 1)
        end = datetime.date(current_year, 12, 31)

        sqldate_range = str(current_year) +"-12-31 23:59:59.999999+00:00"

        if datatype == 'yearly':
            while start <= end:
                date_range.append(start)
                start += relativedelta(years=1)

            if parameters is None or parameters is '':
                sqlQuery = "SELECT fews.locations.id, fews.parameterstable.name, \
                            DATE_TRUNC('year', fews.timeseriesvaluesandflags.datetime AT TIME ZONE 'UTC') AS \"date\",\
                            " + aggregate_function + "(fews.timeseriesvaluesandflags.scalarvalue) AS \"aggregate\"\
                        FROM fews.timeseriesvaluesandflags INNER JOIN fews.timeserieskeys ON\
                            (fews.timeseriesvaluesandflags.serieskey = fews.timeserieskeys.serieskey) INNER JOIN fews.locations ON\
                            (fews.timeserieskeys.locationkey = fews.locations.locationkey) RIGHT JOIN geography.wrua AS g ON\
                        (ST_Contains(g.geom, ST_setSRID(ST_MakePoint(fews.locations.x, fews.locations.y),fews.locations.srid))) INNER JOIN fews.parameterstable ON \
                        (fews.timeserieskeys.parameterkey = fews.parameterstable.parameterkey)\
                        WHERE fews.locations.id='" + str(station_id) + "' AND fews.timeseriesvaluesandflags.datetime <= '"+ sqldate_range +"' AND\
                            g.wrua_name = '"+wrua_name+"'\
                        GROUP BY fews.timeseriesvaluesandflags.serieskey,\
                            fews.parameterstable.name,\
                            DATE_TRUNC('year', fews.timeseriesvaluesandflags.datetime AT TIME ZONE 'UTC'),\
                            fews.locations.id ORDER BY \"date\" ASC"
            else:
                sqlQuery = "SELECT fews.locations.id, fews.parameterstable.name, \
                            DATE_TRUNC('year', fews.timeseriesvaluesandflags.datetime AT TIME ZONE 'UTC') AS \"date\",\
                            " + aggregate_function + "(fews.timeseriesvaluesandflags.scalarvalue) AS \"aggregate\"\
                        FROM fews.timeseriesvaluesandflags INNER JOIN fews.timeserieskeys ON\
                            (fews.timeseriesvaluesandflags.serieskey = fews.timeserieskeys.serieskey) INNER JOIN fews.locations ON\
                            (fews.timeserieskeys.locationkey = fews.locations.locationkey) RIGHT JOIN geography.wrua AS g ON\
                        (ST_Contains(g.geom, ST_setSRID(ST_MakePoint(fews.locations.x, fews.locations.y),fews.locations.srid))) INNER JOIN fews.parameterstable ON \
                        (fews.timeserieskeys.parameterkey = fews.parameterstable.parameterkey)\
                        WHERE fews.locations.id='" + str(station_id) + "' AND fews.timeseriesvaluesandflags.datetime <= '"+ sqldate_range +"' AND \
                                fews.timeserieskeys.parameterkey = " + str(parameters) + " AND\
                                g.wrua_name = '"+wrua_name+"'\
                        GROUP BY fews.timeseriesvaluesandflags.serieskey,\
                            fews.parameterstable.name,\
                            DATE_TRUNC('year', fews.timeseriesvaluesandflags.datetime AT TIME ZONE 'UTC'),\
                            fews.locations.id ORDER BY \"date\" ASC"

            result_data = {}
            result_json = {"data": []}
            query.execute(sqlQuery)
            result = query.fetchall()

            for temp in result:
                if temp['id'] in result_data:
                    result_data[temp['id']].append({ 'date':temp['date'], 'aggregate':temp['aggregate'], 'parameter':temp['name'] })
                else:
                    result_data[temp['id']] = [{ 'date':temp['date'], 'aggregate':temp['aggregate'], 'parameter':temp['name'] }]

            for key, value in result_data.items():
                temp = {'station': key, 'enable': True}
                for x in value:
                    temp['parameter'] = x['parameter']
                    temp[x['date'].strftime("%Y").lower()] = x['aggregate']

                result_json["data"].append(temp)

            for index, d in enumerate( result_json['data'] ):
                for date in date_range:
                    str_date = date.strftime("%Y").lower()
                    if d.get( str_date ) is None:
                        result_json['data'][index][str_date] = None

            result_json["metaData"] = {}
            result_json["metaData"]["successProperty"] = "success"
            result_json["metaData"]["root"] = "data"
            result_json["metaData"]["idProperty"] = "id"
            result_json["metaData"]["fields"] = []

            result_json['metaData']['columns'] = [{'header':'Station','dataIndex':'station'}]#, 'stateId':'station'}]
            result_json['metaData']['columns'].append( {'header':'Parameters','dataIndex':'parameter'} )

            for date in date_range:
                date = date.strftime('%Y')
                result_json["metaData"]["columns"].append( {'header':date,'dataIndex':date})#, 'stateId':date} )

            for index,fields in enumerate(result_json['metaData']['columns']):
                if index <= 1:
                    temp_data = {'name':fields['dataIndex'],'type':'string'}
                else:
                    temp_data = {'name':fields['dataIndex'],'type':'auto'}
                result_json['metaData']['fields'].append(temp_data)
            #print result_json
            print(json.dumps(result_json))

def main():
    print("Content-type: application/json\n")
    ticket = params.getvalue('ticket', 'ST-1490953898-CHDuBijfQrl40J6uaYX3ewPeGUGeLAZi')
    service = params.getvalue('service', 'http%3A%2F%2F127.0.0.1%2Fapp%2F')
    cas_url = "http://41.212.6.75:8073/serviceValidate/?"
    
    if ticket is not None and service is not None:
        url_results = urllib.request.urlopen(cas_url+"ticket="+ticket+"&service="+service+"&check=True").read()
        results_json = json.loads(str(url_results, 'utf-8'))
        if results_json['error']=='None':
            multi_watertab_scripts()
        else:
            json_message = json.dumps( [{'error': 'Invalid ticket'}] )
            print(json_message)
    else:
        json_message = json.dumps( [{'error':'No valid parameters were passed'}] )
        print(json_message)

if __name__ == '__main__':
    main()
