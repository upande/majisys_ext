#!/usr/bin/python3

import json
import psycopg2
# from psycopg2.extras import RealDictCursor
import cgi
import urllib.request

#-- Query Parameters
params = cgi.FieldStorage()

def bounds_scripts(): 
	file = open("../../../api/.credentials/.postgres")
	connection_string = file.readline()
	pg = psycopg2.connect(connection_string)

	query = pg.cursor()
	boundsQuery = "SELECT ST_Extent(geom) AS bounds FROM fews.stations;"
	query.execute(boundsQuery)

	result = str(query.fetchall())
	result = result.replace("[('BOX(", '')
	result = result.replace(")',)]", '')
	result = result.replace(' ', ',')

	print(json.dumps([result]))

def main():
	print("Content-type: application/json\n")

	ticket = params.getvalue('ticket')
	service = params.getvalue('service')
	cas_url = "http://41.212.6.75:8073/serviceValidate/?"
    if ticket is not None and service is not None:
        url_results = urllib.request.urlopen(cas_url+"ticket="+ticket+"&service="+service+"&check=True").read()
        results_json = json.loads(str(url_results, 'utf-8'))
        if results_json['error']=='None':
            bounds_scripts()
        else:
            json_message = json.dumps( [{'error': 'Invalid ticket'}] )
            print(json_message)
    else:
        json_message = json.dumps( [{'error':'No valid parameters were passed'}] )
        print(json_message)

if __name__ == '__main__':
 	main()
