#!/usr/bin/python3

import json
import psycopg2
from psycopg2.extras import RealDictCursor

import cgi

from datetime import datetime
from dateutil.relativedelta import relativedelta
from dateutil.rrule import rrule, MONTHLY

import urllib.request

params = cgi.FieldStorage()

def stations_script():
	parameterId =  params.getvalue('parameters')
	stationId = params.getvalue('station')
	year = params.getvalue('year')# datetime.now().strftime('%Y'))
	wrua_name = params.getvalue('wrua')
	wrua_name = wrua_name.upper()

	if year is not None:
		startdate = year +"-01-01 00:00:00+00:00"
		enddate = year +"-12-31 23:59:59.999999+00:00"

	file = open("../../../api/.credentials/.postgres")
	connection_string = file.readline()
	pg = psycopg2.connect(connection_string)
	query = pg.cursor(cursor_factory=RealDictCursor)

	arguments = ''
	stationsQuery = ''

	if parameterId is not None and stationId is not None and year is not None:
		arguments = 'fews.timeserieskeys.parameterkey = '+str(parameterId)+' AND fews.locations.locationkey ='+str(stationId)+' AND fews.timeseriesvaluesandflags.datetime BETWEEN "'+ startdate +'" AND "'+ enddate +'"'
	elif parameterId is None and stationId is not None and year is not None:
		arguments = 'fews.locations.locationkey ='+str(stationId)+' AND fews.timeseriesvaluesandflags.datetime BETWEEN "'+ startdate +'" AND "'+ enddate +'"'
	elif parameterId is not None and stationId is None and year is not None:
		arguments = 'fews.timeserieskeys.parameterkey = '+str(parameterId)+' AND fews.timeseriesvaluesandflags.datetime BETWEEN \''+ startdate +'\' AND \''+ enddate +'\''
	elif parameterId is None and stationId is None and year is not None:
		arguments = 'fews.timeseriesvaluesandflags.datetime BETWEEN \''+ startdate +'\' AND \''+ enddate +'\''

	elif parameterId is not None and stationId is not None and year is None:
		arguments = 'fews.timeserieskeys.parameterkey = '+str(parameterId)+' AND fews.locations.locationkey ='+str(stationId)
	elif parameterId is not None and stationId is None and year is None:
		arguments = 'fews.timeserieskeys.parameterkey = '+str(parameterId)
	elif parameterId is None and stationId is not None and year is None:
		arguments = 'fews.locations.locationkey ='+str(stationId)

	if not arguments:
		stationsQuery = "SELECT 'FeatureCollection' AS type, json_agg(row_to_json(items)) AS features FROM \
		    (SELECT DISTINCT ON(fews.locations.id) 'Feature' AS type, fews.locations.locationkey AS id, row_to_json(( \
		        SELECT list \
		        FROM (SELECT  fews.locations.id AS station_id, \
		        				fews.timeserieskeys.parameterkey AS station_type) AS list )) AS properties, \
		        				st_asgeojson(st_transform(st_setsrid(st_point(fews.locations.x,fews.locations.y),fews.locations.srid),4326)) as geometry \
		    FROM fews.timeseriesvaluesandflags INNER JOIN fews.timeserieskeys ON \
			(fews.timeseriesvaluesandflags.serieskey = fews.timeserieskeys.serieskey) INNER JOIN fews.locations ON \
			(fews.timeserieskeys.locationkey = fews.locations.locationkey) JOIN geography.wrua AS g ON (ST_Contains(g.geom, ST_setSRID(ST_MakePoint(fews.locations.x, fews.locations.y),fews.locations.srid)))\
			WHERE g.wrua_name = '"+wrua_name+"'\
			) AS items"
	else:
		stationsQuery = stationsQuery = "SELECT 'FeatureCollection' AS type, json_agg(row_to_json(items)) AS features FROM \
		    (SELECT DISTINCT ON(fews.locations.id) 'Feature' AS type, fews.locations.locationkey AS id, row_to_json(( \
		        SELECT list \
		        FROM (SELECT  fews.locations.id AS station_id, \
		        				fews.timeserieskeys.parameterkey AS station_type) AS list )) AS properties, \
		        				st_asgeojson(st_transform(st_setsrid(st_point(fews.locations.x,fews.locations.y),fews.locations.srid),4326)) as geometry \
		    FROM fews.timeseriesvaluesandflags INNER JOIN fews.timeserieskeys ON \
			(fews.timeseriesvaluesandflags.serieskey = fews.timeserieskeys.serieskey) INNER JOIN fews.locations ON \
			(fews.timeserieskeys.locationkey = fews.locations.locationkey) JOIN geography.wrua AS g ON (ST_Contains(g.geom, ST_setSRID(ST_MakePoint(fews.locations.x, fews.locations.y),fews.locations.srid)))\
			WHERE "+arguments+" AND g.wrua_name = '"+wrua_name+"') AS items"
	query.execute(stationsQuery)
	result = json.dumps(query.fetchall())

	result = result[1:]
	result = result[:-1]
	result = result.replace('\\','')
	result = result.replace('"[','[')
	result = result.replace(']"',']')
	result = result.replace('"{','{')
	result = result.replace('}"','}')

	print(result)

def main(): 
	print("Content-type: application/json\n")

	ticket = params.getvalue('ticket')
	service = params.getvalue('service')
	cas_url = "http://41.212.6.75:8073/serviceValidate/?"
	
	if ticket is not None and service is not None:
		url_results = urllib.request.urlopen(cas_url+"ticket="+ticket+"&service="+service+"&check=True").read()
		results_json = json.loads(str(url_results, 'utf-8'))
		if results_json['error']=='None':
			stations_script()
		else:
			json_message = json.dumps( [{'error': 'Invalid ticket'}] )
			print(json_message)
	else:
		json_message = json.dumps( [{'error':'No valid parameters were passed'}] )
		print(json_message)

if __name__ == '__main__':
	main()