#!/usr/bin/python3

import json
import psycopg2
from psycopg2.extras import RealDictCursor

import re

import cgi
import urllib.request

params = cgi.FieldStorage()

def parameters_scripts():
	file = open("../../../api/.credentials/.postgres")
	connection_string = file.readline()
	pg = psycopg2.connect(connection_string)

	query = pg.cursor(cursor_factory=RealDictCursor)
	paramsQuery = "SELECT ( \
			SELECT array_to_json(array_agg(row_to_json(d))) \
			FROM ( \
				SELECT fews.parameterstable.name AS parameter_name, \
					fews.parameterstable.parameterkey AS parameter_id \
				FROM fews.parameterstable \
			)d \
		) AS params"

	query.execute(paramsQuery)
	temp_data = '[{"parameter_name":"All","parameter_id":""},'
	result_list = query.fetchall()

	result_data = temp_data + str(result_list[0]['params'])[1:]
	result_data = result_data.replace("'", '"')

	# Replaces double quotes in parameter name with single quotes
	result_data = re.sub("\" ", "' ", result_data)
	result_data = re.sub("[^: || ^,] \"", " '", result_data)

	result_data = [result_data[1:-1]]

	result = {}
	result['params'] = result_data
	result = json.dumps(result)
	result = result.replace('\\', '')
	result = result.replace('"[', '[')
	result = result.replace(']"', ']')
	result = result.replace('"{', '{')
	result = result.replace('}"', '}')

	print(result)

def main():
	print("Content-type: application/json\n")
	ticket = params.getvalue('ticket', 'ST-1490953898-CHDuBijfQrl40J6uaYX3ewPeGUGeLAZi')
	service = params.getvalue('service', 'http%3A%2F%2F127.0.0.1%2Fapp%2F')
	cas_url = "http://41.212.6.75:8073/serviceValidate/?"
	
	if ticket is not None and service is not None:
		url_results = urllib.request.urlopen(cas_url+"ticket="+ticket+"&service="+service+"&check=True").read()
		results_json = json.loads(str(url_results, 'utf-8'))
		if results_json['error']=='None':
			parameters_scripts()
		else:
			json_message = json.dumps( [{'error': 'Invalid ticket'}] )
			print(json_message)
	else:
		json_message = json.dumps( [{'error':'No valid parameters were passed'}] )
		print(json_message)
	
if __name__ == '__main__':
	main()
