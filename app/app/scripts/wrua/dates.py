#!/usr/bin/python3

import json
import psycopg2
from psycopg2.extras import RealDictCursor
import datetime
print("Content-type: application/json\n")

file = open("../../../api/.credentials/.postgres")
connection_string = file.readline()
pg = psycopg2.connect(connection_string)

query = pg.cursor(cursor_factory=RealDictCursor)
yearQuery = "SELECT DISTINCT DATE_TRUNC('year', fews.timeseriesvaluesandflags.datetime AT TIME ZONE 'UTC') AS \"datetimefield\" \
	FROM fews.timeseriesvaluesandflags \
	WHERE fews.timeseriesvaluesandflags.datetime IS NOT NULL AND fews.timeseriesvaluesandflags.datetime <= CURRENT_DATE ORDER BY \"datetimefield\" DESC"

query.execute(yearQuery)
result = query.fetchall()

sql_string = {'datetimefield': []}
now = datetime.datetime.now().year
sql_string['datetimefield'].append( { 'date_id': now, 'date_name': str(now) } )

for year in result:
	year_value = year['datetimefield'].year
	sql_string['datetimefield'].append( { 'date_id': year_value, 'date_name': str(year_value) } )


print(json.dumps(sql_string))
