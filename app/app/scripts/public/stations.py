#!/usr/bin/python3
import json
import psycopg2
from psycopg2.extras import RealDictCursor

import cgi

from datetime import datetime
from dateutil.relativedelta import relativedelta
from dateutil.rrule import rrule, MONTHLY

print("Content-type: application/json\n")

params = cgi.FieldStorage()
parameterId =  params.getvalue('parameters')

if parameterId is None:
	parameterId = 'all' 

# Gets the current date
current_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

# Gets the date relative to one year ago
offset_date = (datetime.now()-relativedelta(years=1)).strftime('%Y-%m-%d %H:%M:%S')

file = open("../../../api/.credentials/.postgres")
connection_string = file.readline()
pg = psycopg2.connect(connection_string)
query = pg.cursor(cursor_factory=RealDictCursor)

if parameterId is not 'all':
	stationsQuery = "SELECT 'FeatureCollection' AS type, json_agg(row_to_json(items)) AS features FROM \
	    (SELECT DISTINCT ON(fews.locations.id) 'Feature' AS type, fews.locations.locationkey AS id, row_to_json(( \
	        SELECT list \
	        FROM (SELECT  fews.locations.id AS station_id, \
	        				fews.timeserieskeys.parameterkey AS station_type) AS list )) AS properties, \
	        				st_asgeojson(st_transform(st_setsrid(st_point(fews.locations.x,fews.locations.y),fews.locations.srid),4326)) as geometry \
	    FROM fews.timeseriesvaluesandflags INNER JOIN fews.timeserieskeys ON \
		(fews.timeseriesvaluesandflags.serieskey = fews.timeserieskeys.serieskey) INNER JOIN fews.locations ON \
		(fews.timeserieskeys.locationkey = fews.locations.locationkey) \
		WHERE fews.timeseriesvaluesandflags.datetime BETWEEN '"+ offset_date +"' AND '"+ current_date +"' AND fews.timeserieskeys.parameterkey = "+str(parameterId)+") AS items"
else:
	stationsQuery = "SELECT 'FeatureCollection' AS type, json_agg(row_to_json(items)) AS features FROM \
	    (SELECT DISTINCT ON(fews.locations.id) 'Feature' AS type, fews.locations.locationkey AS id, row_to_json(( \
	        SELECT list \
	        FROM (SELECT  fews.locations.id AS station_id, \
	        				fews.timeserieskeys.parameterkey AS station_type) AS list )) AS properties, \
	        				st_asgeojson(st_transform(st_setsrid(st_point(fews.locations.x,fews.locations.y),fews.locations.srid),4326)) as geometry \
	    FROM fews.timeseriesvaluesandflags INNER JOIN fews.timeserieskeys ON \
		(fews.timeseriesvaluesandflags.serieskey = fews.timeserieskeys.serieskey) INNER JOIN fews.locations ON \
		(fews.timeserieskeys.locationkey = fews.locations.locationkey) \
		WHERE fews.timeseriesvaluesandflags.datetime BETWEEN '"+ offset_date +"' AND '"+ current_date +"') AS items"

query.execute(stationsQuery)
result = json.dumps(query.fetchall())

result = result[1:]
result = result[:-1]
result = result.replace('\\','')
result = result.replace('"[','[')
result = result.replace(']"',']')
result = result.replace('"{','{')
result = result.replace('}"','}')

print(result)

