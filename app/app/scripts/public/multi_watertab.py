#!/usr/bin/python3

import json
import psycopg2
from psycopg2.extras import RealDictCursor

from itertools import count
from datetime import datetime
from dateutil.relativedelta import relativedelta
from dateutil.rrule import rrule, MONTHLY
from collections import OrderedDict
import cgi

print("Content-type: application/json\n")

# Gets the current date
current_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

# Gets the date relative to one year ago
offset_date = (datetime.now()-relativedelta(years=1)).strftime('%Y-%m-%d %H:%M:%S')

params = cgi.FieldStorage()

parameterId =  params.getvalue('parameters', 'all')
aggregate_type = params.getvalue('aggregate_type', 'average')
serieskeyId = params.getvalue('serieskey')

file = open("../../../api/.credentials/.postgres")
connection_string = file.readline()

# Connects to database using credentials
pg = psycopg2.connect(connection_string)
query = pg.cursor()

filter_string = ""
aggregate_string = ""

# Sets string to be used for filtering when parameterId 
# and/or serieskeyId is passed as an argument
if parameterId is not 'all' and serieskeyId is not None:
	filter_string = "timeserieskeys.parameterkey = " + str(parameterId) + " AND timeserieskeys.locationkey = "+str(serieskeyId)
elif parameterId is 'all' and serieskeyId is not None:
	filter_string = "timeserieskeys.locationkey = "+str(serieskeyId)
elif parameterId is not 'all' and serieskeyId is None:
	filter_string = "timeserieskeys.parameterkey = "+str(parameterId)

# Sets string to be used to aggregate data values
# depending on aggregate function chosen
if aggregate_type == "sum":
	aggregate_string = "SUM(timeseriesvaluesandflags.scalarvalue)"
elif aggregate_type == "count":
	aggregate_string = "COUNT (timeseriesvaluesandflags.scalarvalue)"
elif aggregate_type == "min":
	aggregate_string = "MIN(timeseriesvaluesandflags.scalarvalue)"
elif aggregate_type == "max":
	aggregate_string = "MAX(timeseriesvaluesandflags.scalarvalue)"
elif aggregate_type == "average":
	aggregate_string = "AVG(timeseriesvaluesandflags.scalarvalue)"

# If filter parameter has been passed set SQL query to get filtered results
if filter_string:
	sqlQuery = "SELECT timeseriesvaluesandflags.serieskey, locations.id, parameterstable.name, \
	EXTRACT(YEAR FROM timeseriesvaluesandflags.datetime) AS year, \
	EXTRACT(MONTH FROM timeseriesvaluesandflags.datetime) AS month, \
	"+aggregate_string+"AS aggregate \
	FROM fews.timeseriesvaluesandflags INNER JOIN fews.timeserieskeys ON (timeseriesvaluesandflags.serieskey = timeserieskeys.serieskey) \
	INNER JOIN fews.parameterstable ON (timeserieskeys.parameterkey = parameterstable.parameterkey) \
	INNER JOIN fews.locations ON (timeserieskeys.locationkey = locations.locationkey) \
	WHERE (timeseriesvaluesandflags.datetime BETWEEN '" +offset_date+ "' AND '"+current_date+"' AND \
	"+filter_string+") GROUP BY timeseriesvaluesandflags.serieskey, \
	EXTRACT(YEAR FROM timeseriesvaluesandflags.datetime), \
	EXTRACT(MONTH FROM timeseriesvaluesandflags.datetime), locations.id, parameterstable.name \
	ORDER BY timeseriesvaluesandflags.serieskey ASC, month ASC"
else:
	sqlQuery = "SELECT fews.timeseriesvaluesandflags.serieskey, fews.locations.id, fews.parameterstable.name,\
	EXTRACT(YEAR FROM fews.timeseriesvaluesandflags.datetime) AS year,\
	EXTRACT(MONTH FROM fews.timeseriesvaluesandflags.datetime) AS month,\
	"+aggregate_string+" AS aggregate FROM fews.timeseriesvaluesandflags\
	INNER JOIN fews.timeserieskeys ON (fews.timeseriesvaluesandflags.serieskey = fews.timeserieskeys.serieskey)\
	INNER JOIN fews.locations ON (fews.timeserieskeys.locationkey = fews.locations.locationkey)\
	INNER JOIN fews.parameterstable ON (fews.timeserieskeys.parameterkey = fews.parameterstable.parameterkey) WHERE fews.timeseriesvaluesandflags.datetime\
	BETWEEN '"+offset_date+"' AND '"+current_date+"' GROUP BY fews.timeseriesvaluesandflags.serieskey, fews.timeserieskeys.locationkey,\
	EXTRACT(YEAR FROM fews.timeseriesvaluesandflags.datetime),\
	EXTRACT(MONTH FROM fews.timeseriesvaluesandflags.datetime),\
	fews.locations.id, fews.parameterstable.name ORDER BY fews.timeseriesvaluesandflags.serieskey ASC, fews.timeserieskeys.locationkey ASC, month ASC"

# Executes query in the db
query.execute(sqlQuery)

# Gets the result
result = query.fetchall()
# result = sorted(result, key=lambda x: x[1])

data = []
result_data = {}
result_data['metaData'] = {}
result_data['data'] = {}

result_data['metaData']['successProperty'] = "success"
result_data['metaData']['root'] = "data"
result_data['metaData']['idProperty'] = "id"
result_data['metaData']['fields'] = []

starting_date = datetime.now()-relativedelta(years=1)
interval = 1
MONTH = relativedelta(months=1)
rr = (starting_date + x * MONTH for x in count(0, interval))

dates = [ next(rr).strftime('%b %y') for x in range(12+1) ]
result_data['metaData']['columns'] = [{'header':'Station','dataIndex':'station'}]
result_data['metaData']['columns'].append( {'header':'Parameters','dataIndex':'parameter'} )
for date in dates:
	result_data['metaData']['columns'].append({'header':date,'dataIndex':date.lower().replace(" ", "_")})

for index,fields in enumerate(result_data['metaData']['columns']):
	if index <= 1:
		temp_data = {'name':fields['dataIndex'],'type':'string'}
	else:
		temp_data = {'name':fields['dataIndex'],'type':'auto'}
	result_data['metaData']['fields'].append(temp_data)

temp_result = {}
for item_val in result:
	if item_val[0] in temp_result:
		temp_result[item_val[0]].append( (item_val[1:])  ) 
	else:
		temp_result[item_val[0]] = [ (item_val[1:]) ]

serieskey = []
for key, value in temp_result.items():
	serieskey.append( key )
	temp_values = [ value[0][0], value[0][1], None, None, None, None, None, None, None, None, None, None, None, None, None]
	for item_val in value:
		temp_values[ int(item_val[3]+1) ] = item_val[4]
	data.append( temp_values )
items = []
for i, x in enumerate(data):
	temp_data = []
	temp_data.append( ("enable", True) )
	temp_data.append( ("serieskey", serieskey[i]) )
	for index, fields in enumerate(result_data['metaData']['columns']):
		temp_data.append( (fields['dataIndex'], x[index]) )
	items.append(OrderedDict( temp_data) )
items = sorted(items, key=lambda x: x['station'])
result_data['data'] = items
print(json.dumps(result_data))
