#!/usr/bin/python3
import json
import psycopg2
from psycopg2.extras import RealDictCursor
import re

print("Content-type: application/json\n")

file = open("../../../api/.credentials/.postgres")
connection_string = file.readline()
pg = psycopg2.connect(connection_string)

query = pg.cursor(cursor_factory=RealDictCursor)
paramsQuery = "SELECT ( \
        SELECT array_to_json(array_agg(row_to_json(d))) \
        FROM ( \
            SELECT fews.parameterstable.name AS parameter_name, \
                fews.parameterstable.parameterkey AS parameter_id \
            FROM fews.parameterstable \
        )d \
    ) AS params"

query.execute(paramsQuery)
temp_data = "[{\'parameter_name\':\'All\',\'parameter_id\':\'\'},"
result_list = query.fetchall()
# result_list[0]["params"] = temp_data + str(result_list[0]["params"])[1:]
result_data = temp_data + str(result_list[0]["params"])[1:]
result_data = result_data.replace("'", '"')

# Replaces double quotes in parameter name with single quotes
result_data = re.sub("\" ", "' ", result_data)
result_data = re.sub("[^: || ^,] \"", " '", result_data)
result_data = [result_data[1:-1]]

result = {}
result['params'] = result_data
result = json.dumps(result)
result = result.replace('\\', '')
result = result.replace('"[', '[')
result = result.replace(']"', ']')
result = result.replace('"{', '{')
result = result.replace('}"', '}')
print(result)
