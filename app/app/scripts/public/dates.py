#!/usr/bin/python3

import json
from datetime import datetime
from dateutil.relativedelta import relativedelta
from dateutil.rrule import rrule, MONTHLY

import cgi
#import cgitb
#cgitb.enable()

print("Content-type: application/json\n")

starting_date = datetime.now()-relativedelta(years=1)
ending_date = datetime.now()
dates = [dt.strftime('%B %y') for dt in rrule(MONTHLY, dtstart=starting_date, until=ending_date)]
date_dict = {'dates':dates}
print(json.dumps(date_dict))