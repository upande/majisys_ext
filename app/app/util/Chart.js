Ext.define('majisys_dds.util.Chart', {

    seriesColor : ['#00FFFF','#8A2BE2','#5F9EA0','#FF0000','#0000FF','#D2691E','#A52A2A','#6495ED','#FF7F50','#DC143C',
                   '#7B68EE','#808000','#6B8E23','#DA70D6','#C71585','#CD853F','#FF4500','#800080','#663399','#7FFF00',
                   '#2F4F4F','#00BFFF','#1E90FF','#B22222','#FF1493','#228B22','#696969','#DAA520','#FF69B4','#FFD700',
                   '#FF69B4','#CD5C5C','#20B2AA','#778899','#008000','#32CD32','#4B0082','#FF00FF','#00FF00','#191970',
                   '#DDA0DD','#8B4513','#2E8B57','#6A5ACD','#4169E1','#708090','#FA8072','#00FF7F','#4682B4','#008080',
                   '#00008B','#B8860B','#006400','#8B008B','#008B8B','#FF8C00','#A9A9A9','#9932CC','#8B0000','#483D8B',
                   '#FF6347','#EE82EE','#9ACD32'],

    config: {
        panelId:    '',
        withSlider: true,
        yStretch:   true
    },

    deployed:       false,
    panelCmp:       null,
    panel:          null,
    units:          null,
    parameter:      null,
    tipEl:          null,
    yPadding:       0.1,
    nullValue:      null,
    /* nullValue:    0.0000000001, */
    formatDate:     d3.time.format("%Y-%m-%d %H:%M:%S"),
    tipDate:        d3.time.format("%d-%b-%Y %H:%M"),

    clsPrefix:     '',

    newSeriesAdded: null,

    colorIndex:     null,
    series:         null,
    seriesObs:      null,
    seriesDim:      null,
    main:           null,
    slider:         null,
    chart :         null,
    temporalScale:  true,
    withPoints:     true,

    /* --- */
    init: function(){
        this.colorIndex = 0;
        this.series = new Array();
        this.seriesObs = new Array();
        this.seriesDim = new Array();
        this.main = new Object();
        this.slider = new Object();
        this.chart = null;
        this.temporalScale = true;
    },

    /* --- */
    constructor : function(config){
        this.initConfig(config);
    },

    /* --- */
    applyPanelId: function(panelId){
        if (panelId){
            this.panelCmp = Ext.getCmp(panelId);
            this.panel = this.panelCmp.body;
            this.init();
            return panelId;
        }
    },

    /* --- */
    applyWithSlider: function(withSlider){
        if (Ext.isBoolean(withSlider)) {
            if (!withSlider) {
                this.setYStretch(false);
                return withSlider;
            } else {
                return withSlider;
            }
        }
    },

    /* --- */
    applyYStretch: function(yStretch){
        if (Ext.isBoolean(yStretch)){
            if (this.getWithSlider()){
                return yStretch
            } else {
                return false;
            }
        }
    },

    /* --- */
    brushed: function(){ /* Zoom function */
        var me = Ext.getDom('brushedObj').graph;

        me.main.x.domain(me.brush.empty() ? me.slider.x.domain() : me.brush.extent());

        me.main.chart.select(".x.axis").call(me.main.xAxis);

        if (me.getYStretch()){
            var filteredSeries = [];
            var filteredSet;

            for ( i=0; i<me.series.length; i++ ){

                me.seriesDim[i].filter([me.main.x.domain()[0],me.main.x.domain()[1]]);
                filteredSet = me.seriesDim[i].top(Infinity);

                for ( j=0; j<filteredSet.length; j++ ){
                    filteredSeries.push(filteredSet[j].value);
                };

            };
            me.main.y.domain([
                d3.min(filteredSeries.map(function(d) { return d; })) - me.yPadding,
                d3.max(filteredSeries.map(function(d) { return d; })) + me.yPadding
            ]);
            me.main.chart.select(".y.axis").call(me.main.yAxis);
        }

        me.main.chart.selectAll(".ts_lines")
            .attr("d", function(d) { return me.main.line(d.obs); });

        me.main.chart.selectAll(".ts_points")
            .attr("cx", function(d){ return me.main.x(d.date); })
            .attr("cy", function(d){ return (d.value == me.nullValue) ? me.main.y(0) : me.main.y(d.value); })
            .style("opacity", function(d){ return (d.value == me.nullValue) ? 0 : 1 });
    },

    /* --- */
    createSliderChart: function(){
        var me = this;
        /* Chart settings */
        this.slider.margin = {top: this.panel.getHeight() - 65, right: 30, bottom: 25, left: 50},
            this.slider.width = this.main.width,
            this.slider.height = this.panel.getHeight() - this.slider.margin.top - this.slider.margin.bottom;

        /* x & y Functions */
        if (this.temporalScale)
            this.slider.x = d3.time.scale().range([0, this.slider.width]);
        else    
            this.slider.x = d3.scale.linear().range([0, this.slider.width]);
        this.slider.y = d3.scale.linear().range([this.slider.height, 0]);

        /* Line function */
        this.slider.line = d3.svg.line()
            .defined(function(d) { return d.value != me.nullValue; })
            .x(function(d) { return me.slider.x(d.date); })
            .y(function(d) { return me.slider.y(d.value); });

        /* Container */
        this.slider.chart = this.chart.append("g")
            .attr("class", "slider-chart")
            .attr("transform", "translate(" + this.slider.margin.left + "," + this.slider.margin.top + ")")
            .attr("fill", "transparent");

        /* Frame */
        this.slider.chart.append("rect")
            .attr("class", "sliderbox")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", this.slider.width)
            .attr("height", this.slider.height + 1);
        /* x Axis */
        this.slider.xAxis = d3.svg.axis()
            .scale(this.slider.x)
            .orient("bottom");

        this.slider.chart.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + this.slider.height + ")")
            .call(this.slider.xAxis);

        /* Enable focusing */
        this.brush = d3.svg.brush()
            .x(this.slider.x)
            .on("brush", me.brushed);

        this.slider.chart.append("g")
            .attr('id','brushedObj')
            .attr("class", "brush")
            .call(this.brush)
          .selectAll("rect")
            .attr("y", 1)
            .attr("height", this.slider.height)
            .attr("fill", "black");

        Ext.getDom('brushedObj').graph = this;
    },

    /* --- */
    createMainChart: function(){
        var me = this;

        Ext.util.CSS.createStyleSheet(
            '.' + this.clsPrefix + '_line_series { clip-path: url(#' + this.clsPrefix + '_clip); }',
            this.clsPrefix + '_line_cls'
        );
        Ext.util.CSS.createStyleSheet(
            '.' + this.clsPrefix + '_point_series { clip-path: url(#' + this.clsPrefix + '_clip); }',
            this.clsPrefix + '_point_cls'
        );

        /* Chart settings */
        this.main.margin = {top: 20, right: 30, bottom: 100, left: 50};
        if (!this.getWithSlider()) { this.main.margin.bottom = 40; }
        this.main.width = this.panel.getWidth() - this.main.margin.left - this.main.margin.right,
          this.main.height = this.panel.getHeight() - this.main.margin.top - this.main.margin.bottom;

        /* x & y Functions */
        if (this.temporalScale)
            this.main.x = d3.time.scale().range([0, this.main.width]);
        else    
            this.main.x = d3.scale.linear().range([0, this.main.width]);
        this.main.y = d3.scale.linear().range([this.main.height, 0]);

        /* Chart Object */
        this.chart = d3.select("#"+this.panel.id)
            .append("svg")
            .attr("id","chart-svg")
            .attr("width", this.main.width + this.main.margin.left + this.main.margin.right)
            .attr("height", this.main.height + this.main.margin.top + this.main.margin.bottom);

        /* Line function */
        this.main.line = d3.svg.line()
            .defined(function(d) { return d.value != me.nullValue; })
            .x(function(d) { return me.main.x(d.date); })
            .y(function(d) { return me.main.y(d.value); });

        /* ClipPath - SVG settings for the slider */
        this.chart.append("defs").append("clipPath")
            .attr("id", this.clsPrefix+"_clip")
          .append("rect")
            .attr("width", this.main.width)
            .attr("height",this.main.height);

        /* Container */
        this.main.chart = this.chart.append("g")
            .attr("class", "main-chart")
            .attr("transform", "translate(" + this.main.margin.left + "," + this.main.margin.top + ")")
            .attr("fill", "transparent");

        /* x & y Axes */
        this.main.xAxis = d3.svg.axis()
            .scale(this.main.x)
            .orient("bottom")
          .tickSize(-this.main.height);

        this.main.yAxis = d3.svg.axis()
            .scale(this.main.y)
            .orient("left")
          .tickSize(-this.main.width);

        this.main.chart.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + this.main.height + ")")
            .call(this.main.xAxis);

        this.main.chart.append("g")
            .attr("class", "y axis")
            .call(this.main.yAxis);

        /* Tip */
        this.tip = d3.tip()
          .attr('class', 'd3-tip')
          .offset([-5, 0])
          .html(function(d) {
            var label = (me.temporalScale) ?  me.tipDate(d.date) : d,date;
            return label + ": <span style='color:SteelBlue;'>" + d.value.toFixed(2) + "</span>";
          });

        this.chart.call(this.tip);


        if (this.getWithSlider()){
          this.createSliderChart();
        };
    },

    /* --- */
    parseData: function(records){
        var me = this;
        var dataSeries = [];
        for ( i=0; i<records.length; i++ ){
            this.colorIndex = Math.floor(Math.random()*63);
            ds = records[i].series;

            if (this.temporalScale)
                ds.forEach(function(d) { d.date = me.formatDate.parse(d.date); });
            dataSeries.push({
                timeseriesid: records[i].timeseriesid,
                locationid : records[i].locationid,
                obs : ds,
                active: true,
                color : this.seriesColor[this.colorIndex]
            });
        };
        return dataSeries;
    },

    /* --- */
    addSeries: function(param, indexes, callbackfn){ /* param is: (key list, 1 or more) or (time series object array) */
        var me = this;
        this.newSeriesAdded = true;
        if (typeof param == 'object') {
            var newSeries = param;
            var cs;
            for ( i=0; i<newSeries.length; i++ ){
                this.series.push(newSeries[i]);
                cs = crossfilter(newSeries[i].obs);
                this.seriesObs.push(cs);
                this.seriesDim.push(cs.dimension(function(d) { return d.date; }));
            };
            this.refresh();
        } else {
            var sourceUrl = 'api/chart/getseriesvalues.py';
            Ext.Ajax.request({
                url: sourceUrl,
                disableCaching: false,
                method: 'GET',
                params: {
                    serieskeys: param
                },
                success: function(response){
                    var colorList = new Array();
                    if (indexes) indexList = indexes.split(',');
                    var data = response.responseText;
                    var sep = data.indexOf(',');
                    me.parameter = data.substring(0,sep);
                    data = data.replace(me.parameter+',','');
                    sep = data.indexOf(',');
                    me.units = data.substring(0,sep);
                    data = data.replace(me.units+',','');
                    if (me.series.length == 0) {  /* Initialize the chart */
                        me.createMainChart();
                        me.deployed = true;
                    };
                    var newSeries = me.parseData(JSON.parse(data));
                    var cs;
                    for ( i=0; i<newSeries.length; i++ ){
                        if (indexes){
                            var item = {'index':indexList[i],'color':newSeries[i].color};
                            colorList.push(item);
                        }
                        me.series.push(newSeries[i]);
                        cs = crossfilter(newSeries[i].obs);
                        me.seriesObs.push(cs);
                        me.seriesDim.push(cs.dimension(function(d) { return d.date; }));
                        };
                    if (callbackfn){
                        callbackfn(colorList);
                    }
                    me.refresh();
                },
                failure: function(response){
                    console.log('error: '+response.status)
                }
            });
        };

    },

    /* --- */
    reset: function(){
        if (this.chart) {
            this.chart.remove();
            Ext.util.CSS.removeStyleSheet(this.clsPrefix + '_line_cls');
            Ext.util.CSS.removeStyleSheet(this.clsPrefix + '_point_cls');
        }
        /* this.init(this.panelId, this.getWithSlider(), this.getYStretch()); */
        this.init();
    },

    /* --- */
    removeSeries: function(key) {
        for (i=0; i<this.series.length; i++){
            if (this.series[i].timeseriesid == key) {
                this.series.splice(i,1);
                this.main.chart.selectAll("#key_"+key).remove();
                this.slider.chart.selectAll("#key_"+key).remove();
            }
            (this.series.length == 0) ? this.reset() : this.refresh();
        }
    },

    /* --- */
    getSeriesColor: function(key){
        if (this.series.length > 0) {
            var ts = this.series.find(function(e) { return e.timeseriesid == key; });
            if (ts) {return ts.color} else {return null}
        } else {
            return null;
        }
    },

    /* --- */
    resize: function(){
        if (this.deployed){
            this.main.width = this.panel.getWidth() - this.main.margin.left - this.main.margin.right,
                this.main.height = this.panel.getHeight() - this.main.margin.top - this.main.margin.bottom;

            this.main.x.range([0, this.main.width]);
            this.main.y.range([this.main.height, 0]).nice();

            this.chart
                .attr("width", this.panel.getWidth())
                .attr("height", this.panel.getHeight());

            this.chart.select('#'+this.clsPrefix+'_clip rect')
                .attr("width", this.main.width)
                .attr("height", this.main.height);
                
            this.main.xAxis    
                .tickSize(-this.main.height);

            this.main.chart.selectAll(".x.axis")
                .attr("transform", "translate(0," + this.main.height + ")")
                .call(this.main.xAxis);
                
            this.main.yAxis
                .tickSize(-this.main.width);

            this.main.chart.selectAll(".y.axis")
                .call(this.main.yAxis);

            if (this.getWithSlider()){
                this.slider.margin.top = this.panel.getHeight() - 65,
                    this.slider.width = this.main.width,
                    this.slider.height = this.panel.getHeight() - this.slider.margin.top - this.slider.margin.bottom;

                this.slider.x.range([0, this.main.width]);

                this.slider.chart
                    .attr("transform", "translate(" + this.slider.margin.left + "," + this.slider.margin.top + ")");

                this.slider.chart.selectAll(".x.axis")
                    .call(this.slider.xAxis);

                this.slider.chart.selectAll(".sliderbox")
                    .attr("width", this.slider.width);

                d3.selectAll(".brush").call(this.brush.clear());
            };

            this.refresh();
        }
    },

    /*------------------*/
    refresh: function() {
        var me = this;
        /* x & y Domains */
        var currXDomain = this.main.x.domain().slice(),
            currYDomain = this.main.y.domain().slice();

        this.main.x.domain([
            d3.min(this.series, function(s) { return d3.min(s.obs, function(d) {return d.date; })}),
            d3.max(this.series, function(s) { return d3.max(s.obs, function(d) {return d.date; })})
        ]);
        this.main.y.domain([
            d3.min(this.series, function(s) { return d3.min(s.obs, function(d) {return d.value; })}) - this.yPadding,
            d3.max(this.series, function(s) { return d3.max(s.obs, function(d) {return d.value; })}) + this.yPadding
        ]);
        if (this.getWithSlider()){
            this.slider.x.domain(this.main.x.domain());
            this.slider.y.domain(this.main.y.domain());
        }

        /* Update x & y domains */
        if ( currXDomain[0] != this.main.x.domain()[0] || currXDomain[1] != this.main.x.domain()[1] ) {
            this.main.chart.select(".x.axis")
                /* .transition() */
                .call(this.main.xAxis);
            if (this.getWithSlider()){
                this.slider.chart.select(".x.axis")
                    .transition()
                    .call(this.slider.xAxis);
            }
        };
        if ( currYDomain[0] != this.main.y.domain()[0] || currYDomain[1] != this.main.y.domain()[1] ) {
            this.main.chart.select(".y.axis")
                /* .transition() */
                .call(this.main.yAxis);
        };

        /* Refresh existing series */
        this.main.chart.selectAll('.ts_lines')
            .transition().ease('sin-in-out')
            .attr("d", function(d) { return me.main.line(d.obs); });
            
        if (this.withPoints){
            this.main.chart.selectAll(".ts_points")
                .transition()
                .attr("cx", function(d){ return me.main.x(d.date); })
                .attr("cy", function(d){
                    return (d.value == me.nullValue) ? me.main.y(0) : me.main.y(d.value);
                })
                .style("opacity", function(d){ return (d.value == me.nullValue) ? 0 : 1 });
        }

        if (this.getWithSlider()) {
            this.slider.chart.selectAll(".sl_lines")
                .transition().ease('sin-in-out')
                .attr("d", function(d) { return me.slider.line(d.obs); });

            if (this.withPoints){
                this.slider.chart.selectAll(".sl_points")
                    .attr("cx", function(d){ return me.slider.x(d.date); })
                    .attr("cy", function(d){ return (d.value == me.nullValue) ? me.slider.y(0) : me.slider.y(d.value); })
                    .style("opacity", function(d){ return (d.value == me.nullValue) ? 0 : 1 });
            }
        }


        /* Load new series */
        if (this.newSeriesAdded){

            this.main.lines = this.main.chart.selectAll("."+this.clsPrefix+"_line_series")
                .data(this.series)
              .enter().append("g")
                .attr("class", this.clsPrefix+"_line_series")
                .attr("id", function(d) { return "key_"+d.timeseriesid; });

            this.main.lines.append("path")
                .attr("class", "ts_lines")
                .attr("id", function(d) { return "ts_"+d.timeseriesid; })
                .attr("d", function(d) { return me.main.line(d.obs); })
                .style("opacity", 1)
                .style("stroke", function(d){ return d.color; });

            if (this.withPoints){
                this.main.points = this.main.lines.append('g')
                    .attr("class",this.clsPrefix+"_point_series");

                this.main.points.selectAll("circle")
                    .data(function(d){ return d.obs; })
                  .enter().append('circle')
                    .attr("class","ts_points")
                    .attr("id", function(p){ return "ts_"+this.parentNode.__data__.timeseriesid; })
                    .attr("r", 3)
                    .attr("cx", function(d){ return me.main.x(d.date); })
                    .attr("cy", function(d){
                        return (d.value == me.nullValue) ? me.main.y(0) : me.main.y(d.value);
                    })
                    .style("opacity", function(d){ return (d.value == me.nullValue) ? 0 : 1 })
                    .style("fill","white")
                    .style("stroke-width","1.5px")
                    .style("stroke", function(d) { return this.parentNode.__data__.color; })
                    .on('mouseover', function(p){
                        if (p.value != me.nullValue){
                            me.tipEl = 'value';
                            me.tip.show(p);
                        };
                    })
                    .on('mouseout', function(p){
                        if (p.value != me.nullValue){
                            me.tip.hide(p);
                        };
                    });
            }

            if (this.getWithSlider()) {
                this.slider.lines = this.slider.chart.selectAll("."+this.clsPrefix+"_line_series")
                    .data(this.series)
                  .enter().append("g")
                    .attr("class", this.clsPrefix+"_line_series")
                    .attr("id", function(d) { return "key_"+d.timeseriesid; });

                this.slider.lines.append("path")
                    .attr("class", "sl_lines")
                    .attr("id", function(d) { return "sl_"+d.timeseriesid; })
                    .attr("d", function(d) { return me.slider.line(d.obs); })
                    .style("opacity", 1)
                    .style("stroke", function(d){ return d.color; });

                if (this.withPoints){    
                    this.slider.points = this.slider.lines.append('g')
                        .attr("class",this.clsPrefix+"_point_series");

                    this.slider.points.selectAll("circle")
                        .data(function(d){ return d.obs; })
                      .enter().append('circle')
                        .attr("class","sl_points")
                        .attr("id", function(p){ return "sl_"+this.parentNode.__data__.timeseriesid; })
                        .attr("r", 2)
                        .attr("cx", function(d){ return me.slider.x(d.date); })
                        .attr("cy", function(d){ return (d.value == me.nullValue) ? me.slider.y(0) : me.slider.y(d.value); })
                        .style("opacity", function(d){ return (d.value == me.nullValue) ? 0 : 1 })
                        .style("fill","white")
                        .style("stroke-width","1.5px")
                        .style("stroke", function(d) { return this.parentNode.__data__.color; });

                    d3.selectAll(".brush").call(me.brush.clear());
                }
            };

            this.newSeriesAdded = false;
        }
    }
});