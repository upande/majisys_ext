Ext.define('majisys_dds.model.SeriesKey', {
    extend: 'Ext.data.Model',
    
    fieldDefaults: {
        type: 'date',
        dateFormat: 'Y-m-d H:i:s'
    },

    fields: [{
        name: 'seriescolor',
        type: 'string'
    },{
        name: 'serieskey',
        type: 'integer'
    },{
        name: 'locationid',
        type: 'string'
    },{
        name: 'parameterid',
        type: 'string'
    },{
        name: 'parametername',
        type: 'string'
    },{
        name: 'featureclasskey',
        type: 'integer'
    },{
        name: 'startdate',
        type: 'date'
    },{
        name: 'enddate',
        type: 'date'
    },{
        name: 'nritems',
        type: 'integer'
    },{
        name: 'lastmodified',
        type: 'date'
    },{
        name: 'selected',
        type: 'boolean',
        defaultValue: false
    }],
    idProperty: 'serieskey'

});