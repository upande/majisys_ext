Ext.define('majisys_dds.model.wrma.FeatureClass', {
    extend: 'Ext.data.Model',
    
    fields: [
        { name: 'featureclasskey', type: 'string' },
        { name: 'featureclassname', type: 'string' },
        { name: 'nritems', type: 'string' }
    ],
    
    idProperty: 'featureclasskey'
});
