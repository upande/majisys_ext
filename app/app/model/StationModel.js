Ext.define('majisys_dds.model.StationModel', {
	extend: 'Ext.data.Model',
	fields: [
		{ name: 'station_id', type: 'string', mapping: 'properties.station_id' },
	    { name: 'station_type', type: 'integer', mapping: 'properties.station_type' },
	    //{ name: 'geometry', type: 'object' }
	    { name: 'geometry', type: 'auto' }
	],
	idProperty: 'station_id'
});
