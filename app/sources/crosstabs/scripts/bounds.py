#!/usr/bin/python3

import json
import psycopg2
from psycopg2.extras import RealDictCursor

print ("Content-type: application/json")
print ()

file = open("../../../api/.credentials/.postgres")
connection_string = file.readline()
pg = psycopg2.connect(connection_string)

query = pg.cursor()
boundsQuery = "SELECT ST_Extent(geom) AS bounds FROM fews.stations;"
query.execute(boundsQuery)

result = str(query.fetchall())
result = result.replace("[('BOX(",'')
result = result.replace(")',)]",'')
result = result.replace(' ',',')

print (result)

