#!/usr/bin/python3

import json
import psycopg2
from psycopg2.extras import RealDictCursor

import cgi

print ("Content-type: application/json")
print ()

#-- Query Parameters
params = cgi.FieldStorage()
filterBy =  params.getvalue('filterby')
filterValue =  params.getvalue('filtervalue')
aggFunction =  params.getvalue('operation')
stationType =  params.getvalue('stationtype')

# filterBy = "year"
# filterValue = 2014
# aggFunction = 'avg'
# stationType = 1

file = open("../../../api/.credentials/.postgres")
connection_string = file.readline()
pg = psycopg2.connect(connection_string)

if filterBy == "year" :

    viewQuery = "CREATE OR REPLACE VIEW monitoring.watertab AS \
        WITH \
          params AS ( \
            SELECT '%s-01-01 00:00:00'::TIMESTAMP WITHOUT TIME ZONE AS period_start, \
                   '%s-12-31 24:00:00'::TIMESTAMP WITHOUT TIME ZONE AS period_end \
          ), \
          aggregates AS ( \
            SELECT t.locationkey, \
                   to_char(s.datetime, 'YYYY'::text) AS obs_year, \
                   to_char(s.datetime, 'MM'::text) AS obs_month, \
                   btrim(to_char(s.scalarvalue, '999D9999'::text))::numeric AS obs_value \
            FROM fews.timeserieskeys AS t join fews.timeseriesvaluesandflags AS s ON s.serieskey = t.serieskey, \
                 params AS p \
            WHERE s.datetime <@ tsrange(p.period_start, p.period_end) AND \
                  t.parameterkey = %s \
            GROUP BY 1, 2, 3, 4 \
            ORDER BY 1, 2, 3 \
          ) \
        SELECT l.id AS station_id, \
          a.obs_year, \
          a.obs_month, \
          a.obs_value \
        FROM aggregates AS a JOIN fews.locations AS l ON a.locationkey = l.locationkey;" % (filterValue, filterValue, stationType)

    dataQuery = "SELECT n.* \
        FROM crosstab ( \
         'select station_id,obs_month,trim(to_char((%s(obs_value)),''999D9999''))::REAL FROM monitoring.watertab group by 1,2 order by 1,2', \
         'select distinct obs_month FROM monitoring.watertab order by 1' \
         ) \
         AS n ( \
           record_id VARCHAR, jan REAL, feb REAL, mar REAL, apr REAL, may REAL, \
           jun REAL, jul REAL, aug REAL, sep REAL, oct REAL, nov REAL, dec REAL \
         );" % (aggFunction)

if filterBy == "station" :

    viewQuery = "CREATE OR REPLACE VIEW monitoring.locationparametertab AS \
        WITH params as ( \
          SELECT id, locationkey \
          FROM fews.locations \
          WHERE id = '%s' \
        ) \
        SELECT  p.locationkey, \
                to_char(s.datetime, 'YYYY') AS obs_year, \
                to_char(s.datetime, 'MM') AS obs_month, \
                trim(to_char((s.scalarvalue),'999D9999'))::NUMERIC AS obs_value \
        FROM params AS p JOIN fews.timeserieskeys AS t ON t.locationkey = p.locationkey \
             JOIN fews.timeseriesvaluesandflags AS s ON t.serieskey = s.serieskey \
        WHERE t.parameterkey = %s AND EXTRACT(YEAR from s.datetime) > 1990 \
        ORDER BY 1,2,3  " % (filterValue, stationType)

    dataQuery = "SELECT n.* from crosstab ( \
        'select obs_year,obs_month,trim(to_char((%s(obs_value)),''999D9999''))::NUMERIC from monitoring.locationparametertab group by 1,2 order by 1,2', \
        'select distinct obs_month from monitoring.locationparametertab order by 1' \
        ) \
        AS n ( \
           record_id VARCHAR, jan REAL, feb REAL, mar REAL, apr REAL, may REAL, \
           jun REAL, jul REAL, aug REAL, sep REAL, oct REAL, nov REAL, dec REAL \
        )" % (aggFunction)

query = pg.cursor(cursor_factory=RealDictCursor)
query.execute(viewQuery)
query.execute(dataQuery)

result = json.dumps(query.fetchall())
result = result.replace("null","0.0000000001")

print ('{"success":"true", "obs":',result.replace("'",'"'),'}')

