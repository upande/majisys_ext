/* CrossTabs controller */
Ext.define('WaterTabs.controller.CrossTabs', {
  extend: 'Ext.app.Controller',

  stores: [ 'Observations', 'Stations' ],
  models: [ 'RawObs' ],
  views:  [ 'YearlyGrid', 'LineChart' ],

  refs: [{
    ref:'yearGrid',
    selector: 'yearlygrid'
  }],

  svgChart: null,
  seriesLine: null,
  seriesLineInt: null,
  recordPaths: null,
  recordPoints: null,
  chartRecords: null,
  visibleRecords: null,
  hiddenRecords: null,

  x: null,
  y: null,
  xAxis: null,
  yAxis: null,
  
  seriesColor : [ '#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728',
                  '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2',
                  '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5', '#393b79',
                  '#5254a3', '#6b6ecf', '#9c9ede', '#637939', '#8ca252', '#b5cf6b', '#cedb9c',
                  '#8c6d31', '#bd9e39', '#e7ba52', '#e7cb94', '#843c39', '#ad494a', '#d6616b',
                  '#e7969c', '#7b4173', '#a55194', '#ce6dbd', '#de9ed6' ],  

  months : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
  tipEl: null,

  init: function() {
    ctController = this;
    this.control({
      'yearlygrid': {
        itemmouseenter: function(grid,item){
          if (item.get('enable')) { ctController.highlightSvg(item.get('record_id')); };
        },
        itemmouseleave: function(grid,item){
          if (item.get('enable')) {  ctController.maskSvg(item.get('record_id')); };
        }
      },
      'linechart': {
        afterlayout: function(e){
          ctController.reSizeChart();
        }
      }
    });
  },

  /*-- Controller's launch function */
  onLaunch: function(){
    Ext.getCmp('yearchooser').setValue(waterApp.selectionYear);
    Ext.getCmp('aggchooser').setValue(waterApp.selectionFunction);
    Ext.getCmp('filterchooser').setValue(waterApp.filterBy);

    this.refreshGrid();
  },
  /*---*/

  /*-- Recreates the structure of the grid, and reloads the Observations store */
  refreshGrid: function(){
    Ext.get('smoothBtn').dom.disabled = false;
    Ext.get('origBtn').dom.disabled = true;
    Ext.getCmp('chartPanel').disable();
    Ext.getCmp('selectionButton').toggle(true);
    var filterParam;
    
    if  (waterApp.swithchStationType){
      ctController.getStationsStore().clearFilter();
      this.getStationsStore().filter('station_type',waterApp.selectionStationType);
      waterApp.selectionStation = Ext.getStore('Stations').getAt(0).data.station_id;
      Ext.getCmp('stationchooser').setValue(waterApp.selectionStation);      
      waterApp.swithchStationType = false;
      ctController.getObservationsStore().each(function(item){
        d3.selectAll("#feat_"+item.get('record_id')).attr('class','hidden');      
      });
    };
    
    waterApp.filterBy == 'year' ? filterParam = waterApp.selectionYear : filterParam = waterApp.selectionStation;

    this.getObservationsStore().getProxy().extraParams = {
      filterby: waterApp.filterBy,
      filtervalue: filterParam,
      stationtype: waterApp.selectionStationType,
      operation: waterApp.selectionFunction
    };
    this.getObservationsStore().load();
  },
  /*---*/

  /*-- Connects the hovering over the multiple interface components */
  highlightSvg: function(id){
    d3.selectAll("#st_"+id).attr('class','stline-focus');
    d3.selectAll("#hg_"+id).attr('class','hgline');
    d3.selectAll("#lb_"+id).attr('class','stid');
    d3.selectAll("#feat_"+id).attr('class','stpoint-focus');
    d3.selectAll("#id_"+id).attr('class','idpoint');
    var record = ctController.getYearGrid().getStore().getById(id)
    ctController.getYearGrid().getSelectionModel().setLastFocused(record);
  },
  maskSvg: function(id){
    d3.selectAll("#st_"+id).attr('class','stline');
    d3.selectAll("#hg_"+id).attr('class','hidden');
    d3.selectAll("#lb_"+id).attr('class','hidden');
    d3.selectAll("#feat_"+id).attr('class',waterApp.stationCss);
    d3.selectAll("#id_"+id).attr('class','hidden');
    ctController.getYearGrid().getSelectionModel().setLastFocused(null);
  },
  /*---*/

  /*-- Generates the time series as required for the chart */
  generateSeries: function(){
    if (Ext.getStore('Observations').count() == 0){
      alert('The chosen combination of parameters did not produced any result.');
      return null;
    } else {
      var dataSeries = '[';
      var stComma = false;
      var color = 0;
      this.getObservationsStore().each(function(item){
        stComma ? dataSeries += ',' : stComma = true;
        var series = '{ "record_id" : "'+item.data.record_id+'", "values" : [';
        var itemComma = false;
        for (var i=2; i<14; i++){
          itemComma ? series += ',' : itemComma = true;
          var fieldName = item.fields.keys[i];
          var fieldValue = item.get(fieldName);
          series += '{ "date" : parseDate("'+waterApp.selectionYear+fieldName+'01"), "level" : ' + fieldValue + ' }';
        };
        var sColor = Math.floor(Math.random() * 40); /* use 'color++' for sequential coloring */
        item.set('lineColor', ctController.seriesColor[sColor]);
        series += '], "color" : "'+ ctController.seriesColor[sColor] +'" }';
        dataSeries += series;
      });
      dataSeries += ']';
      return dataSeries;
    };
  },
  /*---*/

  /*--  Generates the line graph */
  drawChart: function(dataSeries){

    var parseDate = d3.time.format("%Y%b%d").parse; /* function required in the 'eval(dataSeries)' to parse the dates */
    this.chartRecords = eval(dataSeries);
    this.visibleRecords = eval(dataSeries);
    this.hiddenRecords = new Array;

    var margin = {top: 20, right: 65, bottom: 70, left: 50},
      width = Ext.get('chartPanel').getWidth() - margin.left - margin.right,
      height = Ext.get('chartPanel').getHeight() - margin.top - margin.bottom;

    this.x = d3.time.scale()
        .range([0, width]);

    this.y = d3.scale.linear()
        .range([height, 0]);

    this.xAxis = d3.svg.axis()
        .scale(this.x)
      .tickSize(-height)
      .tickPadding(10)
      .tickFormat(d3.time.format("%b"))
        .orient("bottom");

    this.yAxis = d3.svg.axis()
        .scale(this.y)
      .tickSize(-width)
      .tickPadding(5)
        .orient("left");

    this.svgChart = d3.select("#chartPanel-body")
        .append("svg")
        .attr("id","svgChart")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var tip = d3.tip()
      .attr('class', 'd3-tip')
      .offset([-5, 0])
      .html(function(d) {
        return ctController.tipEl + ": <span style='color:SteelBlue;'>" + d.level + "</span>";
      })

    this.svgChart.call(tip);


    this.x.domain(d3.extent(this.chartRecords[0].values, function(obs) { return obs.date; }));

    var minVal = d3.min(this.chartRecords, function(s) { return d3.min(s.values, function(v) { return v.level; }); });
    var maxVal = d3.max(this.chartRecords, function(s) { return d3.max(s.values, function(v) { return v.level; }); });
    maxVal = maxVal*1.05;
    this.y.domain([ minVal, maxVal ]);

    var filterParam;
    waterApp.filterBy == 'year' ? filterParam = waterApp.selectionYear : filterParam = waterApp.selectionStation;

    this.svgChart.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(this.xAxis)
      .append("text")
        .attr("x", width/2)
        .attr("y", 45)
        .style("text-anchor", "middle")
        .text(Ext.getCmp('aggchooser').getDisplayValue()+" monthly values for "+filterParam);

    this.svgChart.append("g")
        .attr("class", "y axis")
        .call(this.yAxis);

    this.seriesLine = d3.svg.line()
        .defined(function(obs) { return obs.level != waterApp.nullValue; })
        .x(function(obs) { return ctController.x(obs.date); })
        .y(function(obs) { return ctController.y(obs.level); });

    this.seriesLineInt = d3.svg.line()
        .defined(function(obs) { return obs.level != waterApp.nullValue; })
        .interpolate("monotone")
        .x(function(obs) { return ctController.x(obs.date); })
        .y(function(obs) { return ctController.y(obs.level); });

    this.recordPaths = this.svgChart.selectAll(".record")
        .data(this.chartRecords)
      .enter().append("g")
        .attr("class", "record");

    this.recordPaths.append("path")
        .attr("class", "hidden")
        .attr("id", function(s) { 
          if (waterApp.filterBy == "year"){
            d3.selectAll("#feat_"+s.record_id).attr('class',waterApp.stationCss);
          };  
          return "hg_"+s.record_id; 
        })
        .attr("d", function(s) { return ctController.seriesLine(s.values); })
        .style("stroke", function(s) { return s.color; })
        .style("stroke-opacity", ".2");

    this.recordPaths.append("path")
        .attr("class", "stline")
        .attr("id", function(s) { return "st_"+s.record_id; })
        .attr("d", function(s){ return ctController.seriesLine(s.values); })
        .style("stroke", function(s){ return s.color; })
        .on('mouseover', function(e){
          var stId = d3.select(this)[0][0].id;
          ctController.highlightSvg(stId.replace("st_",""));
        })
        .on('mouseout', function(e){
          var stId = d3.select(this)[0][0].id;
          ctController.maskSvg(stId.replace("st_",""));
        });

    this.recordPoints = this.recordPaths.append('g')
        .attr("class","rf-points");

    this.recordPoints.selectAll("circle")
        .data(function(s){ return s.values; })
      .enter().append('circle')
        .attr("class","rfpoint")
        .attr("id", function(p){ return "rp_"+this.parentNode.__data__.record_id; })
        .attr("r", 2.5)
        .attr("month",function(p,idx) { return idx; }) 
        .attr("cx", function(p){ return ctController.x(p.date); })
        .attr("cy", function(p){ return ctController.y(p.level); })
        .style("opacity", function(d){ return d.level == waterApp.nullValue ? 0 : 1 })
        .style("fill", function(p) { return this.parentNode.__data__.color; })
        .on('mouseover', function(p){
          if (p.level != waterApp.nullValue){
            ctController.tipEl = ctController.months[this.getAttribute('month')];
            tip.show(p);
            ctController.highlightSvg(this.parentNode.__data__.record_id);
          };
        })
        .on('mouseout', function(p){
          if (p.level != waterApp.nullValue){          
            tip.hide(p);
            ctController.maskSvg(this.parentNode.__data__.record_id);
          };
        });

    this.recordPaths.append("text")
        .datum(function(s) { return {record_id: s.record_id, value: s.values[s.values.length - 1]}; })
        .attr("transform", function(s) {
          return "translate(" + ctController.x(s.value.date) + "," + ctController.y(s.value.level) + ")";
        })
        .attr("x", 3)
        .attr("dy", ".35em")
        .attr("class", "hidden")
        .attr("id", function(s) { return "lb_"+s.record_id; })
        .text(function(s) { return s.record_id; });

  },
  /*---*/

  /*-- Refrreshes the scale of the graph when the y domain changes */
  reScaleChart: function(){
    if (this.visibleRecords.length > 0){
      var minVal = d3.min(this.visibleRecords, function(s) { return d3.min(s.values, function(v) { return v.level; }); });
      var maxVal = d3.max(this.visibleRecords, function(s) { return d3.max(s.values, function(v) { return v.level; }); });
      maxVal < 0.1 ? maxVal = 0.1 : maxVal = maxVal*1.02;
      minVal = minVal*.9;
      ctController.y.domain([ minVal, maxVal ]);
    };

    this.svgChart.selectAll(".y.axis")
        .transition()
        .call(this.yAxis);

    this.reDrawChart(Ext.get('smoothBtn').dom.disabled);
  },
  /*---*/

  /*-- Refrreshes the scale of the graph when the y domain changes */
  reSizeChart: function(){
    if(launched){
      var margin = {right: 65, left: 50},
            width = Ext.get('chartPanel-body').getWidth() - margin.left - margin.right;

      ctController.x.range([0,width]);
      d3.selectAll('#svgChart')
          .attr("width", Ext.get('chartPanel-body').getWidth());

      ctController.svgChart.selectAll(".x.axis")
          .transition()
          .call(ctController.xAxis);

      ctController.yAxis
        .tickSize(-width);
        
      ctController.svgChart.selectAll(".y.axis")
          .transition()
          .call(ctController.yAxis);

      ctController.reDrawChart(Ext.get('smoothBtn').dom.disabled);
      compController.navMap.updateSize();
    };
  },
  /*---*/

  /*-- Redraws the chart with a different interpolation method */
  reDrawChart: function(smooth){
    if (smooth){
      Ext.get('smoothBtn').dom.disabled = true;
      Ext.get('origBtn').dom.disabled = false;
      this.recordPaths.selectAll('path')
          .transition().ease('sin-in-out')
          .attr("d", function(s) {
            return ctController.seriesLineInt(s.values);
          });
    } else {
      Ext.get('origBtn').dom.disabled = true;
      Ext.get('smoothBtn').dom.disabled = false;
      this.recordPaths.selectAll('path')
          .transition().ease('sin-in-out')
          .attr("d", function(s) {
            return ctController.seriesLine(s.values);
          });
    };

    this.recordPaths.selectAll("text")
        .transition()
        .attr("transform", function(s) {
          return "translate(" + ctController.x(s.value.date) + "," + ctController.y(s.value.level) + ")";
        });

    this.recordPoints.selectAll("circle")
        .data(function(s){ return s.values; })
        .transition()
          .attr("cx", function(p) { return ctController.x(p.date); })
          .attr("cy", function(p) { return ctController.y(p.level); })
  }
  /*---*/

});
/*-----*/