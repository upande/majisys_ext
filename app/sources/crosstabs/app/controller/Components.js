/* Components controller */
Ext.define('WaterTabs.controller.Components', {
  extend: 'Ext.app.Controller',

  views: [ 'YearlyGrid', 'MapDisplay' ],

  store: 'Stations',

  refs: [{
    ref: 'yearGrid',
    selector: 'yearlygrid'
  }],

  csvWindow : Ext.create('Ext.window.Window', {
    title: 'CSV Data',
    closeAction: 'hide',
    height: 400,
    width: 800,
    modal: true,
    layout: 'fit',
    items: {
      xtype: 'panel',
      id: 'csv-panel',
      layout: 'fit',
      padding : 5,
      overflowY: 'auto',
      html: '<div id="csv-div"></div>',
      border: false
    }
  }),

  navMap: null,
  mapBounds : null,
  sm: new OpenLayers.Projection("EPSG:3857"),
  wgs: new OpenLayers.Projection("EPSG:4326"),

  station_data: null,

  init: function() {
    compController = this;
  },

  onLaunch: function(){
  
    Ext.Ajax.request({
      url: 'scripts/bounds.py',
      disableCaching: false,
      method: 'GET',
      success: function(response){
        compController.mapBounds = response.responseText;  
        compController.generateMap();
      },
      failure: function(response){
        console.log('error: '+response.status)
      }
    });          
    
  },

  /*-- Map generation */
  generateMap: function(){

    // var mapExtent = new OpenLayers.Bounds(36.1522, -0.9395,  36.8170, -0.1129).transform(this.wgs, this.sm);
    var mapExtent = new OpenLayers.Bounds(this.mapBounds).transform(this.wgs, this.sm);
    var mapOptions = {
      controls:[],
      projection: this.sm,
      displayProjection: this.wgs,
      units: "m",
      maxExtent: mapExtent
    };
    this.navMap = new OpenLayers.Map("mapPanel-body",mapOptions);
    this.navMap.addControl(new OpenLayers.Control.MousePosition({numDigits:4}));
    this.navMap.addControl(new OpenLayers.Control.Navigation());
    this.navMap.addControl(new OpenLayers.Control.Zoom());
    this.navMap.allOverlays = true;

    arrayOSM = [
      "http://a.tile.openstreetmap.org/${z}/${x}/${y}.png",
      "http://b.tile.openstreetmap.org/${z}/${x}/${y}.png",
      "http://c.tile.openstreetmap.org/${z}/${x}/${y}.png"
    ];

    /* var osmLayer = new OpenLayers.Layer.OSM(); */
    var baseOSM = new OpenLayers.Layer.OSM("MapQuest-OSM Tiles", arrayOSM);
    this.navMap.addLayer(baseOSM);
    // baseOSM.setVisibility(false);

    var wruaLayer = new OpenLayers.Layer.WMS(
      'WRUAs','http://192.168.12.10/cgi-bin/mapserv?map=/home/majisys/webapps/crosstabs/geography.map&',
      {
        layers: 'wrua',
        version: '1.3.0',
        transparent: true,
        styles: "beige_line"
      },{
        singleTile: true
      }
    );
    // wruaLayer.setVisibility(false);
    
    var lakeLayer = new OpenLayers.Layer.WMS(
      'Lake Naivasha','http://192.168.12.10/cgi-bin/mapserv?map=/home/majisys/webapps/crosstabs/geography.map&',
      {
        layers: 'lake',
        version: '1.3.0',
        transparent: true
      },{
        singleTile: true
      }
    );
    this.navMap.addLayers([wruaLayer,lakeLayer]);

    waterApp.appCenter = new OpenLayers.LonLat(36.422425, -0.578773).transform(this.wgs, this.sm);
    this.navMap.setCenter(waterApp.appCenter, 10);

  },
  /*---*/

  /*-- Generation of vector layers for the various stations */
  drawLayers: function(){
    this.station_data = Ext.getStore('Stations').proxy.reader.rawData;

    function projectPoint(x) {
      var point = compController.navMap.getViewPortPxFromLonLat(new OpenLayers.LonLat(x[0], x[1])
           .transform(compController.wgs, compController.sm));
      return [point.x, point.y];
    };

    var bounds = d3.geo.bounds(this.station_data);
    bounds[0][0] = bounds[0][0]-0.1;
    bounds[0][1] = bounds[0][1]-0.1;
    bounds[1][0] = bounds[1][0]+0.1;
    bounds[1][1] = bounds[1][1]+0.1;
    var path = d3.geo.path().projection(projectPoint);

    var st_overlay = new OpenLayers.Layer.Vector("Gauging Stations");
    var st_label = new OpenLayers.Layer.Vector("Gauging Stations Labels");

    this.navMap.events.register("moveend", compController.navMap, renderFeatures);

    var svg1, svg3;
    var feature1, feature3;

    /*-- Generates and updates features */
    function renderFeatures() {
      var bottomLeft = projectPoint(bounds[0]),
          topRight = projectPoint(bounds[1]);

      svg1.attr("width", topRight[0] - bottomLeft[0])
          .attr("height", bottomLeft[1] - topRight[1])
          .style("margin-left", bottomLeft[0] + "px")
          .style("margin-top", topRight[1] + "px");

      svg3.attr("width", topRight[0] - bottomLeft[0])
          .attr("height", bottomLeft[1] - topRight[1])
          .style("margin-left", bottomLeft[0] + "px")
          .style("margin-top", topRight[1] + "px");

      g1.attr("transform", "translate(" + -bottomLeft[0] + "," + -topRight[1] + ")");
      g3.attr("transform", "translate(" + -bottomLeft[0] + "," + -topRight[1] + ")");

      feature1.attr("transform", function(d) { return "translate(" + projectPoint(d.geometry.coordinates) + ")"; });
      feature3.attr("d", path);
    };
    /*---*/

    st_label.afterAdd = function(){
      var div = d3.select(Ext.getDom(st_label.div.id));
      div.selectAll("svg").remove();
      svg1 = div.append("svg");
      g1 = svg1.append("g");

      feature1 = g1.selectAll("path")
          .data(compController.station_data.features)
        .enter().append("text")
          .attr("class", "hidden")
          .attr("id", function(st) { return "id_" + st.properties.station_id; })
          .attr("transform", function(d) { return "translate(" + projectPoint(d.geometry.coordinates) + ")"; })
          .attr("dy", -12)
          .attr("x", -20)
          .text(function(d) { return d.properties.station_id; });
    };
    this.navMap.addLayer(st_label);

    st_overlay.afterAdd = function () {
      var div = d3.select(Ext.getDom(st_overlay.div.id))
      div.selectAll("svg").remove();
      svg3 = div.append("svg");
      g3 = svg3.append("g");

      feature3 = g3.selectAll("path")
          .data(compController.station_data.features)
        .enter().append("path")
          .attr("class","hidden")
          // .attr("class","stpoint")
          .attr("id", function(st) { return "feat_" + st.properties.station_id; })
          .attr("d",path.pointRadius(6))
          .on("mouseover", function (d) {
            var feat_id = d3.select(this)[0][0].id;
            ctController.highlightSvg(feat_id.replace("feat_",""));
          })
          .on("mouseout", function (d) {
            var feat_id = d3.select(this)[0][0].id;
            ctController.maskSvg(feat_id.replace("feat_",""));
          });

      renderFeatures();
    };
    this.navMap.addLayer(st_overlay);

  },
  /*---*/

  /*-- Creates a set of columns  when the filter parameters change */
  generateGridColumns: function(gridLabel){
    var months = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'];
    var columns = new Array;

    var summaryFunction, aggFunction = waterApp.selectionFunction;
    if (aggFunction == 'avg'){ summaryFunction = 'average'; }
      else if (aggFunction == 'count') { summaryFunction = 'sum'; }
      else { summaryFunction = aggFunction; };
    var aggLabel = Ext.getCmp('aggchooser').getDisplayValue();

    if (aggFunction == 'count') { ctController.getYearGrid().aggFormat = '00' }
      else { ctController.getYearGrid().aggFormat = '00.000' };

    columns.push({
      header   : '',
      width    : 30,
      menuDisabled: true,
      dataIndex: 'lineColor',
      renderer : function(value){
        return '<span style="color:' + value + '; font-weight: 700; font-size: 200%;">&Tilde;</span>';
      },
      summaryRenderer: function(value){
        return '<span style="color:' + value + '; font-weight: 700; font-size: 200%;">&apid;</span>';
      }
    });

    columns.push({
      xtype    : 'checkcolumn',
      id       : 'selectionColumn',
      text     : '&#10004;',
      width    : 25,
      dataIndex: 'enable',
      sortable : true,
      resizable: false,
      summaryRenderer: function(){},
      listeners: {
        checkchange: function(column, recordIndex, checked){
          var item = Ext.getStore('Observations').getAt(recordIndex);
          compController.triggerSelected(item,checked);
        }
      }
    });

    columns.push({
      header   : '<strong>' + gridLabel + '</strong>',
      flex     : 1,
      id       : "st_column",
      sortable : true,
      resizable: true,
      align    : 'left',
      dataIndex: "record_id",
      summaryRenderer: function(){
        return '<strong>' + aggLabel + ' :</strong>';
      }
    });

    for (var i=0; i<12; i++) {
      columns.push({
        header   : months[i].substr(0,1).toUpperCase() + months[i].substr(1,2),
        dataIndex: months[i],
        renderer : this.getYearGrid().formatValues,
        width    : 50,
        align    : 'center',
        resizable: false,
        summaryType: summaryFunction,
        sortable : false,
        summaryRenderer: function(value){
          return Ext.util.Format.number(value, ctController.getYearGrid().aggFormat);
        }
      });
    };

    columns.push({
      header   : '<strong>Year</strong>',
      width    : 52,
      summaryType: summaryFunction,
      sortable : false,
      resizable: false,
      align    : 'center',
      renderer : function(v,m,record){
        var aggValue = compController.generateAggVal(record,aggFunction);
        return '<span style="color:SlateGray;">'+Ext.util.Format.number(aggValue,ctController.getYearGrid().aggFormat)+'</span>';
      }
    });

    return columns;
  },
  /*---*/

  /*-- Keeps control of the synchronization between GUI components */
  triggerSelected: function(item,checked){
    if (checked){
      d3.selectAll("#st_"+item.get('record_id')).attr('class','stline');
      d3.selectAll("#hg_"+item.get('record_id')).attr('class','hgline');
      d3.selectAll("#feat_"+item.get('record_id')).attr('class',waterApp.stationCss);
      d3.selectAll("#id_"+item.get('record_id')).attr('class','idpoint');
      d3.selectAll("#rp_"+item.get('record_id')).attr('class','rfpoint');
      var addedRecord = ctController.hiddenRecords.filter(function (el) { return el.record_id == item.data.record_id; });
      ctController.hiddenRecords = ctController.hiddenRecords.filter(function (el) { return el.record_id != item.data.record_id; });
      ctController.visibleRecords.push(addedRecord[0]);
      ctController.reScaleChart();
    } else {
      d3.selectAll("#lb_"+item.get('record_id')).attr('class','hidden');
      d3.selectAll("#st_"+item.get('record_id')).attr('class','hidden');
      d3.selectAll("#hg_"+item.get('record_id')).attr('class','hidden');
      d3.selectAll("#feat_"+item.get('record_id')).attr('class','hidden');
      d3.selectAll("#id_"+item.get('record_id')).attr('class','hidden');
      d3.selectAll("#rp_"+item.get('record_id')).attr('class','hidden');
      var removedRecord = ctController.visibleRecords.filter(function (el) { return el.record_id == item.data.record_id; });
      ctController.hiddenRecords.push(removedRecord[0]);
      ctController.visibleRecords = ctController.visibleRecords.filter(function (el) { return el.record_id != item.data.record_id; });
      ctController.reScaleChart();
    };
  },
  /*---*/

  /*-- Generate aggregate values per row based on current aggregate function */
  generateAggVal: function(record,aggFunction){
    var aggValue, avgOver, fieldName;
    if (aggFunction == 'min') {
      fieldName = record.fields.keys[2];
      aggValue = record.get(fieldName);
      for (var i=3; i<14; i++){
        fieldName = record.fields.keys[i];
        if(record.get(fieldName) < aggValue) { aggValue = record.get(fieldName); };
      };
    } else if (aggFunction == 'max') {
      fieldName = record.fields.keys[2];
      aggValue = record.get(fieldName);
      for (var i=3; i<14; i++){
        fieldName = record.fields.keys[i];
        if (record.get(fieldName) > aggValue) { aggValue = record.get(fieldName); };
      };
    } else {
      aggValue = 0;
      aggFunction == 'avg' ? avgOver = 12 : avgOver = 1;
      for (var i=2; i<14; i++){
        fieldName = record.fields.keys[i];
        aggValue += record.get(fieldName);
      };
      aggValue = aggValue/avgOver;
    };
    return aggValue;
  },
  /*---*/

  /*-- Generates csv text from the records selected in the grid */
  generateCsv: function(){
    var obsValue, csvData = '';
    var header = true;
    Ext.getStore('Observations').each(function(item){
      if (item.data.enable){
        if (header){
          csvData+='record_id';
          for (var i=2; i<14; i++){
            csvData += ','+item.fields.keys[i];
          };
          csvData += ',' + waterApp.selectionFunction +'<br />';
          header = false;
        };
        csvData+=item.get('record_id');
        for (var i=2; i<14; i++){
          var fieldName = item.fields.keys[i];
          item.get(fieldName) == waterApp.nullValue ? obsValue = '' : obsValue = item.get(fieldName);
          csvData += ',' + Ext.util.Format.number(obsValue,'00.00000');
        };
        obsValue = compController.generateAggVal(item, waterApp.selectionFunction);
        csvData += ',' + Ext.util.Format.number(obsValue,'00.00000') + '<br />';
      };
    });
    return csvData;
  }

});
/*-----*/
