/* MAJI-SYS - Viewer */

Ext.require([
  'Ext.container.Viewport',
  'Ext.window.Window',
  'Ext.grid.RowNumberer',
  'Ext.grid.column.CheckColumn',
  'Ext.grid.feature.Summary',
  'Ext.selection.CheckboxModel',
  'Ext.tab.Panel',
  'Ext.toolbar.TextItem',
  'Ext.toolbar.Spacer',
  'Ext.form.field.ComboBox',
  'Ext.layout.container.Border'
]);

Ext.Loader.setConfig({
  enabled : true
  ,disableCaching : false
});

launched = false;

Ext.application({

  name: 'WaterTabs',
  appFolder: 'app',

  controllers : [ 'CrossTabs', 'Components' ],

  stationCss: 'stpoint_gaug',
  swithchStationType: false,
  filterBy: 'year',
  selectionStation: null,
  selectionStationType: 1,
  selectionYear: 2010,
  selectionFunction: 'avg',
  nullValue: 0.0000000001,
  appCenter: null, /* center for the map */

  launch: function() {
    Ext.create('Ext.container.Viewport', { /* Viewport */
      padding: 2,
      layout: 'fit',
      items: [{
        xtype: 'tabpanel',
        id: 'tab-panel',
        // title: '<center>MAJI-SYS - Monitoring Data Viewer App</center>',
        title: 'AGGRGATED TIME SERIES DISPLAY',
        activeTab: 0,
        tabPosition: 'bottom',
        tools: [{
          type:'print',
          tooltip: 'Print',
          handler: function(){
            waterApp.print();
          }
        },{
          type:'help',
          tooltip: 'Sow the help panel',
          // handler: function(event, toolEl, panel){
          handler: function(){
            Ext.getCmp('tab-panel').setActiveTab(3);
          }          
        }],
        items: [{
          name: 'main-panel',
          title: '&nbsp;&nbsp;Aggregated Values&nbsp;&nbsp;',
          layout: 'border',
          items: [{
            xtype: 'mapdisplay', /*  Navigation View */
            id: 'map-display-panel',
            region: 'center',
            margins: '2 -2 2 2',
            collapseDirection: 'left',
            minWidth: 200,
            layout: 'fit'
          },{
            xtype: 'panel', /* CrossTabs View */
            region: 'east',
            split: true,
            id: 'print-panel',
            margins: '2 2 2 0',
            width: 850,
            minWidth: 750,
            border: false,
            layout: 'border',
            items: [{
              xtype: 'linechart', /* Graph View */
              layout: 'fit',
              region: 'north',
              height : 400,
              padding: 0
            },{
              xtype: 'yearlygrid', /* Grid View */
              region: 'center',
              margins: '2 0 0 0'
            }]
          }]
        // },{
          // name: 'ts-panel',
          // id: 'time-series',
          // title: '&nbsp;&nbsp;<strong>Time Series</strong>&nbsp;&nbsp;',
          // layout: 'border',
          // items: [{
            // xtype: 'panel',
            // region: 'center',
            // margins: '2 2 2 2',
            // layout: 'fit'
          // },{
            // xtype: 'panel',
            // region: 'east',  
            // margins: '2 2 2 0',
            // width: 300
          // },{
            // xtype: 'panel',
            // region: 'west',  
            // margins: '2 0 2 2',
            // width: 250,
            // layout: 'border',
            // items : [{
              // xtype: 'panel',
              // region: 'center',
              // title: 'Stations',
              // margins: '2 0 0 0',
              // layout: 'fit'
            // },{
              // xtype: 'panel',
              // region: 'north',
              // title: 'Parameters',
              // height: 300,
              // layout: 'fit'
            // }]  
          // }]  
        },{          
          name: 'other-panel',
          id: 'ooo',
          title: '&nbsp;&nbsp;<strong>&ctdot;</strong>&nbsp;&nbsp;',
          html: '<br />&nbsp;&nbsp;Under construction ...'
        },{          
          name: 'about-panel',
          title: '&nbsp;&nbsp;About...&nbsp;&nbsp;',
          html: '<br />&nbsp;&nbsp;This is a demostrantion platform to be used for testing purposes only.' +
                '<br />&nbsp;&nbsp;' +
                '<br />&nbsp;&nbsp;The platform is being developed under the scope of the Integrated Water Resources Action Plan (IWRAP) program, Lake Naivasha, Kenya.' +
                '<br />&nbsp;&nbsp;To support the development of the sub-regional office in Naivasha of the Water Resources Management Authority (WRMA).' +
                '<br />&nbsp;&nbsp;' +
                '<br />&nbsp;&nbsp;This development is the result of a cooperation between multiple partners:' +
                '<br />&nbsp;&nbsp;&nbsp;&nbsp;- WWF Kenya' +
                '<br />&nbsp;&nbsp;&nbsp;&nbsp;- WRMA Naivasha' +
                '<br />&nbsp;&nbsp;&nbsp;&nbsp;- Imarisha' +
                '<br />&nbsp;&nbsp;&nbsp;&nbsp;- UPANDE' +
                '<br />&nbsp;&nbsp;&nbsp;&nbsp;- Deltares' +
                '<br />&nbsp;&nbsp;&nbsp;&nbsp;- Waterschap Noorderzijlvest' +
                '<br />&nbsp;&nbsp;&nbsp;&nbsp;- University Of Twente' +
                '<br />&nbsp;&nbsp;&nbsp;&nbsp;' +
                '<br />&nbsp;&nbsp;&copy; Faculty of Geoinformation Science and Earth Observation (ITC), Univeristy of Twente.'
        },{
          name: 'help-panel',
          title: '&nbsp;&nbsp;Help&nbsp;&nbsp;',
          padding: 5,
          layout: 'fit',
          items: [{
            xtype: 'panel',
            html: '<object type="text/html" data="docs/help.html" width="100%" height="100%"></object>'          
          }]
        }]
      }]
    });

    waterApp = this;
    Ext.getDom('loadgif').remove();

    /*-- Reduces 'Gateway Timeout' responses on Layers  */
    OpenLayers.IMAGE_RELOAD_ATTEMPTS = 3;

  },
  
  /*--  Printing the contents of a panel */
  print: function(pnl) {

    pnl = Ext.getCmp('print-panel');
    
    // instantiate hidden iframe
    iFrameId = "printerFrame";
    var printFrame = Ext.get(iFrameId);

    if (printFrame == null) {
      printFrame = Ext.getBody().appendChild({
        id: iFrameId,
        tag: 'iframe',
        cls: 'x-hidden',
        style: {
          display: "none"
        }
      });
    }

    cw = printFrame.dom.contentWindow;

    // instantiate application stylesheets in the hidden iframe
    var stylesheets = "";
    for (var i = 0; i < document.styleSheets.length; i++) {
      stylesheets += Ext.String.format('<link rel="stylesheet" href="{0}" />', document.styleSheets[i].href);
    }

    // various style overrides
    stylesheets += ''.concat(
      "<style>",
        ".x-panel-body {overflow: visible !important;}",
        ".float-l {visibility: hidden;}",
        ".float-r {visibility: hidden;}",
        // experimental - page break after embedded panels
        // .x-panel {page-break-after: always; margin-top: 10px}",
      "</style>"
     );

    // get the contents of the panel and remove hardcoded overflow properties
    ctController.getObservationsStore().each(function(item){
      if (item.data.enable) {
        d3.selectAll("#lb_"+item.data.record_id).attr('class','stid');
      }
    });
    Ext.ComponentQuery.query('linechart')[0].setBorder(false);
    var markup = pnl.getEl().dom.innerHTML;
    Ext.ComponentQuery.query('linechart')[0].setBorder(true);
    
    markup = markup.replace('print-panel-body', 'body-print-panel-body');
    while (markup.indexOf('overflow: auto;') >= 0) {
      markup = markup.replace('overflow: auto;', '');
    };
    ctController.getObservationsStore().each(function(item){
      if (item.data.enable) {
        d3.selectAll("#lb_"+item.data.record_id).attr('class','hidden');
      }
    });

    str = Ext.String.format('<html><head>{0}</head><body>{1}</body></html>',stylesheets,markup);

    // output to the iframe
    cw.document.open();
    cw.document.write(str);
    cw.document.close();

    // remove style attrib that has hardcoded height property
    // cw.document.getElementsByTagName('DIV')[0].removeAttribute('style');
    
    cw.document.getElementsByClassName('x-toolbar')[0].remove();
    cw.document.getElementsByClassName('x-toolbar')[0].remove()

    // print the iframe
    cw.print();

    // destroy the iframe
    // Ext.fly(iFrameId).destroy();

  }  
  /*---*/

});
/*-----*/
