/* Station model */
Ext.define('WaterTabs.model.Station', {
  extend: 'Ext.data.Model',
  fields: [
    { name: 'station_id', type: 'string', mapping: 'properties.station_id' },
    { name: 'station_type', type: 'integer', mapping: 'properties.station_type' },
    { name: 'geometry', type: 'object' }
  ],
  idProperty: 'station_id'
});
/*-----*/