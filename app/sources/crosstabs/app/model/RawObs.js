/* RawObs model */
Ext.define('WaterTabs.model.RawObs', {
  extend: 'Ext.data.Model',
  fields: [
    { name: 'record_id', type: 'string' },
    { name: 'lineColor', type: 'string' },
    { name: 'jan', type: 'float' },
    { name: 'feb', type: 'float' },
    { name: 'mar', type: 'float' },
    { name: 'apr', type: 'float' },
    { name: 'may', type: 'float' },
    { name: 'jun', type: 'float' },
    { name: 'jul', type: 'float' },
    { name: 'aug', type: 'float' },
    { name: 'sep', type: 'float' },
    { name: 'oct', type: 'float' },
    { name: 'nov', type: 'float' },
    { name: 'dec', type: 'float' },
    { name: 'enable', type: 'boolean', defaultValue: true }
  ],
  idProperty: 'record_id'
});
/*-----*/