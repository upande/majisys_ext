/* Observations store */
Ext.define('WaterTabs.store.Observations', {
  extend: 'Ext.data.Store',
  model: 'WaterTabs.model.RawObs',
 
  proxy: {
    type: 'ajax',
    url: 'scripts/multi_watertab.py',
    reader: {
      type: 'json',
      root: 'obs', 
      successProperty: 'success'
    }
  },
  
  listeners:{
    load: function(){
      Ext.getCmp('chartPanel').enable();
      d3.select("#svgChart").remove();
      launched = true;
      
      var gridLabel;
      waterApp.filterBy == 'year' ? gridLabel = 'Station' : gridLabel = 'Period';
      
      ctController.getYearGrid().reconfigure(ctController.getObservationsStore(),compController.generateGridColumns(gridLabel));
      
      var ds = ctController.generateSeries();
      if (ds != null){
        ctController.drawChart(ds);
      };
    }  
  }
});
