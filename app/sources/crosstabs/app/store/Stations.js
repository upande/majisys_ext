/* Stations store */
Ext.define('WaterTabs.store.Stations', {
  extend: 'Ext.data.Store',
  model: 'WaterTabs.model.Station',
  autoLoad: true,
  proxy: {
    type: 'ajax',
    url: 'scripts/stations.py',
    reader: {
      type: 'json',
      root: 'features', 
      successProperty: 'type'
    }
  },
  listeners:{
    load: function(){
      Ext.getStore('Stations').filter('station_type',waterApp.selectionStationType);
      waterApp.selectionStation = Ext.getStore('Stations').getAt(0).data.station_id;
      Ext.getCmp('stationchooser').setValue(waterApp.selectionStation);
      compController.drawLayers();
    }
  }
});
