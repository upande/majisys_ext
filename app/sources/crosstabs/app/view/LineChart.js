/* LineChart view */
Ext.define('WaterTabs.view.LineChart', {
  extend: 'Ext.panel.Panel',
  alias : 'widget.linechart',

  store: 'Stations',

  initComponent: function() {

    this.items = [{
      xtype: 'panel', /* Panel that hosts the line chart */
      id: 'chartPanel',
      layout: 'fit',
      padding: 3,
      html: ' <button id="origBtn" class="float-l" type="button" onclick="ctController.reDrawChart(false)" disabled="diabled">Original</button> <button id="smoothBtn" class="float-r" type="button" onclick="ctController.reDrawChart(true)">Smooth</button> '
    }];

    var yearStore = new Array;
    for (var i=1980; i<2016; i++) {
      yearStore.push(i);
    };

    this.dockedItems = [{ /* Task bar items */
      dock: "bottom",
      xtype: "toolbar",
      items: [{
        text: '&boxbox;',
        tooltip: 'Layer Switcher',
        menu: {
          items: [{
              text: 'Base Map',
              id: 'MapQuest-OSM Tiles',
              index: '0',
              checked: true,
              checkHandler: this.onItemCheck
          },{
              text: 'Wruas',
              id: 'WRUAs',
              index: '1',
              checked: true,
              checkHandler: this.onItemCheck
          },{
              text: 'Lake Naivasha',
              id: 'Lake Naivasha',
              index: '2',
              checked: true,
              checkHandler: this.onItemCheck
          }]
        }

      },' ','-',{
        xtype: 'tbtext',
        text: '&nbsp;Display Mode:'
      },{
        xtype: 'combo',
        id: 'filterchooser',
        width: 110,
        store: [["year","Year-Based"],["station","Station-Based"]],
        queryMode: 'local',
        selectOnFocus: true,
        triggerAction: 'all',
        listeners: {
          select: function(chooser){
            waterApp.filterBy = chooser.getValue();
            if (waterApp.filterBy == 'year'){
              Ext.getCmp('yearchooser').enable();
              Ext.getCmp('stationchooser').disable();
              d3.selectAll(".year-hidden").attr('class','stpoint');
              compController.navMap.setCenter(waterApp.appCenter,10);
            } else {
              Ext.getCmp('yearchooser').disable();
              Ext.getCmp('stationchooser').enable();
              d3.selectAll(".stpoint").attr('class','year-hidden');
              d3.selectAll("#feat_"+waterApp.selectionStation).attr('class','stpoint');
              var coords = Ext.getStore('Stations').getById(waterApp.selectionStation).data.geometry.coordinates;
              compController.navMap.setCenter(
                new OpenLayers.LonLat(coords[0], coords[1]).transform(compController.wgs, compController.sm),
                12
              );
            };
            ctController.refreshGrid();
          }
        }
      },' ','-',{
        xtype: 'tbtext',
        text: '&nbsp;Select Year:'
      },{
        xtype: 'combo',
        width: 70,
        store: yearStore,
        id: 'yearchooser',
        queryMode: 'local',
        selectOnFocus: true,
        triggerAction: 'all',
        listeners: {
          select: function(chooser){
            waterApp.selectionYear = chooser.getValue();
            ctController.refreshGrid();
          }
        }
      },' ','-',{
        xtype: 'tbtext',
        text: '&nbsp;Select Station:'
      },{
        xtype: 'combo',
        id: 'stationchooser',
        width: 110,
        disabled: true,
        store: Ext.getStore('Stations'),
        queryMode: 'local',
        selectOnFocus: true,
        triggerAction: 'all',
        valueField: 'station_id',
        displayField: 'station_id',
        listeners: {
          select: function(chooser){
            d3.selectAll("#feat_"+waterApp.selectionStation).attr('class','year-hidden');
            waterApp.selectionStation = chooser.getValue();
            d3.selectAll("#feat_"+waterApp.selectionStation).attr('class','stpoint');
            ctController.refreshGrid();
            var coords = Ext.getStore('Stations').getById(waterApp.selectionStation).data.geometry.coordinates;
            compController.navMap.setCenter(
              new OpenLayers.LonLat(coords[0], coords[1]).transform(compController.wgs, compController.sm),
              12
            );
          }
        }
      },{
        xtype: 'tbfill'
      },{
        xtype: "button",
        id: 'csvButton',
        text: "Generate CSV",
        // width: 90,
        handler: function(){
          compController.csvWindow.show();
          Ext.getElementById('csv-div').innerHTML = '';
          Ext.getElementById('csv-div').innerHTML = compController.generateCsv();
        }
      },' ']
    }];

    this.callParent(arguments);
  },

  onItemCheck: function(item,checked){
    compController.navMap.layers[item.index].setVisibility(checked);
  }

});
/*-----*/
