/* YearlyGrid view */
Ext.define('WaterTabs.view.YearlyGrid', {
  extend: 'Ext.grid.Panel',
  alias : 'widget.yearlygrid',

  store: 'Observations',

  viewConfig: {
    stripeRows: true,
    markDirty: false
  },

  columnLines: true,
  enableColumnHide: false,

  features: [{
    ftype: 'summary',
    dock: 'top', 
    id: 'f-summary'
  }],

  aggFormat: '00.000',
  formatValues: function(obsValue){
    if (obsValue == waterApp.nullValue) {
      return "--";
    } else {
      return Ext.util.Format.number(obsValue, this.aggFormat);
    }
  },


  initComponent: function() {

    this.columns = [];

    this.dockedItems = [{ /* Task bar items */
      dock: "bottom",
      xtype: "toolbar",
      items: [' ',{
        xtype: "button",
        id: 'selectionButton',
        tooltip: 'Press to select or unselect all records',
        text: "Unselect All Records",
        // width: 110,
        pressed: true,
        enableToggle: true,
        toggleHandler: function(button,pressed){
          ctController.getObservationsStore().suspendEvents();

          ctController.getObservationsStore().each(function(item){
            item.set('enable', pressed);
            if (pressed) {
              d3.selectAll("#st_"+item.get('record_id')).attr('class','stline');
              d3.selectAll("#feat_"+item.get('record_id')).attr('class','stpoint');
              d3.selectAll("#rp_"+item.get('record_id')).attr('class','rfpoint');
            } else {
              d3.selectAll("#st_"+item.get('record_id')).attr('class','hidden')
              d3.selectAll("#feat_"+item.get('record_id')).attr('class','hidden');
              d3.selectAll("#rp_"+item.get('record_id')).attr('class','hidden');
            };
          });

          if (pressed){
            ctController.visibleRecords = ctController.chartRecords.filter(function(el){
              return el.record_id != null;
            });
            ctController.hiddenRecords = new Array;
            ctController.reScaleChart();
          } else {
            ctController.hiddenRecords = ctController.chartRecords.filter(function(el){
              return el.record_id != null;
            });
            ctController.visibleRecords = new Array;
          };

          ctController.getObservationsStore().resumeEvents();
          ctController.getYearGrid().getView().refresh();
          pressed ? button.setText('Unselect All Records') : button.setText('Select All Records');
        }
      },' ',{
        xtype: "button",
        text: "Filter Selected",
        tooltip: "Press to remove unselected records form the view",
        enableToggle: true,
        toggleHandler: function(button,pressed){
          pressed ? button.setText('Remove Filter') : button.setText('Filter Selected');
          if (pressed){
            Ext.getCmp('selectionButton').disable();
            Ext.getCmp('selectionColumn').disable();
            Ext.getCmp('yearchooser').disable();
            Ext.getCmp('stationchooser').disable();
            Ext.getCmp('aggchooser').disable();
            Ext.getCmp('stationtypechooser').disable();
            Ext.getCmp('filterchooser').disable();
            ctController.getObservationsStore().filter('enable',true);
          } else {
            Ext.getCmp('selectionButton').enable();
            Ext.getCmp('selectionColumn').enable();
            Ext.getCmp('aggchooser').enable();
            Ext.getCmp('stationtypechooser').enable();
            Ext.getCmp('filterchooser').enable();
            if (Ext.getCmp('filterchooser').getValue() == 'year'){
              Ext.getCmp('yearchooser').enable();
            } else {
              Ext.getCmp('stationchooser').enable();
            };
            ctController.getObservationsStore().clearFilter();
          };
        }
      },' ','-',{
        xtype: 'tbtext',
        text: '&nbsp;Aggregate Function:&nbsp;'
      },{
        xtype: 'combo',
        width: 85,
        store: [["avg","Average"],["sum","Total"],["min","Minimum"],["max","Maximum"],["count","Count"]],
        id: 'aggchooser',
        queryMode: 'local',
        selectOnFocus: true,
        triggerAction: 'all',
        listeners: {
          select: function(chooser){
            waterApp.selectionFunction = chooser.getValue();
            ctController.refreshGrid();
          }
        }
      },' ',' ','-',{
        xtype: 'tbtext',
        text: '&nbsp;Parameter:&nbsp;'
      },,{
        xtype: 'combo',
        width: 200,
        store: {
          fields: ["key","val"],
          data: [
            {"key":"1","val":"Water level observed"},
            {"key":"6","val":"Precipitation observed"},
            {"key":"18","val":"WRL: Water rest level observed"}
        ]},
        displayField: "val",
        valueField: "key",
        id: 'stationtypechooser',
        queryMode: 'local',
        // disabled: true,
        selectOnFocus: true,
        triggerAction: 'all',
        listeners: {
          afterrender: function(chooser){
            chooser.setValue(chooser.getStore().getAt(0).get("val"));
          }, 
          select: function(chooser){
            waterApp.selectionStationType = chooser.getValue();
            waterApp.swithchStationType = true;
            if (waterApp.selectionStationType == 1){
              waterApp.stationCss = 'stpoint_gaug';
            } else {
              waterApp.stationCss = 'stpoint_rain';
            };
            ctController.refreshGrid();
          },
          beforeselect: function(chooser,record,index) { /* temporarily disable (WRL) */
            if(index == 2) {
             return false;
            }
          }
        }
      }]
    }],

    this.callParent(arguments);
  }
});
/*-----*/