/* MapDisplay view */
Ext.define('WaterTabs.view.MapDisplay', {
  extend: 'Ext.panel.Panel',
  alias : 'widget.mapdisplay',

  initComponent: function() {

    this.items = [{
      xtype: 'panel', /* Panel that hosts the map */
      id: 'mapPanel',
      layout: 'fit',
      padding: 3
    }];

    this.callParent(arguments);
  }

});
/*-----*/