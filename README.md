# README #

## **Quick summary** ##

MajiSys is a modern web­based open source Water Information System that has an interactive web application for exploration and analysis of time series data. It is based on a centralised data management system, which enables multi‐user access to the data stored in the system. This project deals with one of the components known as the Data Dissemination Subsystem or DDS for short which provides three distinct interfaces for Water Resource Management Authority (WRMA) staff, Water Resource Users Association (WRUA) staff and the general public to visualize and operate on time series according to their specific roles

## **Summary of set up** ##

1.	Ensure you have a properly installed and running Linux Operating System, recommend: Ubuntu (application tested on  Ubuntu 14.04)
2.	Confirm apache and python3 are already installed in Ubuntu (sudo apt-get install apache2, sudo apt-get install python3)
3.	Install pip3 for python 3 (sudo apt-get install python3-pip)
4.	Clone the project from bitbucket into a folder of your choosing.
5.	Change the owner of the folder to a user other than root:chown -R myuser:myuser
6. Navigate to the cloned application /<your_folder_path>/majisys_ext/ and open ‘python3_requirements’ folder and install the python3 libraries in requirements.txt using pip3 (pip3 install –r requirements)
7.Ensure all python script files are executable in the application using the command line:

    a. Go to /<your_folder_path>/majisys_ext/app/api/ folder and run:

        find ./ -name "*.py" -exec chmod +x {} \;
    
    b.Go to /<your_folder_path>/majisys_ext/app/app/scripts/ and run: 

        find ./ -name "*.py" -exec chmod +x {} \;

8.	Install Postgres (sudo apt-get install postgresql)
9.	Install Postgis (sudo apt-get install postgis)
10.	Install PgadminIII (sudo apt-get install pgadmin3)
11.	Follow the instructions here to configure postgres for the first time: https://help.ubuntu.com/lts/serverguide/postgresql.html

12.	Use PgadminIII to connect to the new instance of postgres, login as user postgres, and create a new database named 'majisys'.
13.	Add the postgis and tablefunc extensions to majisys. In the PgadminIII SQL Query window execute:
CREATE EXTENSION postgis;
CREATE EXTENSION tablefunc;
14.	Optionally backup data in database

    a.	Backup the majisys SQL schema on a running postgres instance of majisys:

        i.	Right-click schema "fews"
        ii.	Filename: choose one (e.g. majisys.sql)
        iii.	Format: Plain
        iv.	Dump Options #1: Only schema
        v.	Dump Options #2: Use Insert commands (is this useful for "only schema"?)
        vi.	Leave all other options as-is, and click "Backup"
    b.	Backup the data of six tables from majisys on the same running postgres instance of majisys:

        i.	Right-click schema "fews"
        ii.	Filename: choose one, different than the previous one (e.g. majisys_data.sql)
        iii.	Format: Plain
        iv.	Dump Options #1: Only data, don't save Owner, don't save Privilege
        v.	Dump Options #2: Use Insert commands
        vi.	Objects: select only 6 objects: "featureclass", "locations", "moduleinstances", "parametergroups", "parameterstable", "timesteps"
        vii.	Leave all other options as-is, and click "Backup"

15.	Create a new user (Login Role) "webapp" with password "<your_password>"
16.	Upload the majisys SQL schema to postgres, into database majisys: on the command-line execute: "psql -U postgres -W -d majisys -f ./majisys.sql".
17.	Upload the initial majisys data to postgres, into database majisys: on the command line execute: “psql -U postgres -W -d majisys -f ./majisys_data.sql”
18.	Grant “webapp” user role the required access-rights to majisys by executing in the PgadminIII SQL Query window:

        GRANT ALL ON SCHEMA fews TO webapp;
        GRANT SELECT ON ALL TABLES IN SCHEMA fews TO webapp;
        ALTER TABLE fews.watertab OWNER TO webapp;
        ALTER TABLE fews.locationparametertab OWNER TO webapp;

19.	Optionally create a user called "majisys":
sudo useradd -m -c “Majisys admin” majisys -s /bin/bash
This user isn't required if you already have a user that will take ownership of the files.
20.	Rename folder /var/www/html to /var/www/html.ubuntu (backup the default apache landing page)
21.	Create a link called "html" in folder /var/www, pointing to /home/majisys/webapps: sudo ln -s <your_folder_path> /var/www/html
Edit file /etc/apache2/sites-enabled/000-default.conf (sudo vi /etc/apache2/sites-enabled/000-default.conf), if you don’t have vi installed use any other text editor. Add the following before the closing tag “</VirtualHost>”:
	
        <Directory /var/www/html>
		    AllowOverride AuthConfig Options FileInfo Limit
		    Options –Indexes

22.	Enable modules “cgi” and “rewrite” for apache:
        sudo a2enmod cgi
        sudo a2enmod rewrite

23.	Restart Apache: sudo service apache2 restart
24.	Test if the site is working by opening a browser and going to: http://localhost/app


## **Dependencies** ##

The application has the following dependencies:

* Python 3.4.3 (python-dateutil==2.6.0, psycopg2==2.6.2)
* PostgreSQL 9.3.16
* Sencha Extjs 6.2.0 (for development)